SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[empleado]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[empleado](
	[codigo] [varchar](5) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](100) NOT NULL,
	[correo] [varchar](100) NOT NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_empleadoActualizar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[proc_empleadoActualizar]
@codigo varchar(5), 
@nombre varchar(50),
@apellido varchar(100), 
@correo varchar(100)
as
begin
update empleado set  nombre=@nombre, apellido=@apellido, correo=@correo
where codigo=@codigo
end

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_empleadoConsultarPorCodigo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[proc_empleadoConsultarPorCodigo]
@codigo varchar(5)
as
begin
select codigo, nombre, apellido, correo from empleado
where codigo = @codigo
end

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_empleadoEliminar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[proc_empleadoEliminar]
@codigo varchar(5),
@estado int
as
begin
update empleado set estado = @estado
where codigo = @codigo
end
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_empleadoInsertar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[proc_empleadoInsertar]
@codigo varchar(5), 
@nombre varchar(50),
@apellido varchar(100), 
@correo varchar(100),
@estado int
as
begin
insert into empleado values (@codigo, @nombre, @apellido, @correo, @estado)
end

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_empleadoListarPorApellido]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[proc_empleadoListarPorApellido]
@apellido varchar(100)
as
begin
select codigo, nombre, apellido, correo from empleado
where estado =1 and apellido like @apellido + ''%''
end
' 
END
