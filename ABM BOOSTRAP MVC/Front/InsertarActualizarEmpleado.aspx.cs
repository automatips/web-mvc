﻿using System;
using System.Web.UI;
using CapaNegocio;
using CapaEntidad;
namespace CapaPresentacion
{
    public partial class InsertarActualizarEmpleado : System.Web.UI.Page
    {
        EmpleadoNegocio EmpNego = new EmpleadoNegocio();
        Empleado EmpEnti = new Empleado();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                MostrarDatos();
            }
        }
        private void MostrarDatos()
        {
            try
            {
                string strCD = Session["CodigoEmpleado"].ToString();
                EmpEnti = EmpNego.ConsultarEmpleado(strCD);
                {
                    txtCodigo.Text = EmpEnti.codigoEmpleado;
                    txtNombre.Text = EmpEnti.nombreEmpleado;
                    txtApellido.Text = EmpEnti.apellidoEmpleado;
                    txtCorreo.Text = EmpEnti.correoEmpleado;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnCancelar.Enabled = true;
                }
            }
            catch (Exception)
            {
            }
        }
        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            if (this.txtCodigo.Text.Trim() != "" && this.txtApellido.Text.Trim() != ""
               && this.txtNombre.Text.Trim() != "" && this.txtCorreo.Text.Trim() != "")
            {
                try
                {
                    EmpEnti.codigoEmpleado = txtCodigo.Text;
                    EmpEnti.nombreEmpleado = txtNombre.Text;
                    EmpEnti.apellidoEmpleado = txtApellido.Text;
                    EmpEnti.correoEmpleado = txtCorreo.Text;
                    EmpEnti.estadoEmpleado = 1;
                    if (EmpNego.InsertarEmpleado(EmpEnti) == true)
                    {
                        lblMensaje.Text = "Registro Guardado Correctamente";
                        Response.Redirect("~/ListarEmpleados.aspx");
                    }
                    else
                    {
                        lblMensaje.Text = "Error de grabación de datos";
                    }
                }
                catch (Exception exc)
                {
                    lblMensaje.Text = exc.Message.ToString();
                }
            }
            else
            {
                lblMensaje.Text = "Todo los Campos son Obligatorios.";
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            if (this.txtCodigo.Text.Trim() != "" && this.txtApellido.Text.Trim() != ""
              && this.txtNombre.Text.Trim() != "" && this.txtCorreo.Text.Trim() != "")
            {
                try
                {

                    EmpEnti.codigoEmpleado = txtCodigo.Text;
                    EmpEnti.nombreEmpleado = txtNombre.Text;
                    EmpEnti.apellidoEmpleado = txtApellido.Text;
                    EmpEnti.correoEmpleado = txtCorreo.Text;
                    if (EmpNego.ActualizarEmpleado(EmpEnti) == true)
                    {
                        lblMensaje.Text = "Registro Actualizado Correctamente";
                        Session.RemoveAll();
                        Response.Redirect("~/ListarEmpleados.aspx");
                    }
                    else
                    {
                        lblMensaje.Text = "Error de Actualización de datos";
                    }

                }
                catch (Exception exc)
                {
                    lblMensaje.Text = exc.Message.ToString();
                }
            }
            else
            {
                lblMensaje.Text = "Todo los Campos son Obligatorios.";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MostrarDatos();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("~/ListarEmpleados.aspx");

        }
    }
}