﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;
namespace CapaPresentacion
{
    public partial class ListarEmpleados : System.Web.UI.Page
    {
        EmpleadoNegocio EmpNego = new EmpleadoNegocio();
        Empleado EmpEnti = new Empleado();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    ListarDatos();
                }
            }
            catch (Exception)
            {

            }
        }
        private void ListarDatos()
        {
            try
            {
                lblError.Text = "";
                GridViewDatos.DataSource = EmpNego.ListarEmpleados(txtApellidoEmpleado.Text);
                GridViewDatos.DataBind();

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message + " " + ex.StackTrace;
            }
        }
        protected void btnNuevoEmpleado_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/InsertarActualizarEmpleado.aspx");
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ListarDatos();
        }

        protected void GridViewDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = GridViewDatos.Rows[e.RowIndex];
                string strcod = Convert.ToString(row.Cells[2].Text);

                {
                    EmpEnti.codigoEmpleado = strcod;
                    EmpEnti.estadoEmpleado = 0;
                }
                if (EmpNego.EliminarEmpleado(EmpEnti) == true)
                {
                    ListarDatos();
                }
                else
                {
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GridViewDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                short indicefila;
                indicefila = Convert.ToInt16(e.CommandArgument);
                string strcod;
                if (indicefila >= 0 & indicefila < GridViewDatos.Rows.Count)
                {
                    strcod = GridViewDatos.Rows[indicefila].Cells[2].Text;
                    if (e.CommandName == "Actualizar")
                    {
                        Session["CodigoEmpleado"] = strcod;
                        Response.Redirect("~/InsertarActualizarEmpleado.aspx");
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        protected void GridViewDatos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridViewDatos.PageIndex = e.NewPageIndex;
                GridViewDatos.DataBind();
                ListarDatos();
            }
            catch (Exception)
            {
            }
        }
    }
}