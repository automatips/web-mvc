﻿namespace Model
{
    public class Usuario
    {
        private int id, idEquipo;
        private string nombre, idFacebook, email, password;

        public int IdEquipoUsuario
        {
            get { return idEquipo; }
            set { idEquipo = value; }
        }

        public string PasswordUsuario
        {
            get { return password; }
            set { password = value; }
        }

        public string EmailUsuario
        {
            get { return email; }
            set { email = value; }
        }

        public string IdFacebookUsuario
        {
            get { return idFacebook; }
            set { idFacebook = value; }
        }

        public int IdUsuario
        {
            get { return id; }
            set { id = value; }
        }

        public string NombreUsuario
        {
            get { return nombre; }
            set { nombre = value; }
        }
    }
}
