﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Albions.Default" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Albions - Mercado</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/grayscale.min.css" rel="stylesheet">

    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            ListarDatos();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            ListarDatos();
        }
        
    </script>

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Albions - Mercado</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Arriba</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#grilla">Grilla</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 text-uppercase">Albions</h1>
        <h2 class="text-white-50 mx-auto mt-2 mb-5">blablabla lo q sea</h2>
        <a href="#grilla" class="btn btn-primary js-scroll-trigger">VER REGISTROS DEL MERCADO</a>
      </div>
    </div>
  </header>

  <section id="grilla" class="about-section text-center">
    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="Form1" runat="server">

    <asp:UpdatePanel ID="upErrorHandler" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" />
            </Triggers>
                <ContentTemplate>
                    <asp:GridView ID="gvCompraVenta" 
                        CssClass="table table-bordered" 
                        data-model="Consultas.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" 
                        EmptyDataText="No hay consultas disponibles"
                        OnPageIndexChanging="gvCompraVenta_PageIndexChanging"
                        PageSize="50"
                        OnRowDeleting="gvCompraVenta_RowDeleting"
                        >
                        <Columns>
                            <asp:BoundField DataField="albion_id_oferta" HeaderText="AId Of" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="albion_id_demanda" HeaderText="AId De" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="item_id" HeaderText="item_id" ItemStyle-Width="100" Visible="false"/>
                            <asp:BoundField DataField="Tier" HeaderText="Tier" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="Traduccion" HeaderText="Traduccion" ItemStyle-Width="100"/>
                            <asp:BoundField DataField="Calidad" HeaderText="Calidad" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="Oferta" HeaderText="Oferta" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="Demanda" HeaderText="Demanda" ItemStyle-Width="50" />
                            <asp:BoundField DataField="Ganancia" HeaderText="Ganancia" ItemStyle-Width="50"/>
                            <asp:BoundField DataField="qo" HeaderText="Cantidad oferta" ItemStyle-Width="50" />
                            <asp:BoundField DataField="qr" HeaderText="Cantidad demanda" ItemStyle-Width="50" />
                            <asp:TemplateField HeaderText="Eliminar" ItemStyle-Width="1">
                                <ItemTemplate>
                                    <asp:LinkButton 
                                        ID="btnEliminar"
                                        runat="server"
                                        CssClass="btn btn-danger"
                                        Style="margin-left: 5px;"
                                        CommandName="Delete"
                                        title="Eliminar"
                                        >
                                        <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#7C6F57" />
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#FFCCFF" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>

                    <asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick">
                    </asp:Timer>

                    <asp:ScriptManager ID="ScriptManager1" runat="server">

                    </asp:ScriptManager>
		        </ContentTemplate>
	        </asp:UpdatePanel>
        </form>
    </div>
  </section>



  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/grayscale.min.js"></script>



</body>

</html>
