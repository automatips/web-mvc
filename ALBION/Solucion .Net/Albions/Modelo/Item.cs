﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1119]

using System;
using MagicSQL;

namespace Albions
{
    public partial class Item : ISUD<Item>
    {
        public Item() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdItem { get; set; }

        public string Descripcion { get; set; }

        public string TraduccionLatino { get; set; }

        public DateTime? FHAlta { get; set; }
    }
}