﻿using System;
using MagicSQL;

namespace Albions
{
    public partial class Orden : ISUD<Orden>
    {
        public Orden() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdOrden { get; set; }

        public DateTime? FHAlta { get; set; }

        public long? Identificador { get; set; }

        public string item_id { get; set; }

        public int? location { get; set; }

        public int? quality_level { get; set; }

        public int? enchantment_level { get; set; }

        public long? price { get; set; }

        public int? amount { get; set; }

        public string auction_type { get; set; }

        public DateTime? expires { get; set; }

        public long? albion_id { get; set; }

        public int? initial_amount { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }

        public DateTime? deleted_at { get; set; }
    }
}