﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1119]

using System;
using MagicSQL;

namespace Albions
{
    public partial class Promocion : ISUD<Promocion>
    {
        public Promocion() : base(1) { } 
        public long albion_id_oferta { get; set; }
        public long albion_id_demanda { get; set; }
        public string item_id { get; set; }
        public string Tier { get; set; }
        public string Traduccion { get; set; }
        public string Calidad { get; set; }
        public long Oferta { get; set; }
        public long Demanda { get; set; }
        public long Ganancia { get; set; }
        public int qo { get; set; }
        public int qr { get; set; }
    }
}