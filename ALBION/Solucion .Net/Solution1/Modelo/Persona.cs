﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Persona
    {

        private int Id;

        public int IdPersona

        {
            get { return Id; }

            set { Id = value; }
        }
        private string Nombre;
        public string NombrePersona
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        private string Apellido;
        public string ApellidoPersona
        {
            get { return Apellido; }
            set { Apellido = value; }
        }

        private string DNI;
        public string DNIPersona
        {
            get { return DNI; }
            set { DNI = value; }
        }
    }
}
