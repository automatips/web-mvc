﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
    public partial class Cliente : ISUD<Cliente>
    {
        public Cliente() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCliente { get; set; }

        public string Nombre { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int IdClienteEstado { get; set; }

        public string Domicilio { get; set; }

        public string Ciudad { get; set; }

        public string Provincia { get; set; }

        public string CP { get; set; }

        public string Profesion { get; set; }

        public string DNI { get; set; }

        public string CUIL { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }
    }
}