﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
  public partial class ClienteEstado : ISUD<ClienteEstado>
  {
    public ClienteEstado() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdClienteEstado { get; set; }

    public string Nombre { get; set; }

    public DateTime FHAlta { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}