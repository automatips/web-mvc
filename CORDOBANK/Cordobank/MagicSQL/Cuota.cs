﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace Cordobank
{
  public partial class Cuota : ISUD<Cuota>
  {
    public Cuota() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdCuota { get; set; }

    public int IdPrestamo { get; set; }

    public DateTime FHAlta { get; set; }

    public DateTime? FHBaja { get; set; }

    public decimal Monto { get; set; }

    public int IdCuotaEstado { get; set; }
  }
}