﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Cordobank.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Presentacion.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="Form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">SOLICITE SU PRÉSTAMO EN MINUTOS</h4>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Nombre y Apellido (*)</label>
                                    <asp:TextBox ID="txtNombreApellido" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su nombre y apellido"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>DNI (*)</label>
                                    <asp:TextBox ID="txtDNI" CssClass="form-control" runat="server" TextMode="SingleLine" Type="number" placeholder="Ingrese su DNI"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">CUIL (*)</label>
                                    <asp:TextBox ID="txtCUIL" CssClass="form-control" runat="server" TextMode="SingleLine" Type="number" placeholder="Ingrese su CUIL"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Correo electrónico (*)</label>
                                    <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su Correo electrónico"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Teléfono celular (Código de Area + Nro de Línea) (*)</label>
                                    <asp:TextBox ID="txtCelular" CssClass="form-control" runat="server" TextMode="SingleLine" Type="number" placeholder="Ingrese su celular"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Domicilio (Calle, Nro, Piso, Depto, Barrio) (*)</label>
                                    <asp:TextBox ID="txtDomicilio" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su domicilio (Calle, Nro, Piso, Depto, Barrio)"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Ciudad (*)</label>
                                    <asp:TextBox ID="txtCiudad" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su ciudad"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Provincia (*)</label>
                                    <asp:TextBox ID="txtProvincia" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su provincia"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Codigo postal (*)</label>
                                    <asp:TextBox ID="txtCP" CssClass="form-control" runat="server" TextMode="SingleLine" Type="number" placeholder="Ingrese su CP"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Profesión (*)</label>
                                    <asp:TextBox ID="txtProfesion" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese su profesión"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Motivo del préstamo (*)</label>
                                    <asp:TextBox ID="txtMotivoPrestamo" CssClass="form-control" runat="server" TextMode="SingleLine" placeholder="Ingrese el motivo del préstamo. Ej: Inversión de negocios, compra de vehículo, materiales de construcción, etc"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Monto a solicitar</label>
                                    <asp:DropDownList ID="ddlMonto" CssClass="form-control select-single" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tipo de devolución</label>
                                    <asp:DropDownList ID="ddlDevolucionTipo" CssClass="form-control select-single" runat="server" />
                                </div>
                            </div>
                        </div>

	                    <asp:UpdatePanel ID="upErrorHandler" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                    <ContentTemplate>
                                <asp:Label ID="lblError" Text="" ForeColor="Red" runat="server" />
                                <asp:Button ID="btnSave" Text="Ingresar solicitud" CssClass="btn btn-primary btn-fill pull-right" OnClick="btnSave_Click" runat="server" />

                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
		                    </ContentTemplate>
	                    </asp:UpdatePanel>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</asp:Content>