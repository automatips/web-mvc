﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MagicSQL;
using Cordobank;

namespace Presentacion
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlMonto.Items.Add("$ 2500");
                ddlMonto.Items.Add("$ 5000");
                ddlMonto.Items.Add("$ 7500");
                ddlMonto.Items.Add("$ 10000");
                ddlMonto.Items.Add("$ 20000");
                ddlMonto.Items.Add("$ 50000");

                ddlDevolucionTipo.Items.Add("Por día");
                ddlDevolucionTipo.Items.Add("Por semana");
                ddlDevolucionTipo.Items.Add("Por mes");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                txtNombreApellido.Text != String.Empty &&
                txtDNI.Text != string.Empty &&
                txtCUIL.Text != string.Empty &&
                txtEmail.Text != string.Empty &&
                txtCelular.Text != string.Empty &&
                txtDomicilio.Text != string.Empty &&
                txtCiudad.Text != string.Empty &&
                txtProvincia.Text != string.Empty &&
                txtCP.Text != string.Empty &&
                txtProfesion.Text != string.Empty &&
                txtMotivoPrestamo.Text != string.Empty
                )
                {
                    Cliente clienteBuscar = new Cliente().Select().Where(cli =>
                        cli.DNI == txtDNI.Text
                        ||
                        cli.CUIL == txtCUIL.Text
                        ||
                        cli.Email == txtEmail.Text
                        ||
                        cli.Celular == txtCelular.Text
                        ).FirstOrDefault();
                    if (clienteBuscar == null)
                    {
                        DateTime hora = DateTime.Now;
                        Cliente clienteInsertar = new Cliente();
                        clienteInsertar.FHAlta = hora;
                        clienteInsertar.Nombre = txtNombreApellido.Text;
                        clienteInsertar.DNI = txtDNI.Text;
                        clienteInsertar.CUIL = txtCUIL.Text;
                        clienteInsertar.Email = txtEmail.Text;
                        clienteInsertar.Celular = txtCelular.Text;
                        clienteInsertar.Domicilio = txtDomicilio.Text;
                        clienteInsertar.Ciudad = txtCiudad.Text;
                        clienteInsertar.Provincia = txtProvincia.Text;
                        clienteInsertar.Profesion = txtProfesion.Text;

                        Prestamo prestamoInsertar = new Prestamo();
                        prestamoInsertar.FHAlta = hora;
                        prestamoInsertar.IdCliente = clienteInsertar.IdCliente;
                        prestamoInsertar.Monto = Convert.ToDecimal(ddlMonto.Text.Replace("$ ", ""));
                        prestamoInsertar.Motivo = txtMotivoPrestamo.Text;
                        prestamoInsertar.DevolucionTipo = ddlDevolucionTipo.Text;
                        prestamoInsertar.IdPrestamoEstado = 1;

                        using (Tn tn = new Tn("Cordobank"))
                        {
                            try
                            {
                                clienteInsertar.Insert(tn);
                                prestamoInsertar.Insert(tn);
                                tn.Commit();

                                lblError.ForeColor = System.Drawing.Color.Green;
                                lblError.Text = "SOLICITUD REALIZADA CORRECTAMENTE. NOS COMUNICAREMOS CON UD A LA BREVEDAD!";
                                btnSave.Enabled = false;
                                upErrorHandler.Update();

                            }
                            catch (Exception ex)
                            {
                                tn.RollBack();
                                lblError.Text = "Error intentando solicitar el préstamo. " + ex.Message;
                                upErrorHandler.Update();
                            }
                        }
                    }
                    else
                    {
                        lblError.Text = "UD YA POSEE UNA SOLICITUD ANTERIOR ASIGNADA. POR FAVOR AGUARDE NUESTRA RESPUESTA.";
                        upErrorHandler.Update();
                    }
                }
                else
                {
                    lblError.Text = "DEBE COMPLETAR LOS CAMPOS INDICADOS PARA PODER PROCESAR LA SOLICITUD.";
                    upErrorHandler.Update();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Error intentando solicitar el préstamo. "+ex.Message;
                upErrorHandler.Update();
            }            
        }
    }
}