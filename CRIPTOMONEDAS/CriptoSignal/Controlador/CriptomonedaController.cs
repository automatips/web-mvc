﻿using System.Data;
using Modelo;
using Datos;
namespace Controlador
{
    public class CriptomonedaController
    {
        CriptomonedaDatos _CriptomonedaDatos = new CriptomonedaDatos();

        public DataTable ListarCriptomonedas(string parametro, string idUsuario, string pIp)
        {
            return _CriptomonedaDatos.ListarCriptomoneda(parametro, idUsuario, pIp);
        }
        public DataTable ObtenerSeñalesTradingCompra(string idUsuario, string pIp)
        {
            return _CriptomonedaDatos.ObtenerSeñalesTradingCompra(idUsuario, pIp);
        }
        public DataTable ObtenerSeñalesTradingVenta(string idUsuario, string pIp)
        {
            return _CriptomonedaDatos.ObtenerSeñalesTradingVenta(idUsuario, pIp);
        }
    }
}