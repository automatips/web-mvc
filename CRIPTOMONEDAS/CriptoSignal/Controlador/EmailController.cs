﻿using Modelo;
using System;
using System.Net;
using System.Net.Mail;

namespace Controlador
{
    public class EmailController
    {
        public string Enviar(Email emailEnviado)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.From = new MailAddress("criptosignalweb@gmail.com");
            mail.To.Add(emailEnviado.destinatarioEmail);
            mail.Subject = emailEnviado.asuntoEmail;
            mail.Body = emailEnviado.cuerpoEmail;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 25; //465; //587
            smtp.Credentials = new NetworkCredential("criptosignalweb@gmail.com", "Protocol0zer0");
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(mail);
                return "Enviado";
            }
            catch
            {
                return "Error";
            }
            finally
            {
                smtp.Dispose();
            }
        }
    }
        
}
