﻿using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Datos
{
    public class AirdropDatos
    {
        SqlConnection cnx;
        Airdrop AirdropEntidad = new Airdrop();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public AirdropDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public int ListarAirdrop(string idusuario, string pIp)
        {
            try
            {
                int vRetorno = 0;
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ListarAirdrop";

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = Convert.ToInt32(idusuario);

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    vRetorno = Convert.ToInt32(dtr[0]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return vRetorno;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}