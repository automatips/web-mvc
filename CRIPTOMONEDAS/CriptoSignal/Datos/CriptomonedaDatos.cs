﻿using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;

namespace Datos
{
    public class CriptomonedaDatos
    {
        SqlConnection cnx;
        Criptomoneda criptomonedaEntidad = new Criptomoneda();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public CriptomonedaDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }
        public DataTable ListarCriptomoneda(string parametro, string idUsuario, string pIp)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_CriptomonedaListarPorNombre";
                cmd.Parameters.Add(new SqlParameter("@nombre", parametro));

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = Convert.ToInt32(idUsuario);

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "CriptomonedaView");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["CriptomonedaView"]);
        }

        public DataTable ObtenerSeñalesTradingCompra(string idUsuario, string pIp)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerSeñalesTradingCompra";

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = Convert.ToInt32(idUsuario);

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "CriptomonedaView");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["CriptomonedaView"]);
        }

        public DataTable ObtenerSeñalesTradingVenta(string idUsuario, string pIp)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerSeñalesTradingVenta";

                cmd.Parameters.Add(new SqlParameter("@p_idUsuario", SqlDbType.Int));
                cmd.Parameters["@p_idUsuario"].Value = Convert.ToInt32(idUsuario);

                cmd.Parameters.Add(new SqlParameter("@pIp", SqlDbType.VarChar));
                cmd.Parameters["@pIp"].Value = pIp;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "CriptomonedaView");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["CriptomonedaView"]);
        }
    }
}