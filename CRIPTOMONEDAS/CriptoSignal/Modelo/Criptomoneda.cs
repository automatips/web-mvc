﻿namespace Modelo
{
    public class Criptomoneda
    {
        private string nombre, capital, precio;
        private int estado;

        public string nombreCriptomoneda
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string capitalCriptomoneda
        {
            get { return capital; }
            set { capital = value; }
        }

        public string precioCriptomoneda
        {
            get { return precio; }
            set { precio = value; }
        }

        public int estadoCriptomoneda
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}