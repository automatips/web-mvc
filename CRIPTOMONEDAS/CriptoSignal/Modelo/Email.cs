﻿namespace Modelo
{
    public class Email
    {
        private string asunto, cuerpo, destinatario;
        private int estado;

        public string asuntoEmail
        {
            get { return asunto; }
            set { asunto = value; }
        }

        public string cuerpoEmail
        {
            get { return cuerpo; }
            set { cuerpo = value; }
        }

        public string destinatarioEmail
        {
            get { return destinatario; }
            set { destinatario = value; }
        }

        public int estadoEmail
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}
