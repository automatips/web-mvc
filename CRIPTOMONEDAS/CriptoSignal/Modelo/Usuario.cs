﻿namespace Modelo
{
    public class Usuario
    {
        private int id;
        private string nombre, email, password;

        public string PasswordUsuario
        {
            get { return password; }
            set { password = value; }
        }

        public string EmailUsuario
        {
            get { return email; }
            set { email = value; }
        }

        public int IdUsuario
        {
            get { return id; }
            set { id = value; }
        }

        public string NombreUsuario
        {
            get { return nombre; }
            set { nombre = value; }
        }
    }
}
