﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Presentacion.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<%--    <div class="row">
        <section class="wrapper">
            <form class="registroForm" >
              <div class="login-wrap">

                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon_profile"></i></span>
                  <input type="text" class="form-control" placeholder="Nombre de usuario único" autofocus>
                </div>
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                  <input type="password" class="form-control" placeholder="Contraseña">
                </div>
                <label class="checkbox">
                        <input type="checkbox" value="remember-me"> Recordarme
                        <span class="pull-right"> <a href="#"> ¿Olvidó su contraseña?</a></span>
                </label>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Iniciar sesión</button>
                <button class="btn btn-info btn-lg btn-block" type="submit">Registrarse</button>
              
              </div>
            </form>
        </section>
    </div>--%>

    <form id="form1" runat="server">
        <div class="row">
            <section class="wrapper">
                  <div class="login-wrap">

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_profile"></i></span>
                      <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" class="form-control" placeholder="Email"></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Email' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                      <asp:TextBox ID="txtContraseña" type="password" runat="server" MaxLength="50" class="form-control" placeholder="Contraseña"></asp:TextBox>
                    </div>

                    <div class="input-group">
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContraseña" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo 'Contraseña' no puede ser vacío</asp:RequiredFieldValidator>
                    </div>

                    <p></p>

                    <asp:Button ID="btnLogin" 
                        runat="server"
                        Text="Iniciar sesión" 
                        class="btn btn-primary btn-lg btn-block"
                        ValidationGroup="ValidacionesForm" 
                        OnClick="btnLogin_Click" />

                    <asp:Button ID="btnRegistro" 
                        runat="server"
                        Text="¿Aún no tiene una cuenta? Registrese aquí" 
                        class="btn btn-info btn-lg btn-block"
                        OnClick="btnRegistro_Click" />

                    <div class="input-group">
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                  </div>
            </section>
        </div>
    </form>
</asp:Content>
