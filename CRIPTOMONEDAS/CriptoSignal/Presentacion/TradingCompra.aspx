﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="TradingCompra.aspx.cs" Inherits="Presentacion.TradingCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script language="javascript">
    function alertando() {
        alert('testing');
    }
    function obtenerHora() {
        var d = new Date();
        return d.getDate();
    }
    function filtrarTodoTablas() {
        filtrarTodoTablaCompra();
    }
    function filtrarTodoTablaCompra() {
        var tableReg = document.getElementById('tblOportunidadCompra');
        var searchText = document.getElementById('filterTherm').value.toLowerCase();
        var cellsOfRow = "";
        var found = false;
        var compareWith = "";

        // Recorremos todas las filas con contenido de la tabla
        for (var i = 1; i < tableReg.rows.length; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;
            // Recorremos todas las celdas
            for (var j = 0; j < cellsOfRow.length && !found; j++) {
                compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                    found = true;
                }
            }
            if (found) {
                tableReg.rows[i].style.display = '';
            } else {
                tableReg.rows[i].style.display = 'none';
            }
        }
    }
</script>

<form id="form1" runat="server">

<div class="row">
    <section class="wrapper">
    
    <script runat=server>
        private string getIdUsuarioNormalizado()
        {
            if (Session["CodigoUsuario"] == null)
            {
                return "0";
            }
            else
            {
                return Session["CodigoUsuario"].ToString();
            }
        }
        private string MakeHtmlTable(System.Data.DataTable data)
        {
            string[] table = new string[data.Rows.Count];
            long counter = 1;
            int contador = 1;
            foreach (System.Data.DataRow row in data.Rows)
            {    
                table[counter - 1] =
                @"<tr>"+
                //+ @"<td><div class=""progress thin""><div class=""progress-bar progress-bar-warning"" role=""progressbar"" aria-valuenow=""40"" aria-valuemin=""0"" aria-valuemax=""100"" style=""width: 40%""></div></div><span class=""sr-only"">60%</span></td>" 
                @"<td></td>" 
                +
                String.Join
                (
                @"</td><td>",
                (from o in row.ItemArray select o.ToString()).ToArray()
                ) +
                @"</td></tr>";
                counter += 1;
                contador++;
            }

            return String.Join("", table);
        }
        Controlador.CriptomonedaController criCont = new Controlador.CriptomonedaController();
    </script>
<script runat=server>
                               
    protected String GetIP()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }
        return context.Request.ServerVariables["REMOTE_ADDR"];
    }
                
</script>
    <input 
    id="filterTherm" 
    type="text" 
    onkeyup="filtrarTodoTablas()"
    placeholder="Filtrar" 
    class="form-control"
    autofocus
    />

    <p></p>

    <div class="col-lg-9 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            <h2><i class="fa fa-flag-o red"></i>
                <strong>MEJORES OPORTUNIDADES DE COMPRA</strong></h2>
            </div>
            <div class="panel-body">
            <table id="tblOportunidadCompra" class="table bootstrap-datatable countries">
                <thead>
                <tr>
                    <th></th>
                    <th>Criptomoneda</th>
                    <th>Capital de mercado</th>
                    <th>Precio actual</th>
                    <th>Variación porcentual<span class="help-block">(Ultimas 72 hs)</span></th>
                    <th>Precio máximo<span class="help-block">(Ultimas 72 hs)</span></th>
                </tr>
                </thead>
                <tbody>

                <% = MakeHtmlTable(criCont.ObtenerSeñalesTradingCompra(getIdUsuarioNormalizado(), GetIP()))%>

                </tbody>
            </table>
            </div>
        </div>
    </div>
            <% if (Session["CodigoUsuario"] == null)
                {
                    %> 
                    <center>
                        <div class="input-group">
                            <asp:Button ID="btnRegistro" 
                                runat="server"
                                Text="Ver listado completo de oportunidades. Registrese aquí" 
                                class="btn btn-info btn-lg btn-block"
                                OnClick="btnRegistro_Click" />
                        </div>
                    </center>

                        <p></p>
                    <%
                }
            %>

    <center><span class="help-block">La variación porcentual indica el proporcional potencial de ganancia en su exchange</span></center>
    <center><span class="help-block">Ejemplo: Una caída del 14% implica una ganancia potencial de USD $14 cada USD $100 en operaciónes de COMPRA</span></center>

                <asp:Timer ID="Timer1" runat="server" Interval= "90000" OnTick="Timer1_Tick">
                </asp:Timer>

                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
        </section>
    <div />
    </form>
</asp:Content>