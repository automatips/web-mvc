﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Criptomonedas.aspx.cs" Inherits="Presentacion.Criptomonedas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script language="javascript">
    function alertando() {
        alert('testing');
    }
    function filtrarTodoTablas() {
        filtrarTodoTablaCriptomonedas();
    }
    function filtrarTodoTablaCriptomonedas() {
        var tableReg = document.getElementById('tCriptomonedas');
        var searchText = document.getElementById('filterTherm').value.toLowerCase();
        var cellsOfRow = "";
        var found = false;
        var compareWith = "";

        // Recorremos todas las filas con contenido de la tabla
        for (var i = 1; i < tableReg.rows.length; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;
            // Recorremos todas las celdas
            for (var j = 0; j < cellsOfRow.length && !found; j++) {
                compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                    found = true;
                }
            }
            if (found) {
                tableReg.rows[i].style.display = '';
            } else {
                tableReg.rows[i].style.display = 'none';
            }
        }
    }
</script>

    <form id="form1" runat="server">
    
    <script runat=server>
        private string getIdUsuarioNormalizado()
        {
            if (Session["CodigoUsuario"] == null)
            {
                return "0";
            }
            else
            {
                return Session["CodigoUsuario"].ToString();
            }
        }

        protected String GetIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        
        private string MakeHtmlTableTest3(System.Data.DataTable data)
        {
            string[] table = new string[data.Rows.Count];
            long counter = 1;
            foreach (System.Data.DataRow row in data.Rows)
            {
                table[counter - 1] =
                    @"<tr><td></td>" +
                    String.Join
                    (
                    @"</td><td>",
                        (from o in row.ItemArray select o.ToString()).ToArray()
                    ) +
                    "</td></tr>";
                counter += 1;
            }

            return String.Join("", table);
        }
        
        Controlador.CriptomonedaController criCont = new Controlador.CriptomonedaController();
    </script>

    <div class="row">
        <section class="wrapper">
                
            <input 
            id="filterTherm" 
            type="text" 
            onkeyup="filtrarTodoTablas()"
            placeholder="Filtrar" 
            class="form-control"
            autofocus
            />

            <p></p>

            <% if (Session["CodigoUsuario"] == null)
                {
                    %>  
                    <center>
                        <div class="input-group">
                            <asp:Button ID="btnRegistro" 
                                runat="server"
                                Text="Ver listado completo de criptomonedas. Registrese aquí" 
                                class="btn btn-info btn-lg btn-block"
                                OnClick="btnRegistro_Click" />
                        </div>
                    </center>
                    <p></p>
                    <%
                }
            %>

            <div class=""col-lg-9 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><i class="fa fa-flag-o red"></i><strong> TOP CRIPTODIVISAS ONLINE</strong></h2>
                    </div>
                    
                    <div class="panel-body">

                        <table class="table bootstrap-datatable countries" id="tCriptomonedas">
                            <thead><tr>
                                <th></th>
                                <th>Nombre</th>
                                <th>Capital</th>
                                <th>Precio actual</th></tr>

                            </thead>
                            <tbody>
                                <% = MakeHtmlTableTest3(criCont.ListarCriptomonedas("",getIdUsuarioNormalizado(), GetIP()))%>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

                <asp:Timer ID="Timer1" runat="server" Interval= "90000" OnTick="Timer1_Tick">
                </asp:Timer>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
        </section>
    </div>
            
    </form>
            
</asp:Content>
