﻿using CGADM;
using Code;
using MagicSQL;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class DistribuidorReseller : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.DistribuidorReseller distribuidorReseller = new CGADM.DistribuidorReseller();

        // Id del Modelo
        private int idDistribuidorReseller = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            btnEdit.Visible = false;

            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    idDistribuidorReseller = Request["cryptoID"].ToIntID();
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }

            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                ddlDistribuidor.Items.Clear();
                foreach (CGADM.Distribuidor m in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null)) { ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID())); }
                ddlDistribuidor_OnSelectedIndexChanged(null, null);

                if (idDistribuidorReseller == 0)
                { // Nuevo Modelo
                    // Deshabilita los botones Editar y Eliminar
                    btnEdit.Enabled = false;
                    btnDelete.Enabled = false;
                    var unidadNegocio = new UnidadNegocio();

                    // Habilita el boton Guardar
                    btnSave.Enabled = true;
                }
                else
                { // Ver Modelo
                    // Recupera el Modelo a partir del Id encriptado del Modelo
                    distribuidorReseller.Select(idDistribuidorReseller);

                    // Genera el Hash del modelo y lo guarda en el control hdnHash
                    string jsonModel = distribuidorReseller.ToJson();
                    hdnHash.Value = ModelHash(jsonModel);

                    // Establece los valores de los controles desde las propiedades del Modelo
                    SetControls();
                }

                // Habilita o Deshabilita los controles según el estado del btnSave
                EnableControls();
            }
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // Id del Distribuidor desencriptado
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();

            // Lista de los registros de la tabla de enlace DistribuidorUnidadNegocio con IdDistribuidor igual al seleccionado en el ddlDistribuidor
            List<CGADM.DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio =
                new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();

            // Lista de los registros de la tabla UnidadNegocio con IdUnidadNegocio contenido en la lista listDistribuidorUnidadNegocio (filtrada en el paso anterior)
            List<CGADM.UnidadNegocio> listUnidadNegocio =
                new CGADM.UnidadNegocio().Select().Where(dun => listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();

            ddlUnidadNegocio.Items.Clear();
            ddlVendingReseller.Items.Clear();
            gvUMs.DataSource = null;
            gvUMs.DataBind();
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio) { ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID())); }
            if (ddlUnidadNegocio.Items.Count > 0) { ddlUnidadNegocio_OnSelectedIndexChanged(null, null); }
        }

        protected void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            ddlVendingReseller.Items.Clear();
            gvUMs.DataSource = null;
            gvUMs.DataBind();
            switch (idUnidadNegocio)
            {
                case 1:

                    //List<CGADM.DistribuidorReseller> distribuidorResellerVending = new CGADM.DistribuidorReseller().Select();
                    CGVending.VendingReseller vendingReseller = new CGVending.VendingReseller();
                    foreach (CGVending.VendingReseller m in vendingReseller.Select().Where(m => m.FechaBaja == null)) { ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdVendingReseller.ToCryptoID())); }
                    break;

                case 2:

                    //List<CGAccess.Reseller> distribuidorResellerAccess = new CGAccess.Reseller().Select();
                    CGAccess.Reseller accessReseller = new CGAccess.Reseller();
                    foreach (CGAccess.Reseller m in accessReseller.Select().Where(m => m.FHBaja == null)) { ddlVendingReseller.Items.Add(new ListItem(m.RazonSocial, m.IdReseller.ToCryptoID())); }
                    break;
            }
            if (ddlVendingReseller.Items.Count > 0) { ddlVendingReseller_OnSelectedIndexChanged(null, null); }
        }

        protected void ddlVendingReseller_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int idVendingReseller = 0;
            if (idDistribuidorReseller == 0)
                idVendingReseller = ddlVendingReseller.SelectedValue.ToIntID();
            else
                idVendingReseller = distribuidorReseller.IdReseller;

            if (idVendingReseller > 0)
            {
                gvUMs.DataSource = null;
                gvUMs.DataSource = new SP("CGAdm").Execute("usp_VendingUMxReseller", P.Add("IdVendingReseller", idVendingReseller));
                gvUMs.DataBind();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            // Deshabilita el botón Editar
            btnEdit.Enabled = false;

            // Deshabilita el botón Eliminar
            btnDelete.Enabled = false;

            // Habilita el botón Guardar
            btnSave.Enabled = true;

            // Habilita o Deshabilita los controles según el estado del btnSave
            EnableControls();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            // Recupera el Modelo a partir del Id encriptado
            distribuidorReseller.Select(idDistribuidorReseller);

            // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
            string jsonModel = distribuidorReseller.ToJson();
            if (hdnHash.Value.Equals(ModelHash(jsonModel)))
            {
                distribuidorReseller.FHBaja = DateTime.Now;
                using (Tn tn = new Tn("CGAdm"))
                {
                    distribuidorReseller.Update(tn);

                    foreach (var item in new CGADM.DistribuidorUM().Select().Where(p => p.IdDistribuidorUnidadNegocio == distribuidorReseller.IdDistribuidorUnidadNegocio && p.FHBaja == null))
                    {
                        item.FHBaja = DateTime.Now;
                        item.Update(tn);
                    }
                    tn.Commit();
                }

                // Confirma al usuario
                Message("Asignación dada de baja", "La Asignación ha sido dado de baja exitosamente.", MessagesTypes.success, "DistribuidoresResellers.aspx");
            }
            else
            {// El Modelo está desactualizado
             // Notifica que el Modelo está desactualizado en la DB
                Message("Asignación Desactualizada", "El Asignación cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (idDistribuidorReseller == 0)
            {
                // Establece los valores de las propiedades del Modelo desde los controles
                SetProperties();

                // Inserta el Modelo
                if (new CGADM.DistribuidorReseller().Select().Where(m => m.IdReseller == distribuidorReseller.IdReseller && m.FHBaja == null).Count() > 0)
                {
                    Message("Asignación", "El Reseller está actualmente asignado a otro Distribuidor.", MessagesTypes.warning, "DistribuidoresResellers.aspx");
                    return;
                }

                using (Tn tn = new Tn("CGAdm"))
                {
                    distribuidorReseller.Insert(tn);

                    #region Tratar UMs

                    int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
                    int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
                    DistribuidorUnidadNegocio distribuidorUnidadNegocio = new DistribuidorUnidadNegocio().Select().Where(p => p.IdDistribuidor == idDistribuidor && p.IdUnidadNegocio == idUnidadNegocio).ToList()[0];

                    CGVending.VendingUmxReseller vr = new CGVending.VendingUmxReseller();
                    DistribuidorUM dum = new DistribuidorUM();
                    foreach (var item in vr.Select().Where(p => p.IdVendingReseller == distribuidorReseller.IdReseller && p.FechaBajaAsignacion == null))
                    {
                        dum = new DistribuidorUM();
                        dum.IdDistribuidorUnidadNegocio = distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio;
                        dum.Observacion = item.Observaciones;
                        dum.FHAlta = DateTime.Now;
                        dum.UMId = item.UMId;
                        dum.AccionABMServidorOutput = "A";
                        dum.Insert(tn);
                    }

                    #endregion Tratar UMs

                    tn.Commit();
                }

                // Confirma al usuario
                Message("Asignación creada", "La Asignación ha sido creado exitosamente.", MessagesTypes.success, "DistribuidoresResellers.aspx");
            }
            else
            {
                // Recupera el Modelo a partir del Id encriptado
                distribuidorReseller.Select(idDistribuidorReseller);

                // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
                string jsonModel = distribuidorReseller.ToJson();
                if (hdnHash.Value.Equals(ModelHash(jsonModel)))
                {// El Modelo está actualizado
                    // Establece los valores de las propiedades desde los controles
                    SetProperties();

                    // Actualiza el Modelo
                    using (Tn tn = new Tn("CGAdm"))
                    {
                        distribuidorReseller.Update(tn);

                        #region Tratar UMs

                        int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
                        int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
                        DistribuidorUnidadNegocio distribuidorUnidadNegocio = new DistribuidorUnidadNegocio().Select().Where(p => p.IdDistribuidor == idDistribuidor && p.IdUnidadNegocio == idUnidadNegocio).ToList()[0];

                        CGVending.VendingUmxReseller vr = new CGVending.VendingUmxReseller();
                        DistribuidorUM dum = new DistribuidorUM();
                        foreach (var item in vr.Select().Where(p => p.IdVendingReseller == distribuidorReseller.IdReseller && p.FechaBajaAsignacion == null))
                        {
                            // Me fijo si las UMs ya existen para otro Distribuidor
                            var asignadas = dum.Select().Where(d => d.UMId == item.UMId && dum.FHBaja == null).ToList();

                            //var asignadas = dum.Select().Where(d => d.UMId == item.UMId && dum.FHBaja == null && dum.IdDistribuidorUnidadNegocio != distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio).ToList();
                            if (asignadas.Count > 0)
                            {
                                foreach (var i in asignadas)
                                {
                                    i.FHBaja = DateTime.Now;
                                    i.Update(tn);
                                }
                            }
                            dum = new DistribuidorUM();
                            dum.IdDistribuidorUnidadNegocio = distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio;
                            dum.Observacion = item.Observaciones;
                            dum.FHAlta = DateTime.Now;
                            dum.UMId = item.UMId;
                            dum.AccionABMServidorOutput = "A";
                            dum.Insert(tn);
                        }

                        #endregion Tratar UMs

                        tn.Commit();
                    }

                    // Confirma al usuario
                    Message("Asignación Editada", "La Asignación ha sido modificada exitosamente.", MessagesTypes.success, "DistribuidoresResellers.aspx");
                }
                else
                {// El Modelo está desactualizado
                    // Notifica que el Modelo está desactualizado en la DB
                    Message("Asignación Desactualizada", "La Asignación cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
                }
            }
        }

        protected void EnableControls()
        {
            if (idDistribuidorReseller == 0) { ddlVendingReseller.Enabled = true; }
            else { ddlVendingReseller.Enabled = false; }

            ddlDistribuidor.Enabled = btnSave.Enabled;
            ddlUnidadNegocio.Enabled = btnSave.Enabled;
            txtObservacion.Disabled = !btnSave.Enabled;
        }

        protected void SetProperties()
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            DistribuidorUnidadNegocio distribuidorUnidadNegocio = new DistribuidorUnidadNegocio().Select().Where(p => p.IdDistribuidor == idDistribuidor && p.IdUnidadNegocio == idUnidadNegocio).ToList()[0];

            distribuidorReseller.IdDistribuidorUnidadNegocio = distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio;
            distribuidorReseller.FHAlta = DateTime.Now;
            distribuidorReseller.FHAltaPlanComercial = DateTime.Now;
            distribuidorReseller.IdReseller = ddlVendingReseller.SelectedValue.ToIntID();
            distribuidorReseller.Observaciones = txtObservacion.Value;
            if (idDistribuidorReseller == 0)
            {
                distribuidorReseller.FechaProximaLiquidacion = DateTime.Now.AddMonths(1);
                distribuidorReseller.FechaProximaLiquidacion = new DateTime(distribuidorReseller.FechaProximaLiquidacion.Value.Year, distribuidorReseller.FechaProximaLiquidacion.Value.Month, 1);
            }
        }

        protected void SetControls()
        {
            DistribuidorUnidadNegocio distribuidorUnidadNegocio = new DistribuidorUnidadNegocio().Select().Where(p => p.IdDistribuidorUnidadNegocio == distribuidorReseller.IdDistribuidorUnidadNegocio).ToList()[0];
            ddlDistribuidor.Items.Clear();
            foreach (CGADM.Distribuidor m in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null)) { ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID())); }
            ddlDistribuidor.SelectedValue = distribuidorUnidadNegocio.IdDistribuidor.ToCryptoID();
            ddlDistribuidor_OnSelectedIndexChanged(null, null);

            ddlUnidadNegocio.SelectedValue = distribuidorUnidadNegocio.IdUnidadNegocio.ToCryptoID();
            ddlUnidadNegocio_OnSelectedIndexChanged(null, null);

            ddlVendingReseller.SelectedValue = distribuidorReseller.IdReseller.ToCryptoID();
            txtObservacion.Value = distribuidorReseller.Observaciones;
        }
    }
}