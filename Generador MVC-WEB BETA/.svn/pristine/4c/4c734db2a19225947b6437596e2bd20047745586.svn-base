﻿using CGlobal;
using log4net;
using MagicSQL;
using System;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AccessControlService.Tasks
{
    public class AsignacionResellerUM
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AsignacionResellerUM));

        public static async void Run(CancellationToken cancellationToken, TimeSpan period)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                // Buscar asignaciones de UM y ponerlas en ServidorOutput
                var newMessages = new SP("CGAccess").Execute("dbo.usp_AccessControlServicio_AsignacionesReseller_Pendientes");
                newMessages.DefaultView.Sort = "IdUMAsignacion ASC";
                newMessages = newMessages.DefaultView.ToTable();

                foreach (System.Data.DataRow newMessage in newMessages.Rows)
                {
                    try
                    {
                        var outputMessage = new ServidorOutput()
                        {
                            Anulado = false,
                            FechaHoraAlta = DateTime.Now,
                            ProtocoloId = 2030,
                            Paquete = BuildPayload(newMessage),
                            UMId = (int)newMessage["UMid"]
                        };
                        outputMessage.LongitudPaquete = outputMessage.Paquete.Length;
                        outputMessage.Insert();

                        // Se pudo insertar, actualizar la asignación de UM
                        new SP("CGAccess").Execute("dbo.usp_AccessControlServicio_AsignacionesUM_SetEnviado", P.Add("IdUMAsignacion", (int)newMessage["IdUMAsignacion"]), P.Add("IdServidorOutput", outputMessage.IdServidorOutput));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("AsignacionResellerUM. Error al generar ServidorOutput. UMid: {0}. Exception: {1}", newMessage["UMid"], ex.Message);
                    }
                }

                try { await Task.Delay(period, cancellationToken); } catch (OperationCanceledException) { }
            }
        }

        private static byte[] BuildPayload(DataRow newMessage)
        {
            var payloadFormat = "|{0}|{1}||";

            return ASCIIEncoding.ASCII.GetBytes(string.Format(payloadFormat
                , newMessage["IdReseller"].ToString()
                , newMessage["ResellerRazonSocial"].ToString()
                ));
        }
    }
}