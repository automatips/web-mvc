﻿// Created by MagicMaker

using System.Collections.Generic;
using MagicSQL;
using System;
using System.Data;
using System.Linq;

namespace CGADM
{
    public partial class SDS
    {
        public SDS ObtenerSDSPorSDSNro(Tn transaccionConsultar, int SDSNro)
        {
            CGADM.SDS solicitudActual = new SDS();
            using (transaccionConsultar)
            {
                try
                {
                    solicitudActual = new CGADM.SDS().Select().Where(sol => sol.SDSNro == SDSNro).FirstOrDefault();
                    solicitudActual._listaSDSDetallePreExistente = new CGADM.SDSDetalle().Select().
                        Where(sdsd => 
                        sdsd.IdSDS == solicitudActual.IdSDS &&
                        sdsd.FHBaja == null
                        ).ToList();
                    return solicitudActual;
                }
                catch
                {
                    return null;
                }
            }
        }
        public SDS ObtenerSDSPorIdSDS(Tn transaccionConsultar, int IdSDS)
        {
            CGADM.SDS solicitudActual = new SDS();
            using (transaccionConsultar)
            {
                try
                {
                    solicitudActual = new CGADM.SDS().Select().Where(sol => sol.IdSDS == IdSDS).FirstOrDefault();
                    solicitudActual._listaSDSDetallePreExistente = new CGADM.SDSDetalle().Select().
                        Where(sdsd => 
                        sdsd.IdSDS == solicitudActual.IdSDS &&
                        sdsd.FHBaja == null
                        ).ToList();
                    return solicitudActual;
                }
                catch
                {
                    return null;
                }
            }
        }
        public ErrorHandler setListaSDSDetalleOnHot(List<SDSDetalle> listaSDSDetalleOnHot)
        {
            /*Primero recorre los objeto detalle pre-existentes
             * Si no existe lo preparará para la inserción
             * Si existe lo preparará para la actualización
             * Si existía pero ya no existe, se preparará para la eliminación 
             */
            ErrorHandler eh = new ErrorHandler();
            #region SincronizacionDeDetalle
            foreach (SDSDetalle sdsDetalleOnHot in listaSDSDetalleOnHot)
            {
                try
                {
                    bool vExiste = false;
                    foreach (SDSDetalle sdsDetallePreExistente in _listaSDSDetallePreExistente)
                    {
                        if (sdsDetalleOnHot.IdSDSDetalle == sdsDetallePreExistente.IdSDSDetalle)
                        {
                            List<SDSDetalle> listaActualizar = _listaSDSDetalleActualizar;
                            foreach (SDSDetalle sdsDetalleActualizar in listaActualizar)
                            {
                                listaActualizar.Add(sdsDetalleActualizar);
                            }
                        }
                    }

                    if (!vExiste)
                    {
                        List<SDSDetalle> listaInsertar = _listaSDSDetalleInsertar;
                        listaInsertar.Add(sdsDetalleOnHot);
                        _listaSDSDetalleInsertar = listaInsertar;
                    }
                }
                catch (Exception ex)
                {
                    eh.Mensaje = "Error sincronizando productos para insertar y actualizar";
                    eh.Descripcion = ex.Message;
                    return eh;
                }
            }

            foreach (SDSDetalle sdsDetallePreExistente in _listaSDSDetallePreExistente)
            {
                try
                {
                    bool vSigueExistiendo = false;
                    foreach (SDSDetalle sdsDetalleOnHot in listaSDSDetalleOnHot)
                    {
                        if (sdsDetalleOnHot.IdSDSDetalle == sdsDetallePreExistente.IdSDSDetalle)
                        {
                            vSigueExistiendo = true;
                        }
                    }
                    if (!vSigueExistiendo)
                    {
                        _listaSDSDetalleEliminar.Add(sdsDetallePreExistente);
                    }
                }
                catch (Exception ex)
                {
                    eh.Mensaje = "Error sincronizando productos para eliminar";
                    eh.Descripcion = ex.Message;
                    return eh;
                }
            }
            #endregion
            eh.Mensaje = "EXITO";
            return eh;
        }
        public ErrorHandler Guardar(Tn transaccionGuardar, string vAccion)
        {
            ErrorHandler eh = new ErrorHandler();
            switch (vAccion)
            {
                case "Insertar":
                    try
                    {
                        FHAlta = DateTime.Now;

                        Insert(transaccionGuardar);
                    }
                    catch (Exception ex)
                    {
                        eh.Mensaje = "Error al crear nueva solicitud de servicio";
                        eh.Descripcion = ex.Message;
                    }

                    eh = InsertarDetallesNuevos(transaccionGuardar);

                    if (eh.Mensaje != "EXITO")
                    {
                        return eh;
                    }

                    //Si no hay registros detalle para ingresar
                    if ((_listaSDSDetalleInsertar.Count) == 0)
                    {
                        eh.Mensaje = "Error en cantidades destino";
                        eh.Descripcion = "La cantidad de detalles debe ser igual o mayor a 1";
                        return eh;
                    }
                    
                    eh.Mensaje = "EXITO";
                    eh.Descripcion = "Se ha creado exitosamente la solicitud de servicio";
                    break;

                case "Actualizar":
                    try
                    {
                        Update(transaccionGuardar);
                    }
                    catch (Exception ex)
                    {
                        eh.Mensaje = "Error al actualizar la solicitud de servicio";
                        eh.Descripcion = ex.Message;
                        return eh;
                    }

                    eh = InsertarDetallesNuevos(transaccionGuardar);

                    if (eh.Mensaje != "EXITO")
                    {
                        return eh;
                    }

                    eh = ActualizarDetallesExistentes(transaccionGuardar);

                    if (eh.Mensaje != "EXITO")
                    {
                        return eh;
                    }

                    eh = EliminarDetalles(transaccionGuardar);
                    if (eh.Mensaje != "EXITO")
                    {
                        return eh;
                    }

                    //Si no hay registros detalle para ingresar o actualizar, consultamos si aún hay detalles en el objeto y que no estén por ser eliminados
                    if (0 == ((_listaSDSDetalleInsertar.Count + _listaSDSDetalleActualizar.Count)))
                    {
                        int cantidadRegistrosSinEliminar = 0;
                        foreach (SDSDetalle sdsDetallePreexistenteRecorrer in _listaSDSDetallePreExistente)
                        {
                            bool vQuedaEnBD = true;
                            foreach (SDSDetalle sdsDetalleEliminar in _listaSDSDetalleEliminar)
                            {
                                if (sdsDetalleEliminar.IdSDSDetalle == sdsDetallePreexistenteRecorrer.IdSDSDetalle)
                                {
                                    vQuedaEnBD = false;
                                }              
                            }
                            if (vQuedaEnBD)
                            {
                                cantidadRegistrosSinEliminar++;
                            }
                        }
                        if (cantidadRegistrosSinEliminar == 0)
                        {
                            eh.Mensaje = "Error en cantidades destino";
                            eh.Descripcion = "A: Registros preexistentes que no serán eliminados; B: Registros a insertar; C: Registros preexistentes a modificar. La sumatoria entre A, B y C no puede ser igual a cero";
                            return eh;
                        }
                    }

                    eh.Mensaje = "EXITO";
                    eh.Descripcion = "Se ha actualizado exitosamente la solicitud de servicio";
                    break;
            }

            return eh;
        }

        public List<SDSDetalle> _listaSDSDetallePreExistente = new List<SDSDetalle>();

        //Propiedades protegidas para procesamiento interno
        protected List<SDSDetalle> _listaSDSDetalleActualizar = new List<SDSDetalle>();

        protected List<SDSDetalle> _listaSDSDetalleEliminar = new List<SDSDetalle>();

        protected List<SDSDetalle> _listaSDSDetalleInsertar = new List<SDSDetalle>();

        //Control de alta/baja/modificacion
        protected ErrorHandler InsertarDetallesNuevos(Tn transaccionSDSInsertar)
        {
            ErrorHandler eh = new ErrorHandler();
            try
            {
                foreach (SDSDetalle sdsDetalleInsertar in _listaSDSDetalleInsertar)
                {
                    sdsDetalleInsertar.IdSDS = IdSDS;
                    sdsDetalleInsertar.Insert(transaccionSDSInsertar);
                }
            }
            catch (Exception ex)
            {
                eh.Mensaje = "Error al insertar SDSDetalle nuevos";
                eh.Descripcion = ex.Message;
                return eh;
            }
            eh.Mensaje = "EXITO";
            eh.Descripcion = "Se han insertado exitosamente los SDSDetalle";
            return eh;
        }
        protected ErrorHandler ActualizarDetallesExistentes(Tn transaccionSDSActualizar)
        {
            ErrorHandler eh = new ErrorHandler();
            foreach (SDSDetalle sdsDetalleActualizar in _listaSDSDetalleActualizar)
            {
                try
                {
                    sdsDetalleActualizar.Update(transaccionSDSActualizar);
                }
                catch (Exception ex)
                {
                    eh.Mensaje = "Error al actualizar SDSDetalle";
                    eh.Descripcion = ex.Message;
                    return eh;
                }
            }
            eh.Mensaje = "EXITO";
            eh.Descripcion = "Se han actualizado correctamente los SDSDetalle";
            return eh;
        }
        protected ErrorHandler EliminarDetalles(Tn transaccionSDSDetalleEliminar)
        {
            ErrorHandler eh = new ErrorHandler();
            foreach (SDSDetalle sdsDetalleEliminar in _listaSDSDetalleEliminar)
            {
                try
                {
                    sdsDetalleEliminar.FHBaja = DateTime.Now;
                    sdsDetalleEliminar.Update(transaccionSDSDetalleEliminar);
                }
                catch (Exception ex)
                {
                    eh.Mensaje = "Error al dar de baja SDSDetalle";
                    eh.Descripcion = ex.Message;
                    return eh;
                }
            }
            eh.Mensaje = "EXITO";
            eh.Descripcion = "Se ha eliminado exitosamente el SDSDetalle";
            return eh;
        }
    }
}