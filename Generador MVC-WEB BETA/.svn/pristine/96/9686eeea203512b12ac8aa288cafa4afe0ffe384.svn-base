﻿using Code;
using MagicSQL;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class PreContacto : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.PreContacto preContacto = new CGADM.PreContacto();

        // Id del Modelo
        private int idPreContacto = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    idPreContacto = Request["cryptoID"].ToIntID();

                    if (!IsPostBack)
                    {
                        // Establece Título del Formulario = Título de la Página
                        lblTitulo.Text = Title;

                        // Ver Modelo
                        // Recupera el Modelo a partir del Id encriptado del Modelo
                        preContacto.Select(idPreContacto);

                        // Establece los valores de los controles desde las propiedades del Modelo
                        SetControls();

                        // Carga los Dsitribuidores
                        ddlDistribuidor.Items.Add(new ListItem("Seleccione el Distribuidor", (-1).ToCryptoID()));
                        foreach (var distribuidor in new CGADM.Distribuidor().Select().Where(d => d.FHBaja == null))
                        {
                            ddlDistribuidor.Items.Add(new ListItem(distribuidor.RazonSocial, distribuidor.IdDistribuidor.ToCryptoID()));
                        }
                    }
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (idPreContacto > 0)
            {
                // Recupera el Modelo
                preContacto.Select(idPreContacto);

                // Actualiza la Fecha Hora Baja
                preContacto.FHBaja = DateTime.Now;

                // Actualiza el Modelo
                preContacto.Update();

                // Confirma al usuario
                Message("Pre-Contacto dado de baja", "El PreContacto ha sido dado de baja exitosamente.", MessagesTypes.success, "PreContactos.aspx");
            }
            else
            {
                // No hay precontacto
                Message("Error", "No se seleccionó ningún Pre-Contacto.", MessagesTypes.error, "PreContactos.aspx");
            }
        }

        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            if (idDistribuidor > 0)
            {
                preContacto = new CGADM.PreContacto().Select(idPreContacto);

                // Alta de la Oportunidad
                using (Tn tn = new Tn("CGADM"))
                {
                    var oportunidad = new CGADM.Oportunidad()
                    {
                        IdDistribuidor = idDistribuidor,
                        FHAlta = DateTime.Now,
                        Descripcion = txtDescripcion.Text,
                        Detalle = txtDetalle.Text,
                        IdOrigen = preContacto.IdOrigen,
                        IdPreContacto = idPreContacto
                    };
                    oportunidad.Insert(tn);

                    // Alta del Contacto
                    var oportunidadContacto = new CGADM.OportunidadContacto()
                    {
                        FHAlta = DateTime.Now,
                        IdOportunidad = oportunidad.IdOportunidad,
                        Nombre = preContacto.Nombre,
                        Apellido = preContacto.Apellido,
                        Email = preContacto.Email,
                        Fijo = preContacto.Fijo,
                        Movil = preContacto.Movil,
                        Empresa = preContacto.Empresa,
                        Mensaje = preContacto.Mensaje,
                        CodigoContinente = preContacto.CodigoContinente,
                        NombreContinente = preContacto.NombreContinente,
                        CodigoPais = preContacto.CodigoPais,
                        NombrePais = preContacto.NombrePais,
                        CodigoRegion = preContacto.CodigoRegion,
                        NombreRegion = preContacto.NombreRegion,
                        Ciudad = preContacto.Ciudad,
                        CP = preContacto.CP,
                        Latitud = preContacto.Latitud,
                        Longitud = preContacto.Longitud
                    };
                    oportunidadContacto.Insert(tn);

                    tn.Commit();
                }

                Message("Oportunidad", "La Oportunidad para el distribuidor " + ddlDistribuidor.SelectedItem + " se creó exitósamente", MessagesTypes.success, "PreContactos.aspx");
            }
            else
            {
                Message("Atención", "Debe seleccionar un Distribuidor", MessagesTypes.warning);
            }

        }

        protected void SetControls()
        {
            lblFHAlta.Text = preContacto.FHAlta.ToString();

            CGADM.Origen origen = new CGADM.Origen().Select(preContacto.IdOrigen);
            lblOrigen.Text = origen.Descripcion;

            lblNombre.Text = preContacto.Nombre;
            lblApellido.Text = preContacto.Apellido;
            lblEmail.Text = preContacto.Email;
            lblFijo.Text = preContacto.Fijo;
            lblMovil.Text = preContacto.Movil;
            lblEmpresa.Text = preContacto.Empresa;
            lblMensaje.Text = preContacto.Mensaje;
            lblIP.Text = preContacto.IP;
            lblPais.Text = preContacto.NombrePais;
            lblRegion.Text = preContacto.NombreRegion;
            lblCiudad.Text = preContacto.Ciudad;
        }
    }
}