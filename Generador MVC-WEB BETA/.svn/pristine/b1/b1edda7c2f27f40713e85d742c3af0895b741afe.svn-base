﻿using Code;

using System;
using static Code.Utils;

namespace Pages
{
    public partial class TipoCobro : BasePage
    {
        // Modelo que se utilizará en la página
        private CGADM.TipoCobro model = new CGADM.TipoCobro();

        // Id del Modelo
        private int idModel = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Id del Modelo recuperado del Request
            if (Request["cryptoID"] != null)
            {
                try
                {
                    idModel = Request["cryptoID"].ToIntID();
                }
                catch (Exception)
                {
                    NavigateTo("~/Content/html/Alert.html");
                    return;
                }
            }

            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                if (idModel == 0)
                { // Nuevo Modelo
                    // Deshabilita los botones Editar y Eliminar
                    btnEdit.Enabled = false;
                    btnDelete.Enabled = false;

                    // Habilita el boton Guardar
                    btnSave.Enabled = true;
                }
                else
                { // Ver Modelo
                    // Recupera el Modelo a partir del Id encriptado del Modelo
                    model.Select(idModel);

                    // Genera el Hash del modelo y lo guarda en el control hdnHash
                    string jsonModel = model.ToJson();
                    hdnHash.Value = ModelHash(jsonModel);

                    // Establece los valores de los controles desde las propiedades del Modelo
                    SetControls();
                }

                // Habilita o Deshabilita los controles según el estado del btnSave
                EnableControls();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            // Deshabilita el botón Editar
            btnEdit.Enabled = false;

            // Deshabilita el botón Eliminar
            btnDelete.Enabled = false;

            // Habilita el botón Guardar
            btnSave.Enabled = true;

            // Habilita o Deshabilita los controles según el estado del btnSave
            EnableControls();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            // Recupera el Modelo a partir del Id encriptado
            model.Select(idModel);

            // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
            string jsonModel = model.ToJson();
            if (hdnHash.Value.Equals(ModelHash(jsonModel)))
            {// El Modelo está actualizado
                // Actualiza la Fecha Hora Baja
                model.FHBaja = DateTime.Now;

                // Actualiza el Modelo
                model.Update();

                // Confirma al usuario
                Message("Tipo Cobro dado de baja", "El Tipo Cobro ha sido dado de baja exitosamente.", MessagesTypes.success, "TiposCobros.aspx");
            }
            else
            {// El Modelo está desactualizado
             // Notifica que el Modelo está desactualizado en la DB
                Message("Tipo Cobro Desactualizado", "El Tipo Cobro cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (idModel == 0)
            {
                // Establece los valores de las propiedades del Modelo desde los controles
                SetProperties();

                // Inserta el Modelo
                model.Insert();

                // Confirma al usuario
                Message("Tipo Cobro creado", "El Tipo Cobro ha sido creado exitosamente.", MessagesTypes.success, "TiposCobros.aspx");
            }
            else
            {
                // Recupera el Modelo a partir del Id encriptado
                model.Select(idModel);

                // Genera el Hash del Modelo de la DB y lo compara con el guardado en el control hdnHash
                string jsonModel = model.ToJson();
                if (hdnHash.Value.Equals(ModelHash(jsonModel)))
                {// El Modelo está actualizado
                    // Establece los valores de las propiedades desde los controles
                    SetProperties();

                    // Actualiza el Modelo
                    model.Update();

                    // Confirma al usuario
                    Message("Tipo Cobro Editado", "El Tipo Cobro ha sido modificado exitosamente.", MessagesTypes.success, "TiposCobros.aspx");
                }
                else
                {// El Modelo está desactualizado
                    // Notifica que el Modelo está desactualizado en la DB
                    Message("Tipo Cobro Desactualizado", "El Tipo Cobro cambió en la DB desde la última vez que se lo recuperó.", MessagesTypes.warning, Request.Url.ToString());
                }
            }
        }

        protected void EnableControls()
        {
            /* TO-DO */
            txtDescripcion.Enabled = btnSave.Enabled;
            txtObservacion.Enabled = btnSave.Enabled;
        }

        protected void SetProperties()
        {
            /* TO-DO */
            model.Descripcion = txtDescripcion.Text;
            model.Observacion = txtObservacion.Text;
            model.FHAlta = DateTime.Now;
        }

        protected void SetControls()
        {
            /* TO-DO */
            txtDescripcion.Text = model.Descripcion;
            txtObservacion.Text = model.Observacion;
        }

        #region EjemplosMensajesAlUsuario

        // Mensajes en UserInterface desde CodeBehind
        protected void btnInfo_Click(object sender, EventArgs e)
        {
            Message("Info", "Texto del mensaje", MessagesTypes.info, "");
        }

        protected void btnSuccess_Click(object sender, EventArgs e)
        {
            Message("Success", "Texto del mensaje", MessagesTypes.success, "");
        }

        protected void btnWarning_Click(object sender, EventArgs e)
        {
            Message("Warning", "Texto del mensaje", MessagesTypes.warning, "");
        }

        protected void btnError_Click(object sender, EventArgs e)
        {
            Message("Error", "Texto del mensaje", MessagesTypes.error, "");
        }

        #endregion EjemplosMensajesAlUsuario
    }
}