﻿<%@ Page Title="Test Factory" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="TestFactory.aspx.cs" Inherits="Pages.TestFactory" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style></style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        var _sender;

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        function ConfirmarPaso(sender) {

            _sender = sender;

            // Recupera la columna 1 (serial de la UM).
            var serialUM = $(sender).parent().siblings()[1].innerText;

            //Si la longitud no es de 8 carcteres, porque esta en modo responsivo, se busca en la siguiente columna.
            if (serialUM.trim().length != 8) {
                serialUM = $(sender).parent().siblings()[2].innerText;
            }

            // Mensaje al usuario solicitando confirmación.
            swal({
                title: "¿Confirma el paso a Producción?",
                text: "La UM " + serialUM + " se pasará a producción.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#31B0D5",
                confirmButtonText: "Sí",
                cancelButtonText: "No",
                closeOnConfirm: true
            },
            function () {
                setTimeout("__doPostBack('" + _sender.name + "', '')", 0);
            });
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <!-- Nav tabs -->
            <ul id="tabs" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#ums" aria-controls="UM" role="tab" data-toggle="tab">
                        <asp:Label Text="UMs" runat="server" />
                    </a>
                </li>
                <li role="presentation">
                    <a href="#teclados" aria-controls="Teclado" role="tab" data-toggle="tab">
                        <asp:Label Text="Teclados" runat="server" />
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <%-- UMs --%>
                <div role="tabpanel" class="tab-pane active" id="ums">
                    <br />

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvUMs" CssClass="table table-striped table-bordered table-hover grid-view crypto-id"
                                Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvUMs_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                <Columns>
                                    <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <%-- Responsive + - --%>
                                    <asp:BoundField HeaderText="" />
                                    <%-- cryptoID --%>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCryptoId" runat="server" Text='<%# Eval("UMId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <asp:BoundField DataField="Id_UM" HeaderText="UM" />
                                    <asp:BoundField DataField="TipoModelo" HeaderText="Tipo Modelo" />
                                    <asp:BoundField DataField="Modelo" HeaderText="Modelo" />
                                    <asp:BoundField DataField="Soft" HeaderText="Soft" />
                                    <asp:BoundField DataField="FechaAlta" HeaderText="Fecha de Alta" />

                                    <asp:TemplateField HeaderText="Paso a Producción">
                                        <ItemTemplate>

                                            <asp:Button ID="btnPaso" Text="A producción" runat="server" OnClientClick="ConfirmarPaso(this); return false;" OnClick="btnPaso_Click"></asp:Button>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>

                <%-- Teclados --%>
                <div role="tabpanel" class="tab-pane" id="teclados">
                    <br />
                    AQUI VA LA LISTA DE TECLADOS
                </div>

            </div>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label ID="lblTest" Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>

</asp:Content>