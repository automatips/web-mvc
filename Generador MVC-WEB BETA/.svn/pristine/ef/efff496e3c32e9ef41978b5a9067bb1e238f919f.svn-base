﻿using CGUsuarios.Common;
using Code;
using MagicSQL;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Pages
{
    public partial class Cheques : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnNuevo.Visible = false;
                lblTitulo.Text = Title;
                if (SecurityManager.IdAplicacion == idAplicacionDistribuidor)
                {
                    var distribuidor = new CGADM.Distribuidor().Select((int)SecurityManager.IdPropietario);
                    ddlDistribuidor.Items.Add(new ListItem(distribuidor.RazonSocial, distribuidor.IdDistribuidor.ToCryptoID()));
                    ddlDistribuidor.Enabled = false;
                }
                else
                {
                    foreach (CGADM.Distribuidor m in new CGADM.Distribuidor().Select().Where(m => m.FHBaja == null))
                    {
                        ddlDistribuidor.Items.Add(new ListItem(m.RazonSocial, m.IdDistribuidor.ToCryptoID()));
                    }
                }
                if (ddlDistribuidor.Items.Count > 0)
                {
                    ddlDistribuidor_OnSelectedIndexChanged(null, null);
                }
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // Id del Distribuidor desencriptado
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();

            // Lista de los registros de la tabla de enlace DistribuidorUnidadNegocio con IdDistribuidor igual al seleccionado en el ddlDistribuidor
            List<CGADM.DistribuidorUnidadNegocio> listDistribuidorUnidadNegocio =
                new CGADM.DistribuidorUnidadNegocio().Select().Where(dun => dun.IdDistribuidor == idDistribuidor).ToList();

            // Lista de los registros de la tabla UnidadNegocio con IdUnidadNegocio contenido en la lista listDistribuidorUnidadNegocio (filtrada en el paso anterior)
            List<CGADM.UnidadNegocio> listUnidadNegocio =
                new CGADM.UnidadNegocio().Select().Where(dun => listDistribuidorUnidadNegocio.Any(un => dun.IdUnidadNegocio == un.IdUnidadNegocio)).ToList();

            // Limpia el ddlUnidadNegocio
            ddlUnidadNegocio.Items.Clear();
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio)
            {
                // Agrega el item UnidadNegocio
                ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID()));
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            int idDistribuidor = ddlDistribuidor.SelectedValue.ToIntID();
            int idUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();

            if (idDistribuidor <= 0 || idUnidadNegocio <= 0)
            {
                gvCheques.DataSource = null;
                gvCheques.DataBind();
                return;
            }

            gvCheques.DataSource = new SP("CGAdm").Execute("usp_cheques",
                                                    P.Add("IdDistribuidor", idDistribuidor),
                                                    P.Add("IdUnidadNegocio", idUnidadNegocio),
                                                    P.Add("@IncluirVencidos", chkIncluirVencidos.Checked));
            gvCheques.DataBind();
        }

        protected void gvCheques_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Convierte el Id del modelo en CryptoID.
                string plainId = e.Row.Cells[1].Text;
                e.Row.Cells[1].Text = plainId.ToInt().ToCryptoID();
            }
        }
    }
}