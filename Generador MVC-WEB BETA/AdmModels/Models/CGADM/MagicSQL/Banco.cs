﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class Banco : ISUD<Banco>
    {
        public Banco() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdBanco { get; set; }

        public int? IdDistribuidor { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Descripcion { get; set; }
    }
}