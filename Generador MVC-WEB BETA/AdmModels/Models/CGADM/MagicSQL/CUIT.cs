﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class CUIT : ISUD<CUIT>
    {
        public CUIT() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCUIT { get; set; }

        public DateTime FHAlta { get; set; }

        public long NumeroCUIT { get; set; }

        public string RazonSocial { get; set; }

        public string Domicilio { get; set; }

        public string Email { get; set; }

        public byte? CodigoCondicionIVA { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}