﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class Cargo : ISUD<Cargo>
  {
    public Cargo() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdCargo { get; set; }

    public DateTime FHAlta { get; set; }

    public string Descripcion { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}