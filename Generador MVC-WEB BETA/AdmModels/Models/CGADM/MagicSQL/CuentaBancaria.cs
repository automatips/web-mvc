﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class CuentaBancaria : ISUD<CuentaBancaria>
    {
        public CuentaBancaria() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdCuentaBancaria { get; set; }

        public int IdDistribuidor { get; set; }

        public int IdBanco { get; set; }

        public string Descripcion { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}