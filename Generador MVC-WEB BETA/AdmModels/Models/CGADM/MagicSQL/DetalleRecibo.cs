﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DetalleRecibo : ISUD<DetalleRecibo>
    {
        public DetalleRecibo() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDetalleRecibo { get; set; }

        public int IdRecibo { get; set; }

        public int IdTipoCobro { get; set; }

        public decimal Importe { get; set; }

        public int? IdBancoCheque { get; set; }

        public string NumeroCheque { get; set; }

        public DateTime? FechaCobroCheque { get; set; }

        public string TitularidadCheque { get; set; }

        public string ReferenciaTransferencia { get; set; }

        public DateTime? FechaImputacionTransferencia { get; set; }

        public int? IdCuentaBancaria { get; set; }
    }
}