﻿// Created for MagicSQL
using MagicSQL;

namespace CGADM
{
    public partial class Direccion : ISUD<Direccion>
    {
        public Direccion() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public long IdDireccion { get; set; }

        public string Ingresada { get; set; }

        public string Notas { get; set; }

        public double? Latitud { get; set; }

        public double? Longitud { get; set; }

        public string DireccionFormateada { get; set; }

        public string IdLugar { get; set; }

        public string URL { get; set; }

        public byte[] Mapa { get; set; }
    }
}