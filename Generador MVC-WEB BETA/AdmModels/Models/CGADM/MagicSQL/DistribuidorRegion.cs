﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorRegion : ISUD<DistribuidorRegion>
    {
        public DistribuidorRegion() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorRegion { get; set; }

        public int IdDistribuidor { get; set; }

        public int IdRegion { get; set; }

        public DateTime FHAalta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}