﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorReseller : ISUD<DistribuidorReseller>
    {
        public DistribuidorReseller() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorReseller { get; set; }

        public int IdDistribuidorUnidadNegocio { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdReseller { get; set; }

        public DateTime? FHAltaPlanComercial { get; set; }

        public int? IdPlanComercial { get; set; }

        public DateTime? FHHastaForzar { get; set; }

        public string Observaciones { get; set; }

        public DateTime? FHBaja { get; set; }

        public DateTime? FechaProximaLiquidacion { get; set; }

        public int? CantidadUM { get; set; }

        public bool? ForzarCantidadUM { get; set; }

        public bool? LiquidaManual { get; set; }

        public bool? Ex { get; set; }

        public bool? LiquidaSoloOperativas { get; set; }
    }
}