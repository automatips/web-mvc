﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorUM : ISUD<DistribuidorUM>
    {
        public DistribuidorUM() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorUM { get; set; }

        public int IdDistribuidorUnidadNegocio { get; set; }

        public int UMId { get; set; }

        public DateTime FHAlta { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public long? IdServidorOutput { get; set; }

        public DateTime? FHServidorOutputProcesadoEmitido { get; set; }

        public string AccionABMServidorOutput { get; set; }

        public DateTime? FHEmitido { get; set; }

        public DateTime? FHRechazado { get; set; }

        public long? IdSesionTrabajoDetalle { get; set; }

        public DateTime? FHAceptacion { get; set; }

        public int? IdTecnicoAceptacion { get; set; }

        public long? IdServidorDialogoAceptacion { get; set; }
    }
}