﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class DistribuidorUMHistorial : ISUD<DistribuidorUMHistorial>
    {
        public DistribuidorUMHistorial() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdDistribuidorUMHistorial { get; set; }

        public int IdDistribuidorUM { get; set; }

        public DateTime FHAltaRegistro { get; set; }

        public DateTime FHAlta { get; set; }

        public string Observacion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}