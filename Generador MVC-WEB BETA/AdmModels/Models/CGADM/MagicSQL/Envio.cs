﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Envio : ISUD<Envio>
    {
        public Envio() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdEnvio { get; set; }

        public int? IdCuenta { get; set; }

        public string NumeroEnvio { get; set; }

        public string FHEnvio { get; set; }

        public string Observaciones { get; set; }

        public decimal? Importe { get; set; }

        public long? IdDireccion { get; set; }

        public int? IdSDT { get; set; }

        public int? IdSDS { get; set; }

        public string Tipo { get; set; }

        public int? IdTransporte { get; set; }

        public int? IdRemitoExterno { get; set; }

        public long? IdArchivo { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}