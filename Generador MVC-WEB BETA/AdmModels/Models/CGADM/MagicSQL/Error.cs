﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Error : ISUD<Error>
    {
        public Error() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdError { get; set; }

        public string Mensaje { get; set; }

        public string ExceptionMessage { get; set; }

        public string ExceptionStackTrace { get; set; }

        public string Desarrollador { get; set; }

        public string Aplicacion { get; set; }

        public DateTime? FHAlta { get; set; }
    }
}