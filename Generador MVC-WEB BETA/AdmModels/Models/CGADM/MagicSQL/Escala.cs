﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Escala : ISUD<Escala>
    {
        public Escala() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdEscala { get; set; }

        public int IdListaPrecio { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime FHVigencia { get; set; }

        public int CantInferior { get; set; }

        public int CantSuperior { get; set; }

        public int? IdMoneda { get; set; }

        public decimal? PrecioLista { get; set; }

        public decimal? PrecioContado { get; set; }

        public int? Porcentaje { get; set; }

        public decimal? CobroMinimoEnPorcentual { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}