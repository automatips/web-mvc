﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class FormaPago : ISUD<FormaPago>
    {
        public FormaPago() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdFormaPago { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}