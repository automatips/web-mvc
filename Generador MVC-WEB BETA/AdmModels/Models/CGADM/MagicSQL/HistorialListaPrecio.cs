﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class HistorialListaPrecio : ISUD<HistorialListaPrecio>
    {
        public HistorialListaPrecio() : base(2)
        {
        } // base(SPs_Version)

        // Properties

        public int IdCambio { get; set; }

        public int IdListaPrecio { get; set; }

        public int IdPlanComercial { get; set; }

        public DateTime FHAlta { get; set; }

        public bool Servicio { get; set; }

        public bool RecurrenteAutomatico { get; set; }

        public int? IdModuloAutomatico { get; set; }

        public int IdServicioTipo { get; set; }

        public long? IdImagen { get; set; }

        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        public int IdMoneda { get; set; }

        public decimal Precio { get; set; }

        public DateTime? FHBaja { get; set; }

        public string ObservacionBaja { get; set; }

        public decimal? PrecioRebajado { get; set; }

        public bool? PermiteDescuento { get; set; }

        public DateTime FHCambio { get; set; }

        public string CodigoProducto { get; set; }

        public bool? PrecioDinamico { get; set; }
    }
}