﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class ListaPrecio : ISUD<ListaPrecio>
    {
        public ListaPrecio() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdListaPrecio { get; set; }

        public int IdPlanComercial { get; set; }

        public int IdProductoServicio { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime FHVigenciaInicio { get; set; }

        public DateTime? FHBaja { get; set; }

        public string ObservacionBaja { get; set; }

        public bool PrecioFijo { get; set; }

        public bool Escala { get; set; }

        public int IdMoneda { get; set; }

        public decimal? PrecioLista { get; set; }

        public decimal? PrecioContado { get; set; }

        public int? Porcentaje { get; set; }

        public decimal? CobroMinimoEnPorcentual { get; set; }

        public int? IdModuloAutomatico { get; set; }
    }
}