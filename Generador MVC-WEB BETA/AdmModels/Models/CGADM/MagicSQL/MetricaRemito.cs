﻿namespace CGADM
{
    public partial class MetricaRemito
    {
        public MetricaRemito()
        {
        }

        public int IDSOLICITANTE { get; set; }
        public string RZ { get; set; }
        public int DisponiblesRemitar { get; set; }
        public string FechaUltimaGestion { get; set; }
        public int PendientesGestionar { get; set; }
        public string TipoSolicitante { get; set; }
    }
}