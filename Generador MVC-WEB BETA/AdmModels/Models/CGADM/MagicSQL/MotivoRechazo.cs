﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class MotivoRechazo : ISUD<MotivoRechazo>
    {
        public MotivoRechazo() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdMotivoRechazo { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripción { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Observacion { get; set; }
    }
}