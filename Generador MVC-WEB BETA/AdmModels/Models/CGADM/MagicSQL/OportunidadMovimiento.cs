﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class OportunidadMovimiento : ISUD<OportunidadMovimiento>
    {
        public OportunidadMovimiento() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdOportunidadMovimiento { get; set; }

        public int IdOportunidad { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdCuenta { get; set; }

        public int IdOportunidadTipoMovimiento { get; set; }

        public string Detalle { get; set; }
    }
}