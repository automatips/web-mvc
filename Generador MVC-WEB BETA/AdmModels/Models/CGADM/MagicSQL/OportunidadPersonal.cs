﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class OportunidadPersonal : ISUD<OportunidadPersonal>
    {
        public OportunidadPersonal() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdOportunidadPersonal { get; set; }

        public int IdOportunidad { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdDistribuidorPersonal { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Observacion { get; set; }
    }
}