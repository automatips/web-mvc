﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class PendienteFacturar : ISUD<PendienteFacturar>
    {
        public PendienteFacturar() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPendienteFacturar { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime FechaParaImputar { get; set; }

        public decimal Importe { get; set; }

        public int? IdRecibo { get; set; }

        public long? IdComprobante { get; set; }

        public int IdUnidadNegocio { get; set; }

        public int IdReseller { get; set; }

        public int IdDistribuidor { get; set; }

        public string Observacion { get; set; }

        public int IdConcepto { get; set; }

        public int IdPendienteEstado { get; set; }

        public int IdMoneda { get; set; }

        public int? IdListaPrecio { get; set; }

        public string Descripcion { get; set; }
    }
}