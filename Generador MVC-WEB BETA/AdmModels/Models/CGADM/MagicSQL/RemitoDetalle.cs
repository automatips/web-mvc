﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class RemitoDetalle : ISUD<RemitoDetalle>
    {
        public RemitoDetalle() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdRemitoDetalle { get; set; }

        public int IdRemito { get; set; }

        public int Orden { get; set; }

        public int Cantidad { get; set; }

        public int? IdListaPrecio { get; set; }

        public string DescripcionListaPrecio { get; set; }

        public decimal? Precio { get; set; }

        public int? UMId { get; set; }

        public int? IdDispositivos { get; set; }

        public string Observacion { get; set; }

        public string Tipo { get; set; }

        public int? NroSolicitud { get; set; }

        public int? IdSDTDetalle { get; set; }
    }
}