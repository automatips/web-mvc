﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class SDT : ISUD<SDT>
    {
        public SDT() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDT { get; set; }

        public DateTime FHAlta { get; set; }

        public int? IdCuentaEnvio { get; set; }

        public string ObservacionesEnvio { get; set; }

        public DateTime? FHFinalizacion { get; set; }

        public int? IdCuentaFinalizacion { get; set; }

        public string ObservacionesFinalizacion { get; set; }

        public string MailInforme { get; set; }

        public long? IdImagenSDT { get; set; }

        public int? IdUnidadNegocio { get; set; }

        public int? IdPlanComercial { get; set; }

        public int? IdDistribuidorUnidadNegocio { get; set; }

        public int? IdReseller { get; set; }

        public int? SDTNro { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}