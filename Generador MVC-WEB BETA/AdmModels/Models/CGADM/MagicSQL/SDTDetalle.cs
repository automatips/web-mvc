﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class SDTDetalle : ISUD<SDTDetalle>
    {
        public SDTDetalle() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDTDetalle { get; set; }

        public int IdSDT { get; set; }

        public int? Cantidad { get; set; }

        public decimal? Precio { get; set; }

        public int? CantidadRemitados { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string IdSerial { get; set; }

        public string ArtefactoTipo { get; set; }

        public string Incidencia { get; set; }

        public int? IdSDTDetalleEstado { get; set; }

        public DateTime? FHUltimaGestion { get; set; }
    }
}