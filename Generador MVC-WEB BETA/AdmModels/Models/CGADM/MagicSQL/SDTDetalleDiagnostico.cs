﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class SDTDetalleDiagnostico : ISUD<SDTDetalleDiagnostico>
    {
        public SDTDetalleDiagnostico() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDTDetalleDiagnostico { get; set; }

        public decimal? Presupuesto { get; set; }

        public string Descripcion { get; set; }

        public byte? Opcional { get; set; }

        public byte? Aprobado { get; set; }

        public DateTime? FHAprobacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public int? IdSDTDetalle { get; set; }
    }
}