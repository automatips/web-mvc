﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class SDTDetalleGestion : ISUD<SDTDetalleGestion>
    {
        public SDTDetalleGestion() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDTDetalleGestion { get; set; }

        public int? IdCuenta { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public int? IdSDTDetalleEstado { get; set; }
    }
}