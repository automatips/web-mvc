﻿// Created for MagicSQL

using MagicSQL;

namespace CGADM
{
    public partial class SDTDetalleGestionHistorial : ISUD<SDTDetalleGestionHistorial>
    {
        public SDTDetalleGestionHistorial() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSDTDetalleGestionHistorial { get; set; }

        public int? IdSDTDetalleGestion { get; set; }

        public int? IdSDTDetalle { get; set; }
    }
}