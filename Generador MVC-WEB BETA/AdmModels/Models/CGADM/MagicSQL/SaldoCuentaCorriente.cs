﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class SaldoCuentaCorriente : ISUD<SaldoCuentaCorriente>
    {
        public SaldoCuentaCorriente() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdSaldoCuentaCorriente { get; set; }

        public int IdUnidadNegocio { get; set; }

        public int IdReseller { get; set; }

        public decimal Saldo { get; set; }

        public DateTime FHActualizacion { get; set; }

        public DateTime? FHBaja { get; set; }

        public int IdDistribuidor { get; set; }
    }
}