﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGADM
{
    public partial class ServicioTipo : ISUD<ServicioTipo>
    {
        public ServicioTipo() : base(2)
        {
        } // base(SPs_Version)

        // Properties

        public int IdServicioTipo { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public bool EsProducto { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}