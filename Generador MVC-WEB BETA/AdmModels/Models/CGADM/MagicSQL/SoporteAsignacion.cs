﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteAsignacion : ISUD<SoporteAsignacion>
  {
    public SoporteAsignacion() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteAsignacion { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporteIdentidad { get; set; }

    public int IdCuenta { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}