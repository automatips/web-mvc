﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteCuenta : ISUD<SoporteCuenta>
  {
    public SoporteCuenta() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteCuenta { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdCuenta { get; set; }

    public int IdSoporteTipo { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}