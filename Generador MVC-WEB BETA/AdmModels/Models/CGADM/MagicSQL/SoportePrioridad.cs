﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoportePrioridad : ISUD<SoportePrioridad>
  {
    public SoportePrioridad() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoportePrioridad { get; set; }

    public DateTime FHAlta { get; set; }

    public string Descripcion { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}