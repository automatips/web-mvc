﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteSeguimiento : ISUD<SoporteSeguimiento>
  {
    public SoporteSeguimiento() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteSeguimiento { get; set; }

    public DateTime FHAlta { get; set; }

    public int IdSoporte { get; set; }

    public int IdSoporteTipoMovimiento { get; set; }

    public int IdCuenta { get; set; }

    public int IdSoporteEstado { get; set; }

    public string Nota { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}