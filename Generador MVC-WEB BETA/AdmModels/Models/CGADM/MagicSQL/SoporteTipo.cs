﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
  public partial class SoporteTipo : ISUD<SoporteTipo>
  {
    public SoporteTipo() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdSoporteTipo { get; set; }

    public DateTime FHAlta { get; set; }

    public string Descripcion { get; set; }

    public DateTime? FHBaja { get; set; }
  }
}