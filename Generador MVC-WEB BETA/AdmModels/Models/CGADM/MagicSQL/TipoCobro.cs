﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class TipoCobro : ISUD<TipoCobro>
    {
        public TipoCobro() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdTipoCobro { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Observacion { get; set; }
    }
}