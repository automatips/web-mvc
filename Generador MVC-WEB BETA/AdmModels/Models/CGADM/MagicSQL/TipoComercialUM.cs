﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class TipoComercialUM : ISUD<TipoComercialUM>
    {
        public TipoComercialUM() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdTipoComercialUM { get; set; }

        public DateTime FHAlta { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}