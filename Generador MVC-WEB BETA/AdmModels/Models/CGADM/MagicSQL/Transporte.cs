﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGADM
{
    public partial class Transporte : ISUD<Transporte>
    {
        public Transporte() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdTransporte { get; set; }

        public DateTime FHAlta { get; set; }

        public string Proveedor { get; set; }

        public string Sucursal { get; set; }

        public string Domicilio { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public string Observaciones { get; set; }

        public int? IdDistribuidor { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}