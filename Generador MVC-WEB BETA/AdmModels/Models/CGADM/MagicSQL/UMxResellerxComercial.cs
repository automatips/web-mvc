﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGADM
{
    public partial class UMxResellerxComercial : ISUD<UMxResellerxComercial>
    {
        public UMxResellerxComercial() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdUMxResellerxComercial { get; set; }

        public DateTime FHAlta { get; set; }

        public DateTime FHInicioSituacionComercial { get; set; }

        public int IdTipoComercialUM { get; set; }

        public DateTime FHFinSituacionComercial { get; set; }

        public string ObservacionesInicio { get; set; }

        public string ObservacionesFin { get; set; }

        public int? IdSDS { get; set; }
    }
}