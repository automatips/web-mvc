﻿// Created by MagicMaker

using System.Collections.Generic;

namespace CGADM
{
  public partial class Remito 
  {
        // The custom properties name have to start with '_'. Example: _MyProp

        public List<RemitoDetalle> _Detalle = new List<RemitoDetalle>();

        public void Guardar()
        {

            this.Insert();
            foreach (var d in this._Detalle)
            {
                d.Insert();
            }

        }
  }
}