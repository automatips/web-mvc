﻿// Created for MagicSQL
using MagicSQL;
using System;

namespace CGAccess
{
    public partial class Reseller : ISUD<Reseller>
    {
        public Reseller() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdReseller { get; set; }

        public DateTime FHAlta { get; set; }

        public int IdDistribuidor { get; set; }

        public string RazonSocial { get; set; }

        public long? IdImagen { get; set; }

        public string CUIT { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public string Direccion { get; set; }

        public long? IdDireccion { get; set; }

        public string Telefono { get; set; }

        public string Fax { get; set; }

        public string Notas { get; set; }

        public DateTime? FHBaja { get; set; }

        public long? IdSesionTrabajoDetalle { get; set; }
    }
}