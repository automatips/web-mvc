﻿// Created for MagicSQL

using System;
using MagicSQL;

namespace CGCashVend
{
  public partial class ConfiguracionesReseller : ISUD<ConfiguracionesReseller>
  {
    public ConfiguracionesReseller() : base(1) { } // base(SPs_Version)

    // Properties
   
    public int IdConfiguracionesReseller { get; set; }

    public int IdReseller { get; set; }

    public decimal MontoDescubierto { get; set; }
  }
}