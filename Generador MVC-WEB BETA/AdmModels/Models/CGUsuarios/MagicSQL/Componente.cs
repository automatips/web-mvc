﻿// Created for MagicSQL

using MagicSQL;
using System;

namespace CGUsuarios
{
    public partial class Componente : ISUD<Componente>
    {
        public Componente() : base(2)
        {
        } // base(SPs_Version)

        // Properties

        public long IdComponente { get; set; }

        public DateTime FechaHoraAlta { get; set; }

        public DateTime? FechaHoraBaja { get; set; }

        public long? IdTipoComponente { get; set; }

        public string Nombre { get; set; }

        public string Identificador { get; set; }

        public long? IdComponentePadre { get; set; }
    }
}