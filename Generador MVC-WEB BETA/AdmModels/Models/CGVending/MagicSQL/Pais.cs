﻿// Created for MagicSQL
using MagicSQL;

namespace CGVending
{
    public partial class Pais : ISUD<Pais>
    {
        public Pais() : base(1)
        {
        } // base(SPs_Version)

        // Properties

        public int IdPais { get; set; }

        public string Nombre { get; set; }

        public string Prefijo { get; set; }
    }
}