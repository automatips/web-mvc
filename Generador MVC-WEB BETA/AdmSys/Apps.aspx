﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Apps.aspx.cs" Inherits="AdmSys.Apps" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema de gestión de eventos">
    <meta name="author" content="MF">

    <title>Sistema Administrativo</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Bootstrap --%>
    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="Content/site.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #bottom {
            color: whitesmoke;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</head>
<body id="body" runat="server">
    <div class="container">
        <br />
        <br />
        <div class="jumbotron text-center">
            <div class="row">
                <h3>
                    <asp:Label Text="Seleccione la Aplicación" runat="server" />
                </h3>
                <asp:Label Text="del Sistema Administrativo que desea acceder." runat="server" />
            </div>
            <br />
            <br />
            <div class="row">
                <a href="Login.aspx?app=admsysadministrador" class="btn btn-lg btn-info" style="background: url(Content/images/bkg-texture-adm.png); height: 33px;">Iniciar sesión como Administrador</a>
            </div>
            <br />
            <div class="row">
                <a href="Login.aspx?app=admsysdistribuidor" class="btn btn-lg btn-info" style="background: url(Content/images/bkg-texture-dis.png); height: 33px;">Iniciar sesión como Distribuidor</a>
            </div>
        </div>
        <%-- Pie de página --%>
        <div class="centered">
            <div id="bottom" class="text-center">
                <span>©</span>
                <span>2018</span>
                <span>-</span>
                <span>Sistema Administrativo</span>
                <br />
                <small>
                    <asp:Label ID="lblVersion" Text="Versión: ..." runat="server" />
                </small>
            </div>
        </div>
    </div>
</body>
</html>