﻿using CGUsuarios.Common;
using CGUsuarios.Common.Managers;
using log4net;
using Newtonsoft.Json;

using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;

namespace Code
{
    public class BasePage : System.Web.UI.Page
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Cronómetro para medir el tiempo entre la Solicitud de la Respuesta
        private Stopwatch stopwatch = new Stopwatch();

        private bool logout;

        //Ids
        protected static short idAplicacionAdministrador = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionAdministrador"].ToShort();

        protected static short idAplicacionDistribuidor = System.Configuration.ConfigurationManager.AppSettings["IdAplicacionDistribuidor"].ToShort();

        protected override void Construct()
        {
            // Resetea el cronómetro
            stopwatch.Reset();

            // Inica el cronómetro
            stopwatch.Start();
        }

        protected override void InitializeCulture()
        {
            logout = !CurrentSession.ValidateSession();
            if (!logout)
            {
                string culture = "";
                switch (CGUsuarios.Common.SecurityManager.Usuario.IdIdioma)
                {
                    case 1:
                        culture = "es";
                        break;

                    case 4:
                        culture = "en";
                        break;
                }

                base.InitializeCulture();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            // Valida la session actual
            if (logout)
            {
                Utils.NavigateTo("~/Content/html/Logout.html");

                base.Dispose();
            }
        }

        private void Page_PreLoad(object sender, EventArgs e)
        {
            // Formatea los controles para el Code Behind
            Regional regional = new Regional("PreLoad", Page.Controls);

            // Registrar actividad
            try
            {
                if (SecurityManager.Usuario.AuditarActividadSistema)
                {
                    var activityLog = new CGUsuarios.Common.SesionTrabajoLog()
                    {
                        FechaHora = DateTime.Now
                        ,
                        Descripcion = "Acceso a " + Request.RawUrl + (IsPostBack ? " (Postback " + GetPostBackControlName() + ")" : "")
                        ,
                        IdAplicacion = SecurityManager.IdAplicacion
                        ,
                        IdSesionTrabajo = SecurityManager.IdSesionTrabajo
                        ,
                        Ip = SecurityManager.GetUserIPAddress()
                    };

                    var result = GenericManager.Post<CGUsuarios.Common.SesionTrabajoLog>(activityLog);
                }
            }
            catch (Exception) { }
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            // Aplica los permisos de acceso al WebForm y sus controles
            //Permission p = new Permission();

            // Deformatea los controles para la User Interface
            Regional regional = new Regional("PreRender", Page.Controls);

            // Configuracion regional para los plugins JS
            string regionalSettings = JsonConvert.SerializeObject(regional.regionalSettings);
            Utils.ExecJsFn("RegionalSettings", regionalSettings);
        }

        protected void Page_Error(object sender, System.EventArgs e)
        {
            // Recupera el último error del Servidor
            Exception ex = Server.GetLastError();

            // Log del error
            Log.Error(Utils.GetRequestedURL() + " " + Utils.GetRequestIPAddress() + " " + ex.Message);

            // Mensaje al usuario
            Utils.NavigateTo("~/Content/html/500.html?errMessage=" + ex.Message);

            // Limpia el error del Servidor
            Server.ClearError();
        }

        protected override void OnUnload(EventArgs e)
        {
            // Detiene el cronómetro
            stopwatch.Stop();
            if (stopwatch.ElapsedMilliseconds > 500)
            {
                Log.Debug(stopwatch.ElapsedMilliseconds.ToString() + " ms\t\tPage => " + Utils.GetRequestedURL() + " " + Utils.GetRequestIPAddress());
            }
        }

        // Obtiene el control que causó el postback
        private string GetPostBackControlName()
        {
            System.Web.UI.Control control = null;

            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }

            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                System.Web.UI.Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control.ID;
        }
    }
}