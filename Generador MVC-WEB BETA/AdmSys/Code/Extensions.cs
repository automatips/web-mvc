﻿using System;
using System.Reflection;

public static class Extensions
{
    public static void EJEMPLO<ISUD>(this ISUD x)
    {
    }

    public static void DarDeBaja<ISUD>(this ISUD modelo, string fhBaja = "FHBaja")
    {
        PropertyInfo propertyInfo = modelo.GetType().GetProperty(fhBaja);
        propertyInfo.SetValue(modelo, Convert.ChangeType(DateTime.Now, propertyInfo.PropertyType));

        Type type = modelo.GetType();
        object classInstance = Activator.CreateInstance(type, null);
        MethodInfo methodInfo = type.GetMethod("UPDATE");

        methodInfo.Invoke(classInstance, null);
    }
}