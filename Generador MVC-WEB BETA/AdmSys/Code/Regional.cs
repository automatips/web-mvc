﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Code
{
    internal class RegionalSettings
    {
        // Número -----------------------------------------

        public string NumericGroupSeparator { get; set; }
        public string NumericDecimalSymbol { get; set; }
        public bool NumericLeadZeros { get; set; }
        public int NumericDecimalDigits { get; set; }
        public bool NumericFillDecimalsWithZeros { get; set; }

        // Moneda -----------------------------------------

        public string MoneySymbol { get; set; }
        public string MoneySymbolPosition { get; set; } // p: Prefijo, s: Sufijo

        public string MoneyGroupSeparator { get; set; }
        public string MoneyDecimalSymbol { get; set; }
        public bool MoneyLeadZeros { get; set; }
        public int MoneyDecimalDigits { get; set; }
        public bool MoneyFillDecimalsWithZeros { get; set; }

        // Fecha -----------------------------------------

        public string DateFormat { get; set; } // dd/MM/yyyy, MM/dd/yyyy, etc.
        public int FirstDayWeek { get; set; } // 0: Domingo, 1: lunes, ...

        // Hora -----------------------------------------

        public int TimeFormat { get; set; } // 12/24
        public int TimeZoneOffSet { get; set; }
    }

    internal class Regional
    {
        private int serverOffSet = -180; // GMT Server OffSet (en minutos)
        public RegionalSettings regionalSettings = new RegionalSettings();

        // Constructor
        public Regional(string accion, ControlCollection controlCollection)
        {
            // Inicializa la configuración regional

            // Número -----------------------------------------

            regionalSettings.NumericGroupSeparator = "."; // coma, punto o espacio
            regionalSettings.NumericDecimalSymbol = ","; // coma o punto
            regionalSettings.NumericLeadZeros = false;
            regionalSettings.NumericDecimalDigits = 2;
            regionalSettings.NumericFillDecimalsWithZeros = true;

            // Moneda -----------------------------------------

            regionalSettings.MoneySymbol = "AR$";
            regionalSettings.MoneySymbolPosition = "p";

            regionalSettings.MoneyGroupSeparator = "."; // coma, punto o espacio
            regionalSettings.MoneyDecimalSymbol = ","; // coma o punto
            regionalSettings.MoneyLeadZeros = false;
            regionalSettings.MoneyDecimalDigits = 2;
            regionalSettings.MoneyFillDecimalsWithZeros = true;

            // Fecha -----------------------------------------

            regionalSettings.DateFormat = "dd/MM/yyyy"; // CGUsuarios.Common.SecurityManager.Usuario.FormatoFecha;
            regionalSettings.FirstDayWeek = 0; // CGUsuarios.Common.SecurityManager.Usuario.PrimerDiaSemana.ToString().ToInt();

            // Hora -----------------------------------------

            regionalSettings.TimeFormat = 24; // 12 o 24
            regionalSettings.TimeZoneOffSet = -180;

            // Formatea los controles
            SetRegionalSettings(accion, controlCollection);
        }

        private void SetRegionalSettings(string accion, ControlCollection controlCollection)
        {
            // Recorre la lista de controles del WebForm
            foreach (Control control in controlCollection)
            {
                // Id del control
                string controlId = control.ID;
                if (!string.IsNullOrEmpty(controlId))
                {
                    // Tipo de control
                    string contolType = control.GetType().ToString();
                    contolType = contolType.Replace("System.Web.UI.WebControls.", "");
                    contolType = contolType.Replace("System.Web.UI.HtmlControls.", "");

                    // Clase del control
                    string controlClass = "";
                    switch (contolType)
                    {
                        case "Label":
                            Label label = (Label)control;
                            controlClass = label.Attributes["class"] == null ? "" : label.Attributes["class"];

                            // Si no se encuentra la propiedad Class, se intenta buscar como CssClass
                            if (string.IsNullOrEmpty(controlClass))
                            {
                                controlClass = label.CssClass;
                            }

                            // Si tiene valor y clase
                            if (!label.Text.Trim().Equals("") & !controlClass.Equals(""))
                            {
                                if (accion == "PreLoad")
                                {
                                    label.Text = PreLoad(label.Text, controlClass);
                                }
                                else
                                {
                                    label.Text = PreRender(label.Text, controlClass);
                                }
                            }
                            break;

                        case "TextBox":
                            TextBox textBox = (TextBox)control;
                            controlClass = textBox.Attributes["class"] == null ? "" : textBox.Attributes["class"];

                            // Si no se encuentra la propiedad Class, se intenta buscar como CssClass
                            if (string.IsNullOrEmpty(controlClass))
                            {
                                controlClass = textBox.CssClass;
                            }

                            // Si tiene valor y clase
                            if (!textBox.Text.Trim().Equals("") & !controlClass.Equals(""))
                            {
                                // Si el control es tipo fecha hora
                                if (controlClass.Contains("date-picker") || controlClass.Contains("time-picker"))
                                {
                                    // Si el control tiene grupo fecha-hora busca el otro elemento del grupo
                                    string dtGroup = ((textBox.Attributes["dt-group"] == null) ? "" : textBox.Attributes["dt-group"]);
                                    if (!dtGroup.Equals(""))
                                    {
                                        // Busca el otro elemento del grupo
                                        string tmp = "";
                                        if (controlClass.Contains("date-picker"))
                                        {
                                            tmp = GetFHGrupo(accion, controlCollection, dtGroup, controlId);
                                            if (tmp.Length > 11)
                                            {
                                                tmp = tmp.Substring(0, 8);
                                            }
                                            textBox.Text = textBox.Text.Substring(0, 10) + " " + tmp;
                                        }
                                        else
                                        {
                                            if (textBox.Text.Length > 11)
                                            {
                                                textBox.Text = textBox.Text.Substring(0, 8);
                                            }
                                            textBox.Text = "01/01/2000 " + textBox.Text;
                                        }
                                    }
                                    else
                                    {
                                        // Fechas horas sin grupo
                                        if (textBox.Text != "-")
                                        {
                                            if (controlClass.Contains("date-picker"))
                                            {
                                                if (textBox.Text.Length < 11)
                                                {
                                                    textBox.Text = textBox.Text.Substring(0, 10) + " 00:00:00";
                                                }
                                            }
                                            else
                                            {
                                                if (textBox.Text.Length > 11)
                                                {
                                                    textBox.Text = textBox.Text.Substring(11);
                                                }
                                                textBox.Text = "01/01/2000 " + textBox.Text;
                                            }
                                        }
                                    }
                                }
                                if (accion == "PreLoad")
                                {
                                    textBox.Text = PreLoad(textBox.Text, controlClass);
                                }
                                else
                                {
                                    textBox.Text = PreRender(textBox.Text, controlClass);
                                }
                            }
                            break;

                        case "GridView":
                            GridView gridView = (GridView)control;
                            controlClass = gridView.Attributes["class"] == null ? "" : gridView.Attributes["class"];

                            // Si no se encuentra la propiedad Class, se intenta buscar como CssClass
                            if (string.IsNullOrEmpty(controlClass))
                            {
                                controlClass = gridView.CssClass;
                            }

                            // Si tiene valor y clase
                            if (controlClass.Contains("grid-view"))
                            {
                                int row = 0;
                                for (row = 0; row <= gridView.Rows.Count - 1; row++)
                                {
                                    int col = 0;

                                    for (col = 0; col <= gridView.Columns.Count - 1; col++)
                                    {
                                        if (gridView.Columns[col].GetType().ToString() == "System.Web.UI.WebControls.BoundField")
                                        {
                                            BoundField boundField = new BoundField();
                                            boundField = (BoundField)gridView.Columns[col];

                                            // Clase de la columna
                                            controlClass = boundField.ItemStyle.CssClass;

                                            // Si tiene valor y clase
                                            if (!gridView.Rows[row].Cells[col].Text.Trim().Equals("") & !controlClass.Equals(""))
                                            {
                                                if (accion == "PreLoad")
                                                {
                                                    gridView.Rows[row].Cells[col].Text = PreLoad(gridView.Rows[row].Cells[col].Text, controlClass);
                                                }
                                                else
                                                {
                                                    gridView.Rows[row].Cells[col].Text = PreRender(gridView.Rows[row].Cells[col].Text, controlClass);
                                                }
                                            }
                                        }

                                        if (gridView.Columns[col].GetType().ToString() == "System.Web.UI.WebControls.TemplateField")
                                        {
                                            TemplateField templateField = new TemplateField();
                                            templateField = (TemplateField)gridView.Columns[col];

                                            // Clase de la columna
                                            controlClass = templateField.ItemStyle.CssClass;

                                            // Si tiene valor y clase
                                            if (!gridView.Rows[row].Cells[col].Text.Trim().Equals("") & !controlClass.Equals(""))
                                            {
                                                if (accion == "PreLoad")
                                                {
                                                    gridView.Rows[row].Cells[col].Text = PreLoad(gridView.Rows[row].Cells[col].Text, controlClass);
                                                }
                                                else
                                                {
                                                    gridView.Rows[row].Cells[col].Text = PreRender(gridView.Rows[row].Cells[col].Text, controlClass);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }

                // Si el control tiene controles hijos se formatean
                if (control.HasControls())
                {
                    SetRegionalSettings(accion, control.Controls);
                }
            }
        }

        private string PreLoad(string ctrlValue, string controlClass)
        {
            if (controlClass.Contains("numeric-integer") ||
                controlClass.Contains("numeric-integer-positive") ||
                controlClass.Contains("numeric-integer-negative") ||
                controlClass.Contains("numeric-decimal") ||
                controlClass.Contains("numeric-decimal-positive") ||
                controlClass.Contains("numeric-decimal-positive"))
            {
                if (!regionalSettings.NumericGroupSeparator.Equals(""))
                {
                    ctrlValue = ctrlValue.Replace(regionalSettings.NumericGroupSeparator, "");
                }
                ctrlValue = ctrlValue.Replace(regionalSettings.NumericDecimalSymbol, ",");
                ctrlValue = ctrlValue.Trim();
            }

            if (controlClass.Contains("numeric-money") ||
                controlClass.Contains("numeric-money-positive") ||
                controlClass.Contains("numeric-money-negative"))
            {
                if (!regionalSettings.MoneyGroupSeparator.Equals(""))
                {
                    ctrlValue = ctrlValue.Replace(regionalSettings.MoneyGroupSeparator, "");
                }
                ctrlValue = ctrlValue.Replace(regionalSettings.MoneySymbol, "");
                ctrlValue = ctrlValue.Replace(regionalSettings.MoneyDecimalSymbol, ",").Trim();
                ctrlValue = ctrlValue.Trim();
            }

            if (controlClass.Contains("date-picker") ||
                controlClass.Contains("time-picker") ||
                controlClass.Contains("date-time"))
            {
                if (!string.IsNullOrEmpty(ctrlValue) & ctrlValue != "&nbsp;" & ctrlValue != "-" & ctrlValue != "01/01/0001 0:00:00" & ctrlValue != "Label")
                {
                    DateTimeFormatInfo dateTimeFormat = new DateTimeFormatInfo();
                    string date = "";
                    if (controlClass.Contains("date-picker") || controlClass.Contains("date-time"))
                    {
                        date = ctrlValue.Substring(0, 10);
                        dateTimeFormat.ShortDatePattern = regionalSettings.DateFormat.Replace("mm", "MM");
                        date = DateTime.ParseExact(date, dateTimeFormat.ShortDatePattern, DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy");
                    }
                    string time = "";
                    if (controlClass.Contains("time-picker") || controlClass.Contains("date-time"))
                    {
                        if (ctrlValue.Length > 11)
                        {
                            time = ctrlValue.Substring(11);
                        }
                        else
                        {
                            time = ctrlValue;
                        }
                    }
                    if (controlClass.Contains("date-picker"))
                    {
                        ctrlValue = Convert.ToDateTime(date + " " + time).AddMinutes(serverOffSet - regionalSettings.TimeZoneOffSet).ToString("dd/MM/yyyy");
                    }
                    if (controlClass.Contains("time-picker"))
                    {
                        ctrlValue = Convert.ToDateTime(date + " " + time).AddMinutes(serverOffSet - regionalSettings.TimeZoneOffSet).ToString("HH:mm:ss");
                    }
                    if (controlClass.Contains("date-time"))
                    {
                        ctrlValue = Convert.ToDateTime(date + " " + time).AddMinutes(serverOffSet - regionalSettings.TimeZoneOffSet).ToString("dd/MM/yyyy HH:mm:ss");
                    }
                }
            }
            return ctrlValue;
        }

        private string PreRender(string ctrlValue, string controlClass)
        {
            if (controlClass.Contains("numeric-integer") ||
                controlClass.Contains("numeric-integer-positive") ||
                controlClass.Contains("numeric-integer-negative"))
            {
                if (IsNumeric(ctrlValue))
                {
                    string pattern = "### ### ### ##0";
                    if (regionalSettings.NumericLeadZeros)
                    {
                        pattern = pattern.Replace("### ### ### ##0", "000 000 000 000");
                    }
                    ctrlValue = double.Parse(ctrlValue).ToString(pattern).Trim();
                    ctrlValue = ctrlValue.Replace(" ", regionalSettings.NumericGroupSeparator);
                    ctrlValue = ctrlValue.Replace("*", regionalSettings.NumericDecimalSymbol);
                }
            }

            if (controlClass.Contains("numeric-decimal") ||
                controlClass.Contains("numeric-decimal-positive") ||
                controlClass.Contains("numeric-decimal-positive"))
            {
                if (IsNumeric(ctrlValue))
                {
                    string pattern = "### ### ### ##0.####";
                    if (regionalSettings.NumericLeadZeros)
                    {
                        pattern = pattern.Replace("### ### ### ##0.", "000 000 000 000.");
                    }
                    if (regionalSettings.NumericFillDecimalsWithZeros)
                    {
                        pattern = pattern.Replace(".####", ".0000");
                    }
                    pattern = pattern.Substring(0, pattern.Length - (4 - regionalSettings.NumericDecimalDigits));
                    ctrlValue = double.Parse(ctrlValue).ToString(pattern).Trim();
                    ctrlValue = ctrlValue.Replace(regionalSettings.NumericDecimalSymbol, "*");
                    ctrlValue = ctrlValue.Replace(" ", regionalSettings.NumericGroupSeparator);
                    ctrlValue = ctrlValue.Replace("*", regionalSettings.NumericDecimalSymbol);
                }
            }

            if (controlClass.Contains("numeric-money") ||
                controlClass.Contains("numeric-money-positive") ||
                controlClass.Contains("numeric-money-negative"))
            {
                if (IsNumeric(ctrlValue))
                {
                    string pattern = "### ### ### ##0.####";
                    if (regionalSettings.MoneyLeadZeros)
                    {
                        pattern = pattern.Replace("### ### ### ##0.", "000 000 000 000.");
                    }
                    if (regionalSettings.MoneyFillDecimalsWithZeros)
                    {
                        pattern = pattern.Replace(".####", ".0000");
                    }
                    pattern = pattern.Substring(0, pattern.Length - (4 - regionalSettings.MoneyDecimalDigits));
                    ctrlValue = double.Parse(ctrlValue).ToString(pattern).Trim();
                    ctrlValue = ctrlValue.Replace(regionalSettings.MoneyDecimalSymbol, "*");
                    ctrlValue = ctrlValue.Replace(" ", regionalSettings.MoneyGroupSeparator);
                    ctrlValue = ctrlValue.Replace("   ", "").Replace(",,,", "").Replace("...", "").Replace("  ", "").Replace(",,", "").Replace("..", "");
                    ctrlValue = ctrlValue.Replace("*", regionalSettings.MoneyDecimalSymbol);

                    if (regionalSettings.MoneySymbolPosition.Equals("p"))
                    {
                        // Si el importe es negativo, el signo - antes del simbolo de la moneda
                        if (ctrlValue.StartsWith("-"))
                        {
                            ctrlValue = "-" + regionalSettings.MoneySymbol + " " + ctrlValue.Substring(1);
                        }
                        else
                        {
                            ctrlValue = regionalSettings.MoneySymbol + " " + ctrlValue;
                        }
                    }
                    else
                    {
                        ctrlValue = ctrlValue + " " + regionalSettings.MoneySymbol;
                    }
                }
            }

            if (controlClass.Contains("date-picker") ||
                controlClass.Contains("time-picker") ||
                controlClass.Contains("date-time"))
            {
                if (!string.IsNullOrEmpty(ctrlValue) & ctrlValue != "&nbsp;" & ctrlValue != "-" & ctrlValue != "01/01/0001 0:00:00")
                {
                    DateTimeFormatInfo dateTimeFormat = new DateTimeFormatInfo();
                    string date = "";
                    string time = "";
                    ctrlValue = Convert.ToDateTime(ctrlValue).AddMinutes((serverOffSet - regionalSettings.TimeZoneOffSet) * -1).ToString();
                    string pattern = regionalSettings.DateFormat.Replace("mm", "MM");
                    date = Convert.ToDateTime(ctrlValue).ToString(pattern);
                    if (controlClass.Contains("date-picker"))
                    {
                        if (regionalSettings.TimeFormat == 12)
                        {
                            time = Convert.ToDateTime(ctrlValue).ToString("hh:mm tt", new CultureInfo("en-US"));
                        }
                        else
                        {
                            time = Convert.ToDateTime(ctrlValue).ToString("HH:mm", new CultureInfo("de-DE"));
                        }
                    }
                    else
                    {
                        if (regionalSettings.TimeFormat == 12)
                        {
                            time = Convert.ToDateTime(ctrlValue).ToString("hh:mm:ss tt", new CultureInfo("en-US"));
                        }
                        else
                        {
                            time = Convert.ToDateTime(ctrlValue).ToString("HH:mm:ss", new CultureInfo("de-DE"));
                        }
                    }
                    if (controlClass.Contains("date-picker"))
                    {
                        ctrlValue = date;
                    }
                    if (controlClass.Contains("time-picker"))
                    {
                        ctrlValue = time;
                    }
                    if (controlClass.Contains("date-time"))
                    {
                        ctrlValue = date + " " + time;
                    }
                }
                if (ctrlValue == "01/01/0001 0:00:00")
                {
                    ctrlValue = "";
                }
            }

            return ctrlValue;
        }

        private string GetFHGrupo(string accion, ControlCollection controlCollection, string fhGrupo, string controlId)
        {
            string returnValue = "";
            foreach (Control control in controlCollection)
            {
                string crtlId = control.ID;
                if (!string.IsNullOrEmpty(crtlId))
                {
                    string type = control.GetType().ToString();
                    type = type.Replace("System.Web.UI.WebControls.", "");
                    type = type.Replace("System.Web.UI.HtmlControls.", "");
                    if (type == "TextBox")
                    {
                        TextBox textBox = (TextBox)control;
                        string dtGroup = textBox.Attributes["dt-group"] == null ? "" : textBox.Attributes["dt-group"];
                        if (dtGroup.Equals(dtGroup) & !crtlId.Equals(controlId))
                        {
                            returnValue = textBox.Text.Trim();
                            break;
                        }
                    }
                }
                if (control.HasControls())
                {
                    SetRegionalSettings(accion, control.Controls);
                }
            }
            return returnValue;
        }

        private static bool IsNumeric(string value)
        {
            float output;
            return float.TryParse(value, out output);
        }
    }
}