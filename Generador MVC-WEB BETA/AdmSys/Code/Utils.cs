﻿using CGADM;

using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Code
{
    internal class Utils
    {
        // Inicia el generador Random publico
        private static Random random = new Random();

        /// <summary>
        /// Tipos de mensajes: info, success, warning, error.
        /// </summary>
        public enum MessagesTypes
        {
            info,
            success,
            warning,
            error
        }

        /// <summary>
        /// Recupera el valor de una key del archivo de configuracion.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Genera el HASH del Modelo.
        /// </summary>
        /// <param name="jsonModel"></param>
        /// <returns></returns>
        public static string ModelHash(string jsonModel)
        {
            return Encrypt(jsonModel.Replace(" ", ""));
        }

        #region Cookies

        /// <summary>
        /// Crea una Cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        /// <param name="value"></param>
        public static void CreateCookie(string cookieName, string value)
        {
            HttpCookie httpCookie = new HttpCookie(cookieName);

            // encripta el valor de la cookie
            value = Encrypt(value);

            // establece el valor de la cookie
            httpCookie.Value = value;

            // establece la fecha de expiracion de la cookie
            httpCookie.Expires = DateTime.Now.AddYears(50);

            // agrega la cookie
            HttpContext.Current.Response.Cookies.Add(httpCookie);
        }

        /// <summary>
        /// Lee el valor de una Cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string ReadCookie(string cookieName)
        {
            // recupera la cookie del request
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[cookieName];

            string value = null;
            if (httpCookie != null)
            {
                // lee el valor de la cookie
                value = httpCookie.Value;

                // desencript el valor de la cookie. si se produce un error de elimina la cookie y se redirige a la pagina Apps.aspx.
                try
                {
                    value = Decrypt(value);
                }
                catch
                {
                    DeleteCookie(cookieName);
                    value = null;
                }
            }

            return value;
        }

        /// <summary>
        /// Elimina una cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        public static void DeleteCookie(string cookieName)
        {
            // recupera la cookie del request
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (httpCookie != null)
            {
                // elimina la cookie
                HttpContext.Current.Response.Cookies.Remove(cookieName);

                // establece el valor de la cookie en null
                httpCookie.Value = null;

                // establece la fecha de expiracion de la cookie en una fecha pasada
                httpCookie.Expires = DateTime.Now.AddDays(-10);

                // establece las propiedades de la cookie
                HttpContext.Current.Response.SetCookie(httpCookie);
            }
        }

        #endregion Cookies

        /// <summary>
        /// Genera un número random para realizar las llamadas a funciones JjavaScript en la UI.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static string GetRandom(int minValue = 0, int maxValue = int.MaxValue)
        {
            return random.Next(minValue, maxValue).ToString();
        }

        /// <summary>
        /// Ejecuta una función JavaScript en el cliente.
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="parameters"></param>
        public static void ExecJsFn(string functionName, string parameters = "")
        {
            // JsFnId permite identificar de forma unívoca cada ejecución de JS.
            // Si se repite este valor para dos o mas ejecuciones, se ejecutará solo la primera.
            // Las llamadas a las funciones se encuentran agregadas cronológicamente al principio
            // de la ultima sección script del html.
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string JsFnId = GetRandom();
                ScriptManager.RegisterStartupScript(page, page.GetType(), "Utils.ExecJSFn." + JsFnId, functionName + "(" + parameters + ");\n", true);
            }
        }

        /// <summary>
        /// Muestra un mensaje en el UI.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="redirectTo"></param>
        public static void Message(string title, string message, MessagesTypes messageType, string redirectTo = "")
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string js = @"
    swal(
    {
        title: '{0}',
        text: '{1}',
        type: '{2}',
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '{3}',
        html: true 
    },
    function()
    {
        var redirectTo = '{4}';
        if (redirectTo != '') {
            window.location.href = redirectTo;
        }
    });
                            ";
                string buttonColor = "";
                switch (messageType)
                {
                    case MessagesTypes.info:
                        buttonColor = "#5bc0de";
                        break;

                    case MessagesTypes.success:
                        buttonColor = "#5cb85c";
                        break;

                    case MessagesTypes.warning:
                        buttonColor = "#f0ad4e";
                        break;

                    case MessagesTypes.error:
                        buttonColor = "#d9534f";
                        break;
                }
                message = message.Replace("\n", "<br>");
                js = js.Replace("{0}", title).Replace("{1}", message).Replace("{2}", messageType.ToString()).Replace("{3}", buttonColor).Replace("{4}", redirectTo);
                ScriptManager.RegisterStartupScript(page, page.GetType(), "Utils.Message", js, true);
            }
        }

        /// <summary>
        /// Obtiene la URL solicitada.
        /// </summary>
        /// <returns></returns>
        public static string GetRequestedURL()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
        }

        /// <summary>
        /// Obtiene la IP que está realizando el Request.
        /// </summary>
        /// <returns></returns>
        public static string GetRequestIPAddress()
        {
            HttpContext context = HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        /// <summary>
        /// Navega a la URL indicada y evita el error de thread.
        /// </summary>
        /// <param name="url"></param>
        public static void NavigateTo(string url)
        {
            HttpContext.Current.Response.Redirect(url, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Imprime la página indicada en el parámetro url.
        /// </summary>
        /// <param name="url"></param>
        public static void Print(string url)
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string js = @"
function Print() {
    var printWindow = window.open('{0}');
    printWindow.addEventListener('load', function () {
        printWindow.print();
        printWindow.close();
    }, true);
}
setTimeout(Function('Print();'), 100);
";
                js = js.Replace("{0}", url);
                page.ClientScript.RegisterStartupScript(page.GetType(), "Print", js, true);
            }
        }

        #region Criptografía

        /// <summary>
        /// Encripta un string utilizando AES. Devuelve el resultado en Base64.
        /// </summary>
        /// <param name="decryptedText"></param>
        /// <returns></returns>
        public static string Encrypt(string decryptedText)
        {
            var decryptedBytes = Encoding.UTF8.GetBytes(decryptedText);
            return Convert.ToBase64String(Encrypt(decryptedBytes, GetRijndaelManaged(GetAppSetting("CryptoKey")))).Replace("+", "-").Replace("/", "_");
        }

        /// <summary>
        /// Desencripta un string en Base64 utilizando AES. Devuelve el resultado en texto plano.
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <returns></returns>
        public static string Decrypt(string encryptedText)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText.Replace("-", "+").Replace("_", "/"));
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(GetAppSetting("CryptoKey")))).Replace('\0', ' ').TrimEnd();
        }

        /// <summary>
        /// Implementa Encrypt de la clase RijndaelManaged.
        /// </summary>
        /// <param name="decryptedBytes"></param>
        /// <param name="rijndaelManaged"></param>
        /// <returns></returns>
        private static byte[] Encrypt(byte[] decryptedBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor().TransformFinalBlock(decryptedBytes, 0, decryptedBytes.Length);
        }

        /// <summary>
        /// Implementa Decrypt de la clase RijndaelManaged.
        /// </summary>
        /// <param name="encryptedData"></param>
        /// <param name="rijndaelManaged"></param>
        /// <returns></returns>
        private static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        private static RijndaelManaged GetRijndaelManaged(string secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        #endregion Criptografía

        #region Direccion

        public static Nullable<long> CGAdmDirectionAdd(long? direccionId)
        {
            Page page = HttpContext.Current.Handler as Page;
            MasterPage masterPage = page.Master;

            if (string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionFormateada")).Text) || string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionLatitud")).Text) || string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionLongitud")).Text)) { return direccionId; }

            var direccion = new CGADM.Direccion();
            if (direccionId > 0)
            {
                direccion = new CGADM.Direccion();
                direccion.Select(direccionId.Value);
            }

            direccion.Ingresada = ((TextBox)masterPage.FindControl("txtDireccionIngresada")).Text;
            direccion.Notas = ((TextBox)masterPage.FindControl("txtDireccionAclaraciones")).Text;
            direccion.Latitud = ((TextBox)masterPage.FindControl("txtDireccionLatitud")).Text.ToDouble();
            direccion.Longitud = ((TextBox)masterPage.FindControl("txtDireccionLongitud")).Text.ToDouble();
            direccion.IdLugar = ((TextBox)masterPage.FindControl("txtDireccionPlaceId")).Text;
            direccion.DireccionFormateada = ((TextBox)masterPage.FindControl("txtDireccionFormateada")).Text;
            direccion.URL = ((TextBox)masterPage.FindControl("txtDireccionURL")).Text;
            var mapaParams = ((TextBox)masterPage.FindControl("txtDireccionMapa")).Text;
            direccion.Mapa = GetStaticMap(Utils.GetAppSetting("StaticMapsUrlBaseURL") + mapaParams);

            if (direccionId > 0)
            {
                direccion.Update();
            }
            else
            {
                direccion.Insert();
            }
            return direccion.IdDireccion;
        }

        public static Nullable<long> CGAccessDirectionAdd(long? direccionId)
        {
            Page page = HttpContext.Current.Handler as Page;
            MasterPage masterPage = page.Master;

            if (string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionFormateada")).Text) || string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionLatitud")).Text) || string.IsNullOrEmpty(((TextBox)masterPage.FindControl("txtDireccionLongitud")).Text)) { return direccionId; }

            var direccion = new CGAccess.Direccion();
            if (direccionId > 0)
            {
                direccion = new CGAccess.Direccion();
                direccion.Select(direccionId.Value);
            }

            direccion.Ingresada = ((TextBox)masterPage.FindControl("txtDireccionIngresada")).Text;
            direccion.Notas = ((TextBox)masterPage.FindControl("txtDireccionAclaraciones")).Text;
            direccion.Latitud = ((TextBox)masterPage.FindControl("txtDireccionLatitud")).Text.ToDouble();
            direccion.Longitud = ((TextBox)masterPage.FindControl("txtDireccionLongitud")).Text.ToDouble();
            direccion.IdLugar = ((TextBox)masterPage.FindControl("txtDireccionPlaceId")).Text;
            direccion.DireccionFormateada = ((TextBox)masterPage.FindControl("txtDireccionFormateada")).Text;
            direccion.URL = ((TextBox)masterPage.FindControl("txtDireccionURL")).Text;
            var mapaParams = ((TextBox)masterPage.FindControl("txtDireccionMapa")).Text;
            direccion.Mapa = GetStaticMap(Utils.GetAppSetting("StaticMapsUrlBaseURL") + mapaParams);

            if (direccionId > 0)
            {
                direccion.Update();
            }
            else
            {
                direccion.Insert();
            }
            return direccion.IdDireccion;
        }

        private static byte[] GetStaticMap(string urlImage)
        {
            string html = string.Empty;
            Bitmap buddyIcon;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlImage);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    buddyIcon = new Bitmap(stream);
                }
            }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(buddyIcon, typeof(byte[]));
        }

        public static Nullable<long> ArchivoAdd(long? archivoId, FileUpload archivo)
        {
            if (!archivo.HasFile) return archivoId;

            var archivoNuevo = new Archivo();
            if (archivoId > 0)
            {
                archivoNuevo = new Archivo();
                archivoNuevo.Select(archivoId.Value);
            }

            MemoryStream target = new MemoryStream();
            archivo.FileContent.CopyTo(target);
            archivoNuevo.ArchivoContenido = target.ToArray();
            archivoNuevo.FHAlta = DateTime.Now;
            archivoNuevo.TipoArchivo = System.IO.Path.GetExtension(archivo.FileName);

            if (archivoId > 0)
            {
                archivoNuevo.Update();
            }
            else
            {
                archivoNuevo.Insert();
            }
            return archivoNuevo.IdArchivo;
        }

        #endregion Direccion

        #region Configuración Regional

        public static void SetDefaultRegionalConfiguration(CGUsuarios.Common.Usuario usuario)
        {
            //TODO: parametrizar
            var defaultRegionalConfigurationId = 1;
            var regConf = CGUsuarios.Common.Managers.GenericManager.Get<CGUsuarios.Common.ConfiguracionRegional>("ConfiguracionRegional?id=" + defaultRegionalConfigurationId);

            usuario.FormatoFecha = regConf.FormatoFecha;
            usuario.FormatoHora = regConf.FormatoHora;
            usuario.IdZonaHoraria = regConf.IdZonaHoraria;
            usuario.MonedaCerosIzquierda = regConf.MonedaCerosIzquierda;
            usuario.MonedaCompletarDecimalesCeros = regConf.MonedaCompletarDecimalesCeros;
            usuario.MonedaDigitosDecimales = regConf.MonedaDigitosDecimales;
            usuario.MonedaFormato = regConf.MonedaFormato;
            usuario.MonedaSeparacionMiles = regConf.MonedaSeparacionMiles;
            usuario.MonedaSimbolo = regConf.MonedaSimbolo;
            usuario.MonedaSimboloDecimal = regConf.MonedaSimboloDecimal;
            usuario.NumeroCerosIzquierda = regConf.NumeroCerosIzquierda;
            usuario.NumeroCompletarDecimalesCeros = regConf.NumeroCompletarDecimalesCeros;
            usuario.NumeroDigitosDecimales = regConf.NumeroDigitosDecimales;
            usuario.NumeroSeparacionMiles = regConf.NumeroSeparacionMiles;
            usuario.NumeroSimboloDecimal = regConf.NumeroSimboloDecimal;
            usuario.PrimerDiaSemana = regConf.PrimerDiaSemana;
            usuario.SistemaMedida = regConf.SistemaMedida;
        }

        #endregion Configuración Regional

        #region Descargar Archivos

        public static void DescargarArchivo(System.Web.UI.Page page, string contentType, byte[] archivoContenido, string fileName)
        {
            page.Response.Clear();
            page.Response.Buffer = true;
            page.Response.Charset = "";
            page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            page.Response.ContentType = contentType;
            page.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename= {0}.{1}", fileName, contentType));
            page.Response.BinaryWrite(archivoContenido);
            page.Response.Flush();
            page.Response.End();
        }

        #endregion Descargar Archivos

        /// <summary>
        /// Devuelve el IdCuenta del sistema de usuarios leyendola de la cookie de session
        /// </summary>
        /// <returns></returns>
        public static int GetIdCuentaFromCookie()
        {
            try
            {
                dynamic login = Utils.ReadCookie("SessionSysAdm").ToDynamic();
                return login.IdCuenta;
            }
            catch
            {
                return -1;
            }
        }

        public static void GeneratePdf(string url, string pdf)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "PDFs\\";

            if (!pdf.EndsWith(".pdf"))
            {
                pdf = pdf + ".pdf";
            }

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo();

            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            processStartInfo.FileName = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
            processStartInfo.Arguments = url + " " + "\"" + path + pdf + "\"";
            process.StartInfo = processStartInfo;
            process.Start();
            process.WaitForExit();
        }
    }
}