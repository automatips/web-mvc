﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdmSys.Controls
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        public string Saludo
        {

            get { return lblSaludo.Text; }
            set { Saludo = value; }

        }

        public event EventHandler btnSaveClick;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.btnSaveClick(this, e);
        }
    }
}