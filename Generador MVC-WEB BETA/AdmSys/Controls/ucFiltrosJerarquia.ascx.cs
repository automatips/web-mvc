﻿using Code;
using MagicSQL;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace AdmSys.Controls
{
    public partial class ucFiltrosJerarquia : System.Web.UI.UserControl
    {
        public int IdUnidadNegocio
        {
            get
            {
                if (ddlUnidadNegocio.SelectedValue.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return ddlUnidadNegocio.SelectedValue.ToIntID();
                }
            }
            set { }
        }

        public int idDistribuidor
        {
            get
            {
                if (ddlDistribuidor.SelectedValue.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return ddlDistribuidor.SelectedValue.ToIntID();
                }
            }
            set { }
        }

        public int IdReseller
        {
            get
            {
                if (ddlReseller.SelectedValue.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return ddlReseller.SelectedValue.ToIntID();
                }
            }
            set { }
        }

        public int IdContratista
        {
            get
            {
                if (ddlContratista.SelectedValue.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return ddlContratista.SelectedValue.ToIntID();
                }
            }
            set { }
        }

        public int IdConsumidor
        {
            get
            {
                if (hdnIdConsumidor.Value.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return hdnIdConsumidor.Value.ToIntID();
                }
            }
            set { }
        }

        public int IdAgente
        {
            get
            {
                if (ddlAgente.SelectedValue.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return ddlAgente.SelectedValue.ToIntID();
                }
            }
            set { }
        }

        public int IdLlave
        {
            get
            {
                if (hdnIdLlave.Value.Equals(""))
                {
                    return -1;
                }
                else
                {
                    return hdnIdLlave.Value.ToIntID();
                }
            }
            set { }
        }

        public event EventHandler IdUnidadNegocio_Changed;

        public event EventHandler IdDistribuidor_Changed;

        public event EventHandler IdReseller_Changed;

        public event EventHandler IdContratista_Changed;

        public event EventHandler IdConsumidor_Changed;

        public event EventHandler IdAgente_Changed;

        public event EventHandler IdLlave_Changed;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Carga las Unidades de Negocio
                ddlUnidadNegocio.Items.Add(new ListItem("Seleccione la Unidad de Negocio...", (-1).ToCryptoID()));
                foreach (CGADM.UnidadNegocio unidadNegocio in new CGADM.UnidadNegocio().Select().Where(m => m.FHBaja == null))
                {
                    ddlUnidadNegocio.Items.Add(new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID()));
                }
                ddlUnidadNegocio_OnSelectedIndexChanged(sender, e);
            }
        }

        protected void ddlUnidadNegocio_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlUnidadNegocio.SelectedValue.ToIntID())
            {
                case 0: // Control Global
                    pnlDistribuidor.Visible = true;
                    pnlReseller.Visible = true;
                    pnlContratista.Visible = true;
                    pnlConsumidor.Visible = true;
                    pnlAgente.Visible = true;
                    pnlLlave.Visible = true;

                    break;

                case 1: // Vending Control
                    pnlDistribuidor.Visible = true;
                    pnlReseller.Visible = true;
                    pnlContratista.Visible = true;
                    pnlConsumidor.Visible = true;
                    pnlAgente.Visible = false;
                    pnlLlave.Visible = false;

                    break;

                case 2: // Access Control
                    pnlDistribuidor.Visible = true;
                    pnlReseller.Visible = true;
                    pnlContratista.Visible = true;
                    pnlConsumidor.Visible = false;
                    pnlAgente.Visible = true;
                    pnlLlave.Visible = true;

                    break;

                default: // Otras Unidades de Negocio
                    pnlDistribuidor.Visible = false;
                    pnlReseller.Visible = false;
                    pnlContratista.Visible = false;
                    pnlConsumidor.Visible = false;
                    pnlAgente.Visible = false;
                    pnlLlave.Visible = false;

                    break;
            }

            ddlDistribuidor.Items.Clear();
            ddlReseller.Items.Clear();
            ddlContratista.Items.Clear();
            ddlAgente.Items.Clear();

            pnlConsumidor.Enabled = false;
            hdnIdConsumidor.Value = "";
            pnlLlave.Enabled = false;
            hdnIdLlave.Value = "";

            if (ddlUnidadNegocio.SelectedValue.ToIntID() != -1)
            {
                DataTable dt = new SP("CGADM").Execute("usp_GetDistribuidorByUnidadNegocio",
                                                        P.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID()));

                ddlDistribuidor.Items.Add(new ListItem("Seleccione el Distribuidor...", (-1).ToCryptoID()));
                foreach (DataRow row in dt.Rows)
                {
                    ddlDistribuidor.Items.Add(new ListItem(row["Distribuidor"].ToString(), row["IdDistribuidor"].ToString().ToInt().ToCryptoID()));
                }
            }

            IdUnidadNegocio_Changed?.Invoke(this, e);
        }

        protected void ddlDistribuidor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlReseller.Items.Clear();
            ddlContratista.Items.Clear();
            ddlAgente.Items.Clear();

            pnlConsumidor.Enabled = false;
            hdnIdConsumidor.Value = "";
            pnlLlave.Enabled = false;
            hdnIdLlave.Value = "";

            if (ddlUnidadNegocio.SelectedValue.ToIntID() != -1)
            {
                DataTable dt = new SP("CGADM").Execute("usp_GetResellersByDistDistUnidadNegocio",
                                        P.Add("idDistribuidor", ddlDistribuidor.SelectedValue.ToIntID()),
                                        P.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID()));

                ddlReseller.Items.Add(new ListItem("Seleccione el Reseller...", (-1).ToCryptoID()));
                foreach (DataRow row in dt.Rows)
                {
                    ddlReseller.Items.Add(new ListItem(row["RazonSocial"].ToString(), row["IdReseller"].ToString().ToInt().ToCryptoID()));
                }
            }

            IdDistribuidor_Changed?.Invoke(this, e);
        }

        protected void ddlReseller_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlContratista.Items.Clear();
            ddlAgente.Items.Clear();

            pnlConsumidor.Enabled = false;
            hdnIdConsumidor.Value = "";
            pnlLlave.Enabled = false;
            hdnIdLlave.Value = "";

            if (ddlReseller.SelectedValue.ToIntID() != -1)
            {
                DataTable dt = new SP("CGADM").Execute("usp_GetContratistasxResellerxDistribuidorxUnidadNegocio",
                                        P.Add("idUnidadNegocio", ddlUnidadNegocio.SelectedValue.ToIntID()),
                                        P.Add("IdDistribuidor", ddlDistribuidor.SelectedValue.ToIntID()),
                                        P.Add("idReseller", ddlReseller.SelectedValue.ToIntID()));

                ddlContratista.Items.Add(new ListItem("Seleccione el Contratista...", (-1).ToCryptoID()));
                foreach (DataRow row in dt.Rows)
                {
                    ddlContratista.Items.Add(new ListItem(row[0].ToString(), row[1].ToString().ToInt().ToCryptoID()));
                }
            }

            IdReseller_Changed?.Invoke(this, e);
        }

        protected void ddlContratista_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlAgente.Items.Clear();

            pnlConsumidor.Enabled = false;
            hdnIdConsumidor.Value = "";
            pnlLlave.Enabled = false;
            hdnIdLlave.Value = "";

            switch (ddlUnidadNegocio.SelectedValue.ToIntID())
            {
                case 0: // Control Global
                    break;

                case 1: // Vending

                    // Cargar Consumidores

                    if (ddlContratista.SelectedValue.ToIntID() != -1)
                    {
                        pnlConsumidor.Enabled = true;
                        Utils.ExecJsFn("SelectAjaxConsumidor");
                    }
                    else
                    {
                        pnlConsumidor.Enabled = false;
                        hdnIdConsumidor.Value = "";
                    }

                    break;

                case 2: // Access

                    // Cargar Agentes

                    if (ddlContratista.SelectedValue.ToIntID() != -1)
                    {
                        DataTable dt = new SP("CGADM").Execute("usp_GetAgentexContratistaxResellerxDistribuidor",
                                                P.Add("IdDistribuidor", ddlDistribuidor.SelectedValue.ToIntID()),
                                                P.Add("idReseller", ddlReseller.SelectedValue.ToIntID()),
                                                P.Add("idContratista", ddlContratista.SelectedValue.ToIntID()));

                        ddlAgente.Items.Add(new ListItem("Seleccione el Agente...", (-1).ToCryptoID()));
                        foreach (DataRow row in dt.Rows)
                        {
                            ddlAgente.Items.Add(new ListItem(row["RazonSocial"].ToString(), row["IdAgente"].ToString().ToInt().ToCryptoID()));
                        }
                    }
                    break;
            }

            IdContratista_Changed?.Invoke(this, e);
        }

        protected void ddlAgente_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAgente.SelectedValue.ToIntID() != -1)
            {
                pnlLlave.Enabled = true;
                Utils.ExecJsFn("SelectAjaxLlave");
            }
            else
            {
                pnlLlave.Enabled = false;
                hdnIdLlave.Value = "";
            }

            IdAgente_Changed?.Invoke(this, e);
        }
    }
}