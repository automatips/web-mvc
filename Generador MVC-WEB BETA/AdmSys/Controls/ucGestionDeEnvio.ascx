﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGestionDeEnvio.ascx.cs" Inherits="AdmSys.Controls.ucGestionDeEnvio" %>

<asp:UpdatePanel ID="upGestionEnvio" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        
        <%-- Selección de tipo de envío --%>
        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label>
                <asp:Label Text="Configuración de envío" runat="server" />
            </label>
            <asp:DropDownList 
                ID="ddlEnvioTipo" 
                CssClass="form-control select-single" 
                AutoPostBack="true" 
                OnSelectedIndexChanged="ddlEnvioTipo_OnSelectedIndexChanged" 
                runat="server" />
        </div>

        <asp:UpdatePanel ID="upEnvioExterno" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" Visible="false">
		    <ContentTemplate>

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" runat="server" id="mnuDatosDeTransporte" href="#menuDatosDeTransporte">Datos de Transporte</a></li>
                    <li><a data-toggle="tab" runat="server" id="mnuDatosDeEnvio" href="#menuDatosDeEnvio">Datos de Envío</a></li>
                    <li><a data-toggle="tab" runat="server" id="mnuDatosDeRemitoExterno" href="#menuDatosDeRemitoExterno">Remito externo</a></li>
                </ul>

                <div class="tab-content">
                    <div id="menuDatosDeTransporte" class="tab-pane fade in active">
                        <%-- Datos de transporte --%>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Proveedor" runat="server" />
                            </label>
                            <asp:TextBox ID="txtTransporteProveedor" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese el nombre del proveedor. Ej: OCA, DHL, CORREO ARGENTINO, etc"
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Sucursal" runat="server" />
                            </label>
                            <asp:TextBox ID="txtTransporteSucursal" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese la sucursal del proveedor de envío..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Domicilio de sucursal" runat="server" />
                            </label>
                            <asp:TextBox ID="txtTransporteDomicilio" 
                                CssClass="form-control"
                                PlaceHolder="Ej: Avenida Don Bosco 5000, Cordoba Capital, Argentina"
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Teléfono" runat="server" />
                            </label>
                            <asp:TextBox ID="txtTransporteTelefono" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese el número telefónico del proveedor de envío..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Email" runat="server" />
                            </label>
                            <asp:TextBox ID="txtTransporteEmail" 
                                CssClass="form-control"
                                PlaceHolder="ingresecorreoelectronico@controlglobal.com"
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Datos de transporte - CAMBIAR" runat="server" />
                            </label>
                            <asp:Button ID="btnCambiarDatosDeTransporte" 
                                Text="Cambiar proveedor de transporte"
                                CssClass="btn btn-info col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                runat="server" 
                                Enabled="false"
                                OnClick="btnCambiarDatosDeTransporte_click" />
                        </div>
            
                        <%-- Grilla de transportes --%>
                        <asp:GridView ID="gvTransportes" CssClass="table table-bordered table-hover col-lg-12 col-md-12 col-sm-12 col-xs-12 grid-view"
                            Width="100%" AutoGenerateColumns="False" 
                            runat="server" 
                            EmptyDataText="Aún no existen transportes asociados a los filtros ingresados." 
                            AutoPostBack="true"
                            OnSelectedIndexChanging="gvTransportes_SelectedIndexChanging"
                            OnRowDeleting="gvTransportes_RowDeleting"
                            >
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="IdTransporte" />
                                <asp:BoundField HeaderText="Proveedor" DataField="Proveedor" />
                                <asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
                                <asp:BoundField HeaderText="Domicilio" DataField="Domicilio" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Teléfono" DataField="Telefono" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Email" DataField="Email" ItemStyle-Width="125"/>
                                <asp:TemplateField HeaderText="Seleccionar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="btnSeleccionarTransporte"
                                                runat="server"
                                                CssClass="btn btn-info"
                                                CommandName="Select"
                                                Style="margin-bottom: 5px;"
                                                ItemStyle-Width="50"
                                                title="Seleccionar transporte">
                                                <span aria-hidden="true" class="glyphicon glyphicon-send"></span>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="LinkButton1"
                                                runat="server"
                                                CssClass="btn btn-danger"
                                                CommandName="Delete"
                                                Style="margin-bottom: 5px;"
                                                ItemStyle-Width="50"
                                                title="Eliminar transporte">
                                                <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                            </asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                
                    <div id="menuDatosDeEnvio" class="tab-pane fade">
                        <%-- Datos de envío --%>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="NÚMERO DE ENVÍO" runat="server" />
                            </label>
                            <asp:TextBox ID="txtEnvioNumero" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese el número de envío..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="FECHA/HORA DE ENVÍO" runat="server" />
                            </label>
                            <asp:TextBox ID="txtEnvioFecha" 
                                CssClass="form-control date-picker"
                                PlaceHolder="DD/MM/AAAA HH:MM"
                                runat="server" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Importe del envío" runat="server" />
                            </label>
                            <asp:TextBox ID="txtEnvioImporte" 
                                CssClass="form-control numeric-money"
                                PlaceHolder="Ej: $150"
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <asp:Label ID="Label1" Text="Importar comprobante de envío" runat="server" />
                            </label>
                            <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>
                            <asp:FileUpload ID="fileUploadComprobanteEnvio" runat="server" CssClass="form-control file" Enabled="true" data-show-preview="true" />
                        </div>
                    </div>

                    <div id="menuDatosDeRemitoExterno" class="tab-pane fade">
                        <%-- Datos del remito externo --%>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Remito de ingreso - Numero" runat="server" />
                            </label>
                            <asp:TextBox ID="txtRemitoNumero" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese el número de remito..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Remito de ingreso - Fecha" runat="server" />
                            </label>
                            <asp:TextBox ID="txtRemitoFecha" 
                                CssClass="form-control date-picker"
                                PlaceHolder="DD/MM/AAAA HH:MM"
                                runat="server"/>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label>
                                <asp:Label Text="Remito de ingreso - Responsable" runat="server" />
                            </label>
                            <asp:TextBox ID="txtRemitoResponsable" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese el responsable de remito..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>
                                <asp:Label Text="Remito de ingreso - Observaciones" runat="server" />
                            </label>
                            <asp:TextBox ID="txtRemitoObservaciones" 
                                CssClass="form-control"
                                PlaceHolder="Ingrese las observaciones del remito..."
                                runat="server" 
                                TextMode="SingleLine" />
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>
                                <asp:Label ID="lblImportarComprobante" Text="Importar remito scaneado" runat="server" />
                            </label>
                            <asp:Label ID="lblEncrypId" runat="server" Visible="false"></asp:Label>
                            <asp:FileUpload ID="fileUploadRemitoExternoComprobante" runat="server" CssClass="form-control file" Enabled="true" data-show-preview="true" />
                        </div>
                    </div>
                </div>

		    </ContentTemplate>
	    </asp:UpdatePanel>

    </ContentTemplate>
</asp:UpdatePanel>