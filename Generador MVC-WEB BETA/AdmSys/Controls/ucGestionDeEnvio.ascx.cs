﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using MagicSQL;
using CGADM;
using static Code.Utils;

namespace AdmSys.Controls
{
    public partial class ucGestionDeEnvio : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refrescarTransportes();
                ddlEnvioTipo.Items.Add("Sin configuración de envío");
                ddlEnvioTipo.Items.Add("Transporte externo");

                ddlEnvioTipo.SelectedValue = "Sin configuración de envío";
            }
        }

        public ErrorHandler envioGuardar
            (
            int idSDT,
            int idSDS,
            Tn transaccionEnvioGuardar
            )
        {
            ErrorHandler eh = new ErrorHandler();
            bool transaccionUpdate = false;

            //Buscar si existe el envío por la solicitud de taller o servicio
            Envio envio = new Envio().
                Select().Where
                (
                    env => 
                    env.IdSDT == idSDT || env.IdSDS == idSDS
                ).FirstOrDefault();

            if (envio != null)
            {
                transaccionUpdate = true;
            }
            else
            {
                envio = new Envio();
            }
            envio.IdSDS = idSDS;
            envio.IdSDT = idSDT;
            envio.Tipo = ddlEnvioTipo.SelectedValue;
            try
            {
                if (envio.Tipo == "Transporte externo")
                {
                    if (validarEnvioTransporteExterno().Mensaje == "OK")
                    {
                        Transporte transporte = new Transporte().
                            Select().Where(tra =>
                            tra.Proveedor == txtTransporteProveedor.Text &&
                            tra.Sucursal == txtTransporteSucursal.Text &&
                            tra.IdDistribuidor == Convert.ToInt32(Session["idDistribuidor"])
                            ).FirstOrDefault();

                        if (transporte != null)
                        {
                            //Si el transporte ya existe, obtenerlo e inyectarlo al envío
                            envio.IdTransporte = transporte.IdTransporte;
                        }
                        else
                        {
                            //Si el transporte aun no existe, crearlo
                            transporte = new Transporte();
                            transporte.Proveedor = txtTransporteProveedor.Text;
                            transporte.Sucursal = txtTransporteSucursal.Text;
                            transporte.Sucursal = txtTransporteSucursal.Text;
                            transporte.Domicilio = txtTransporteDomicilio.Text;
                            transporte.Email = txtTransporteEmail.Text;
                            transporte.Telefono = txtTransporteEmail.Text;
                            transporte.IdDistribuidor = Convert.ToInt32(Session["idDistribuidor"].ToString());
                            transporte.FHAlta = DateTime.Now;

                            transporte.Insert(transaccionEnvioGuardar);
                        }

                        RemitoExterno remitoExterno =
                            new RemitoExterno().Select().Where
                            (
                                rem =>
                                   rem.Numero == txtRemitoNumero.Text
                                && rem.Responsable == txtRemitoResponsable.Text
                            ).FirstOrDefault();

                        if (remitoExterno != null)
                        {
                            //Si el remito existe, consultar envìo
                            envio.IdRemitoExterno = remitoExterno.IdRemitoExterno;
                        }
                        else
                        {
                            //Sino, crearlo. Si es que se ha indicado un número de remito externo
                            if (txtRemitoNumero.Text != "")
                            {
                                remitoExterno = new RemitoExterno();
                                remitoExterno.Numero = txtRemitoNumero.Text;

                                try
                                {
                                    remitoExterno.Fecha = txtRemitoFecha.Text;
                                }
                                catch
                                {
                                    remitoExterno.Fecha = null;
                                }

                                remitoExterno.Responsable = txtRemitoResponsable.Text;
                                remitoExterno.Observaciones = txtRemitoObservaciones.Text;
                                remitoExterno.IdArchivo = ArchivoAdd(remitoExterno.IdArchivo, fileUploadRemitoExternoComprobante);
                                remitoExterno.FHAlta = DateTime.Now;

                                remitoExterno.Insert(transaccionEnvioGuardar);
                            }
                        }

                        envio.IdArchivo = ArchivoAdd(envio.IdArchivo,fileUploadComprobanteEnvio);

                        if (!transaccionUpdate)
                        {
                            if (envio.IdSDS == 0)
                            {
                                envio.IdSDS = null;
                            }

                            if (envio.IdSDT == 0)
                            {
                                envio.IdSDT = null;
                            }

                            envio.FHAlta = DateTime.Now;

                            //2019/01/31: No es más obligatorio el remito externo
                            if (remitoExterno != null)
                            {
                                envio.IdRemitoExterno = remitoExterno.IdRemitoExterno;
                            }
                            else
                            {
                                envio.IdRemitoExterno = null;
                            }
                            
                            envio.NumeroEnvio = txtEnvioNumero.Text;
                            envio.Importe = Convert.ToDecimal(txtEnvioImporte.Text);

                            if (envio.Importe > 0)
                            {
                                try
                                {
                                    SaldoCuentaCorriente saldo = null;
                                    saldo =
                                        new SaldoCuentaCorriente().
                                            Select().Where(sal =>
                                                sal.IdDistribuidor == Convert.ToInt32(Session["idDistribuidor"]) &&
                                                sal.IdUnidadNegocio == Convert.ToInt32(Session["idUnidadNegocio"])
                                                ).FirstOrDefault();
                                    if (saldo != null)
                                    {
                                        saldo.Saldo = (saldo.Saldo - Convert.ToDecimal(envio.Importe));
                                        saldo.Update(transaccionEnvioGuardar);
                                    }
                                    else
                                    {
                                        eh.Mensaje = "No se pudo imputar la reparación en la cuenta corriente";
                                        eh.Mensaje = "No existe una cuenta corriente asociada al distribuidor seleccionado";
                                        return eh;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    eh.Mensaje = "Error al imputar el monto en cuenta corriente";
                                    eh.Descripcion = ex.Message;
                                    return eh;
                                }
                            }

                            envio.IdCuenta = GetIdCuentaFromCookie();
                            envio.FHEnvio = txtEnvioFecha.Text;
                            
                            envio.Insert(transaccionEnvioGuardar);
                        }
                        else
                        {
                            //2019/01/31: No es obligatorio el remito exrterno
                            if (remitoExterno != null)
                            {
                                envio.IdRemitoExterno = remitoExterno.IdRemitoExterno;
                            }
                            else

                            {
                                envio.IdRemitoExterno = null;
                            }
                            
                            envio.NumeroEnvio = txtEnvioNumero.Text;
                            envio.Importe = Convert.ToDecimal(txtEnvioImporte.Text);

                            if (envio.Importe > 0)
                            {
                                try
                                {
                                    SaldoCuentaCorriente saldo = null;
                                    saldo =
                                        new SaldoCuentaCorriente().
                                            Select().Where(sal =>
                                                sal.IdDistribuidor == Convert.ToInt32(Session["idDistribuidor"]) &&
                                                sal.IdUnidadNegocio == Convert.ToInt32(Session["idUnidadNegocio"])
                                                ).FirstOrDefault();
                                    if (saldo != null)
                                    {
                                        saldo.Saldo = (saldo.Saldo - Convert.ToDecimal(envio.Importe));
                                        saldo.Update(transaccionEnvioGuardar);
                                    }
                                    else
                                    {
                                        eh.Mensaje = "No se pudo imputar la reparación en la cuenta corriente";
                                        eh.Mensaje = "No existe una cuenta corriente asociada al distribuidor seleccionado";
                                        return eh;
                                    }
                                }
                                catch(Exception ex)
                                {
                                    eh.Mensaje = "Error al imputar el monto en cuenta corriente";
                                    eh.Descripcion = ex.Message;
                                    return eh;
                                }
                            }

                            envio.IdCuenta = GetIdCuentaFromCookie();
                            envio.FHEnvio = txtEnvioFecha.Text;
                            envio.Update(transaccionEnvioGuardar);
                        }

                        eh.Mensaje = "EXITO";
                        return eh;
                    }
                    else
                    {
                        return validarEnvioTransporteExterno();
                    }
                }

                if (envio.Tipo == "Sin configuración de envío")
                {
                    eh.Mensaje = "EXITO";
                }
            }
            catch(Exception ex)
            {
                eh.Mensaje = "Error: Los datos de envío son requeridos. Detalle: "+ex.Message;
                return eh;
            }
            return eh;            
        }

        public ErrorHandler validarEnvioTransporteExterno()
        {
            ErrorHandler eh = new ErrorHandler();

            if (ddlEnvioTipo.SelectedValue == "Transporte externo")
            {
                #region campos de Transporte

                    if (txtTransporteProveedor.Text == "")
                    {
                        eh.Mensaje = "No tiene transporte";
                        return eh;
                    }

                    if (txtTransporteSucursal.Text == "")
                    {
                        eh.Mensaje = "No ha indicado la sucursal del proveedor";
                        return eh;
                    }

                    if (txtTransporteTelefono.Text == "")
                    {
                        eh.Mensaje = "No ha indicado el teléfono del proveedor";
                        return eh;
                    }

                    if (txtTransporteEmail.Text == "")
                    {
                        eh.Mensaje = "No ha indicado el email del proveedor";
                        return eh;
                    }

                    if (txtTransporteDomicilio.Text == "")
                    {
                        eh.Mensaje = "No ha indicado el domicilio del proveedor";
                        return eh;
                    }

                #endregion
                #region campos de Envio

                    if (txtEnvioNumero.Text == "")
                    {
                        eh.Mensaje = "El número de envío no puede ser vacío";
                    }

                    if (txtEnvioImporte.Text == "")
                    {
                        eh.Mensaje = "El importe del envío no puede ser vacío";
                        return eh;
                    }

                    //if (txtEnvioFecha.Text == "")
                    //{
                    //    eh.Mensaje = "La fecha del envìo no puede ser vacía";
                    //    return eh;
                    //}

                #endregion
                #region campos de RemitoExterno
                    // 2019/01/31: No se validarán los remitos externos como campo obligatorio en los datos del envío
                #endregion

                if (eh.Mensaje == null)
                {
                    eh.Mensaje = "OK";
                }
            }
            else
            {
                eh.Mensaje = "Sin configuración de envío";
                return eh;
            }
            return eh;
        }

        public Envio Envio_ObtenerPorId(int idEnvio)
        {
            Envio envioReturn = null;
            try
            {
                envioReturn =
                new Envio().Select().
                    Where(env =>
                        env.IdEnvio == idEnvio
                    ).FirstOrDefault();
            }
            catch
            {
                envioReturn = null;
            }

            return envioReturn;
        }

        public Envio Envio_ObtenerPorSDTId(int idSDT)
        {
            Envio envioReturn = null;
            try
            {
                envioReturn =
                new Envio().Select().
                    Where(env =>
                        env.IdSDT == idSDT
                    ).FirstOrDefault();
            }
            catch
            {
                envioReturn = null;
            }

            return envioReturn;
        }

        public Envio Envio_ObtenerPorSDSId(int idSDS)
        {
            Envio envioReturn = null;
            try
            {
                envioReturn =
                new Envio().Select().
                    Where(env =>
                        env.IdSDS == idSDS
                    ).FirstOrDefault();
            }
            catch
            {
                envioReturn = null;
            }

            return envioReturn;
        }

        public void ddlEnvioTipo_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEnvioTipo.SelectedValue == "Sin configuración de envío")
            {
                upEnvioExterno.Visible = false;
                upGestionEnvio.Update();
            }

            if (ddlEnvioTipo.SelectedValue == "Transporte externo")
            {
                upEnvioExterno.Visible = true;
                upGestionEnvio.Update();
            }
        }

        public void envioMostrar(Envio envio)
        {
            ddlEnvioTipo.SelectedValue = envio.Tipo;
            ddlEnvioTipo.Text = envio.Tipo;

            if (envio.Tipo == "Transporte externo")
            {
                ddlEnvioTipo.Visible = false;
                upEnvioExterno.Visible = true;

                Transporte transporte = null;
                RemitoExterno remitoExterno = null;

                if (envio.IdTransporte != null)
                {
                    transporte =
                        new Transporte().
                            Select().Where(tra =>
                            tra.IdTransporte == envio.IdTransporte
                            ).FirstOrDefault();
                }

                if (transporte != null)
                {
                    txtTransporteProveedor.Text = transporte.Proveedor;
                    txtTransporteSucursal.Text = transporte.Sucursal;
                    txtTransporteTelefono.Text = transporte.Telefono;
                    txtTransporteDomicilio.Text = transporte.Domicilio;
                    txtTransporteEmail.Text = transporte.Email;
                    gvTransportes.Visible = false;

                    txtTransporteProveedor.Enabled = false;
                    txtTransporteSucursal.Enabled = false;
                    txtTransporteTelefono.Enabled = false;
                    txtTransporteDomicilio.Enabled = false;
                    txtTransporteEmail.Enabled = false;

                    btnCambiarDatosDeTransporte.Enabled = true;
                }

                txtEnvioNumero.Text = envio.NumeroEnvio;

                if (envio.FHEnvio != null)
                {
                    try
                    {
                        DateTime vFechaEnvio = Convert.ToDateTime(envio.FHEnvio);
                        txtEnvioFecha.TextMode = TextBoxMode.SingleLine;
                        txtEnvioFecha.Text = vFechaEnvio.ToString();
                    }
                    catch
                    {
                        
                    }
                }
                
                txtEnvioImporte.Text = envio.Importe.ToString();

                if (envio.IdRemitoExterno != null)
                {
                    remitoExterno =
                        new RemitoExterno().
                            Select().Where(rem =>
                            rem.IdRemitoExterno == envio.IdRemitoExterno
                            ).FirstOrDefault();
                }

                if (remitoExterno != null)
                {
                    txtRemitoNumero.Text = remitoExterno.Numero;
                    if (remitoExterno.Fecha != null)
                    {
                        try
                        {
                            DateTime vFechaRemito = Convert.ToDateTime(remitoExterno.Fecha);
                            txtRemitoFecha.Text = vFechaRemito.ToString();
                            txtRemitoFecha.TextMode = TextBoxMode.SingleLine;
                            
                        }
                        catch
                        {

                        }
                    }
                    
                    txtRemitoResponsable.Text = remitoExterno.Responsable;
                    txtRemitoObservaciones.Text = remitoExterno.Observaciones;
                }

                actualizarTodo();
            }            
        }

        public void actualizarTodo()
        {
            upGestionEnvio.Update();
            upEnvioExterno.Update();
        }

        public string TransporteProveedor
        {
            get { return txtTransporteProveedor.Text; }
            set
            {
                txtTransporteProveedor.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string TransporteSucursal
        {
            get { return txtTransporteSucursal.Text; }
            set
            {
                txtTransporteSucursal.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string TransporteDomicilio
        {
            get { return txtTransporteDomicilio.Text; }
            set
            {
                txtTransporteDomicilio.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string TransporteTelefono
        {
            get { return txtTransporteTelefono.Text; }
            set
            {
                txtTransporteTelefono.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string TransporteEmail
        {
            get { return txtTransporteEmail.Text; }
            set
            {
                txtTransporteEmail.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string EnvioNumero
        {
            get { return txtEnvioNumero.Text; }
            set
            {
                txtEnvioNumero.Text = value;
                upGestionEnvio.Update();
            }
        }

        public string EnvioFecha
        {
            get { return txtEnvioFecha.Text; }
            set
            {
                txtEnvioFecha.Text = value;
                upGestionEnvio.Update();
            }
        }

        public decimal EnvioImporte
        {
            get { return EnvioImporte; }
            set
            {
                txtEnvioImporte.Text = value.ToString();
                upGestionEnvio.Update();
            }
        }
        
        protected void gvTransportes_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string vProveedor = gvTransportes.Rows[e.NewSelectedIndex].Cells[1].Text;
            string vSucursal = gvTransportes.Rows[e.NewSelectedIndex].Cells[2].Text;
            string vDomicilio = gvTransportes.Rows[e.NewSelectedIndex].Cells[3].Text;
            string vTelefono = gvTransportes.Rows[e.NewSelectedIndex].Cells[4].Text;
            string vEmail = gvTransportes.Rows[e.NewSelectedIndex].Cells[5].Text;

            //Inhabilitamos los controles
            txtTransporteProveedor.Enabled = false;
            txtTransporteSucursal.Enabled = false;
            txtTransporteDomicilio.Enabled = false;
            txtTransporteEmail.Enabled = false;
            txtTransporteTelefono.Enabled = false;

            //Completamos los datos del transporte
            txtTransporteProveedor.Text = vProveedor;
            txtTransporteSucursal.Text = vSucursal;
            txtTransporteDomicilio.Text = vDomicilio;
            txtTransporteTelefono.Text = vTelefono;
            txtTransporteEmail.Text = vEmail;

            gvTransportes.Visible = false;
            btnCambiarDatosDeTransporte.Enabled = true;
            upGestionEnvio.Update();
        }

        protected void gvTransportes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int vIdTransporte = Convert.ToInt32(gvTransportes.Rows[e.RowIndex].Cells[0].Text);

            using (Tn transaccionTransporteEliminar = new Tn("CGADM"))
            {
                CGADM.Transporte transporteEliminar = 
                    new CGADM.Transporte().
                        Select().
                            Where(tran => 
                                tran.IdTransporte == vIdTransporte
                                ).FirstOrDefault();
                transporteEliminar.FHBaja = DateTime.Now;

                if (transporteEliminar.Proveedor == txtTransporteProveedor.Text && transporteEliminar.Sucursal == txtTransporteSucursal.Text)
                {
                    txtTransporteProveedor.Text = "";
                    txtTransporteSucursal.Text = "";
                    txtTransporteDomicilio.Text = "";
                    txtTransporteTelefono.Text = "";
                    txtTransporteEmail.Text = "";
                }

                transporteEliminar.Update(transaccionTransporteEliminar);

                transaccionTransporteEliminar.Commit();

                refrescarTransportes();

                Message("Transporte eliminado",
                    "Se ha eliminado el transporte. Solo conservará las relaciones con los envíos preexistentes que lo tengan asignado actualmente.",
                    MessagesTypes.info);
            }
        }

        protected void btnCambiarDatosDeTransporte_click(object sender, EventArgs e)
        {
            //Se vacían los controles
            txtTransporteProveedor.Text = "";
            txtTransporteSucursal.Text = "";
            txtTransporteDomicilio.Text = "";
            txtTransporteEmail.Text = "";
            txtTransporteTelefono.Text = "";

            //Se habilitan los controles
            txtTransporteProveedor.Enabled = true;
            txtTransporteSucursal.Enabled = true;
            txtTransporteDomicilio.Enabled = true;
            txtTransporteEmail.Enabled = true;
            txtTransporteTelefono.Enabled = true;

            btnCambiarDatosDeTransporte.Enabled = false;
            gvTransportes.Visible = true;
            upGestionEnvio.Update();
        }

        protected void refrescarTransportes()
        {
            int idDistribuidor = Convert.ToInt32(Session["idDistribuidor"].ToString());
            List<Transporte> listaTransportes = new Transporte().
                Select().Where(
                    trans =>
                    trans.IdDistribuidor == idDistribuidor &&
                    trans.FHBaja == null
                    ).ToList();
            gvTransportes.DataSource = listaTransportes.ToDataTable();
            gvTransportes.DataBind();
            upGestionEnvio.Update();
        }
    }
}