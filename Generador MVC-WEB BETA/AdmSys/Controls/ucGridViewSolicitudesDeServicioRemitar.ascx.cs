﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;
using Code;

namespace AdmSys.Controls
{
    public partial class ucGridViewSolicitudesDeServicioRemitar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public GridView gridviewSolicitudesDeServicios
        {
            get { return gvSolicitudesDeServicios; }
        }

        public void ActualizarGrillaSolicitudes(List<SDS> listaSolicitudesS)
        {
            if (listaSolicitudesS != null)
            {
                gvSolicitudesDeServicios.DataSource = listaSolicitudesS.OrderByDescending(s => s.SDSNro);
                gvSolicitudesDeServicios.DataBind();
                foreach (GridViewRow rowListaSDS in gvSolicitudesDeServicios.Rows)
                {
                    if (rowListaSDS.RowType == DataControlRowType.DataRow)
                    {
                        try
                        {
                            ((CheckBox)gvSolicitudesDeServicios.Rows[rowListaSDS.RowIndex].Cells[11].FindControl("cbxRemitar")).Checked = true;
                        }
                        catch (Exception)
                        {
                            Message("Errores en actualización en la grilla de solicitudes de servicio", "No se ha actualizado correctamente, por favor reintente nuevamente", MessagesTypes.error);
                        }
                    }
                }
            }
            else
            {
                gvSolicitudesDeServicios.DataSource = null;
                gvSolicitudesDeServicios.DataBind();
            }
            upSolicitudesServicio.Update();
        }

        protected void gvSolicitudesDeServicios_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string vIdSDTDetalleCripto = Convert.ToInt32(gvSolicitudesDeServicios.Rows[e.NewSelectedIndex].Cells[0].Text).ToCryptoID();
            Utils.NavigateTo("SolicitudDeServicio.aspx?cryptoID=" + vIdSDTDetalleCripto);
        }
    }
}