﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGridViewSolicitudesDeTallerRemitar.ascx.cs" Inherits="AdmSys.Controls.ucGridViewSolicitudesDeTallerRemitar" %>
<asp:UpdatePanel ID="upSolicitudesTaller" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <label>
            <asp:Label Text="Solicitudes de Taller" runat="server" />
        </label>
        <asp:GridView ID="gvSolicitudesDeTaller" CssClass="table table-striped table-bordered table-hover"
            data-model="Remitar.aspx"
            Width="100%" AutoGenerateColumns="False" runat="server"
            EmptyDataText="Sin solicitudes de taller compatibles con los filtros ingresados"
            OnSelectedIndexChanging="gvSolicitudesDeTaller_SelectedIndexChanging"
            >
            <Columns>
                <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                <%-- Responsive + - --%>
                <%-- cryptoID --%>
                <asp:BoundField HeaderText="#" DataField="IdSDT" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                <asp:BoundField HeaderText="Id Distribuidor Unidad Negocio" DataField="IdDistribuidorUnidadNegocio" />
                <asp:BoundField HeaderText="SDT Nro" DataField="SDTNro" />
                <asp:BoundField HeaderText="Mail informe" DataField="MailInforme" />

                <asp:BoundField HeaderText="FH Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                <asp:BoundField HeaderText="Obs Envío" DataField="ObservacionesEnvio" />

                <asp:TemplateField HeaderText="Remitar" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:CheckBox ID="cbxRemitar" runat="server" Width="50" />
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Ver solicitud" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnVerSolicitud"
                            runat="server"
                            CssClass="btn btn-info"
                            CommandName="Select"
                            title="Ver detalle de la solicitud"
                            Style="margin-left: 5px;">
                                <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                        </asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>