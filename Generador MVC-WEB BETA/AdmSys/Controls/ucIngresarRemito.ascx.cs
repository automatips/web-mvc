﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;
using System.Data;

namespace AdmSys.Controls
{
    public partial class ucIngresarRemito : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        public string MailInforme
        {
            get { return txtMailInforme.Text; }
            set
            {
                txtMailInforme.Text = value;
                txtMailInforme.Enabled = false;
                upIngresarRemito.Update();
            }
        }

        public string Observaciones
        {
            get { return txtObsAlta.Text; }
            set
            {
                txtObsAlta.Text = value;
                txtObsAlta.Enabled = false;
                upIngresarRemito.Update();
            }
        }

        public void IngresarRemito(
            int idDistribuidor,
            int idUnidadNegocio,
            DistribuidorUnidadNegocio distribuidorUnidadNegocio,
            int idReseller,
            ucProductosRemitar ucProductosRemitar1
            )
        {
            PlanComercial planComercial = 
                new PlanComercial().
                    Select().
                    Where(
                pc =>
                pc.IdDistribuidorUnidadNegocio == distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio
                ).FirstOrDefault();

            Remito remPaginaActual = new Remito();

            //Intercambiar por sp que obtiene el nro de remito por distribuidor
            DataTable dtGetSdsNro = new SP("CGADM").Execute("usp_GET_REMNroAlta");
            int vREMNroAlta = 0;
            foreach (DataRow r in dtGetSdsNro.Rows)
            {
                vREMNroAlta = r[0].ToString().ToInt();
            }
            dtGetSdsNro.Dispose();


            Distribuidor dist = new Distribuidor().Select().Where(di => di.IdDistribuidor == distribuidorUnidadNegocio.IdDistribuidor).FirstOrDefault();

            string vRemitoImpresion = "";

            string vIdRemitero = dist.IdRemitero.ToString();
            int vIdNuevoRemito =  Convert.ToInt32(dist.UltimoRemito+1);
            string vNroRemitoGenerar = vIdNuevoRemito.ToString();

            dist.UltimoRemito = vIdNuevoRemito;

            while (vIdRemitero.Length < 4)
            {
                vIdRemitero = "0" + vIdRemitero;
            }

            while (vNroRemitoGenerar.Length < 8)
            {
                vNroRemitoGenerar = "0" + vNroRemitoGenerar;
            }

            vRemitoImpresion = vIdRemitero + "-" + vNroRemitoGenerar;

            remPaginaActual.IdDistribuidorUnidadNegocio = planComercial.IdDistribuidorUnidadNegocio;
            remPaginaActual.RemitoNro = vREMNroAlta;
            remPaginaActual.MailInforme = txtMailInforme.Text.ToString();
            remPaginaActual.IdResellerDestino = idReseller;
            remPaginaActual.FHAlta = DateTime.Now;
            remPaginaActual.Observacion = txtObsAlta.Text.ToString();
            remPaginaActual.IdCuentaAlta = GetIdCuentaFromCookie();
            remPaginaActual.RemiteroId = Convert.ToInt32(vIdRemitero);
            remPaginaActual.RemitoNro = Convert.ToInt32(vNroRemitoGenerar);
            remPaginaActual.RemitoImpresion = vRemitoImpresion;

            //primero vamos a listar las solicitudes implicadas en el remito
            List<SDS> listaSDSRemitar = new List<SDS>();
            List<SDT> listaSDTRemitar = new List<SDT>();
            List<SDSDetalle> listaSDSDetalleRemitar = new List<SDSDetalle>();
            List<SDTDetalle> listaSDTDetalleRemitar = new List<SDTDetalle>();
            List<RemitoDetalle> listaRemitoDetalle = new List<RemitoDetalle>();

            SDTDetalleEstado estado = new SDTDetalleEstado().Select().Where(est => est.Descripcion == "Remitado").FirstOrDefault();

            //Nuevo requerimiento. Almacenar historial de gestiones 06/12/2018
            SDTDetalleGestion gestionRemitar = new SDTDetalleGestion();
            gestionRemitar.IdCuenta = GetIdCuentaFromCookie();
            gestionRemitar.FHAlta = DateTime.Now;
            gestionRemitar.IdSDTDetalleEstado = estado.IdSDTDetalleEstado;

            List<SDTDetalleGestionHistorial> listaGestionHistorial = new List<SDTDetalleGestionHistorial>();

            GridView gridviewListaProductosCargados = new GridView();
            gridviewListaProductosCargados = ucProductosRemitar1.gridviewListaProductosCargados;

            foreach (GridViewRow rowListaProductos in gridviewListaProductosCargados.Rows)
            {
                if (rowListaProductos.RowType == DataControlRowType.DataRow)
                {
                    int vIdDetalle = rowListaProductos.Cells[0].Text.ToInt();
                    string vDescripcion = rowListaProductos.Cells[1].Text.ToString();
                    decimal vPrecioUnitario = rowListaProductos.Cells[2].Text.ToDecimal();
                    string vTipoSolicitud = rowListaProductos.Cells[4].Text.ToString();
                    int vSolicitudNro = rowListaProductos.Cells[5].Text.ToInt();
                    int vCantidadSolicitada = rowListaProductos.Cells[6].Text.ToInt();
                    int vCantidadRemitados = rowListaProductos.Cells[7].Text.ToInt();
                    string vIdSerialSDT = rowListaProductos.Cells[8].Text.ToString();
                    string vTipoSDT = rowListaProductos.Cells[9].Text.ToString();

                    int vCantidadPosibleRemitar = vCantidadSolicitada - vCantidadRemitados;

                    TextBox txtRow = (TextBox)rowListaProductos.Cells[10].FindControl("txtCantidadRemitar");

                    int vCantidadRemitar = 0;
                    try
                    {
                        vCantidadRemitar = Convert.ToInt32(txtRow.Text);
                    }
                    catch { }

                    if (vCantidadRemitar > 0) //Si aún no se ha remitado el total del detalle de la solicitud en cuestión
                    {
                        RemitoDetalle remitoDetalleGrilla = new RemitoDetalle();
                        remitoDetalleGrilla.DescripcionListaPrecio = vDescripcion;
                        remitoDetalleGrilla.Precio = vPrecioUnitario;
                        remitoDetalleGrilla.Tipo = vTipoSolicitud;
                        remitoDetalleGrilla.NroSolicitud = vSolicitudNro;

                        switch (vTipoSolicitud)
                        {
                            ////////////////////////////////////////////////////////////
                            case "SDS":

                                //Obtenemos la solicitud a remitar
                                SDS solicitudSRemitar = new SDS();
                                solicitudSRemitar.SDSNro = vSolicitudNro;

                                bool solicitudSCargada = false;
                                foreach (SDS solicitud in listaSDSRemitar)
                                {
                                    if (solicitud.SDSNro == solicitudSRemitar.SDSNro)
                                    {
                                        solicitudSRemitar = solicitud;
                                        solicitudSCargada = true;
                                    }
                                }

                                if (!solicitudSCargada)
                                {
                                    solicitudSRemitar = new SDS().Select().Where(sds => sds.SDSNro == vSolicitudNro).FirstOrDefault();
                                    listaSDSRemitar.Add(solicitudSRemitar);
                                }

                                //si la cantidad de productos remitados en ésta instancia se encuentran disponibles para remitar
                                if (vCantidadRemitar > vCantidadPosibleRemitar)
                                {
                                    Message("Remito incorrecto", "En la Solicitud de Servicio " + solicitudSRemitar.SDSNro + " está intentando remitar más cantidades que las solicitadas", MessagesTypes.warning);
                                    return;
                                }
                                else
                                {
                                    SDSDetalle solicitudSDetalleRemitar = new SDSDetalle();
                                    solicitudSDetalleRemitar.IdListaPrecio = vIdDetalle;
                                    solicitudSDetalleRemitar.IdSDS = solicitudSRemitar.IdSDS;

                                    solicitudSDetalleRemitar =
                                        new SDSDetalle().Select().Where(sdt =>
                                        sdt.IdSDS == solicitudSRemitar.IdSDS &&
                                        sdt.IdListaPrecio == solicitudSDetalleRemitar.IdListaPrecio).FirstOrDefault();

                                    //solicitudSDetalleRemitar.PrecioLista = vPrecioUnitario;
                                    solicitudSDetalleRemitar.Cantidad = vCantidadSolicitada;
                                    if (solicitudSDetalleRemitar.CantidadRemitados == null)
                                    {
                                        solicitudSDetalleRemitar.CantidadRemitados = 0;
                                    }
                                    solicitudSDetalleRemitar.CantidadRemitados = (solicitudSDetalleRemitar.CantidadRemitados + vCantidadRemitar);

                                    listaSDSDetalleRemitar.Add(solicitudSDetalleRemitar);

                                    remitoDetalleGrilla.Cantidad = vCantidadRemitar;
                                    remitoDetalleGrilla.IdListaPrecio = vIdDetalle;
                                    remitoDetalleGrilla.IdSDTDetalle = 0;
                                }
                                break;
                            ////////////////////////////////////////////////////////////
                            case "SDT":

                                //Obtenemos la solicitud a remitar
                                SDT solicitudTRemitar = new SDT();
                                solicitudTRemitar.SDTNro = vSolicitudNro;

                                bool solicitudTCargada = false;
                                foreach (SDT solicitud in listaSDTRemitar)
                                {
                                    if (solicitud.SDTNro == solicitudTRemitar.SDTNro)
                                    {
                                        solicitudTRemitar = solicitud;
                                        solicitudTCargada = true;
                                    }
                                }

                                if (!solicitudTCargada)
                                {
                                    solicitudTRemitar = new SDT().Select().Where(sds => sds.SDTNro == vSolicitudNro).FirstOrDefault();
                                    listaSDTRemitar.Add(solicitudTRemitar);
                                }

                                //si la cantidad de productos remitados en ésta instancia se encuentran disponibles para remitar
                                if (vCantidadRemitar > vCantidadPosibleRemitar)
                                {
                                    Message("Remito incorrecto", "En la Solicitud de Taller " + solicitudTRemitar.SDTNro + " está intentando remitar más cantidades que las solicitadas", MessagesTypes.warning);
                                    return;
                                }
                                else
                                {
                                    SDTDetalle solicitudDetalleRemitar = new SDTDetalle();
                                    solicitudDetalleRemitar.IdSDT = solicitudTRemitar.IdSDT;
                                    solicitudDetalleRemitar.IdSerial = vIdSerialSDT;
                                    solicitudDetalleRemitar.ArtefactoTipo = vTipoSDT;
                                    solicitudDetalleRemitar.IdSDTDetalle = vIdDetalle;

                                    solicitudDetalleRemitar =
                                        new SDTDetalle().Select().Where(sdt => 
                                        sdt.IdSDT == solicitudTRemitar.IdSDT &&
                                        sdt.IdSerial == solicitudDetalleRemitar.IdSerial && 
                                        sdt.ArtefactoTipo == solicitudDetalleRemitar.ArtefactoTipo
                                        ).FirstOrDefault();

                                    solicitudDetalleRemitar.Precio = vPrecioUnitario;
                                    solicitudDetalleRemitar.Cantidad = vCantidadSolicitada;

                                    if (solicitudDetalleRemitar.CantidadRemitados == null)
                                    {
                                        solicitudDetalleRemitar.CantidadRemitados = 0;
                                    }
                                    solicitudDetalleRemitar.CantidadRemitados = (solicitudDetalleRemitar.CantidadRemitados + vCantidadRemitar);

                                    //Instancia de verificación
                                    listaSDTDetalleRemitar.Add(solicitudDetalleRemitar);

                                    remitoDetalleGrilla.Cantidad = vCantidadRemitar;
                                    remitoDetalleGrilla.IdListaPrecio = 0;
                                    remitoDetalleGrilla.IdSDTDetalle = vIdDetalle;

                                    //Obtener detalle de seriales de cada dispositivo para remitar en solicitud de taller
                                    switch (vTipoSDT)
                                    {
                                        case "UM":
                                            DataTable dtObtener_UMIdxIdUM = new SP("CGADM").Execute("usp_Obtener_UMIdxIdUM"
                                                , P.Add("@p_IdSerial", vIdSerialSDT.Trim())
                                                );
                                            foreach (DataRow r in dtObtener_UMIdxIdUM.Rows)
                                            {
                                                remitoDetalleGrilla.UMId = r[0].ToString().ToInt();
                                            }
                                            break;

                                        case "DISPOSITIVO":
                                            DataTable dtObtener_DispositivoIDxIdDispositivos = new SP("CGADM").Execute("usp_Obtener_IdDispositivoxDispositivoID"
                                                , P.Add("@p_IdSerial", vIdSerialSDT.Trim())
                                                );
                                            foreach (DataRow r in dtObtener_DispositivoIDxIdDispositivos.Rows)
                                            {
                                                remitoDetalleGrilla.IdDispositivos = r[0].ToString().ToInt();
                                            }
                                            break;
                                    }
                                    //Nuevo requerimiento. Almacenar historial de gestiones 06/12/2018
                                    SDTDetalleGestionHistorial gestionHistorial = new SDTDetalleGestionHistorial();
                                    gestionHistorial.IdSDTDetalle = solicitudDetalleRemitar.IdSDTDetalle;
                                    listaGestionHistorial.Add(gestionHistorial);
                                }
                                break;
                        }
                        //agregar remitoDetalle a la lista de remitar
                        listaRemitoDetalle.Add(remitoDetalleGrilla);
                    }
                }
            }

            //AQUI REMITAR CON REGISTROS DE LAS LISTAS
            Remito rem = remPaginaActual;
            using (Tn transaccionRemitar = new Tn("CGADM"))
            {
                try
                {
                    //Insertamos el remito
                    rem.Insert(transaccionRemitar);

                    //Insertamos los remitoDetalle asociados
                    foreach (RemitoDetalle remitoDetalle in listaRemitoDetalle)
                    {
                        remitoDetalle.IdRemito = rem.IdRemito;
                        remitoDetalle.Insert(transaccionRemitar);
                    }

                    //Modificamos la cantidad remitada de los productos de las SDS
                    foreach (SDSDetalle sdsDetalle in listaSDSDetalleRemitar)
                    {
                        sdsDetalle.Update(transaccionRemitar);
                    }

                    //Modificamos la cantidad remitada de los artefactos de las SDT
                    foreach (SDTDetalle sdtDetalle in listaSDTDetalleRemitar)
                    {
                        sdtDetalle.IdSDTDetalleEstado = estado.IdSDTDetalleEstado;
                        sdtDetalle.FHUltimaGestion = DateTime.Now;
                        sdtDetalle.Update(transaccionRemitar);
                    }

                    //Insertamos la gestion asociada al remito
                    gestionRemitar.Insert(transaccionRemitar);

                    //Modificamos el identificador de remito del distribuidor
                    dist.Update(transaccionRemitar);

                    //Insertamos el historial de cada SDTDETALLE asociada a la gestión de remito
                    foreach (SDTDetalleGestionHistorial gestionHistorial in listaGestionHistorial)
                    {
                        gestionHistorial.IdSDTDetalleGestion = gestionRemitar.IdSDTDetalleGestion;
                        gestionHistorial.Insert(transaccionRemitar);
                    }

                    transaccionRemitar.Commit();

                    Session.Add("idRemito",rem.IdRemito.ToString());                  

                    new BulkMail.Mail()
                    {
                        AddDateTime = DateTime.Now,
                        DisplayName = "Sistema Administrativo de CG",
                        EmailFrom = "info@controlglobal.com.ar",
                        Email = rem.MailInforme.ToString(),
                        Subject = "CG - Remito " + rem.IdRemito.ToString(),
                        Body = "Ha ingresado un remito con el identificador: " + rem.IdRemito.ToString()
                    }.Insert();

                    Message("Remito creado exitosamente", "Remito " + rem.IdRemito.ToString(), MessagesTypes.success);
                }
                catch (Exception ex)
                {
                    Message("Error al remitar", "Se ha producido un error impactando la transaccion del remito, por favor reintente nuevamente. Detalle del problema: " + ex.Message, MessagesTypes.warning);
                }
            }
        }
    }
}