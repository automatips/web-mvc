﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CGADM;
using MagicSQL;
using static Code.Utils;
using Code;

namespace AdmSys.Controls
{
    public partial class ucPendienteManufacturaSDT : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        public void alternarPanelesDeGestion(string vPanel)
        {
            if (vPanel.Contains("administracion"))
            {
                upGestionDiagnosticados.Update();
                upGestionReparados.Update();
            }
        }

        public void obtenerArtefactoHistorial()
        {
            lblErrorHandler.Text = "prueba";
            upErrorHandler.Update();
        }

        public int idDistribuidorParam
        {
            get
            {
                int enteroDevolver = Convert.ToInt32(lblIdDistribuidorFiltro.Text);
                return enteroDevolver;
            }
            set
            {
                lblIdDistribuidorFiltro.Text = value.ToString();
                upFiltros.Update();
            }
        }

        public int idUnidadNegocioParam
        {
            get
            {
                int enteroDevolver = Convert.ToInt32(lblIdUnidadNegocioFiltro.Text);
                return enteroDevolver;
            }
            set
            {
                lblIdUnidadNegocioFiltro.Text = value.ToString();
                upFiltros.Update();
            }
        }

        public int idResellerParam
        {
            get
            {
                int enteroDevolver = Convert.ToInt32(lblIdResellerFiltro.Text);
                return enteroDevolver;
            }
            set
            {
                lblIdResellerFiltro.Text = value.ToString();
                upFiltros.Update();
            }
        }

        public ErrorHandler cambiarEstadoArtefacto
            (
                int pIdSDTDetalle,
                string pEstadoOrigen,
                string pEstadoDestino
            )
        {
            ErrorHandler eh = new ErrorHandler();
            try
            {
                lblErrorHandler.Text = "";
                lblErrorHandler.ForeColor = System.Drawing.Color.Red;
                List<SDTDetalleEstado> listaEstados = null;
                SDTDetalle sdtDetalleCambioEstado = null;
                SDTDetalleEstado estadoOrigen = null;
                SDTDetalleEstado estadoDestino = null;

                using (Tn transaccionEstados = new Tn("CGADM"))
                {
                    listaEstados = new SDTDetalleEstado().Select(transaccionEstados).Where(est => est.FHBaja == null).ToList();
                }

                if (listaEstados != null)
                {
                    foreach (SDTDetalleEstado estadoAsignar in listaEstados)
                    {
                        if (estadoAsignar.Descripcion == pEstadoOrigen)
                        {
                            estadoOrigen = estadoAsignar;
                        }

                        if (estadoAsignar.Descripcion == pEstadoDestino)
                        {
                            estadoDestino = estadoAsignar;
                        }
                    }
                }
                else
                {
                    eh.Mensaje = "Transacción con problemas";
                    eh.Descripcion = "No se pueden completar los parámetros necesarios para procesar la transacción";
                    return eh;
                }

                using (Tn transaccionSelect = new Tn("CGADM"))
                {
                    sdtDetalleCambioEstado = new SDTDetalle().
                        Select(transaccionSelect).
                            Where(sdtd => 
                                sdtd.IdSDTDetalle == pIdSDTDetalle && 
                                sdtd.IdSDTDetalleEstado == estadoOrigen.IdSDTDetalleEstado
                                ).FirstOrDefault();
                }

                if (sdtDetalleCambioEstado != null)
                {
                    if (estadoOrigen != null && estadoDestino != null)
                    {
                        sdtDetalleCambioEstado.IdSDTDetalleEstado = estadoDestino.IdSDTDetalleEstado;
                        sdtDetalleCambioEstado.FHUltimaGestion = DateTime.Now;

                        SDTDetalleGestion gestionArtefacto = new SDTDetalleGestion();
                        gestionArtefacto.IdSDTDetalleEstado = estadoDestino.IdSDTDetalleEstado;
                        gestionArtefacto.IdCuenta = GetIdCuentaFromCookie();
                        gestionArtefacto.FHAlta = sdtDetalleCambioEstado.FHUltimaGestion;


                        List<SDTDetalleDiagnostico> listaDiagnosticos = null;
                        SaldoCuentaCorriente saldo = null;

                        using (Tn transaccionCambioEstado = new Tn("CGADM"))
                        {
                            switch (estadoDestino.Descripcion)
                            {
                                case "Reparado":
                                    //Al imputar los diagnósticos, impactar el presupuesto de los mismos en el precio del artefacto
                                    listaDiagnosticos = new SDTDetalleDiagnostico().
                                        Select(transaccionCambioEstado).Where(
                                            diag =>
                                                diag.IdSDTDetalle == sdtDetalleCambioEstado.IdSDTDetalle
                                                ).ToList();

                                    foreach (SDTDetalleDiagnostico diagnosticoImputar in listaDiagnosticos)
                                    {
                                        sdtDetalleCambioEstado.Precio =
                                            sdtDetalleCambioEstado.Precio + diagnosticoImputar.Presupuesto;
                                    }


                                    DistribuidorUnidadNegocio distribuidorUnidadNegocio = 
                                        new DistribuidorUnidadNegocio().
                                            Select().Where(dun => 
                                                dun.IdDistribuidor == idDistribuidorParam && 
                                                dun.IdUnidadNegocio == idUnidadNegocioParam
                                                ).FirstOrDefault();
                                    CGADM.PlanComercial planComercial = 
                                        new CGADM.PlanComercial().
                                            Select().Where(pc => 
                                                pc.IdDistribuidorUnidadNegocio == distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio
                                                ).FirstOrDefault();

                                    saldo =
                                        new SaldoCuentaCorriente().
                                            Select().Where(sal =>
                                                sal.IdDistribuidor == idDistribuidorParam &&
                                                sal.IdUnidadNegocio == idUnidadNegocioParam
                                                ).FirstOrDefault();

                                    if (saldo != null)
                                    {
                                        saldo.Saldo = (saldo.Saldo - Convert.ToDecimal(sdtDetalleCambioEstado.Precio));
                                    }
                                    else
                                    {
                                        eh.Mensaje = "No se pudo imputar la reparación en la cuenta corriente";
                                        eh.Mensaje = "No existe una cuenta corriente asociada al Reseller/Distribuidor seleccionado";
                                        return eh;
                                    }

                                    //También indicar que el artefacto en cuestión debe ser remitado
                                    sdtDetalleCambioEstado.CantidadRemitados = 0;

                                    break;

                                case "En devolucion":
                                    //Volvemos la cantidad remitada a 0, para volver a procesar desde el formulario de remito
                                    sdtDetalleCambioEstado.CantidadRemitados = 0;
                                    break;
                            }

                            if (saldo != null)
                            {
                                saldo.Update(transaccionCambioEstado);
                            }

                            sdtDetalleCambioEstado.Update(transaccionCambioEstado);

                            gestionArtefacto.Insert(transaccionCambioEstado);

                            SDTDetalleGestionHistorial gestionHistorial = new SDTDetalleGestionHistorial();
                            gestionHistorial.IdSDTDetalle = sdtDetalleCambioEstado.IdSDTDetalle;
                            gestionHistorial.IdSDTDetalleGestion = gestionArtefacto.IdSDTDetalleGestion;

                            gestionHistorial.Insert(transaccionCambioEstado);

                            SDT sdtPadre = new SDT().Select(transaccionCambioEstado).Where(sdt => sdt.IdSDT == sdtDetalleCambioEstado.IdSDT).FirstOrDefault();
                            new BulkMail.Mail()
                            {
                                AddDateTime = DateTime.Now,
                                DisplayName = "Sistema Administrativo de CG",
                                EmailFrom = "info@controlglobal.com.ar",
                                Email = sdtPadre.MailInforme,
                                Subject = "CG - Gestion de artefactos. SDT " + sdtPadre.SDTNro.ToString(),
                                Body = "Se ha realizado una gestión de artefacto en la SDT nro " + sdtPadre.SDTNro + Environment.NewLine +
                                ". Descripcion: " + sdtDetalleCambioEstado.Descripcion + Environment.NewLine +
                                ". Tipo de Hardware: " + sdtDetalleCambioEstado.ArtefactoTipo + Environment.NewLine +
                                ". Incidencia: " + sdtDetalleCambioEstado.Incidencia + Environment.NewLine +
                                ". Estado actual: " + estadoDestino.Descripcion
                            }.Insert();

                            transaccionCambioEstado.Commit();

                            eh.Mensaje = "EXITO";

                            return eh;
                        }
                    }
                    else
                    {
                        eh.Mensaje = "Transacción con problemas";
                        eh.Descripcion = "No se pueden completar los parámetros necesarios para procesar la transacción";
                        return eh;
                    }
                }
                else
                {
                    eh.Mensaje = "Concurrencia de gestiones";
                    eh.Descripcion = "El artefacto seleccionado ya no se encuentra disponible para el cambio de estado porque ha sido cambiado desde una gestión concurrente";
                    return eh;
                }
            }
            catch (Exception ex)
            {
                eh.Mensaje = "Error intentando cambiar el estado del artefacto: " + ex.Message;
                eh.Descripcion = "Descripción del error: " + ex.StackTrace;
                return eh;
            }
        }

        public List<SDT> obtenerSDTConArtefactos(
            int pIdReseller, 
            int pIdDistribuidorUnidadNegocio, 
            int pIdPlanComercial, 
            int pIdUnidadNegocio)
        {
            List<SDT> lista = new List<SDT>();
            try
            {
                DataTable dt =
                    new SP("CGADM").Execute("usp_ObtenerSDTConArtefactos",
                        P.Add("pIdReseller", pIdReseller),
                        P.Add("pIdDistribuidorUnidadNegocio", pIdDistribuidorUnidadNegocio),
                        P.Add("pIdPlanComercial", pIdPlanComercial),
                        P.Add("pIdUnidadNegocio", pIdUnidadNegocio)
                    );

                string arraySDTIdentificadores = "";
                int contadorIndice = 0;
                foreach (DataRow r in dt.Rows)
                {
                    int vidSDT = r[0].ToString().ToInt();
                    if (contadorIndice == 0)
                    {
                        arraySDTIdentificadores = vidSDT.ToString();
                    }
                    else
                    {
                        arraySDTIdentificadores = arraySDTIdentificadores + ", " + vidSDT.ToString();
                    }
                    contadorIndice++;
                }

                if (arraySDTIdentificadores != "")
                {
                    //Optimizacion. Consultar todas las SDT en una sola transaccion
                    using (Tn transaccion = new Tn("CGADM"))
                    {
                        lista = new SDT().Select("IdSDT in (" + arraySDTIdentificadores + ")");
                    }
                }
                return lista;
            }
            catch
            {
                return lista;
            }
        }

        public void filtrarProductos()
        {
            try
            {
                lblErrorHandler.Text = string.Empty;

                #region limpiarGrillasGestiondeArtefactos

                    gvGestionRecibidos.DataSource = null;
                    gvGestionRecibidos.DataBind();

                    gvGestionDiagnosticados.DataSource = null;
                    gvGestionDiagnosticados.DataBind();

                    gvGestionEnReparacion.DataSource = null;
                    gvGestionEnReparacion.DataBind();

                    gvGestionAprobados.DataSource = null;
                    gvGestionAprobados.DataBind();

                    gvGestionReparados.DataSource = null;
                    gvGestionReparados.DataBind();

                #endregion

                DistribuidorUnidadNegocio distribuidorUnidadNegocio =
                    new DistribuidorUnidadNegocio().
                        Select().Where(dun =>
                            dun.IdDistribuidor == idDistribuidorParam &&
                            dun.IdUnidadNegocio == idUnidadNegocioParam
                            ).FirstOrDefault();
                PlanComercial planComercial =
                    new PlanComercial().
                        Select().Where(pc =>
                            pc.IdDistribuidorUnidadNegocio == distribuidorUnidadNegocio.IdDistribuidorUnidadNegocio
                            ).FirstOrDefault();
                Session.Add("idPlanComercial", planComercial.IdPlanComercial.ToString());

                List<SDT> lista = obtenerSDTConArtefactos(
                    idResellerParam,
                    planComercial.IdDistribuidorUnidadNegocio,
                    planComercial.IdPlanComercial,
                    idUnidadNegocioParam);

                if (lista.Count > 0)
                {
                    DataTable dtSolicitudesDeTaller = new DataTable();
                    dtSolicitudesDeTaller.Columns.AddRange(new DataColumn[5]
                    {
                        new DataColumn("IdSDT",typeof(int)),
                        new DataColumn("IdDistribuidorUnidadNegocio",typeof(string)),
                        new DataColumn("SDTNro",typeof(string)),
                        new DataColumn("MailInforme",typeof(string)),
                        new DataColumn("FHAlta",typeof(string)),
                    });

                    foreach (SDT sdtRecorrer in lista)
                    {
                        if (sdtRecorrer != null)
                        {
                            dtSolicitudesDeTaller.Rows.Add(
                            sdtRecorrer.IdSDT,
                            sdtRecorrer.IdDistribuidorUnidadNegocio.ToString(),
                            sdtRecorrer.SDTNro.ToString(),
                            sdtRecorrer.MailInforme.ToString(),
                            sdtRecorrer.FHAlta.ToString()
                            );
                        }
                    }
                    obtenerArtefactosGestionar(Session["vPanel"].ToString(), lista);
                }

                lblCantidadPresupuestar.Text = gvGestionRecibidos.Rows.Count.ToString();
                lblCantidadEnEspera.Text = gvGestionDiagnosticados.Rows.Count.ToString();               
                lblCantidadAReparar.Text = (gvGestionAprobados.Rows.Count + gvGestionEnReparacion.Rows.Count).ToString();
                lblCantidadARemitar.Text = gvGestionReparados.Rows.Count.ToString();
            }
            catch
            {
                lblErrorHandler.Text = "Error al filtrar los artefactos pendientes de manufactura. No existe un plan comercial activo asignado a los filtros seleccionados";
                lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            }

            upErrorHandler.Update();
            upCantidadPresupuestar.Update();
            upCantidadEnEspera.Update();
            upCantidadAReparar.Update();
            upCantidadAremitar.Update();
            upGestionRecibidos.Update();
            upGestionDiagnosticados.Update();
            upGestionEnReparacion.Update();
            upGestionAprobados.Update();
            upGestionReparados.Update();
        }

        public int cantidadArtefactosSDT
        {
            get
            {
                int cantidadArtefactosSDT = gvGestionRecibidos.Rows.Count +
                                     gvGestionDiagnosticados.Rows.Count +
                                     gvGestionAprobados.Rows.Count +
                                     gvGestionEnReparacion.Rows.Count +
                                     gvGestionReparados.Rows.Count;
                return cantidadArtefactosSDT;
            }
        }

        public void obtenerArtefactosGestionar(string vPanel, List<SDT> listaSolicitudesDeTaller)
        {
            #region creacionDeDatatables

            DataTable dtArtefactosRecibidos = new DataTable();
            dtArtefactosRecibidos.Columns.AddRange(new DataColumn[4]
            {
                new DataColumn("IdSDTDetalle",typeof(int)),
                new DataColumn("SolNro",typeof(int)),
                new DataColumn("Serial",typeof(string)),
                new DataColumn("ArtefactoTipo",typeof(string)),
            });

            DataTable dtArtefactosDiagnosticados = new DataTable();
            dtArtefactosDiagnosticados.Columns.AddRange(new DataColumn[4]
            {
                new DataColumn("IdSDTDetalle",typeof(int)),
                new DataColumn("SolNro",typeof(int)),
                new DataColumn("Serial",typeof(string)),
                new DataColumn("ArtefactoTipo",typeof(string)),
            });

            DataTable dtArtefactosAprobados = new DataTable();
            dtArtefactosAprobados.Columns.AddRange(new DataColumn[4]
            {
                new DataColumn("IdSDTDetalle",typeof(int)),
                new DataColumn("SolNro",typeof(int)),
                new DataColumn("Serial",typeof(string)),
                new DataColumn("ArtefactoTipo",typeof(string)),
            });

            DataTable dtArtefactosEnReparacion = new DataTable();
            dtArtefactosEnReparacion.Columns.AddRange(new DataColumn[4]
            {
                new DataColumn("IdSDTDetalle",typeof(int)),
                new DataColumn("SolNro",typeof(int)),
                new DataColumn("Serial",typeof(string)),
                new DataColumn("ArtefactoTipo",typeof(string)),
            });

            DataTable dtArtefactosReparados = new DataTable();
            dtArtefactosReparados.Columns.AddRange(new DataColumn[5]
            {
                new DataColumn("IdSDTDetalle",typeof(int)),
                new DataColumn("SolNro",typeof(int)),
                new DataColumn("Serial",typeof(string)),
                new DataColumn("ArtefactoTipo",typeof(string)),
                new DataColumn("Precio",typeof(string)),
            });

            #endregion creacionDeDatatables

            try
            {
                ErrorHandler eh = new ErrorHandler();

                #region obtenerObjetosLista
                List<SDTDetalleEstado> listaEstados = new List<SDTDetalleEstado>();
                List<SDTDetalle> listaArtefactosTotales = new List<SDTDetalle>();
                List<SDTDetalleDiagnostico> listaDiagnosticosTotales = new List<SDTDetalleDiagnostico>();

                string arrayIDSDTWhere = "";
                int contadorArraySDT = 0;
                foreach (SDT sdtSetearArray in listaSolicitudesDeTaller)
                {
                    if (contadorArraySDT == 0)
                    {
                        arrayIDSDTWhere = sdtSetearArray.IdSDT.ToString();
                    }
                    else
                    {
                        arrayIDSDTWhere = arrayIDSDTWhere + ", " + sdtSetearArray.IdSDT.ToString();
                    }
                    contadorArraySDT++;
                }

                listaEstados = new SDTDetalleEstado().Select().ToList();
                listaArtefactosTotales = new SDTDetalle().Select("IdSDT in (" + arrayIDSDTWhere + ")");

                int contadorArraySDTDetalle = 0;
                string arrayIdSDTDetalleWhere = "";
                foreach (SDTDetalle sdtDetalleSetearArray in listaArtefactosTotales)
                {
                    if (contadorArraySDTDetalle == 0)
                    {
                        arrayIdSDTDetalleWhere = sdtDetalleSetearArray.IdSDTDetalle.ToString();
                    }
                    else
                    {
                        arrayIdSDTDetalleWhere = arrayIdSDTDetalleWhere + ", " + sdtDetalleSetearArray.IdSDTDetalle.ToString();
                    }
                    contadorArraySDTDetalle++;
                }

                listaDiagnosticosTotales = new SDTDetalleDiagnostico().Select("IdSDTDetalle in (" + arrayIdSDTDetalleWhere + ")");
                #endregion

                //Primer recorrido por las SDT para el seteo de los datatables
                foreach (SDT sdtPadreRecorrido in listaSolicitudesDeTaller)
                {
                    List<SDTDetalle> listasdtDetalleGestionParcial = new List<SDTDetalle>();
                    foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                    {
                        if (sdtDetalleRecorrer.IdSDT == sdtPadreRecorrido.IdSDT)
                        {
                            listasdtDetalleGestionParcial.Add(sdtDetalleRecorrer);
                        }
                    }

                    #region completarListasDeArtefactosGestionar
                    foreach (SDTDetalle artefacto in listasdtDetalleGestionParcial)
                    {
                        SDTDetalleEstado estado = new SDTDetalleEstado();
                        foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                        {
                            if (estadoRecorrer.IdSDTDetalleEstado == artefacto.IdSDTDetalleEstado)
                            {
                                estado = estadoRecorrer;
                            }
                        }
                        switch (estado.Descripcion)
                        {
                            case "Recibido":

                                //Artefactos en estado Recibido se pueden Diagnosticar
                                dtArtefactosRecibidos.Rows.Add(
                                    Convert.ToUInt32(artefacto.IdSDTDetalle),
                                    Convert.ToInt32(sdtPadreRecorrido.SDTNro),
                                    artefacto.IdSerial,
                                    artefacto.ArtefactoTipo
                                );
                                break;

                            case "Diagnosticado":

                                //Artefactos en estado Diagnosticado se pueden Aprobar, Cancelar y Pausar
                                dtArtefactosDiagnosticados.Rows.Add(
                                    Convert.ToUInt32(artefacto.IdSDTDetalle),
                                    Convert.ToInt32(sdtPadreRecorrido.SDTNro),
                                    artefacto.IdSerial,
                                    artefacto.ArtefactoTipo
                                );
                                break;

                            case "Aprobado":
                                dtArtefactosAprobados.Rows.Add(
                                    Convert.ToUInt32(artefacto.IdSDTDetalle),
                                    Convert.ToInt32(sdtPadreRecorrido.SDTNro),
                                    artefacto.IdSerial,
                                    artefacto.ArtefactoTipo
                                    );
                                break;

                            case "En reparacion":
                                dtArtefactosEnReparacion.Rows.Add(
                                    Convert.ToUInt32(artefacto.IdSDTDetalle),
                                    Convert.ToInt32(sdtPadreRecorrido.SDTNro),
                                    artefacto.IdSerial,
                                    artefacto.ArtefactoTipo
                                    );
                                break;

                            case "Reparado":
                                dtArtefactosReparados.Rows.Add(
                                    Convert.ToInt32(artefacto.IdSDTDetalle),
                                    Convert.ToInt32(sdtPadreRecorrido.SDTNro),
                                    artefacto.IdSerial,
                                    artefacto.ArtefactoTipo,
                                    artefacto.Precio.ToString()
                                    );
                                break;
                        }
                    }

                    dtArtefactosRecibidos.DefaultView.Sort = "SolNro Desc";
                    gvGestionRecibidos.DataSource = dtArtefactosRecibidos;
                    gvGestionRecibidos.DataBind();

                    dtArtefactosDiagnosticados.DefaultView.Sort = "SolNro Desc";
                    gvGestionDiagnosticados.DataSource = dtArtefactosDiagnosticados;
                    gvGestionDiagnosticados.DataBind();

                    dtArtefactosAprobados.DefaultView.Sort = "SolNro Desc";
                    gvGestionAprobados.DataSource = dtArtefactosAprobados;
                    gvGestionAprobados.DataBind();

                    dtArtefactosEnReparacion.DefaultView.Sort = "SolNro Desc";
                    gvGestionEnReparacion.DataSource = dtArtefactosEnReparacion;
                    gvGestionEnReparacion.DataBind();

                    dtArtefactosReparados.DefaultView.Sort = "SolNro Desc";
                    gvGestionReparados.DataSource = dtArtefactosReparados;
                    gvGestionReparados.DataBind();

                    #endregion seteoDeDatatables
                }

                /*INICIO. Obtener los nombres de los reseller de cada solicitud. Requerimiento 2019/02/05*/
                DataTable dtResellers = new DataTable();
                dtResellers.Columns.AddRange(new DataColumn[3]
                {
                new DataColumn("IdReseller",typeof(string)),
                new DataColumn("Nombre",typeof(string)),
                new DataColumn("IdUnidadNegocio",typeof(string))
                });
                
                using (Tn transaccionSelectResellers = new Tn("CGADM"))
                {
                    foreach (SDT solicitudDeTallerConsultarReseller in listaSolicitudesDeTaller)
                    {
                        if (solicitudDeTallerConsultarReseller.IdReseller != 0)
                        {
                            if (solicitudDeTallerConsultarReseller.IdUnidadNegocio == 1)
                            {
                                bool vExiste = false;
                                foreach (DataRow dtRowResellerRecorrer in dtResellers.Rows)
                                {
                                    if (solicitudDeTallerConsultarReseller.IdReseller == dtRowResellerRecorrer["IdReseller"].ToString().ToInt())
                                    {
                                        vExiste = true;
                                    }
                                }

                                if (!vExiste)
                                {
                                    DataTable dtGetReseller = new SP("CGADM").Execute("usp_GetVendingResellerById", P.Add("idReseller", solicitudDeTallerConsultarReseller.IdReseller));
                                    foreach (DataRow dtRow in dtGetReseller.Rows)
                                    {
                                        string resellerNombre = dtRow["RazonSocial"].ToString();
                                        dtResellers.Rows.Add(
                                            solicitudDeTallerConsultarReseller.IdReseller.ToString(), 
                                            resellerNombre, 
                                            solicitudDeTallerConsultarReseller.IdUnidadNegocio.ToString()
                                            );
                                    }
                                }
                            }

                            if (solicitudDeTallerConsultarReseller.IdUnidadNegocio == 2)
                            {
                                bool vExiste = false;
                                foreach (DataRow dtRowResellerRecorrer in dtResellers.Rows)
                                {
                                    if (solicitudDeTallerConsultarReseller.IdReseller == dtRowResellerRecorrer["IdReseller"].ToString().ToInt())
                                    {
                                        vExiste = true;
                                    }
                                }

                                if (!vExiste)
                                {
                                    DataTable dtGetReseller = new SP("CGADM").Execute("usp_GetResellerAccessById", P.Add("idReseller", solicitudDeTallerConsultarReseller.IdReseller));
                                    foreach (DataRow dtRow in dtGetReseller.Rows)
                                    {
                                        string resellerNombre = dtRow["RazonSocial"].ToString();
                                        dtResellers.Rows.Add(solicitudDeTallerConsultarReseller.IdReseller.ToString(), resellerNombre, solicitudDeTallerConsultarReseller.IdUnidadNegocio.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
                /*FIN. Obtener los nombres de los reseller de cada solicitud. Requerimiento 2019/02/05*/

                //Segundo recorrido en listado de SDT para actualizar los datos de las tablas, ya con los datatables generados
                foreach (SDT sdtPadre2doRecorrido in listaSolicitudesDeTaller)
                {
                    //Luego de asignar los datatables a las gridView de gestiones, las recorremos para asignarles a cada una los valores correspondientes en la columna DESCRIPPCION y DIAGNOSTICO
                    switch (vPanel)
                    {
                        case "taller":
                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS EN ESPERA
                            SDTDetalleEstado estadoGestionarEnEspera = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "Diagnosticado")
                                {
                                    estadoGestionarEnEspera = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvAprobacionRow in gvGestionDiagnosticados.Rows)
                            {
                                string vIdSDTDetalle = gvAprobacionRow.Cells[0].Text;
                                SDTDetalle sdtDetalleAprobacion = new SDTDetalle();
                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarEnEspera.IdSDTDetalleEstado &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleAprobacion = sdtDetalleRecorrer;
                                    }
                                }

                                DataTable dtDiagnosticosAprobar = new DataTable();
                                dtDiagnosticosAprobar.Columns.AddRange(new DataColumn[3]
                                {
                                        new DataColumn("IdSDTDetalleDiagnostico",typeof(string)),
                                        new DataColumn("Presupuesto",typeof(string)),
                                        new DataColumn("Diagnostico",typeof(string)),
                                });

                                foreach (SDTDetalleDiagnostico diagnosticoDelArtefacto in listaDiagnosticosTotales)
                                {
                                    if (diagnosticoDelArtefacto.IdSDTDetalle == sdtDetalleAprobacion.IdSDTDetalle && diagnosticoDelArtefacto.FHBaja == null)
                                    {
                                        dtDiagnosticosAprobar.
                                        Rows.
                                            Add(
                                                diagnosticoDelArtefacto.
                                                    IdSDTDetalleDiagnostico.ToString(),
                                                    "$ " + diagnosticoDelArtefacto.Presupuesto.ToString(),
                                                    diagnosticoDelArtefacto.Descripcion
                                                );
                                    }
                                }

                                foreach (DataRow dtRowRecorrerSDT in dtResellers.Rows)
                                {
                                    if (dtRowRecorrerSDT["IdReseller"].ToString().ToInt() == sdtPadre2doRecorrido.IdReseller && sdtPadre2doRecorrido.IdSDT == sdtDetalleAprobacion.IdSDT)
                                    {
                                        ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblReseller")).Text = "Reseller: "+dtRowRecorrerSDT["Nombre"].ToString();
                                    }
                                }

                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticosAprobar;
                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).BackColor = System.Drawing.Color.LightSteelBlue;
                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataBind();
                                ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleAprobacion.Descripcion;
                                ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleAprobacion.Incidencia;
                                ((ucSDTDetalleFajaIngreso)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("ucSDTDetalleFajaIngresoRow")).FajaIngreso = sdtDetalleAprobacion.IdSDTDetalle.ToString();
                            }
                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS RECIBIDOS
                            SDTDetalleEstado estadoGestionarRecibido = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "Recibido")
                                {
                                    estadoGestionarRecibido = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvRecibidosRow in gvGestionRecibidos.Rows)
                            {
                                string vIdSDTDetalle = gvRecibidosRow.Cells[0].Text;
                                SDTDetalle sdtDetalleDiagnosticar = new SDTDetalle();

                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarRecibido.IdSDTDetalleEstado
                                        &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleDiagnosticar = sdtDetalleRecorrer;
                                    }
                                }

                                DataTable dtDiagnosticos = new DataTable();
                                dtDiagnosticos.Columns.AddRange(new DataColumn[3]
                                {
                                        new DataColumn("IdSDTDetalleDiagnostico",typeof(int)),
                                        new DataColumn("Presupuesto",typeof(string)),
                                        new DataColumn("Diagnostico",typeof(string)),
                                });

                                foreach (SDTDetalleDiagnostico diagnosticoDelArtefacto in listaDiagnosticosTotales)
                                {
                                    if (
                                        diagnosticoDelArtefacto.IdSDTDetalle == sdtDetalleDiagnosticar.IdSDTDetalle &&
                                        diagnosticoDelArtefacto.FHBaja == null
                                        )
                                    {
                                        dtDiagnosticos.
                                        Rows.
                                            Add(
                                                diagnosticoDelArtefacto.
                                                    IdSDTDetalleDiagnostico.ToString(),
                                                    "$ " + diagnosticoDelArtefacto.Presupuesto.ToString(),
                                                    diagnosticoDelArtefacto.Descripcion
                                                );
                                    }
                                }

                                foreach (DataRow dtRowRecorrerSDT in dtResellers.Rows)
                                {
                                    if (dtRowRecorrerSDT["IdReseller"].ToString().ToInt() == sdtPadre2doRecorrido.IdReseller && sdtPadre2doRecorrido.IdSDT == sdtDetalleDiagnosticar.IdSDT)
                                    {
                                        ((Label)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[1].FindControl("lblReseller")).Text = "Reseller: " + dtRowRecorrerSDT["Nombre"].ToString();
                                    }
                                }

                                ((TextBox)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[4].FindControl("txtIdSerial")).Text = sdtDetalleDiagnosticar.IdSerial.Trim();
                                ((GridView)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[2].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticos;
                                ((GridView)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[2].FindControl("gvDiagnosticos")).BackColor = System.Drawing.Color.LightSteelBlue;
                                ((GridView)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[2].FindControl("gvDiagnosticos")).DataBind();
                                ((Label)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleDiagnosticar.Descripcion;
                                ((Label)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleDiagnosticar.Incidencia;
                                ((ucSDTDetalleFajaIngreso)gvGestionRecibidos.Rows[gvRecibidosRow.RowIndex].Cells[1].FindControl("ucSDTDetalleFajaIngresoRow")).FajaIngreso = sdtDetalleDiagnosticar.IdSDTDetalle.ToString();
                            }
                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS APROBADOS
                            SDTDetalleEstado estadoGestionarAprobado = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "Aprobado")
                                {
                                    estadoGestionarAprobado = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvGestionAprobadosRow in gvGestionAprobados.Rows)
                            {
                                string vIdSDTDetalle = gvGestionAprobadosRow.Cells[0].Text;
                                SDTDetalle sdtDetalleGestionReparacion = new SDTDetalle();
                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarAprobado.IdSDTDetalleEstado &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleGestionReparacion = sdtDetalleRecorrer;
                                    }
                                }

                                DataTable dtDiagnosticos = new DataTable();
                                dtDiagnosticos.Columns.AddRange(new DataColumn[3]
                                {
                                        new DataColumn("IdSDTDetalleDiagnostico",typeof(string)),
                                        new DataColumn("Presupuesto",typeof(string)),
                                        new DataColumn("Diagnostico",typeof(string)),
                                });

                                foreach (SDTDetalleDiagnostico diagnosticoDelArtefacto in listaDiagnosticosTotales)
                                {
                                    if (
                                        diagnosticoDelArtefacto.IdSDTDetalle == sdtDetalleGestionReparacion.IdSDTDetalle &&
                                        diagnosticoDelArtefacto.FHBaja == null &&
                                        diagnosticoDelArtefacto.Aprobado == 1
                                        )
                                    {
                                        dtDiagnosticos.
                                        Rows.
                                            Add(
                                                diagnosticoDelArtefacto.
                                                    IdSDTDetalleDiagnostico.ToString(),
                                                    "$ " + diagnosticoDelArtefacto.Presupuesto.ToString(),
                                                    diagnosticoDelArtefacto.Descripcion
                                                );
                                    }
                                }

                                foreach (DataRow dtRowRecorrerSDT in dtResellers.Rows)
                                {
                                    if (dtRowRecorrerSDT["IdReseller"].ToString().ToInt() == sdtPadre2doRecorrido.IdReseller && sdtPadre2doRecorrido.IdSDT == sdtDetalleGestionReparacion.IdSDT)
                                    {
                                        ((Label)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[1].FindControl("lblReseller")).Text = "Reseller: " + dtRowRecorrerSDT["Nombre"].ToString();
                                    }
                                }

                                ((GridView)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticos;
                                ((GridView)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).BackColor = System.Drawing.Color.LightSteelBlue;
                                ((GridView)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataBind();
                                ((Label)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleGestionReparacion.Descripcion;
                                ((Label)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleGestionReparacion.Incidencia;
                                ((ucSDTDetalleFajaIngreso)gvGestionAprobados.Rows[gvGestionAprobadosRow.RowIndex].Cells[1].FindControl("ucSDTDetalleFajaIngresoRow")).FajaIngreso = sdtDetalleGestionReparacion.IdSDTDetalle.ToString();
                            }
                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS EN REPARACION
                            SDTDetalleEstado estadoGestionarEnReparacion = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "En reparacion")
                                {
                                    estadoGestionarEnReparacion = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvGestionEnReparacionRow in gvGestionEnReparacion.Rows)
                            {
                                string vIdSDTDetalle = gvGestionEnReparacionRow.Cells[0].Text;
                                SDTDetalle sdtDetalleGestionEnReparacion = new SDTDetalle();
                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarEnReparacion.IdSDTDetalleEstado &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleGestionEnReparacion = sdtDetalleRecorrer;
                                    }
                                }

                                DataTable dtDiagnosticos = new DataTable();
                                dtDiagnosticos.Columns.AddRange(new DataColumn[3]
                                {
                                        new DataColumn("IdSDTDetalleDiagnostico",typeof(string)),
                                        new DataColumn("Presupuesto",typeof(string)),
                                        new DataColumn("Diagnostico",typeof(string)),
                                });

                                foreach (SDTDetalleDiagnostico diagnosticoDelArtefacto in listaDiagnosticosTotales)
                                {
                                    if (
                                        diagnosticoDelArtefacto.IdSDTDetalle == sdtDetalleGestionEnReparacion.IdSDTDetalle &&
                                        diagnosticoDelArtefacto.FHBaja == null &&
                                        diagnosticoDelArtefacto.Aprobado == 1
                                        )
                                    {
                                        dtDiagnosticos.
                                        Rows.
                                            Add(
                                                diagnosticoDelArtefacto.
                                                    IdSDTDetalleDiagnostico.ToString(),
                                                    "$ " + diagnosticoDelArtefacto.Presupuesto.ToString(),
                                                    diagnosticoDelArtefacto.Descripcion
                                                );
                                    }
                                }

                                foreach (DataRow dtRowRecorrerSDT in dtResellers.Rows)
                                {
                                    if (dtRowRecorrerSDT["IdReseller"].ToString().ToInt() == sdtPadre2doRecorrido.IdReseller && sdtPadre2doRecorrido.IdSDT == sdtDetalleGestionEnReparacion.IdSDT)
                                    {
                                        ((Label)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[1].FindControl("lblReseller")).Text = "Reseller: " + dtRowRecorrerSDT["Nombre"].ToString();
                                    }
                                }

                                ((GridView)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticos;
                                ((GridView)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).BackColor = System.Drawing.Color.LightSteelBlue;
                                ((GridView)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataBind();
                                ((Label)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleGestionEnReparacion.Descripcion;
                                ((Label)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleGestionEnReparacion.Incidencia;
                                ((ucSDTDetalleFajaIngreso)gvGestionEnReparacion.Rows[gvGestionEnReparacionRow.RowIndex].Cells[1].FindControl("ucSDTDetalleFajaIngresoRow")).FajaIngreso = sdtDetalleGestionEnReparacion.IdSDTDetalle.ToString();
                            }
                            //////////////////////////////////////////////////////////////////


                            break;

                        case "administracion":

                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS DIAGNOSTICADOS
                            SDTDetalleEstado estadoGestionarDiagnosticado = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "Diagnosticado")
                                {
                                    estadoGestionarDiagnosticado = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvAprobacionRow in gvGestionDiagnosticados.Rows)
                            {
                                string vIdSDTDetalle = gvAprobacionRow.Cells[0].Text;
                                SDTDetalle sdtDetalleAprobacion = new SDTDetalle();
                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarDiagnosticado.IdSDTDetalleEstado &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleAprobacion = sdtDetalleRecorrer;
                                    }
                                }

                                DataTable dtDiagnosticosAprobar = new DataTable();
                                dtDiagnosticosAprobar.Columns.AddRange(new DataColumn[3]
                                {
                                        new DataColumn("IdSDTDetalleDiagnostico",typeof(string)),
                                        new DataColumn("Presupuesto",typeof(string)),
                                        new DataColumn("Diagnostico",typeof(string)),
                                });

                                foreach (SDTDetalleDiagnostico diagnosticoDelArtefacto in listaDiagnosticosTotales)
                                {
                                    if (diagnosticoDelArtefacto.IdSDTDetalle == sdtDetalleAprobacion.IdSDTDetalle && diagnosticoDelArtefacto.FHBaja == null)
                                    {
                                        dtDiagnosticosAprobar.
                                        Rows.
                                            Add(
                                                diagnosticoDelArtefacto.
                                                    IdSDTDetalleDiagnostico.ToString(),
                                                    "$ " + diagnosticoDelArtefacto.Presupuesto.ToString(),
                                                    diagnosticoDelArtefacto.Descripcion
                                                );
                                    }
                                }

                                foreach (DataRow dtRowRecorrerSDT in dtResellers.Rows)
                                {
                                    if (dtRowRecorrerSDT["IdReseller"].ToString().ToInt() == sdtPadre2doRecorrido.IdReseller && sdtPadre2doRecorrido.IdSDT == sdtDetalleAprobacion.IdSDT)
                                    {
                                        ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblReseller")).Text = "Reseller: " + dtRowRecorrerSDT["Nombre"].ToString();
                                    }
                                }

                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticosAprobar;
                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).BackColor = System.Drawing.Color.LightSteelBlue;
                                ((GridView)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[5].FindControl("gvDiagnosticos")).DataBind();
                                ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleAprobacion.Descripcion;
                                ((Label)gvGestionDiagnosticados.Rows[gvAprobacionRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleAprobacion.Incidencia;
                            }

                            //////////////////////////////////////////////////////////////////
                            //ARTEFACTOS REPARADOS
                            SDTDetalleEstado estadoGestionarReparado = new SDTDetalleEstado();
                            foreach (SDTDetalleEstado estadoRecorrer in listaEstados)
                            {
                                if (estadoRecorrer.Descripcion == "Reparado")
                                {
                                    estadoGestionarReparado = estadoRecorrer;
                                }
                            }

                            foreach (GridViewRow gvGestionReparadoRow in gvGestionReparados.Rows)
                            {
                                string vIdSDTDetalle = gvGestionReparadoRow.Cells[0].Text;
                                SDTDetalle sdtDetalleGestionReparado = new SDTDetalle();
                                foreach (SDTDetalle sdtDetalleRecorrer in listaArtefactosTotales)
                                {
                                    if (
                                        sdtDetalleRecorrer.IdSDTDetalleEstado == estadoGestionarReparado.IdSDTDetalleEstado &&
                                        sdtDetalleRecorrer.IdSDTDetalle == Convert.ToInt32(vIdSDTDetalle)
                                        )
                                    {
                                        sdtDetalleGestionReparado = sdtDetalleRecorrer;
                                    }
                                }

                                ((Label)gvGestionReparados.Rows[gvGestionReparadoRow.RowIndex].Cells[1].FindControl("lblArtefacto")).Text = sdtDetalleGestionReparado.Descripcion;
                                ((Label)gvGestionReparados.Rows[gvGestionReparadoRow.RowIndex].Cells[1].FindControl("lblIncidencia")).Text = "INCIDENCIA: " + sdtDetalleGestionReparado.Incidencia;
                            }

                            upGestionReparados.Update();
                            //////////////////////////////////////////////////////////////////

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Message("Error en 'obtenerArtefactos'", ex.Message, MessagesTypes.error);
            }
        }

        protected void gvDiagnosticos1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Completar código para eliminar los diagnosticos individualmente

            Message(
                "Prueba eliminar",
                "Funcionalidad en desarrollo",
                MessagesTypes.info);
        }

        protected void gvGestionAprobados_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            string vIdSDTDetalle = gvGestionAprobados.Rows[e.NewSelectedIndex].Cells[0].Text;
            ErrorHandler artefactoCambioEstado = cambiarEstadoArtefacto(Convert.ToInt32(vIdSDTDetalle), "Aprobado", "En reparacion");
            if (artefactoCambioEstado.Mensaje == "EXITO")
            {

                lblErrorHandler.Text = "Gestión de artefacto procesada exitosamente! Se ha enviado el hardware a estado 'En reparación'";
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();
                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionAprobados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            string vIdSDTDetalle = gvGestionAprobados.Rows[e.RowIndex].Cells[0].Text;
            ErrorHandler artefactoCambioEstado = cambiarEstadoArtefacto(Convert.ToInt32(vIdSDTDetalle), "Aprobado", "Recibido");
            if (artefactoCambioEstado.Mensaje == "EXITO")
            {

                lblErrorHandler.Text = "Gestión de artefacto procesada exitosamente! Se ha devuelto el hardware a la etapa de diagnóstico";
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();
                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionEnReparacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            string vIdSDTDetalle = gvGestionEnReparacion.Rows[e.RowIndex].Cells[0].Text;
            ErrorHandler artefactoCambioEstado = cambiarEstadoArtefacto(Convert.ToInt32(vIdSDTDetalle), "En reparacion", "Recibido");
            if (artefactoCambioEstado.Mensaje == "EXITO")
            {

                lblErrorHandler.Text = "Gestión de artefacto procesada exitosamente! Se ha devuelto el hardware a la etapa de diagnóstico";
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();
                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionEnReparacion_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            string vIdSDTDetalle = gvGestionEnReparacion.Rows[e.NewSelectedIndex].Cells[0].Text;
            ErrorHandler artefactoCambioEstado = cambiarEstadoArtefacto(Convert.ToInt32(vIdSDTDetalle), "En reparacion", "Reparado");
            if (artefactoCambioEstado.Mensaje == "EXITO")
            {

                lblErrorHandler.Text = "Gestión de artefacto procesada exitosamente! Se ha marcado el artefacto como 'Reparado'";
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();
                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionDiagnosticados_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            ErrorHandler eh = new ErrorHandler();
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;

            using (Tn transaccionDiagnosticoAprobar = new Tn("CGADM"))
            {
                string vSolNro = gvGestionDiagnosticados.Rows[e.NewSelectedIndex].Cells[2].Text;
                string vSerial = gvGestionDiagnosticados.Rows[e.NewSelectedIndex].Cells[3].Text;
                string vArtefactoTipo = gvGestionDiagnosticados.Rows[e.NewSelectedIndex].Cells[4].Text;

                ////agregar transaccion para todo el evento
                SDT sdtPadre = new SDT().Select(transaccionDiagnosticoAprobar).Where(sol =>
                                            sol.SDTNro == Convert.ToInt32(vSolNro) &&
                                            sol.FHBaja == null &&
                                            sol.FHFinalizacion == null
                                            ).FirstOrDefault();
                SDTDetalle artefactoPadre = new SDTDetalle();
                SDTDetalleDiagnostico diagnosticoAprobar = new SDTDetalleDiagnostico();
                if (sdtPadre == null)
                {
                    eh.Mensaje = "Concurrencia de gestiones";
                    eh.Descripcion = "La SDT ha sido modificada desde una gestión concurrente o ya no se encuentra disponible para la transacción, por favor intente nuevamente";
                }
                else
                {
                    SDTDetalleEstado estado = new SDTDetalleEstado().Select(transaccionDiagnosticoAprobar).Where(est => est.Descripcion == "Diagnosticado").FirstOrDefault();
                    artefactoPadre =
                        new SDTDetalle().Select(transaccionDiagnosticoAprobar).Where(sdtd =>
                            sdtd.IdSDT == sdtPadre.IdSDT &&
                            sdtd.IdSerial == vSerial &&
                            sdtd.ArtefactoTipo == vArtefactoTipo &&
                            sdtd.IdSDTDetalleEstado == estado.IdSDTDetalleEstado
                            ).FirstOrDefault();
                }
                if (artefactoPadre == null)
                {
                    eh.Mensaje = "Concurrencia de gestiones";
                    eh.Descripcion = "El artefacto ha sido modificado desde una gestión concurrente o ya no se encuentra disponible para la transacción, por favor intente nuevamente";
                }

                if (eh.Mensaje == null)
                {
                    List<SDTDetalleDiagnostico> listaDiagnosticosAprobar = new List<SDTDetalleDiagnostico>();
                    GridView gvDiagnosticosPrecargados = ((GridView)gvGestionDiagnosticados.Rows[e.NewSelectedIndex].Cells[5].FindControl("gvDiagnosticos"));
                    foreach (GridViewRow rowDiagnostico in gvDiagnosticosPrecargados.Rows)
                    {
                        if (rowDiagnostico.RowType == DataControlRowType.DataRow)
                        {
                            try
                            {
                                string vIdDiagnostico = rowDiagnostico.Cells[0].Text;
                                string precio = rowDiagnostico.Cells[1].Text.Replace("$", "").Trim();
                                string diagnostico = rowDiagnostico.Cells[2].Text;

                                diagnosticoAprobar = new SDTDetalleDiagnostico().Select(transaccionDiagnosticoAprobar).Where(diag =>
                                    diag.IdSDTDetalle == artefactoPadre.IdSDTDetalle &&
                                    diag.IdSDTDetalleDiagnostico == Convert.ToInt32(vIdDiagnostico) &&
                                    diag.FHBaja == null
                                    ).FirstOrDefault();
                                Boolean vAprobar = ((CheckBox)rowDiagnostico.Cells[3].FindControl("cbxAgregar")).Checked;
                                if (vAprobar == true)
                                {
                                    if (diagnosticoAprobar != null)
                                    {
                                        diagnosticoAprobar.Aprobado = 1;
                                        diagnosticoAprobar.FHAprobacion = DateTime.Now;
                                        listaDiagnosticosAprobar.Add(diagnosticoAprobar);
                                    }
                                    else
                                    {
                                        eh.Mensaje = "Concurrencia de gestiones";
                                        eh.Descripcion = "El artefacto ha sido modificado desde una gestión concurrente o ya no se encuentra disponible para la transacción, por favor intente nuevamente";
                                        break;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                eh.Mensaje = "Error listando diagnosticos";
                                eh.Descripcion = "Error: " + ex.Message;
                                break;
                            }
                        }
                    }
                    if (listaDiagnosticosAprobar.Count > 0)
                    {
                        SDTDetalleGestion gestionAprobacion = new SDTDetalleGestion();
                        gestionAprobacion.IdCuenta = GetIdCuentaFromCookie();
                        SDTDetalleEstado estado = new SDTDetalleEstado().Select(transaccionDiagnosticoAprobar).Where(est => est.Descripcion == "Aprobado").FirstOrDefault();
                        gestionAprobacion.IdSDTDetalleEstado = estado.IdSDTDetalleEstado;
                        gestionAprobacion.FHAlta = DateTime.Now;
                        gestionAprobacion.Insert(transaccionDiagnosticoAprobar);

                        artefactoPadre.FHUltimaGestion = DateTime.Now;

                        //Nuevo requerimiento. Almacenar historial de gestiones 06/12/2018

                        artefactoPadre.IdSDTDetalleEstado = estado.IdSDTDetalleEstado;
                        artefactoPadre.Update(transaccionDiagnosticoAprobar);

                        SDTDetalleGestionHistorial gestionHistorial = new SDTDetalleGestionHistorial();
                        gestionHistorial.IdSDTDetalle = artefactoPadre.IdSDTDetalle;
                        gestionHistorial.IdSDTDetalleGestion = gestionAprobacion.IdSDTDetalleGestion;

                        gestionHistorial.Insert(transaccionDiagnosticoAprobar);

                        foreach (SDTDetalleDiagnostico diagnosticoAgregado in listaDiagnosticosAprobar)
                        {
                            diagnosticoAgregado.Update(transaccionDiagnosticoAprobar);
                        }

                        transaccionDiagnosticoAprobar.Commit();

                        lblErrorHandler.Text = "Gestión de aprobación procesada exitosamente!";
                        lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                        upErrorHandler.Update();
                        filtrarProductos();
                    }
                    else
                    {
                        eh.Mensaje = "No ha seleccionado diagnósticos";
                        eh.Descripcion = "";
                    }
                }
            }

            if (eh.Mensaje != null)
            {
                lblErrorHandler.Text = eh.Mensaje + " " + eh.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();

                Message(eh.Mensaje, eh.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionDiagnosticados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            string vIdSDTDetalle = gvGestionDiagnosticados.Rows[e.RowIndex].Cells[0].Text;
            ErrorHandler artefactoCambioEstado = new ErrorHandler();

            artefactoCambioEstado = cambiarEstadoArtefacto(Convert.ToInt32(vIdSDTDetalle), "Diagnosticado", "Cancelado");

            if (artefactoCambioEstado.Mensaje == "EXITO")
            {
                lblErrorHandler.Text = "Gestión de cancelación procesada exitosamente!";
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();

                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionRecibidos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "VerDetalle":

                    //int index = Convert.ToInt32(e.CommandArgument);
                    //int index2 = Convert.ToInt32(e.CommandArgument);

                    break;
            }
        }

        protected void gvGestionRecibidos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            lblErrorHandler.Text = "";
            lblErrorHandler.ForeColor = System.Drawing.Color.Red;
            int pIdSDTDetalle = Convert.ToInt32(gvGestionRecibidos.Rows[e.RowIndex].Cells[0].Text);

            //Obtenemos un listado de los diagnósticos que ya existen en la BD
            List<SDTDetalleDiagnostico> listaDiagnosticosNoRepetir = new SDTDetalleDiagnostico().Select().Where(diag => diag.IdSDTDetalle == pIdSDTDetalle).ToList();
            List<SDTDetalleDiagnostico> listaDiagnosticosImpactar = new List<SDTDetalleDiagnostico>();

            //primero obtener la grilla de la celda 1
            GridView gvDiagnosticos = (GridView)gvGestionRecibidos.Rows[e.RowIndex].Cells[2].FindControl("gvDiagnosticos");

            //luego recorrer la grilla obtenida dinamicamente
            foreach (GridViewRow gvRowDiagnostico in gvDiagnosticos.Rows)
            {
                string vSubPrecio = gvRowDiagnostico.Cells[0].Text;
                string vSubDiagnostico = gvRowDiagnostico.Cells[1].Text;
                SDTDetalleDiagnostico sdtDetalleDiagnosticoAgregar = new SDTDetalleDiagnostico();
                sdtDetalleDiagnosticoAgregar.Presupuesto = Convert.ToDecimal(vSubPrecio.Replace("$", "").Trim());
                sdtDetalleDiagnosticoAgregar.Descripcion = vSubDiagnostico;
                sdtDetalleDiagnosticoAgregar.IdSDTDetalle = pIdSDTDetalle;

                bool vDiagnosticoExite = false;

                //Por cada diagnostico preexistente en la BD, identificamos el registro para apartarlo y no duplicarlo
                foreach (SDTDetalleDiagnostico diagnosticoEnBD in listaDiagnosticosNoRepetir)
                {
                    if (Convert.ToDecimal(vSubPrecio.Replace("$", "").Trim()) == diagnosticoEnBD.Presupuesto && vSubDiagnostico == diagnosticoEnBD.Descripcion)
                    {
                        vDiagnosticoExite = true;
                    }
                }

                if (!vDiagnosticoExite)
                {
                    listaDiagnosticosImpactar.Add(sdtDetalleDiagnosticoAgregar);
                }
            }

            //creación del objeto que impactará la gestión de diagnóstico
            ErrorHandler artefactoCambioEstado = new ErrorHandler();
            if (listaDiagnosticosImpactar.Count > 0)
            {
                foreach (SDTDetalleDiagnostico diagnosticoImpactar in listaDiagnosticosImpactar)
                {
                    diagnosticoImpactar.Insert();
                }

                //Luego de recorrer los diagnósticos, insertarlos asociados al artefacto, dejando éste último disponible para aprobar/cancelar/etc
                artefactoCambioEstado = cambiarEstadoArtefacto(pIdSDTDetalle, "Recibido", "Diagnosticado");
                if (artefactoCambioEstado.Descripcion == "EXITO")
                {
                    artefactoCambioEstado.Descripcion = "Se asignaron " + listaDiagnosticosImpactar.Count.ToString() + " diagnósticos nuevos al artefacto";
                }
            }
            else
            {
                /*
                 * Si listaDiagnosticosNoRepetir no contiene resultados, se envía a diagnóstico con los mismos sdtDetalleDiagnostico
                 * con los que fue devuelto a estado "Recibido".
                 * Se permite pero se notifica al usuario que se envía a aprobación con los mismos diagnósticos con los que
                 * fue devuelto desde administración, por lo que podría ser un error de operatoria, pero se deja a criterio del técnico
                 */
                if (listaDiagnosticosNoRepetir.Count > 0)
                {
                    artefactoCambioEstado = cambiarEstadoArtefacto(pIdSDTDetalle, "Recibido", "Diagnosticado");
                    if (artefactoCambioEstado.Mensaje == "EXITO")
                    {
                        artefactoCambioEstado.Descripcion = "No se han asignado nuevos diagnósticos, pero se envía con los diagnósticos precargados";
                    }
                }
                else
                {
                    /* En éste caso no se permite solicitar aprobaciones de artefactos que no han sido diangosticados por el área de taller
                     */
                    artefactoCambioEstado.Mensaje = "No se han cargado diagnósticos asociados al artefacto";
                    artefactoCambioEstado.Descripcion = "Debe ingresar al menos 1 diagnóstico para el artefacto en cuestión, por favor reintente nuevamente";
                }
            }

            if (artefactoCambioEstado.Mensaje == "EXITO")
            {
                //Se agrega la posibilidad de cambiar el id de serial del artefacto
                SDTDetalle sdtDetalleModificado = new SDTDetalle().Select().Where(sdtd => sdtd.IdSDTDetalle == pIdSDTDetalle).FirstOrDefault();
                if (sdtDetalleModificado != null)
                {
                    TextBox txtIdSerial = (TextBox)gvGestionRecibidos.Rows[e.RowIndex].Cells[4].FindControl("txtIdSerial");
                    sdtDetalleModificado.IdSerial = txtIdSerial.Text;
                    sdtDetalleModificado.Update();
                }

                lblErrorHandler.Text = "Gestión de diagnóstico procesada exitosamente! " + artefactoCambioEstado.Descripcion;
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
                upErrorHandler.Update();
                filtrarProductos();
            }
            else
            {
                lblErrorHandler.Text = artefactoCambioEstado.Mensaje + ". " + artefactoCambioEstado.Descripcion;
                upErrorHandler.Update();
                filtrarProductos();

                Message(artefactoCambioEstado.Mensaje, artefactoCambioEstado.Descripcion, MessagesTypes.warning);
            }
        }

        protected void gvGestionRecibidos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            TextBox txtPresupuesto = ((TextBox)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("txtPresupuesto"));
            TextBox txtDiagnostico = ((TextBox)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("txtDiagnostico"));
            if (txtPresupuesto.Text == "" || txtDiagnostico.Text == "")
            {
                Message("Presupuesto incompleto ", "Debe ingresar el monto del presupuesto y la descripción asociada", MessagesTypes.warning);
            }
            else
            {
                bool diagnosticoExiste = false;
                GridView gvDiagnosticosPrecargados = ((GridView)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("gvDiagnosticos"));
                DataTable dtDiagnosticosPrecargados = new DataTable();
                dtDiagnosticosPrecargados.Columns.AddRange(new DataColumn[2]
                {
                    new DataColumn("Presupuesto", typeof(string)),
                    new DataColumn("Diagnostico", typeof(string))
                });
                foreach (GridViewRow rowDiagnostico in gvDiagnosticosPrecargados.Rows)
                {
                    if (rowDiagnostico.RowType == DataControlRowType.DataRow)
                    {
                        try
                        {
                            string precio = rowDiagnostico.Cells[0].Text.Replace("$", "").Trim();
                            string diagnostico = rowDiagnostico.Cells[1].Text;
                            if (precio == txtPresupuesto.Text && diagnostico == txtDiagnostico.Text)
                            {
                                diagnosticoExiste = true;
                                Message("Diagnóstico duplicado", "Está intentando agregar un diagnóstico ya asignado para el artefacto en cuestión", MessagesTypes.warning);
                                break;
                            }
                            else
                            {
                                dtDiagnosticosPrecargados.Rows.Add(" $ " + precio, diagnostico);
                            }
                        }
                        catch (Exception ex)
                        {
                            Message("Error listando diagnosticos", "Error: " + ex.Message, MessagesTypes.error);
                        }
                    }
                }

                if (!diagnosticoExiste)
                {
                    dtDiagnosticosPrecargados.Rows.Add(" $ " + txtPresupuesto.Text, txtDiagnostico.Text);
                    ((GridView)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("gvDiagnosticos")).DataSource = dtDiagnosticosPrecargados;
                    ((GridView)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("gvDiagnosticos")).DataBind();
                    ((TextBox)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("txtPresupuesto")).Text = "";
                    ((TextBox)gvGestionRecibidos.Rows[e.NewSelectedIndex].Cells[2].FindControl("txtDiagnostico")).Text = "";
                    upGestionRecibidos.Update();
                }
                gvGestionRecibidos.Rows[e.NewSelectedIndex].BackColor = System.Drawing.Color.LightSteelBlue;
            }
        }

        protected void btnIrARemitar_Click(object sender, EventArgs e)
        {
            Utils.NavigateTo("Remitar.aspx");
        }
    }
}