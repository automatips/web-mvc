﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;
using System.Data;



namespace AdmSys.Controls
{
    public partial class ucProductoServicioCarga : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargaPantalla();
            }
        }

        public void setearStatusHabilitacion(bool vHabilitado)
        {
            ddlUnidadNegocio.Enabled = vHabilitado;
            chkProducto.Enabled = vHabilitado;
            ddlServicioTipo.Enabled = vHabilitado;
            ddlModeloProducto.Enabled = false;
            txtProductoCodigo.Enabled = vHabilitado;
            txtProductoDescripcion.Enabled = vHabilitado;
            txtProductoObservaciones.Enabled = vHabilitado;
            fileImagen.Enabled = vHabilitado;

            upProductoServicioCarga.Update();
        }

        public string idProductoServicioSession
        {
            get
            {
                string idProductoServicio = "";
                if (Session["idProductoServicio"] != null)
                {
                    idProductoServicio = Session["idProductoServicio"].ToString();
                }
                return idProductoServicio;
            }
        }

        public void cargaPantalla()
        {
            cargarUnidadesDenegocio();

            cargarServicioTipo(chkProducto.Checked);

            if (Session["idProductoServicio"] != null)
            {

                int idProductoServicio = Session["idProductoServicio"].ToString().ToInt();
                ProductoServicio proSer = new ProductoServicio().Select().Where(pS => pS.IdProductoServicio == idProductoServicio).FirstOrDefault();
                if (proSer.IdUnidadNegocio != null)
                {
                    ddlUnidadNegocio.SelectedValue = proSer.IdUnidadNegocio.ToCryptoID();
                }
                chkProducto.Checked = proSer.Servicio;
                cargarServicioTipo(chkProducto.Checked);
                ddlServicioTipo.SelectedValue = proSer.IdServicioTipo.ToCryptoID();
                txtProductoCodigo.Text = proSer.CodigoProducto;
                txtProductoDescripcion.Text = proSer.Descripcion;
                txtProductoObservaciones.Text = proSer.Observacion;

                upProductoServicioCarga.Update();
            }
        }

        protected void cargarUnidadesDenegocio()
        {
            List<CGADM.UnidadNegocio> listUnidadNegocio = new CGADM.UnidadNegocio().
                Select().Where(uNe =>
                uNe.Descripcion == "Vending Control" ||
                uNe.Descripcion == "Access Control"
                ).ToList();
            ddlUnidadNegocio.Items.Clear();

            ddlUnidadNegocio.Items.Add(new ListItem("Todas", 0.ToCryptoID()));
            foreach (CGADM.UnidadNegocio unidadNegocio in listUnidadNegocio)
            {
                ddlUnidadNegocio.Items.Add(
                    new ListItem(unidadNegocio.Descripcion, unidadNegocio.IdUnidadNegocio.ToCryptoID())
                    );
            }
        }

        protected void cargarServicioTipo(bool esProductoServicio)
        {
            foreach (
                CGADM.ServicioTipo servicioTipo in
                    new CGADM.ServicioTipo()
                        .Select()
                            .Where(st =>
                                st.FHBaja == null
                                && st.EsProducto == esProductoServicio
                                ))
            {
                ddlServicioTipo.Items.Add(new ListItem(servicioTipo.Descripcion, servicioTipo.IdServicioTipo.ToCryptoID()));
            }
            refrescarModelos();
        }

        public void ddlServicioTipo_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            refrescarModelos();
        }

        protected void chkProducto_CheckedChanged(object sender, EventArgs e)
        {
            ddlServicioTipo.Items.Clear();

            if (chkProducto.Checked == true)
            {
                lblServicioTipo.Text = "Tipo de Producto";
            }
            else
            {
                lblServicioTipo.Text = "Tipo de Servicio";
            }
            ddlServicioTipo.Items.Clear();
            cargarServicioTipo(chkProducto.Checked);
        }

        protected void refrescarModelos()
        {
            ddlModeloProducto.Items.Clear();
            bool aplicarModelos = false;
            switch (ddlServicioTipo.SelectedItem.Text)
            {
                case "UM":
                    aplicarModelos = true;
                    break;

                case "DISPOSITIVO":
                    aplicarModelos = true;
                    break;
            }

            if (!aplicarModelos)
            {
                ListItem itemCero = new ListItem();
                itemCero.Text = "No aplica";
                itemCero.Value = 0.ToCryptoID();

                ddlModeloProducto.Items.Add(itemCero);
                ddlModeloProducto.Enabled = false;
                upModeloProducto.Update();
            }
            else
            {
                ddlModeloProducto.Enabled = true;
                DataTable dtModelos = new SP("CGADM").Execute("usp_ObtenerModelos", P.Add("pHardwareTipo", ddlServicioTipo.SelectedItem.Text));
                foreach (DataRow dtRow in dtModelos.Rows)
                {
                    string valor = dtRow[0].ToString();
                    int identificador = dtRow[1].ToString().ToInt();
                    ddlModeloProducto.Items.Add(
                        new ListItem(valor, identificador.ToCryptoID())
                        );
                }
                upModeloProducto.Update();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        public void guardarProductoServicio()
        {
            bool vUpdate = false;

            ProductoServicio proSer = new ProductoServicio();

            if (Session["idProductoServicio"] != null)
            {
                proSer = new ProductoServicio().Select().Where(pS => pS.IdProductoServicio == Session["idProductoServicio"].ToString().ToInt()).FirstOrDefault();
                vUpdate = true;
                Session.Remove("idProductoServicio");
            }
            else
            {
                proSer = new ProductoServicio();
                proSer.FHAlta = DateTime.Now;
            }

            if (ddlUnidadNegocio.SelectedValue.ToIntID() == 0)
            {
                proSer.IdUnidadNegocio = null;
            }
            else
            {
                proSer.IdUnidadNegocio = ddlUnidadNegocio.SelectedValue.ToIntID();
            }

            proSer.Servicio = chkProducto.Checked;
            proSer.IdServicioTipo = ddlServicioTipo.SelectedValue.ToIntID();
            proSer.CodigoProducto = txtProductoCodigo.Text;
            proSer.Descripcion = txtProductoDescripcion.Text;
            proSer.Observacion = txtProductoObservaciones.Text;
            proSer.IdImagen = ArchivoAdd(proSer.IdImagen, fileImagen);

            switch (ddlServicioTipo.SelectedItem.Text)
            {
                case "UM":
                    proSer.UMModeloId = ddlModeloProducto.SelectedValue.ToIntID();
                    break;

                case "DISPOSITIVO":
                    proSer.IdDispositivosModelos = ddlModeloProducto.SelectedValue.ToIntID();
                    break;
            }


            using (Tn transaccionProductoServicio = new Tn("CGADM"))
            {
                string tipoTrans = "Alta";
                string tipoObj = "Servicio";

                if (proSer.Servicio == true)
                {
                    tipoObj = "Producto";
                }

                if (!vUpdate)
                {
                    proSer.Insert(transaccionProductoServicio);
                }
                else
                {
                    proSer.Update(transaccionProductoServicio);
                    tipoTrans = "Modificación";
                }

                transaccionProductoServicio.Commit();
                Session.Remove("idProductoServicio");
                Message("Guardado correctamente", tipoTrans + " de " + tipoObj + " realizada exitosamente", MessagesTypes.success, "ProductoServicioListar.aspx");
            }
        }
    }
}