﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CGADM;
using MagicSQL;
using static Code.Utils;

namespace AdmSys.Controls
{
    public partial class ucSDTDetalleFajaIngreso : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public string FajaIngreso
        {
            get
            {
                return txtFajaIngreso.Text;
            }
            set
            {
                string idSDTDetalle = value;
                while (idSDTDetalle.Length < 6)
                {
                    idSDTDetalle = "0" + idSDTDetalle;
                }
                txtFajaIngreso.Text = "SDTD-"+idSDTDetalle;
                upSDTDetalleFajaIngreso.Update();
            }
        }

        public void noAplica()
        {
            txtFajaIngreso.Text = "NO APLICA";
            upSDTDetalleFajaIngreso.Update();
        }
    }
}