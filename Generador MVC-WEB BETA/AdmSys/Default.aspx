﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>
<html lang="es">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema de gestión de eventos">
    <meta name="author" content="MF">

    <title>Sistema Administrativo</title>
    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="Content/images/favicon.png" />
        <%-- Bootstrap --%>
    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <%-- Custom --%>
    <link href="Content/site.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        body {
            overflow-y: hidden;
            background-color: white;
        }

        #site-name {
            width: 100%;
            position: absolute;
            z-index: -99999;
            text-align: -webkit-center;
            height: inherit;
            margin-top: 10px;
            color: #00518d;
            font-size: 30px;
            font-family: 'Roboto Condensed', sans-serif;
        }

        #navbar {
            margin-bottom: 0;
        }

        nav > div.navbar-header {
            width: 100%;
            background: url(content/images/bg.png);
        }

            nav > div.navbar-header > ul {
                float: right;
            }

        .navbar-brand {
            padding: 5px !important;
        }

        #side-menu {
            border-right-width: 1px;
            border-right-style: solid;
            border-right-color: #e7e7e7;
            height: calc(100vh - 51px) !important;
            overflow-y: auto;
            background: url(content/images/bg.png);
        }

        #page-wrapper {
            padding: 0px;
            border: none;
            background: url("Content/images/logo-watermark.png") center center no-repeat;
        }

        #content-page {
            padding: 0px;
        }

        #iframe-page {
            height: calc(100vh - 56px);
        }

        .services-up {
            font-size: large;
            color: green;
        }

        .services-up-down {
            font-size: large;
            color: orange;
        }

        .services-down {
            font-size: large;
            color: red;
        }

        .service-up {
            color: green !important;
            margin-right: 5px;
        }

        .service-down {
            color: red !important;
            margin-right: 5px;
        }

        .service-unknown {
            color: gray !important;
            margin-right: 5px;
        }

        #lnkSeccionControlGlobal {
            border-left-style: solid;
            border-left-width: 10px;
            border-left-color: #2196f3;
        }

        #lnkSeccionDistribuidor {
            border-left-style: solid;
            border-left-width: 10px;
            border-left-color: #9C27B0;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>
    <%-- CollapsibleMenu --%>
    <script src="Scripts/vendor/collapsiblemenu.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Aplica el plugin menu colapsible
            $("#lateral-menu").collapsibleMenu();

            // Colapsa todos los sub-niveles del menú lateral
            $("#side-menu ul").css("display", "none");

            // Carga la correcta barra lateral y colapsa la barra lateral cuando se redimensiona la ventana.
            // Establece el valor min-height del elemento #page-wrapper
            $(function () {
                $(window).bind("load resize", function () {

                    var topOffset = 50;
                    var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;

                    if (width < 768) { // Menu mobile

                        $('div.navbar-collapse').addClass('collapse');

                        // Los .item-menu colapsan el menu
                        $('.item-menu').attr('data-toggle', 'collapse');
                        $('.item-menu').attr('data-target', '.navbar-collapse');

                        topOffset = 100;

                    } else { // Menu desktop

                        $('div.navbar-collapse').removeClass('collapse');

                        // Los .item-menu no colapsan el menu
                        $('.item-menu').attr('data-toggle', '');
                        //$('.item-menu').attr('data-target', '');

                    }

                    var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
                    height = height - topOffset;
                    if (height < 1) height = 1;
                    if (height > topOffset) {
                        $("#page-wrapper").css("min-height", (height) + "px");
                    }

                });

                var url = window.location;
                var element = $('ul.nav a').filter(function () {
                    return this.href == url;
                }).addClass('active').parent();

                while (true) {
                    if (element.is('li')) {
                        element = element.parent().addClass('in').parent();
                    } else {
                        break;
                    }
                }

            });

            // Evento al presionar una tecla en el buscador
            $("#search").keyup(function () {
                MenuSearch();
            });

            // Borrar la expresion de búsqueda
            $("#clear").click(function () {
                $("#search").val("");
                MenuSearch();
            });

            // Mantiene resaltado el item de menú activo
            $("#lateral-menu a").click(function () {
                // Item de menú inactivo
                $("#lateral-menu a").css("color", "rgb(51, 122, 183)");
                $("#lateral-menu a").css("background-color", "transparent");
                // Item de menú activo
                $(this).css("color", "#ffffff");
                $(this).css("background-color", "#337ab7");
            });

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Función de búsqueda de items del menú
        function MenuSearch() {
            var filter = $("#search").val(), count = 0;
            $("#side-menu li:not(.sidebar-search)").each(function () {
                if (filter == "") {
                    $('#clear').prop('disabled', true);
                    $(this).show();
                    $("#side-menu ul").css("display", "none");
                } else if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $('#clear').prop('disabled', false);
                    $(this).hide();
                } else {
                    $('#clear').prop('disabled', false);
                    $("#side-menu ul").css("display", "block");
                    $(this).show();
                }
            });
        }

    </script>
</head>
<body>
    <div id="wrapper">
        <nav id="navbar" class="navbar navbar-default navbar-static-top" role="navigation">

            <%-- Titulo del sitio --%>
            <div id="site-name" class="navbar-brand visible-lg visible-md visible-sm">
                <asp:Label Text="Sistema Administrativo" runat="server" CssClass="hidden-sm hidden-xs" />
                <small>
                    <asp:Label ID="lblApp" Text="..." runat="server" />
                </small>
            </div>

            <div class="navbar-header">

                <%-- Botón menú colapsado --%>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">
                        <asp:Label Text="Menú" runat="server" />
                    </span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <%-- Isologo header --%>
                <img src="Content/images/logo-xs.png" class="navbar-brand" />

                <%-- Menu del usuario --%>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span>
                                <asp:Label ID="lblNombre" Text="Usuario loggeado" runat="server" />
                            </span>
                            <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="Pages/Usuario.aspx" class="item-menu" target="iframePage" id="lnkPerfilUsuario" runat="server">
                                    <i class="fa fa-user fa-fw"></i>
                                    <asp:Label Text="Perfil de usuario" runat="server" />
                                </a>
                            </li>
                            <li>
                                <a href="Pages/CambiarPassword.aspx" class="item-menu" target="iframePage" runat="server">
                                    <i class="fa fa-key fa-fw"></i>
                                    <asp:Label Text="Cambiar Contraseña" runat="server" />
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="Login.aspx">
                                    <i class="fa fa-sign-out fa-fw"></i>
                                    <asp:Label Text="Cerrar sesión" runat="server" />
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>

            <%-- Menu lateral --%>
            <div id="lateral-menu" class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul id="side-menu" class="nav">

                        <%-- Buscador de elementos del menú --%>
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input id="search" type="text" class="form-control" placeholder="Buscar..." autocomplete="off">
                                <span class="input-group-btn">
                                    <button id="clear" class="btn btn-default" type="button" disabled="disabled">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </span>
                            </div>
                        </li>

                        <%-- : : : : : : : : : : : : : : : : : : :   Items del menu   : : : : : : : : : : : : : : : : : : : : --%>

                        <%--
                        -----------------------------------------------------------
                            Ejemplos
                        -----------------------------------------------------------
                        <li>
                            <a href="#">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                <asp:Label Text="Ejemplos" runat="server" />
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Pages/_Models_.aspx" target="iframePage">
                                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        <asp:Label Text="Model" runat="server" />
                                    </a>
                                </li>
                                <li>
                                    <a href="Pages/_Grid_.aspx" target="iframePage">
                                        <i class="fa fa-table" aria-hidden="true"></i>
                                        <asp:Label Text="In Line Edit" runat="server" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        --%>



                        <li id="lnkSeccionDistribuidor" runat="server" data-menu-securable="mnuSectionDistribuidor">

                            <%-- Distribuidor --%>
                            <a href="#">
                                <i class="fa fa-sitemap fa-fw"></i>
                                <asp:Label Text="Configuraciones" runat="server" />
                            </a>
                            <ul class="nav nav-second-level">


                                <li id="Li5" runat="server">
                                    <%-- Manufactura --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Generador MVC" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <%-- Pendientes desde taller --%>
                                            <a href="Pages/Generador.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Carga de Entidades" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div id="content-page" class="container-fluid">

                <%-- Paginas --%>
                <iframe id="iframe-page" name="iframePage" src="Content/html/Welcome.html?version=<%=Version%>" frameborder="0" width="100%"></iframe>

            </div>
        </div>
    </div>

    <script src="scripts/ServicesStatus.js"></script>
</body>
</html>