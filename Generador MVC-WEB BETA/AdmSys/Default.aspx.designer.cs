﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------



public partial class Default {
    
    /// <summary>
    /// Control lblApp.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblApp;
    
    /// <summary>
    /// Control lblNombre.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblNombre;
    
    /// <summary>
    /// Control lnkPerfilUsuario.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkPerfilUsuario;
    
    /// <summary>
    /// Control lnkSeccionDistribuidor.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl lnkSeccionDistribuidor;
    
    /// <summary>
    /// Control Li5.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl Li5;
}
