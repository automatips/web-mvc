﻿using MagicSQL;
using System.Data;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de ConfActivacionSoftDisp
    /// </summary>
    public class ConfActivacionSoftDisp : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];
                string confirmActivacionDisp = "";

                DataTable dt;
                using (SP sp = new SP("CGADM"))
                {
                    // ¿La UM tiene un soft para Dispositivos pendiente de envio o activacion?
                    dt = sp.Execute("usp_GetSoftDispPendiente", P.Add("SerialUM", serialUM));

                    // Se encontro algun registro?
                    if (dt.Rows.Count == 0)
                    {
                        confirmActivacionDisp = "Ok";
                    }
                }

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Result = confirmActivacionDisp
                }.ToJson();
                context.Response.Write(response);
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}