﻿using MagicSQL;
using System.Data;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de ConfRecepcionSoftDisp
    /// </summary>
    public class ConfRecepcionSoftDisp : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];
                string confirmRecepSoftDisp = "";

                DataTable dt;
                using (SP sp = new SP("CGADM"))
                {
                    // ¿La UM tiene un soft para Dispositivos pendiente de envio o activacion?
                    dt = sp.Execute("usp_GetSoftDispPendiente", P.Add("SerialUM", serialUM));

                    // Se encontro algun registro
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][0].ToString().ToInt() == 0)
                        {
                            confirmRecepSoftDisp = "Disponible";
                        }
                    }
                }

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Result = confirmRecepSoftDisp
                }.ToJson();
                context.Response.Write(response);
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}