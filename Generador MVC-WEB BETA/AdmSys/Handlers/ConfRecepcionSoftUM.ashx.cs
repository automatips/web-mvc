﻿using MagicSQL;
using System.Data;
using System.Web;

namespace AdmSys.Handlers
{
    public class ConfRecepcionSoftUM : IHttpHandler
    {
        // *** UM ***

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];
                string confirmRecepSoftUM = "";

                DataTable dt;
                using (SP sp = new SP("CGADM"))
                {
                    // ¿La UM tiene un soft pendiente de envio o activacion?
                    dt = sp.Execute("usp_GetSoftUmPendiente", P.Add("SerialUM", serialUM));

                    // Se encontro algun registro
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][0].ToString().ToInt() == 0)
                        {
                            confirmRecepSoftUM = "Disponible";
                        }
                    }
                }

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Result = confirmRecepSoftUM
                }.ToJson();
                context.Response.Write(response);
            }
            catch { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}