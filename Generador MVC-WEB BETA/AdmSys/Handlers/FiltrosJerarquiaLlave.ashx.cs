﻿using MagicSQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de FiltrosJerarquiaLlave
    /// </summary>
    public class FiltrosJerarquiaLlave : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int idAgente = context.Request.QueryString["idAgente"].ToIntID();

            string term = context.Request.QueryString["term"];
            List<object> data = new List<object>();

            DataTable dt;
            using (SP sp = new SP("CGADM"))
            {
                dt = sp.Execute("usp_GetLlaveAjax",
                                P.Add("llave", term),
                                P.Add("idAgente", idAgente));
                foreach (DataRow dr in dt.Rows)
                {
                    data.Add(new { id = dr["idLlave"].ToString().ToInt().ToCryptoID(), text = dr["Llave"].ToString() });
                }
            }

            // Respuesta en formato json
            context.Response.ContentType = "application/json";
            context.Response.Write(data.ToJson().ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}