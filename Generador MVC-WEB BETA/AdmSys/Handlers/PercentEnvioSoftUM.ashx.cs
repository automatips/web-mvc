﻿using MagicSQL;
using System;
using System.Web;

namespace AdmSys.Handlers
{
    public class PercentEnvioSoftUM : IHttpHandler
    {
        // *** UM ***

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // Serial de la UM
                string serialUM = context.Request.QueryString["serialUM"];

                // Ejecutal el SP que devuelve el porcentaje de envio del soft a la UM
                int percentage = 0;
                using (SP sp = new SP("CGADM"))
                {
                    percentage = sp.Execute("usp_GetPercentEnvioSoftUM", P.Add("serialUM", serialUM)).Rows[0][0].ToString().ToInt();
                }
                if (percentage > 100) percentage = 100;

                // Respuesta en formato json
                context.Response.ContentType = "application/json";
                string response = new
                {
                    Percentage = percentage
                }.ToJson();
                context.Response.Write(response);
            }
            catch (Exception ex) { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}