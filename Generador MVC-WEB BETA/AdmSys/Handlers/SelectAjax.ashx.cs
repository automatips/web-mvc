﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdmSys.Handlers
{
    /// <summary>
    /// Descripción breve de SelectAjax
    /// </summary>
    public class SelectAjax : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string term = context.Request.QueryString["term"];
            List<object> data = new List<object>();

            var models = new CGADM.Model().Select().Where(m => m.Descripcion.Contains(term));
            foreach (var model in models)
            {
                data.Add(new { id = model.IdModel.ToCryptoID(), text = model.Nombre + " [" + model.Descripcion + "]" });
            }

            // Respuesta en formato json
            context.Response.ContentType = "application/json";
            context.Response.Write(data.ToJson().ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}