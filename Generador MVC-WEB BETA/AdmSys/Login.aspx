﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema de gestión de eventos">
    <meta name="author" content="MF">

    <title>Sistema Administrativo</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Bootstrap --%>
    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- Password --%>
    <link href="Content/vendor/password.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="Content/site.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        .login-panel {
            box-shadow: 0px 0px 20px #000000;
        }

        #imgLogo {
            padding: 20px;
        }

        #bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            font-size: 11px;
            width: 100%;
            color: #ffffff;
            font-weight: lighter;
        }

        #login-app-name {
            text-align: center;
            color: #00518d;
            font-variant: small-caps;
            font-family: monospace;
            font-size: xx-large;
        }

        #btnLogin {
            height: 35px !important;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>
    <%-- Password --%>
    <script src="Scripts/vendor/password.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
            $('input:password').password({
                shortPass: 'La contraseña es demasiado corta',
                badPass: 'Débil. Combine mayúsculas, minúsculas, numeros',
                goodPass: 'Media. Incluya caracteres especiales',
                strongPass: 'Contraseña fuerte',
                containsUsername: 'La contraseña contiene el nombre de usuario',
                enterPass: 'Ingrese la contraseña',
                showPercent: false,
                showText: true, // shows the text tips
                animate: true, // whether or not to animate the progress bar on input blur/focus
                animateSpeed: 'fast', // the above animation speed
                username: false, // select the username field (selector or jQuery instance) for better password checks
                usernamePartialMatch: true, // whether to check for username partials
                minimumLength: 6 // minimum password length (below this threshold, the score is 0)
            });
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</head>
<body id="body" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img id="imgLogo" class="img-responsive center-block" src="Content/images/logo-login.png" />
                        <%-- Aplicación en la que se iniciará sesión --%>
                        <h4 id="login-app-name">
                            <asp:Label ID="lblApp" Text="..." runat="server" />
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form role="form" runat="server">
                            <fieldset>
                                <asp:Panel ID="pnlLoginError" Visible="false" runat="server">
                                    <div class="alert alert-danger">
                                        <asp:Label ID="lblError" Text="Nombre de usuario o contraseña no válidos." runat="server" />
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <asp:TextBox ID="txtUsername" CssClass="form-control" placeholder="Nombre de usuario" autocomplete="off" autofocus runat="server" />
                                    <asp:RequiredFieldValidator ErrorMessage="Ingrese el nombre de usuario" CssClass="required-field-validator" ControlToValidate="txtUsername" runat="server" />
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPassword" CssClass="form-control" placeholder="Contraseña" TextMode="Password" runat="server" />
                                    <asp:RequiredFieldValidator ErrorMessage="Ingrese la contraseña" CssClass="required-field-validator" ControlToValidate="txtPassword" runat="server" />
                                </div>
                                <asp:Button ID="btnLogin" Text="Iniciar sesión" CssClass="btn btn-lg btn-info btn-block" runat="server" OnClick="btnLogin_Click" />
                            </fieldset>

                            <asp:HiddenField ID="app" runat="server" />
                            <asp:HiddenField ID="appId" runat="server" />
                        </form>

                        <h5 class="info text-center">
                            <br />
                            <asp:HyperLink Text="¿Olvidó su contraseña?" CssClass="btn btn-sm btn-link btn-block" NavigateUrl="~/Pages/RecuperarPassword.aspx" runat="server" />
                            <br />
                            <br />

                            <small>
                                <asp:Label Text="Este sitio utiliza cookies para conservar su usuario." runat="server" />
                            </small>
                            <br />
                            <small>
                                <asp:Label Text="Recuerde cerrar la sesión al finalizar." runat="server" />
                            </small>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <%-- Pie de página --%>
        <div class="centered">
            <div id="bottom" class="text-center">
                <span>©</span>
                <span>2018</span>
                <span>-</span>
                <span>Sistema Administrativo</span>
                <br />
                <small>
                    <asp:Label ID="lblVersion" Text="Versión: ..." runat="server" />
                </small>
            </div>
        </div>
    </div>
</body>
</html>