﻿using CGUsuarios.Common;
using Code;
using log4net;
using Newtonsoft.Json;

using System;
using System.Reflection;
using static Code.Utils;

public partial class Login : System.Web.UI.Page
{
    private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    protected void Page_Load(object sender, EventArgs e)
    {
        // Version de la compilación.
        lblVersion.Text = "Versión: " + Assembly.GetExecutingAssembly().GetName().Version.ToString();

        if (!IsPostBack)
        {
            // Si hay sesión de trabajo iniciada, cerrarla
            // Recupera la cookie Session
            var sessionCookie = Utils.ReadCookie("SessionSysAdm");
            if (sessionCookie != null)
            {
                var session = JsonConvert.DeserializeObject<CurrentSession>(sessionCookie);

                Log.DebugFormat("Cerrando Sesión con login '{0}'. Id de Sesión de Trabajo: {1}.", session.Username, session.IdSesionTrabajo);

                SecurityManager.Logout(Context, session.IdSesionTrabajo, session.Username);

                Log.InfoFormat("Cierre de Sesión exitoso con login '{0}'. Id de Sesión de Trabajo: {1}.", session.Username, session.IdSesionTrabajo);

                // Elimina la cookie Session
                DeleteCookie("SessionSysAdm");

                NavigateTo("Apps.aspx");
                return;
            }

            // Muestra el nombre de la aplicacion.
            var aplicacion = CGUsuarios.Common.Managers.GenericManager.Get<CGUsuarios.Common.Aplicacion>("aplicacion/deurl?url=" + Request["app"]);
            if (aplicacion == null)
            {
                NavigateTo("Apps.aspx");
                return;
            }
            lblApp.Text = aplicacion.Nombre;

            if (aplicacion.Nombre.Equals("Administrador"))
            {
                body.Attributes.CssStyle.Add("background", "url(Content/images/bkg-texture-adm.png)");
            }
            else
            {
                body.Attributes.CssStyle.Add("background", "url(Content/images/bkg-texture-dis.png)");
            }

            // Determinar aplicación a ejecutar
            app.Value = Request["app"];
            if (string.IsNullOrWhiteSpace(app.Value))
            {
                NavigateTo("Apps.aspx");
                return;
            }
            appId.Value = SecurityManager.GetApplicationIdFromUrl(app.Value).ToString();
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        // Usuario y Contraseña en minúsculas y sin espacios
        string username = txtUsername.Text.Trim().ToLower();
        string password = txtPassword.Text.Trim();

        try
        {
            SecurityManager.IdAplicacion = appId.Value.ToShort();
            var result = SecurityManager.PerfilesLogin(Context, username, password);
            if (result.Perfiles.Length == 0)
            {
                throw new ApplicationException("El usuario no tiene perfiles habilitados.");
            }
            if (result.Perfiles.Length > 1)
            {
                throw new ApplicationException("El usuario posee más de un perfil habilitado.");
            }
            else
            {
                Log.DebugFormat("Iniciando Sesión con login '{0}'.", username);

                //Tiene un solo perfil, iniciar la sesión
                SecurityManager.Login(Context, username, password, result.Perfiles[0].IdPerfil);

                Log.InfoFormat("Inicio de Sesión exitoso con login '{0}'. Id de Sesión de Trabajo: {1}.", username, SecurityManager.IdSesionTrabajo);

                // Objeto Session
                CurrentSession session = new CurrentSession();
                session.Username = username;
                session.Password = password;
                session.IdAplicacion = appId.Value.ToShort();
                session.IdPerfil = result.Perfiles[0].IdPerfil;
                session.IdCuenta = SecurityManager.IdCuenta;
                session.IdSesionTrabajo = SecurityManager.IdSesionTrabajo;
                session.Version = CurrentSession.currentVersion;
                string json = JsonConvert.SerializeObject(session).ToString();

                // Crea la cookie para almacenar el usuario loggeado
                CreateCookie("SessionSysAdm", json);

                // Navega a la home del sitio
                NavigateTo("~/Default.aspx");
            }
        }
        catch (ApplicationException ex)
        {
            Log.WarnFormat("Error de Inicio de Sesión con login '{0}': {1}", username, ex.Message);
            lblError.Text = ex.Message;
            pnlLoginError.Visible = true;
        }
        catch (Exception ex)
        {
            Log.ErrorFormat("Error de Inicio de Sesión con login '{0}': {1}", username, ex.ToString());

            // Muestra el mensaje de error de inicio de sesión
            pnlLoginError.Visible = true;
        }
    }
}