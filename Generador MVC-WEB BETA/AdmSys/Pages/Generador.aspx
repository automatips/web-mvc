﻿<%@ Page Title="Generador" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Generador.aspx.cs" Inherits="Pages.Generador" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:GridView ID="gridViewAscendentes" CssClass="table table-striped table-hover"
                    runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None"
                    AllowPaging="True" 
                    PageSize="10">
                    <AlternatingRowStyle BackColor="AliceBlue" />
                    <Columns>

                        <asp:TemplateField HeaderText="Eliminar" ShowHeader="False" Visible="true">
                            <ItemTemplate>

                                <asp:LinkButton ID="btnEliminar"
                                    runat="server"
                                    CssClass="btn btn-danger"
                                    Style="margin-bottom: 5px;"
                                    title="Agregar escala"
                                    OnClientClick="return confirm('Esta seguro que desea eliminar el registro?');"
                                    >
                                    <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                </asp:LinkButton> <asp:Label id="lblIdListaPrecio" Visible="false" Text="" runat="server" />

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:ButtonField ButtonType="Image" CommandName="Actualizar" HeaderText="Editar" ImageUrl="~/Imagenes/lapiz.png" Text="Botón" Visible="false"/>
                
                        <asp:BoundField DataField="Entidad" HeaderText="Nombre" />

                        <asp:TemplateField HeaderText="Padre" ShowHeader="False" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtPadre" 
                                    runat="server" 
                                    CssClass=" text-success"
                                    PlaceHolder="" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:GridView ID="gridViewDescendentes" CssClass="table table-striped table-hover"
                    runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None"
                    AllowPaging="True" 
                    PageSize="10">
                    <AlternatingRowStyle BackColor="AliceBlue" />
                    <Columns>

                        <asp:TemplateField HeaderText="Eliminar" ShowHeader="False" Visible="true">
                            <ItemTemplate>

                                <asp:LinkButton ID="btnEliminar"
                                    runat="server"
                                    CssClass="btn btn-danger"
                                    Style="margin-bottom: 5px;"
                                    title="Agregar escala"
                                    OnClientClick="return confirm('Esta seguro que desea eliminar el registro?');"
                                    >
                                    <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                </asp:LinkButton> <asp:Label id="lblIdListaPrecio" Visible="false" Text="" runat="server" />

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:ButtonField ButtonType="Image" CommandName="Actualizar" HeaderText="Editar" ImageUrl="~/Imagenes/lapiz.png" Text="Botón" Visible="false"/>
                
                        <asp:BoundField DataField="Entidad" HeaderText="Nombre" />

                        <asp:TemplateField HeaderText="Padre" ShowHeader="False" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtPadre" 
                                    runat="server" 
                                    CssClass=" text-success"
                                    PlaceHolder="" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>

        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <asp:GridView ID="GridViewDatos" CssClass="table table-striped table-hover"
                runat="server" 
                AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                GridLines="None" OnRowDeleting="GridViewDatos_RowDeleting" 
                OnRowCommand="GridViewDatos_RowCommand" 
                AllowPaging="True" 
                OnPageIndexChanging="GridViewDatos_PageIndexChanging" 
                PageSize="10">
                <AlternatingRowStyle BackColor="AliceBlue" />
                <Columns>

                    <asp:TemplateField HeaderText="Eliminar" ShowHeader="False" Visible="true">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnEliminar"
                                runat="server"
                                CssClass="btn btn-danger"
                                Style="margin-bottom: 5px;"
                                title="Agregar escala"
                                OnClientClick="return confirm('Esta seguro que desea eliminar el registro?');"
                                >
                                <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                            </asp:LinkButton> <asp:Label id="lblIdListaPrecio" Visible="false" Text="" runat="server" />

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:ButtonField ButtonType="Image" CommandName="Actualizar" HeaderText="Editar" ImageUrl="~/Imagenes/lapiz.png" Text="Botón" Visible="false"/>
                
                    <asp:BoundField DataField="Entidad" HeaderText="Nombre" />

                    <asp:TemplateField HeaderText="Padre" ShowHeader="False" >
                        <ItemTemplate>
                            <asp:TextBox ID="txtPadre" 
                                runat="server" 
                                CssClass=" text-success"
                                PlaceHolder="" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Maneja Tipo" ShowHeader="False" >
                        <ItemTemplate>
                            <asp:CheckBox ID="cbxManejaTipo" 
                                runat="server" 
                                Checked="false"
                                />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Maneja Categoria" ShowHeader="False" >
                        <ItemTemplate>
                            <asp:CheckBox ID="cbxManejaCategoria" 
                                runat="server" 
                                Checked="false"
                                />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Maneja Status" ShowHeader="False" >
                        <ItemTemplate>
                            <asp:CheckBox ID="cbxManejaStatus" 
                                runat="server" 
                                Checked="true"
                                />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Alias de Nombre" ShowHeader="False" >
                        <ItemTemplate>
                            <asp:TextBox ID="txtAliasNombre" 
                                runat="server" 
                                CssClass=" text-success"
                                PlaceHolder="" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>

                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <asp:Button ID="btnGenerarSistema" runat="server" CssClass="btn btn-success pull-left" Text="Generar capas del sistema" OnClick="btnGenerarSistema_Click"/>
        </div>
    </div>

</asp:Content>