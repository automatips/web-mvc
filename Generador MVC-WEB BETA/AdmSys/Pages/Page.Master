﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Page.master.cs" Inherits="Pages.Page" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema de gestión de eventos">
    <meta name="author" content="MF">

    <title>Sistema Administrativo</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="../Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Bootstrap --%>
    <link href="../Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="../Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- SweetAlert --%>
    <link href="../Content/vendor/sweetalert.min.css" rel="stylesheet" />
    <%-- Select2 --%>
    <link href="../Content/vendor/select2/select2.min.css" rel="stylesheet" />
    <link href="../Content/vendor/select2/select2-bootstrap.min.css" rel="stylesheet" />
    <%-- DatePicker --%>
    <link href="../Content/vendor/datepicker.min.css" rel="stylesheet">
    <%-- TimePicker --%>
    <link href="../Content/vendor/timepicker.min.css" rel="stylesheet">
    <%-- Switchery --%>
    <link href="../Content/vendor/switchery.min.css" rel="stylesheet">
    <%-- DataTables Plugins --%>
    <link href="../Content/vendor/datatables.plugins/datatables.bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/vendor/datatables.plugins/datatables.responsive.min.css" rel="stylesheet" />
    <%-- FileInput --%>
    <link href="../Content/vendor/fileinput.min.css" rel="stylesheet" />
    <%-- ToolTipster --%>
    <link href="../Content/vendor/tooltipster/tooltipster-sideTip-light.min.css" rel="stylesheet" />
    <link href="../Content/vendor/tooltipster/tooltipster.bundle.min.css" rel="stylesheet" />
    <%-- Password --%>
    <link href="../Content/vendor/password.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="../Content/site.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #warning-requiredfieldvalidator {
            margin-top: 17px;
            margin-bottom: 0px;
            display: none;
            border-color: #f0ad4e;
        }

        /* GeoComplete en Bootstrap Modal */
        .pac-container {
            z-index: 99999999999999 !important;
        }

        .address-latitude, .address-longitude, .address-place-id, .address-formatted-address, .address-url, .address-map {
            display: none;
        }

        /* Pie de pagina de las tablas */
        tfoot th {
            border-style: none !important;
            background-color: #e7e7e7;
            font-weight: 100;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="../Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="../Scripts/vendor/bootstrap.min.js"></script>
    <%-- SweetAlert --%>
    <script src="../Scripts/vendor/sweetalert.min.js"></script>
    <%-- Select2 --%>
    <script src="../Scripts/vendor/select2.min.js"></script>
    <%-- AutoNumeric --%>
    <script src="../Scripts/vendor/autonumeric.min.js"></script>
    <%-- DatePicker --%>
    <script src="../Scripts/vendor/datepicker.min.js"></script>
    <%-- TimePicker --%>
    <script src="../Scripts/vendor/timepicker.min.js"></script>
    <%-- Switchery --%>
    <script src="../Scripts/vendor/switchery.min.js"></script>
    <%-- DataTables --%>
    <script src="../Scripts/vendor/datatables.min.js"></script>
    <%-- DataTables Plugins --%>
    <script src="../Scripts/vendor/datatables.plugins/datatables.responsive.min.js"></script>
    <script src="../Scripts/vendor/datatables.plugins/datatables.bootstrap.min.js"></script>
    <script src="../Scripts/vendor/datatables.plugins/datatables.buttons.min.js"></script>
    <script src="../Scripts/vendor/datatables.plugins/buttons.html5.min.js"></script>
    <script src="../Scripts/vendor/datatables.plugins/jszip.min.js"></script>
    <script src="../Scripts/vendor/datatables.plugins/buttons.print.min.js"></script>
    <%-- FileInput --%>
    <script src="../Scripts/vendor/fileinput.min.js"></script>
    <%-- Google Maps --%>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbWH0HhVzDJuxI7Br3QnhDENrBgdlVhqw&amp;libraries=places"></script>
    <%-- GeoComplete --%>
    <script src="../Scripts/vendor/geocomplete.min.js"></script>
    <%-- ToolTipster --%>
    <script src="../Scripts/vendor/tooltipster.bundle.min.js"></script>
    <%-- Password --%>
    <script src="../Scripts/vendor/password.min.js"></script>
    <%-- Custom --%>
    <script src="../Scripts/pages.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Prevenir que esta página sea accedida desde fuera del iFrame
        if (window.self === window.top) {
            //*-*-window.location.replace('../Content/html/403.html');
        }

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Inicializa los plugins
            Init();

            // Requeridos con borde oscuro
            if ($(".required-field-validator").length > 0) {
                for (var i = 0; i < Page_Validators.length; i++) {
                    var val = Page_Validators[i];
                    var ctrl = document.getElementById(val.controltovalidate);
                    if (ctrl != null && ctrl.style != null) {
                        if (!val.isvalid) {
                            ctrl.style.color = '#8a6d3b';
                            ctrl.style.backgroundColor = '#fcf8e3';
                            ctrl.style.borderColor = '#f0ad4e';

                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("color", "#8a6d3b")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("background-color", "#fcf8e3")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("border-color", "#f0ad4e")

                            $("#warning-requiredfieldvalidator").show();
                        }
                        else {
                            ctrl.style.color = '';
                            ctrl.style.backgroundColor = '';
                            ctrl.style.borderColor = '#9E9E9E';

                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("color", "")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("background-color", "")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("border-color", "#9E9E9E")

                            $("#warning-requiredfieldvalidator").hide();
                        }
                    }
                }
            }

            // Oculta el div procesando
            $("#busy").hide();

            // Reinicializa los plugins cuando se actualiza un UpdatePanel
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                Init();

                // Oculta el div procesando
                $("#busy").hide();
            });

            // Muestra el div procesando (busy)
            $("#ctl01").submit(function (event) {
                $("#busy").show();
            });

            // Dirección
            // Evento abrir modal direccion inicializa el plugin GeoComplete
            $('#address-modal').on('shown.bs.modal', function (e) {

                // Inicialización del plugin GeoComplete
                $(".geo-complete").geocomplete({
                    map: ".map-canvas",
                    details: "form ",
                    markerOptions: {
                        draggable: true
                    },
                }).bind("geocode:result", function (event, result) { // evento resultado ok
                    $(".address-latitude").val(result.geometry.location.lat);
                    $(".address-longitude").val(result.geometry.location.lng);
                    $(".address-place-id").val(result.place_id);
                    $(".address-formatted-address").val(result.formatted_address);
                    $(".address-url").val(result.url);
                    GetMapStaticImage();
                }).bind("geocode:error", function (event, result) { // evento error
                }).bind("geocode:multiple", function (event, result) { // evento resultados multiples
                }).bind("geocode:click", function (event, result) { // evento click
                }).bind("geocode:zoom", function (event, result) { // evento zoom
                }).bind("geocode:dragged", function (event, latLng) { // evento desplazamiento de marcador
                    $(".address-latitude").val(latLng.lat());
                    $(".address-longitude").val(latLng.lng());
                    GetMapStaticImage();
                });

                // Click en el boton Aceptar
                $(".address-ok").click(function () {
                    if ($(".address-formatted-address").val() != "") {
                        $(".text-address").val($(".address-formatted-address").val());
                    } else {
                        $(".text-address").val($(".geo-complete").val());
                    }
                });

                // URL de la imagen estática del mapa con la direccion centrada
                function GetMapStaticImage() {
                    var mapImage = "center=" + $(".address-latitude").val() + "," + $(".address-longitude").val() +
                                   "&zoom=16" +
                                   "&size=640x480" +
                                   "&markers=" + $(".address-latitude").val() + "," + $(".address-longitude").val()
                    $(".address-map").val(mapImage);
                }

            });

            // Evento click sobre una pestaña recalcula el ancho de las tablsa responsive
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
            });

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Resalta los RequestValidators en false
        function OnUpdateValidators() {

            // Hay Required Fields Validators activos?
            if ($(".required-field-validator").length > 0) {

                var parentTab = "";

                // Recorre la lista de Required Fields Validators activos
                for (var i = 0; i < Page_Validators.length; i++) {
                    var val = Page_Validators[i];
                    var ctrl = document.getElementById(val.controltovalidate);

                    // El Required Field Validator está activo?
                    if (ctrl != null && ctrl.style != null) {

                        if (!val.isvalid) {

                            // Activo

                            ctrl.style.color = '#8a6d3b';
                            ctrl.style.backgroundColor = '#fcf8e3';
                            ctrl.style.borderColor = '#f0ad4e';

                            // Resalta los Select2 con required-field-validator activos
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("color", "#8a6d3b")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("background-color", "#fcf8e3")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("border-color", "#f0ad4e")

                            // Resalta las tabs que tienen required-field-validator activos
                            $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("color", "#8a6d3b")
                            $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("background-color", "#fcf8e3")
                            $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("border-color", "#f0ad4e")

                            parentTab = 'a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]';

                            $("#warning-requiredfieldvalidator").show();
                        }
                        else {

                            // Inactivo

                            ctrl.style.color = '';
                            ctrl.style.backgroundColor = '#fdfdff';
                            ctrl.style.borderColor = '#9E9E9E';

                            // Borde resaltado sobre los Select2 requeridos
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("color", "")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("background-color", "#fdfdff")
                            $("#" + ctrl.id).next().first("span[role='combobox']").find(".selection").find(".select2-selection").css("border-color", "#9E9E9E")

                            // El parent tab tiene al menos un Required Field Validator activo?
                            if ('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]' != parentTab) {

                                // Quita los estilos a las tabs sin required-field-validator activos
                                $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("color", "")
                                $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("background-color", "")
                                $('a[href$="#' + $("#" + ctrl.id).parent().parent()[0].id + '"]').css("border-color", "")

                            }

                            $("#warning-requiredfieldvalidator").hide();
                        }
                    }
                }
                setTimeout(function () {
                    $("#busy").hide();
                }, 0);
            }
        }

        // Asigna la configuracion regional a la variable global regionalSettings
        function RegionalSettings(json) {
            regionalSettings = json;
        }
    </script>

    <%-- Render Page Head --%>
    <asp:ContentPlaceHolder ID="cphHead" runat="server">
    </asp:ContentPlaceHolder>
</head>
<body style="background-color: white; padding-left: 15px; padding-right: 15px; overflow: auto;">

    <%-- Div procesando --%>
    <div id="busy" style="background: url(../Content/images/cg.gif) center center no-repeat rgba(255, 255, 255, 0.5); position: absolute; top: 5px; left: 0px; width: 100%; height: calc(100% - 5px); z-index: 999999;">
    </div>

    <form runat="server">

        <%-- ScriptManager para manejar las funciones JS desde el CodeBehind y los UpdatePanel --%>
        <asp:ScriptManager runat="server" />

        <div>

            <%-- Cuando se activan los required field validators se muestra este mensaje --%>
            <div id="warning-requiredfieldvalidator" class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>
                    <asp:Label Text="Atención: " runat="server" />
                </strong>
                <asp:Label Text="Revise el formulario y complete todos los datos resaltados." runat="server" />
            </div>

            <%-- Render Page Body --%>
            <asp:ContentPlaceHolder ID="cphBody" runat="server">
            </asp:ContentPlaceHolder>
        </div>

        <%-- Modal Dirección --%>
        <div id="address-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            <asp:Label Text="Dirección" runat="server" />
                        </h4>
                    </div>
                    <div class="modal-body">
                        <%-- Dirección ingresada --%>
                        <div class="form-group">
                            <asp:TextBox ID="txtDireccionIngresada" CssClass="form-control geo-complete" placeholder="Dirección" autocomplete="off" runat="server" />
                        </div>
                        <%-- Aclaraciones --%>
                        <div class="form-group">
                            <asp:TextBox ID="txtDireccionAclaraciones" CssClass="form-control" placeholder="Aclaraciones" autocomplete="off" runat="server" />
                        </div>
                        <%-- Mapa --%>
                        <div class="form-group">
                            <div class="map-canvas panel panel-default" style="width: 100%; height: 250px;"></div>
                        </div>

                        <%-- Latitud & Longitud --%>
                        <asp:TextBox ID="txtDireccionLatitud" CssClass="address-latitude" runat="server" />
                        <asp:TextBox ID="txtDireccionLongitud" CssClass="address-longitude" runat="server" />

                        <%-- Detalles obtenidos de Google Maps --%>
                        <asp:TextBox ID="txtDireccionPlaceId" CssClass="address-place-id" runat="server" />
                        <asp:TextBox ID="txtDireccionFormateada" CssClass="address-formatted-address" runat="server" />
                        <asp:TextBox ID="txtDireccionURL" CssClass="address-url" runat="server" />

                        <%-- URL de la imagen estática del mapa --%>
                        <asp:TextBox ID="txtDireccionMapa" CssClass="address-map" runat="server" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info address-ok" data-dismiss="modal">
                            <asp:Label Text="Aceptar" runat="server" />
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </form>

</body>
</html>
