﻿using Code;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Pages
{
    public partial class _Grid_ : BasePage
    {
        private int idListaPrecio = 15;

        protected void Page_Load(object sender, EventArgs e)
        {
            //var x = new CGADM.Escala().Select(1).Descripcion;

            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                // Si la grilla no tiene filas agrega la primera en blanco
                InitGrivViewData();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            gvEscalas.Enabled = btnSave.Enabled;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }

        protected void btnAddGridRow_Click(object sender, EventArgs e)
        {
            // Si la ViewState no es nula
            if (ViewState["CurrentGridViewState"] != null)
            {
                // Crea una DataTable con el estado actual a partir de la ViewState
                DataTable dt = (DataTable)ViewState["CurrentGridViewState"];

                // Objeto DataRow
                DataRow dr = null;

                // Si la DataTable creada a partir de la ViewState no tiene filas se agrega una en blanco
                if (dt.Rows.Count == 0)
                {
                    // DataRow
                    dr = dt.NewRow();

                    // Fila vacia
                    dr["IdEscala"] = "";
                    dr["Descripcion"] = "";
                    dr["Observacion"] = "";
                    dr["CantInferior"] = "";
                    dr["CantSuperior"] = "";
                    dr["Moneda"] = "";
                    dr["Precio"] = "";
                }
                else
                {
                    // Recorre las filas guardando en la DataTable los valores preexistentes
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Controles creados a partir de las filas de la GridView
                        Label lblIdEscala = (Label)gvEscalas.Rows[i].Cells[1].FindControl("lblIdEscala");
                        TextBox txtDescripcion = (TextBox)gvEscalas.Rows[i].Cells[2].FindControl("txtDescripcion");
                        TextBox txtObservacion = (TextBox)gvEscalas.Rows[i].Cells[3].FindControl("txtObservacion");
                        TextBox txtCantInferior = (TextBox)gvEscalas.Rows[i].Cells[4].FindControl("txtCantInferior");
                        TextBox txtCantSuperior = (TextBox)gvEscalas.Rows[i].Cells[5].FindControl("txtCantSuperior");
                        DropDownList ddlMoneda = (DropDownList)gvEscalas.Rows[i].Cells[6].FindControl("ddlMoneda");
                        TextBox txtPrecio = (TextBox)gvEscalas.Rows[i].Cells[7].FindControl("txtPrecio");

                        // DataRow
                        dr = dt.NewRow();

                        // Fila valuada a partir de los controles
                        dt.Rows[i]["IdEscala"] = lblIdEscala.Text;
                        dt.Rows[i]["Descripcion"] = txtDescripcion.Text;
                        dt.Rows[i]["Observacion"] = txtObservacion.Text;
                        dt.Rows[i]["CantInferior"] = txtCantInferior.Text;
                        dt.Rows[i]["CantSuperior"] = txtCantSuperior.Text;
                        dt.Rows[i]["Moneda"] = ddlMoneda.SelectedValue;
                        dt.Rows[i]["Precio"] = txtPrecio.Text;
                    }
                }

                // Agrega la DataRow
                dt.Rows.Add(dr);

                // Guarda la DataTable en la ViewState
                ViewState["CurrentGridViewState"] = dt;

                // DataTable como origen de datos de la GridView
                gvEscalas.DataSource = dt;
                gvEscalas.DataBind();

                // Enfoca el primer control de la ultima fila
                TextBox textBox = (TextBox)gvEscalas.Rows[dt.Rows.Count - 1].Cells[1].FindControl("txtDescripcion");
                textBox.Focus();
            }

            // Muestra los valores en los controles
            SetRowControls();
        }

        private void InitGrivViewData()
        {
            // DataTable
            DataTable dt = new DataTable();

            // Agrega a la DataTable las columnas que se mostraran en la GridView
            dt.Columns.Add(new DataColumn("IdEscala", typeof(string)));
            dt.Columns.Add(new DataColumn("Descripcion", typeof(string)));
            dt.Columns.Add(new DataColumn("Observacion", typeof(string)));
            dt.Columns.Add(new DataColumn("CantInferior", typeof(string)));
            dt.Columns.Add(new DataColumn("CantSuperior", typeof(string)));
            dt.Columns.Add(new DataColumn("Moneda", typeof(string)));
            dt.Columns.Add(new DataColumn("Precio", typeof(string)));

            // Objeto DataRow
            DataRow dr = null;

            // Registros iniciales
            foreach (CGADM.Escala escala in new CGADM.Escala().Select().Where(e => e.IdListaPrecio == idListaPrecio))
            {
                // DataRow
                dr = dt.NewRow();

                // Valores obtenidos de las propiedades del modelo
                dr["IdEscala"] = escala.IdEscala;
                //dr["Descripcion"] = escala.Descripcion;
                //dr["Observacion"] = escala.Observacion;
                dr["CantInferior"] = escala.CantInferior.ToString();
                dr["CantSuperior"] = escala.CantSuperior.ToString();
                dr["Moneda"] = escala.IdMoneda.ToCryptoID();
                //dr["Precio"] = escala.Precio.ToString();

                // Agrega la DataRow a la DataTable vacía
                dt.Rows.Add(dr);
            }

            // Guarda la DataTable en el ViewState
            ViewState["CurrentGridViewState"] = dt;

            // DataTable como origen de datos de la GridView
            gvEscalas.DataSource = dt;
            gvEscalas.DataBind();

            // Muestra los valores en los controles
            SetRowControls();
        }

        private void SetRowControls()
        {
            if (ViewState["CurrentGridViewState"] != null)
            {
                // DataTable a partir de la ViewState
                DataTable dt = (DataTable)ViewState["CurrentGridViewState"];

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Controles creados a partir de las filas de la GridView
                        Label lblIdEscala = (Label)gvEscalas.Rows[i].Cells[1].FindControl("lblIdEscala");
                        TextBox txtDescripcion = (TextBox)gvEscalas.Rows[i].Cells[2].FindControl("txtDescripcion");
                        TextBox txtObservacion = (TextBox)gvEscalas.Rows[i].Cells[3].FindControl("txtObservacion");
                        TextBox txtCantInferior = (TextBox)gvEscalas.Rows[i].Cells[4].FindControl("txtCantInferior");
                        TextBox txtCantSuperior = (TextBox)gvEscalas.Rows[i].Cells[5].FindControl("txtCantSuperior");
                        DropDownList ddlMoneda = (DropDownList)gvEscalas.Rows[i].Cells[6].FindControl("ddlMoneda");
                        TextBox txtPrecio = (TextBox)gvEscalas.Rows[i].Cells[7].FindControl("txtPrecio");

                        // Carga los ddlMoneda
                        foreach (CGADM.Moneda moneda in new CGADM.Moneda().Select().Where(m => m.FHBaja == null))
                        {
                            ddlMoneda.Items.Add(new ListItem(moneda.Descripcion, moneda.IdMoneda.ToCryptoID()));
                        }

                        // Controles valuados a partir de la DataTable recupera de la ViewState
                        lblIdEscala.Text = dt.Rows[i]["IdEscala"].ToString();
                        txtDescripcion.Text = dt.Rows[i]["Descripcion"].ToString();
                        txtObservacion.Text = dt.Rows[i]["Observacion"].ToString();
                        txtCantInferior.Text = dt.Rows[i]["CantInferior"].ToString();
                        txtCantSuperior.Text = dt.Rows[i]["CantSuperior"].ToString();
                        ddlMoneda.SelectedValue = dt.Rows[i]["Moneda"].ToString();
                        txtPrecio.Text = dt.Rows[i]["Precio"].ToString();
                    }
                }
            }
        }

        private void SetRowData()
        {
            if (ViewState["CurrentGridViewState"] != null)
            {
                // DataTable a partir de la ViewState
                DataTable dt = (DataTable)ViewState["CurrentGridViewState"];

                // Objeto DataRow
                DataRow dr = null;

                if (dt.Rows.Count > 0)
                {
                    for (int i = 1; i <= dt.Rows.Count; i++)
                    {
                        // Controles creados a partir de las filas de la GridView
                        Label lblIdEscala = (Label)gvEscalas.Rows[i - 1].Cells[1].FindControl("lblIdEscala");
                        TextBox txtDescripcion = (TextBox)gvEscalas.Rows[i - 1].Cells[2].FindControl("txtDescripcion");
                        TextBox txtObservacion = (TextBox)gvEscalas.Rows[i - 1].Cells[3].FindControl("txtObservacion");
                        TextBox txtCantInferior = (TextBox)gvEscalas.Rows[i - 1].Cells[4].FindControl("txtCantInferior");
                        TextBox txtCantSuperior = (TextBox)gvEscalas.Rows[i - 1].Cells[5].FindControl("txtCantSuperior");
                        DropDownList ddlMoneda = (DropDownList)gvEscalas.Rows[i - 1].Cells[6].FindControl("ddlMoneda");
                        TextBox txtPrecio = (TextBox)gvEscalas.Rows[i - 1].Cells[7].FindControl("txtPrecio");

                        dr = dt.NewRow();

                        // Fila valuada a partir de las propedades de los controles
                        dt.Rows[i - 1]["IdEscala"] = lblIdEscala.Text;
                        dt.Rows[i - 1]["Descripcion"] = txtDescripcion.Text;
                        dt.Rows[i - 1]["Observacion"] = txtObservacion.Text;
                        dt.Rows[i - 1]["CantInferior"] = txtCantInferior.Text;
                        dt.Rows[i - 1]["CantSuperior"] = txtCantSuperior.Text;
                        dt.Rows[i - 1]["Moneda"] = ddlMoneda.SelectedValue;
                        dt.Rows[i - 1]["Precio"] = txtPrecio.Text;
                    }

                    // Guarda la DataTable en el ViewState
                    ViewState["CurrentGridViewState"] = dt;
                }
            }
        }

        protected void gvEscalas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Establece los valores de las filas
            SetRowData();

            if (ViewState["CurrentGridViewState"] != null)
            {
                // DataTable a partir de la ViewState
                DataTable dt = (DataTable)ViewState["CurrentGridViewState"];

                // Indice de la fila a eliminar
                int rowIndex = Convert.ToInt32(e.RowIndex);

                // Elimina la DataRow de la DataTable
                dt.Rows.Remove(dt.Rows[rowIndex]);

                // Guarda la DataTable en el ViewState
                ViewState["CurrentGridViewState"] = dt;

                // DataTable como origen de datos de la GridView
                gvEscalas.DataSource = dt;
                gvEscalas.DataBind();

                // Muestra los valores en los controles
                SetRowControls();
            }
        }

        protected void btnSaveGrid_Click(object sender, EventArgs e)
        {
            // Establece los valores de las filas
            SetRowData();

            // DataTable a partir de la ViewState
            DataTable table = ViewState["CurrentGridViewState"] as DataTable;

            if (table != null)
            {
                // Lista de todas las escalas del idListaPrecio
                List<CGADM.Escala> escalas = new CGADM.Escala().Select().Where(o => o.IdListaPrecio == idListaPrecio).ToList();

                // Todos los elementos que figuran en la lista Escalas pero no en el DataTable se dan de baja
                foreach (CGADM.Escala escala in escalas)
                {
                    bool del = true;
                    foreach (DataRow row in table.Rows)
                    {
                        string id = row.ItemArray[0] as string;
                        if (escala.IdEscala.ToString() == id)
                        {
                            del = false;
                        }
                    }
                    if (del) escala.Delete();
                }

                foreach (DataRow row in table.Rows)
                {
                    // Variables para valuar las propiedades del modelo
                    string idEscala = row.ItemArray[0] as string;
                    string descripcion = row.ItemArray[1] as string;
                    string observacion = row.ItemArray[2] as string;
                    string cantInferior = row.ItemArray[3] as string;
                    string cantSuperior = row.ItemArray[4] as string;
                    string idMoneda = row.ItemArray[5] as string;
                    idMoneda = idMoneda.ToIntID().ToString();
                    string precio = row.ItemArray[6] as string;

                    if (descripcion != null || observacion != null || cantInferior != null || cantSuperior != null || idMoneda != null || precio != null)
                    {
                        // Objeto escala
                        CGADM.Escala escala = new CGADM.Escala();

                        // Si el IdEscala == "" es nuevo
                        if (idEscala == "")
                        {
                            // Insert
                            escala.IdListaPrecio = idListaPrecio;

                            escala.FHAlta = DateTime.Now;
                            //escala.Descripcion = descripcion;
                            //escala.Observacion = observacion;
                            escala.CantInferior = cantInferior.ToShort();
                            escala.CantSuperior = cantSuperior.ToShort();
                            escala.IdMoneda = idMoneda.ToInt();
                            //escala.Precio = precio.ToDecimal();

                            escala.Insert();
                        }
                        else
                        {
                            // Si IdEscala existe en la lista escalas es actualizacion
                            if (escalas.Find(f => f.IdEscala == idEscala.ToInt()) != null)
                            {
                                // update
                                escala.Select(idEscala.ToInt());

                                //escala.Descripcion = descripcion;
                                //escala.Observacion = observacion;
                                escala.CantInferior = cantInferior.ToShort();
                                escala.CantSuperior = cantSuperior.ToShort();
                                escala.IdMoneda = idMoneda.ToInt();
                                //escala.Precio = precio.ToDecimal();

                                escala.Update();
                            }
                        }
                    }
                }
            }
        }
    }
}