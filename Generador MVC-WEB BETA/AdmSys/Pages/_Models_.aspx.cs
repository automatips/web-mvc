﻿using Code;

using System;
using System.Linq;
using System.Web.UI.WebControls;
using static Code.Utils;

namespace Pages
{
    public partial class _Models_ : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Establece Título del Formulario = Título de la Página
                lblTitulo.Text = Title;

                // Lista de _Models_ en el DropDownList
                ddlFiltro2.Items.Clear();
                foreach (CGADM.Model model in new CGADM.Model().Select().Where(m => m.FHBaja == null))
                {
                    ddlFiltro2.Items.Add(new ListItem(model.Nombre, model.IdModel.ToCryptoID()));
                }

                // Lista los _Models_ en la GridView
                gv_Models_.DataSource = new CGADM.Model().Select();
                gv_Models_.DataBind();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            // Nuevo _Model_
            NavigateTo("_Model_.aspx");
        }

        protected void ddlFiltro2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlFiltro3.Items.Clear();
            foreach (CGADM.Model model in new CGADM.Model().Select().Where(m => m.Nombre == ddlFiltro2.SelectedItem.Text))
            {
                ddlFiltro3.Items.Add(new ListItem(model.Nombre, model.IdModel.ToCryptoID()));
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            // Lista los _Models_ aplicando los filtros
            gv_Models_.DataSource = new CGADM.Model().Select().Where(m => m.Nombre.ToLower().Contains(txtNombre.Text.ToLower()));
            gv_Models_.DataBind();
        }

        protected void gv_Models__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Convierte el Id del modelo en CryptoID.
                string plainId = e.Row.Cells[1].Text;
                e.Row.Cells[1].Text = plainId.ToInt().ToCryptoID();
            }
        }
    }
}