﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImpresionSDS.aspx.cs" Inherits="AdmSys.Printables.ImpresionSDS" %>

<!DOCTYPE html>
<html>
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Comprobante</title>

    <link href="../Content/vendor/bootstrap.min.css" rel="stylesheet" />

    <style>
        html, body {
            color: black;
        }

        @media print {
            html, body {
                color: black;
                line-height: 1;
                font-size: 12px;
                margin-right: 2px;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed;
                width: 100%;
            }

            td {
                padding: 0px;
                border-style: none;
                border-width: 0.1rem;
                border-color: #808080;
            }

            thead {
                display: table-header-group;
                vertical-align: top;
            }

            tbody {
                display: table-row-group;
                font-size: 11px;
            }

            tfoot {
                display: table-footer-group;
            }

            #spacer {
                height: 2em;
            }
            /* height of footer + a little extra */
            #footer {
                position: fixed;
                bottom: 0;
                margin-right: 4px;
            }
        }

        }
    </style>

    <script src="../Scripts/vendor/jquery.min.js"></script>

    <script>

        $(document).ready(function () {
            var rowCount = $('#tComp tbody tr').length - 1;
            var height = 64 - (rowCount * 10);
            $("#lastTr").css("height", height + "vh");
        });

        window.onafterprint = function () {
            window.history.back();
        }
    </script>

</head>
<body onload="window.print()">
    <form id="frmComprobante" runat="server" style="margin-top: 10px;">

        <table id="tComp">

            <thead>

                <%-- Grilla --%>
                <%--                <tr>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                    <td>14</td>
                    <td>15</td>
                    <td>16</td>
                </tr>--%>

                <%-- Encabezado --%>
                <tr>

                    <%-- Logo --%>
                    <td colspan="6">
                        <img src="../Content/images/cg-logo.jpg" style="height: 70px;" />
                        <br />
                        <strong>
                            <asp:Label Text="INTELE S.R.L." runat="server" />
                        </strong>
                        <br />
                        <asp:Label Text="Av. Sabattini 1863 - 5000 - B° Maipú" runat="server" Style="font-size: 11px;" />
                        <br />
                        <asp:Label Text="Córdoba - Argentina - ventas@controlgobal.com.ar" runat="server" Style="font-size: 11px;" />
                    </td>

                    <%-- Tipo --%>
                    <td colspan="3" style="text-align: center;">
                        <strong>
                            <asp:Label Text="SDS"
                                runat="server"
                                Style="font-size: 50px; border: solid; border-width: medium; border-color: black; padding-left: 7px; padding-top: 1px; padding-right: 7px; padding-bottom: 0px;" />
                        </strong>
                    </td>

                    <%-- Numero --%>
                    <td colspan="7">
                        <strong>
                            <asp:Label Text="Solicitud de Servicios Nro:" runat="server" />
                            <asp:Label ID="lblSDSNro" Text="" runat="server" />
                        </strong>
                        <br />
                        <asp:Label Text="Documento no válido como factura" runat="server" />
                        <br />
                        <br />
                        <asp:Label Text="Fecha" runat="server" />
                        <strong>
                            <asp:Label ID="lblFecha" Text="" runat="server" />
                        </strong>
                        <br />
                        <asp:Label Text="Vencimiento" runat="server" />
                        <strong>
                            <asp:Label ID="lblVencimiento" Text="30/03/2019" runat="server" />
                        </strong>
                        <br />
                        <br />
                        <br />
                        <asp:Label Text="CUIT 30-71238125-2 - Ing.Brutos 000-000000-0 - Fecha Inicio 01/01/2000" runat="server" Style="font-size: 9px;" />
                    </td>
                </tr>

                <%-- Cliente --%>
                <tr style="line-height: 1.25;">
                    <td colspan="16" style="padding-top: 10px;">
                        <asp:Label Text="Cliente:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteRazonSocial" Text="" runat="server" />
                        </strong>
                    </td>
                </tr>
                <tr style="line-height: 1.25;">
                    <td colspan="8">
                        <asp:Label Text="Dirección:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteDireccion" Text="" runat="server" />
                        </strong>
                    </td>
                    <td colspan="4">
                        <asp:Label Text="Localidad:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteLocalidad" Text="Córdoba" runat="server" />
                        </strong>
                    </td>
                    <td colspan="4">
                        <asp:Label Text="Código Postal:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteCodigoPostal" Text="5000" runat="server" />
                        </strong>
                    </td>
                </tr>

                <tr style="line-height: 1.25;">
                    <td colspan="7">

                    </td>
                    <td colspan="5">
                        <asp:Label Text="IVA:" runat="server" />
                        <strong>
                            <asp:Label Text="Responsable Inscripto" runat="server" />
                        </strong>
                    </td>
                    <td colspan="4">
                        <asp:Label Text="CUIT:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteCuit" Text="" runat="server" />
                        </strong>
                    </td>
                </tr>

                <%-- Entregar a: --%>
                <tr style="line-height: 1.25;">
                    <td colspan="12" style="padding-top: 10px;">
                        <asp:Label Text="Entregar a:" runat="server" />
                        <strong>
                            <asp:Label ID="lblEntregarA" Text="INTELE S.R.L" runat="server" />
                        </strong>
                    </td>
                    <td colspan="3" style="padding-top: 10px;">
                        <asp:Label Text="Cant. de bultos:" runat="server" />
                        <strong>
                            <asp:Label ID="lblCantidadBultos" Text="" runat="server" />
                        </strong>
                    </td>
                </tr>
                <tr style="line-height: 1.25;">
                    <td colspan="7">
                        <asp:Label Text="Dirección:" runat="server" />
                        <strong>
                            <asp:Label ID="lblDomicilioDestino" Text="Av. Sabattini 1863 - 5000 - B° Maipú" runat="server" />
                        </strong>
                    </td>
                    <td colspan="5">
                        <asp:Label Text="Forma de pago:" runat="server" />
                        <strong>
                            <asp:Label id="lblFormaPago" Text="" runat="server" />
                        </strong>
                    </td>
                    <td colspan="4">
                        <asp:Label Text="Horario:" runat="server" />
                        <strong>
                            <asp:Label Text="8:00 a 18:00" runat="server" />
                        </strong>
                    </td>
                </tr>
                <tr style="height: 10px;">
                </tr>

                <%-- Encabezado de las columnas del detalle --%>
                <tr style="text-align: center; height: 25px; vertical-align: middle;">
                    <td colspan="1" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="Cant." runat="server" />
                    </td>
                    <td colspan="3" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="Codigo" runat="server" />
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="Descripción" runat="server" />
                    </td>
                    <td colspan="2" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="Precio Unitario" runat="server" />
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="Subtotal Sin IVA" runat="server" />
                    </td>
                </tr>
            </thead>

            <tbody>

                <%-- Repeater Detalle - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>

                <asp:Repeater ID="rptDetalle" runat="server">
                    <ItemTemplate>

                        <tr>
                            <td colspan="1" style="border-left: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-right: 5px; text-align: right;">
                                <asp:Label Text='<%# Eval("Cantidad") %>' runat="server" />
                            </td>
                            <td colspan="3" style="border-left: solid; border-width: thin; border-color: darkgray; padding-top: 5px; text-align: center;">
                                <asp:Label Text='<%# Eval("Codigo") %>' runat="server" />
                            </td>
                            <td colspan="5" style="border-left: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-left: 5px; text-align: left;">
                                <asp:Label Text='<%# Eval("Descripcion") %>' runat="server" />
                            </td>
                            <td colspan="2" style="border-left: solid; border-right: solid; border-width: thin; border-color: darkgray; padding-top: 5px; text-align: center;">
                                <asp:Label Text='<%# Eval("PrecioUnitario") %>' runat="server" />
                            </td>
                            <td colspan="5" style="border-left: solid; border-right: solid; border-width: thin; border-color: darkgray; padding-top: 5px; text-align: center;">
                                <asp:Label Text='<%# Eval("SubtotalSinIVA") %>' runat="server" />
                            </td>
                        </tr>

                    </ItemTemplate>
                </asp:Repeater>

                <tr style="text-align: center; height: 25px; vertical-align: middle;">
                    <td colspan="1" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="3" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="2" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="A PAGAR:" runat="server" />
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label ID="lblMontoAPagar" Text="" runat="server" />
                    </td>
                </tr>
                <tr style="text-align: center; height: 25px; vertical-align: middle;">
                    <td colspan="1" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="3" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        
                    </td>
                    <td colspan="2" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label Text="IVA 21%:" runat="server" />
                    </td>
                    <td colspan="5" style="border: solid; border-width: thin; border-color: darkgray;">
                        <asp:Label ID="lblIVA21" Text="" runat="server" />
                    </td>
                </tr>
                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                <tr id="lastTr" style="height: 65vh;">
                    <td colspan="1" style="border-left: solid; border-bottom: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-right: 5px; text-align: right;"></td>
                    <td colspan="3" style="border-left: solid; border-bottom: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-right: 5px; text-align: right;"></td>
                    <td colspan="5" style="border-left: solid; border-bottom: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-right: 5px; text-align: right;"></td>
                    <td colspan="2" style="border-left: solid; border-bottom: solid; border-width: thin; border-color: darkgray; padding-top: 5px; text-align: center;"></td>
                    <td colspan="5" style="border-left: solid; border-right: solid; border-bottom: solid; border-width: thin; border-color: darkgray; padding-top: 5px; padding-left: 5px; text-align: left;"></td>
                </tr>

            </tbody>

        </table>

        <div>
            <table>
                <tr>
                    <td colspan="16" style="border: solid; border-width: thin; border-color: darkgray; padding: 10px;">
                        <asp:Label Text="Términos y condiciones:" runat="server" />
                        <strong>
                            <asp:Label ID="lblClienteTerminosYCondiciones" Text="" runat="server" />
                        </strong>
                    </td>
                </tr>
            </table>
        </div>

        <div id="footer">
            <table>
                <tr>
                    <td colspan="6" style="border: solid; border-width: thin; border-color: darkgray; padding: 10px;">
                        <br />
                        <br />
                        <asp:Label Text="Total:" runat="server" />
                        <strong>
                            <asp:Label ID="lblMontoTotal" Text="" runat="server" />
                        </strong>
                    </td>
                    <td colspan="10" style="border: solid; border-width: thin; border-color: darkgray; padding: 10px;">
                        <br />
                        <br />
                        <br />
                        <br />
                        <asp:Label Text="Sello, firma y aclaración" runat="server" Style="font-size: 10px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="14" style="padding-top: 5px; font-size: 10px;">
<%--                        <strong>
                            <asp:Label Text="MERCADERÍA REVISADA Y CONTROLADA" runat="server" />
                            <br />
                            <asp:Label Text="PASADAS LAS 72 HS DE RECIBIDA LA MISMA NO SE ACEPTAN CAMBIOS NI DEVOLUCIONES" runat="server" />
                        </strong>--%>
                    </td>
                    <td colspan="2" style="padding: 5px; text-align: center; vertical-align: bottom;">
                        <asp:Label Text="ORIGINAL" runat="server" />
                    </td>
                </tr>
            </table>

        </div>

        <div style="page-break-after: always;"></div>

    </form>
</body>
</html>
