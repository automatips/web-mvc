﻿using log4net;
using MagicSQL;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Reflection;
using static Code.Utils;

public partial class ResetPwd : System.Web.UI.Page
{
    private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    protected void Page_Load(object sender, EventArgs e)
    {
        // Version de la compilación.
        lblVersion.Text = "Versión: " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }

    protected void btnResetPwd_OnClick(object sender, EventArgs e)
    {
        // Se valida el captcha
        if (reCAPTCHA())
        {
            // Busca el usuario utilizando el nombre de usuario y la dirección de correo electrónico
            DataTable dt;
            using (SP sp = new SP("CGUsuarios"))
            {
                dt = sp.Execute("usp_GetIdUsuarioByEmail", P.Add("email", txtEmail.Text));
            }

            // Si se encontró un usuario con la direccion de correo electrónico ingresada
            if (dt.Rows.Count > 0)
            {
                // Id del usuario
                long idUsuario = dt.Rows[0][0].ToString().ToLong();

                // Nueva contraseña numérica y aleatoria de 8 dígitos
                string newPwd = GetRandom(10000000, 99999999);

                // Objeto usuario para actualizar la contraseña
                CGUsuarios.Usuario usuario = new CGUsuarios.Usuario().Select(idUsuario);
                usuario.Password = newPwd.ToString().GetSHA256();
                usuario.Update();

                // Se envía un email con la nueva contraseña
                new BulkMail.Mail()
                {
                    AddDateTime = DateTime.Now,
                    DisplayName = "Sistema Administrativo - Control Global",
                    EmailFrom = "noreply@controlglobal.com.ar",
                    Subject = "Recuperación de contraseña",
                    Email = txtEmail.Text,
                    Body = "Su nueva contraseña es: " + newPwd
                }.Insert();

                pnlEmailSent.Visible = true;
                ExecJsFn("DisableControls");
            }
            else
            {
                pnlEmailSent.Visible = true;
                lblResult.Text = "El correo electrónico ingresado no pertenece a un usuario del Sistema de Administración<br/>";
            }
        }
        else
        {
            Message("CAPTCHA", "Debe resolver el CAPTCHA", MessagesTypes.warning);
        }
    }

    private bool reCAPTCHA()
    {
        string secret = "6Ldh-UkUAAAAAHfnOHLvpLudGdEnJx1F_hvZVDrk"; // Clave secreta
        string response = Request["g-recaptcha-response"];
        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + response);
        using (WebResponse webResponse = httpWebRequest.GetResponse())
        {
            using (StreamReader streamReader = new StreamReader(webResponse.GetResponseStream()))
            {
                if (streamReader.ReadToEnd().Contains("\"success\": true"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}