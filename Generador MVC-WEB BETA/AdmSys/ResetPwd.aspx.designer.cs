﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------



public partial class ResetPwd {
    
    /// <summary>
    /// Control txtEmail.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtEmail;
    
    /// <summary>
    /// Control btnResetPwd.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnResetPwd;
    
    /// <summary>
    /// Control pnlEmailSent.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnlEmailSent;
    
    /// <summary>
    /// Control lblResult.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblResult;
    
    /// <summary>
    /// Control lblVersion.
    /// </summary>
    /// <remarks>
    /// Campo generado automáticamente.
    /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblVersion;
}
