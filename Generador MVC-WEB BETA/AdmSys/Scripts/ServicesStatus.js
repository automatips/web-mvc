﻿// Consulta cada # segundos el estado de los servicio
ServicesStatus();

setInterval(ServicesStatus, 30000);

function ServicesStatus() {
    $.ajax("Handlers/ServicesStatus.ashx")
    .done(function (data) {
        console.log(data);

        // Services
        switch (data.Status) {
            case 0:
                $("#iServicesStatus").addClass("services-down").removeClass("services-up-down").removeClass("services-up").removeClass("fa-question-circle service-unknown");
                break;
            case 1:
                $("#iServicesStatus").addClass("services-up-down").removeClass("services-down").removeClass("services-up").removeClass("fa-question-circle service-unknown");
                break;
            case 2:
                $("#iServicesStatus").addClass("services-up").removeClass("services-down").removeClass("services-up-down").removeClass("fa-question-circle service-unknown");
                break;
        }

        switch (data.Liquidaciones) {
            case 1:
                $("#iServiceLiquidaciones").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceLiquidaciones").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceLiquidaciones").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.PrincipalCevallos) {
            case 1:
                $("#iServicePrincipalCevallos").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServicePrincipalCevallos").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServicePrincipalCevallos").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.PrincipalRingo) {
            case 1:
                $("#iServicePrincipalRingo").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServicePrincipalRingo").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServicePrincipalRingo").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.ConfiguracionUM) {
            case 1:
                $("#iServiceConfiguracionUM").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceConfiguracionUM").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceConfiguracionUM").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.VendingControl) {
            case 1:
                $("#iServiceVendingControl").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceVendingControl").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceVendingControl").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.RenovacionesVending) {
            case 1:
                $("#iServiceRenovacionesVending").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceRenovacionesVending").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceRenovacionesVending").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.CashVend) {
            case 1:
                $("#iServiceCashVend").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceCashVend").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceCashVend").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.AccessControl) {
            case 1:
                $("#iServiceAccessControl").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceAccessControl").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceAccessControl").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }

        switch (data.Asignaciones) {
            case 1:
                $("#iServiceAsignaciones").addClass("fa-check-circle service-up").removeClass("fa-times-circle service-down").removeClass("fa-question-circle service-unknown");
                break;
            case 0:
                $("#iServiceAsignaciones").addClass("fa-times-circle service-down").removeClass("fa-check-circle service-up").removeClass("fa-question-circle service-unknown");
                break;
            case -1:
                $("#iServiceAsignaciones").addClass("fa-question-circle service-unknown").removeClass("fa-times-circle service-down").removeClass("fa-check-circle service-up");
                break;
        }
    })
    .fail(function () {
        console.log("Error checking services status");
    })
}