﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucIngresarRemito.ascx.cs" Inherits="AdmSys.Controls.ucIngresarRemito" %>

<asp:UpdatePanel ID="upIngresarRemito" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label>
                <asp:Label Text="Observaciones" runat="server" />
            </label>
            <asp:TextBox ID="txtObsAlta" CssClass="form-control" runat="server" TextMode="MultiLine" />
        </div>

        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label>
                <asp:Label Text="Mail Informe" runat="server" />
            </label>
            <asp:TextBox ID="txtMailInforme" CssClass="form-control" runat="server" TextMode="SingleLine" />
        </div>
	</ContentTemplate>
</asp:UpdatePanel>