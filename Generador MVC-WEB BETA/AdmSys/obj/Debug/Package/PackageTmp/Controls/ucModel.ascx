﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucModel.ascx.cs" Inherits="AdmSys.Controls.ucModel" %>

<asp:UpdatePanel ID="upUCModel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
			<label>
				<asp:Label ID="lblErrorHandler" Text="" runat="server" />
			</label>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
			<label>
				<asp:Label ID="lblModelContenido" Text="Contenido" runat="server" />
			</label>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>
