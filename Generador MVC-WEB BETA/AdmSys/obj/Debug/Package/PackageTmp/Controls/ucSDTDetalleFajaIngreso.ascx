﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSDTDetalleFajaIngreso.ascx.cs" Inherits="AdmSys.Controls.ucSDTDetalleFajaIngreso" %>

<asp:UpdatePanel ID="upSDTDetalleFajaIngreso" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
        
        <center>
            <div class="input-group col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span class="input-group-addon transparent" title="Código barras de la faja de ingreso">
                    
                    <span class="glyphicon glyphicon-barcode"></span>

                </span>
                <asp:TextBox ID="txtFajaIngreso"
                    runat="server"
                    CssClass="form-control numeric-integer-positive"
                    PlaceHolder="Pendiente"
                    Visible="true"
                    Enabled="false"
                    Width="110"
                    />
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <asp:LinkButton ID="btnVerHistorialDeArtefacto"
                    runat="server"
                    CssClass="btn btn-info"
                    Style="margin-left: 5px;"
                    title="Ver historial del artefacto"
                    >
                    <span aria-hidden="true" class="glyphicon glyphicon-th-list"></span>
                </asp:LinkButton>
            </div>
        </center>

	</ContentTemplate>
</asp:UpdatePanel>
