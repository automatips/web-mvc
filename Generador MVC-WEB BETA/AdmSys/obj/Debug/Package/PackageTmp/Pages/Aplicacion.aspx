﻿<%@ Page Title="Aplicacion" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Aplicacion.aspx.cs" Inherits="Pages.Aplicacion" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Aplicacion?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Aplicacion --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12" id="fgrReseller" runat="server">
                        <label>
                            <asp:Label Text="Sistema" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlSistema" CssClass="form-control select-single" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="ddlSistema" InitialValue="#" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Nombre" runat="server" />
                        </label>
                        <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Url" runat="server" />
                        </label>
                        <asp:TextBox ID="txtUrl" CssClass="form-control" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtUrl" runat="server" />
                    </div>

                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Descripción" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtDescripcion" runat="server"></textarea>
                    </div>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <div class="bottom-buttons">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <a href="Aplicaciones.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>