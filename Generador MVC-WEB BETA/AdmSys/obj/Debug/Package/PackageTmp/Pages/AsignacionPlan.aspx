﻿<%@ Page Title="Asignar Plan Comercial" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="AsignacionPlan.aspx.cs" Inherits="Pages.AsignacionPlan" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Desasignar el Plan Comercial?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del AsignacionPlano --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Distribuidor" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Unidad Negocio" runat="server" />
                        </label>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Reseller" runat="server" />
                        </label>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlVendingReseller" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Plan Comercial" runat="server" />
                        </label>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlPlanComercial" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Cantidad UM" runat="server" />
                        </label>
                        <asp:TextBox ID="txtCantidad" CssClass="form-control numeric-integer-positive" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Forzar Cantidad" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="No" runat="server" />
                                <asp:CheckBox ID="chkForzarCantidadUM" runat="server" />
                                <asp:Label Text="Si" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Liquidar Manualmente" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="No" runat="server" />
                                <asp:CheckBox ID="chkLiquidaManual" runat="server" />
                                <asp:Label Text="Si" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Ex" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="No" runat="server" />
                                <asp:CheckBox ID="chkEx" runat="server" />
                                <asp:Label Text="Si" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Liq. s/ Operativas" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="No" runat="server" />
                                <asp:CheckBox ID="chkLiquidaSoloOperativas" runat="server" />
                                <asp:Label Text="Si" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                        <span class="empty-required-field-validator"></span>
                    </div>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón AsignacionPlans, Guardar y Restablecer --%>
            <div class="btn-models">
                <a href="AsignacionesPlanes.aspx" class="btn btn-info">
                    <asp:Label Text="Volver a la Lista" runat="server" />
                </a>
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
            </div>

            <br />
            <br />
        </div>
    </div>
</asp:Content>