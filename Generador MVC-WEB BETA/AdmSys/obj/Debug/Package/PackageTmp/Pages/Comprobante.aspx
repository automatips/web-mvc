﻿<%@ Page Title="Comprobante" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Comprobante.aspx.cs" Inherits="Pages.Comprobante" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar la Lista de Precio?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Comprobanteo --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel-body">

                <div class="row">

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Distribuidor" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Unidad Negocio" runat="server" />
                        </label>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Reseller" runat="server" />
                        </label>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlVendingReseller" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Descripción" runat="server" />
                        </label>
                        <asp:TextBox ID="txtDescripcion" CssClass="form-control" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Tipo Interno" runat="server" />
                        </label>
                        <asp:TextBox ID="txtTipoComprobanteInterno" CssClass="form-control numeric-positive" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Nro. Comprobante Interno" runat="server" />
                        </label>
                        <asp:TextBox ID="txtNroComprobanteInterno" CssClass="form-control numeric-positive" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Tipo Externo" runat="server" />
                        </label>
                        <asp:TextBox ID="txtTipoComprobante" CssClass="form-control numeric-positive" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Nro. Comprobante" runat="server" />
                        </label>
                        <asp:TextBox ID="txtNroComprobante" CssClass="form-control numeric-positive" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Fecha Emision" runat="server" />
                        </label>
                        <asp:TextBox ID="txtFHEmisionComprobante" CssClass="form-control date-time" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Moneda" runat="server" />
                        </label>
                        <asp:TextBox ID="txtMoneda" CssClass="form-control numeric-positive" runat="server" />
                    </div>
                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <h4 class="page-header">
                            <asp:Label ID="lblUM" Text="Información Adicional:" runat="server" />
                        </h4>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvComprobanteDetalle" CssClass="table table-striped table-bordered table-hover grid-view"
                                    Width="100%" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" DataField="IdDistribuidorReseller" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                                        <asp:BoundField HeaderText="Precio Unitario" DataField="PrecioUnitario" ItemStyle-CssClass="numeric-money" />
                                        <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" />
                                        <asp:BoundField HeaderText="SubTotal" DataField="PrecioTotal" ItemStyle-CssClass="numeric-money" />
                                        <asp:BoundField HeaderText="Observación" DataField="Observacion" />
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Importe Neto Gravado" runat="server" />
                            </label>
                            <asp:TextBox ID="txtImporteNetoGravado" CssClass="form-control numeric-money-positive" runat="server" />
                        </div>

                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="IVA" runat="server" />
                            </label>
                            <asp:TextBox ID="txtImporteIva" CssClass="form-control numeric-money-positive" runat="server" />
                        </div>

                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Importe Total" runat="server" />
                            </label>
                            <asp:TextBox ID="txtImporteTotal" CssClass="form-control numeric-money-positive" runat="server" />
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <asp:Label Text="Factura AFIP" runat="server" />
                            </label>
                            <asp:Label ID="lblEncrypId" runat="server" Visible="false"></asp:Label>
                            <asp:FileUpload ID="fileImagen" runat="server" CssClass="form-control file" data-show-preview="false" />
                            <asp:Button ID="btnSubirArchivo" Text="Subir" CssClass="btn btn-success" runat="server" OnClick="btnSubirArchivo_Click" />
                        </div>

                    </div>
                </div>

            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón Comprobantes, Guardar y Restablecer --%>
            <div class="btn-models">
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
            </div>

            <br />
            <br />

        </div>
    </div>
</asp:Content>