﻿<%@ Page Title="Factura" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ComprobanteManual.aspx.cs" Inherits="Pages.ComprobanteManual" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar la Lista de Precio?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }

        function ScrollToBottom() {
            $('html, body').scrollTop($(document).height());
        }

        function ConfirmComprobante() {
            var tipoComprobante = '';
            if ($('input[id$=cphBody_optFactura]:checked').val() == 'optFactura') {
                tipoComprobante = 'Factura';
            }
            if ($('input[id$=cphBody_optCredito]:checked').val() == 'optCredito') {
                tipoComprobante = 'Nota Crédito';
            }
            if ($('input[id$=cphBody_optDebito]:checked').val() == 'optDebito') {
                tipoComprobante = 'Nota Débito';
            }
            var date = new Date($('#cphBody_txtFechaVencimiento').val());
            var vencimiento = '';
            if (date.getDate() < new Date().getDate()) {
                vencimiento = "<li style='color: red;'>Fecha Vencimiento: " + $('#cphBody_txtFechaVencimiento').val() + "</li>";
            }
            else {
                vencimiento = "<li>Fecha Vencimiento: " + $('#cphBody_txtFechaVencimiento').val() + "</li>";
            }
            swal({
                html: true,
                title: "Está por cargar el siguiente comprobante: ",
                text: "<ul><li><strong>" + tipoComprobante + "</strong></li><li>Reseller: " + $('#cphBody_ddlVendingReseller :selected').text() + "</li><li>Importe Neto: " + $('#cphBody_txtImporteTotal').val() + "</li><li>IVA: " + $('#cphBody_txtIva').val() + "</li><li>Importe Total: " + $('#cphBody_txtTotalConImpuestos').val() + "</li>" + vencimiento + "</ul>",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Continuar",
                closeOnConfirm: false
            },
        function () {
            __doPostBack('<%= btnSave.UniqueID %>', "");
        });
        }

    </script>

</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del ListaPrecioo --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel-body">
                <div class="row">

                    <label class="radio-inline">
                        <asp:Label Text="" runat="server" ID="lblIdComprobante" />
                        <asp:Label Text="" runat="server" ID="lblTipoNota" />
                    </label>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Distribuidor" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Unidad Negocio" runat="server" />
                        </label>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Reseller" runat="server" />
                        </label>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlVendingReseller" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendingReseller_SelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Plan Comercial" runat="server" />
                        </label>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlPlanComercial" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlVendingReseller" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">

                        <label>
                            <asp:Label Text="Tipo de Comprobante" runat="server" />
                        </label>

                        <br />

                        <label class="radio-inline">
                            <asp:RadioButton ID="optFactura" runat="server" GroupName="Horizontales" Enabled="true" />
                            <asp:Label Text="Factura" runat="server" />
                        </label>
                        <label class="radio-inline">
                            <asp:RadioButton ID="optCredito" runat="server" GroupName="Horizontales" Checked="False" />
                            <asp:Label Text="Nota Crédito" runat="server" />
                        </label>
                        <label class="radio-inline">
                            <asp:RadioButton ID="optDebito" runat="server" GroupName="Horizontales" Checked="False" />
                            <asp:Label Text="Nota Débito" runat="server" />
                        </label>
                        <label class="radio-inline">
                            <asp:RadioButton ID="optSaldoInicial" runat="server" GroupName="Horizontales" Checked="False" />
                            <asp:Label Text="Saldo Inicial" runat="server" Visible="false" />
                        </label>

                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Fecha Vencimiento" runat="server" />
                        </label>
                        <asp:TextBox ID="txtFechaVencimiento" CssClass="form-control date-picker" runat="server" />
                        <asp:RequiredFieldValidator ControlToValidate="txtFechaVencimiento" ErrorMessage="" CssClass="required-field-validator" SetFocusOnError="True" runat="server"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <label>
                            <asp:Label Text="Factura AFIP" runat="server" />
                        </label>
                        <asp:FileUpload ID="fileImagen" runat="server" CssClass="form-control file" data-show-preview="false" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Precio Lista" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="No" runat="server" />
                                <asp:CheckBox ID="chkPrecioLista" runat="server" />
                                <asp:Label Text="Si" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <h4>
                                    <asp:Label Text="Detalle Artículos" runat="server" />
                                    <asp:Button ID="btnAddGridRow" CssClass="btn btn-info pull-right" Text="Agregar Detalle" OnClick="btnAddGridRow_Click" runat="server" ToolTip="Agrega una nueva fila en el detalle del comprobante" />
                                </h4>

                                <hr />

                                <asp:GridView ID="gvDetalleComprobante" CssClass="table table-striped table-bordered table-hover inline-edit"
                                    Width="100%" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvDetalleComprobante_RowDeleting"
                                    EmptyDataText="No se definieron Artículos">
                                    <Columns>

                                        <%-- IdComprobanteDetalle --%>
                                        <asp:TemplateField HeaderText="IdComprobanteDetalle" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIdComprobanteDetalle" CssClass="form-control-static" Width="100%" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Selector del Producto --%>
                                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="400px">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlItem" CssClass="form-control select-single" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlItem_TextChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Descripcion --%>
                                        <asp:TemplateField HeaderText="Descripción" Visible="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDescripcion" CssClass="form-control" MaxLength="8" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Cantidad --%>
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCantidad" CssClass="form-control numeric-integer" MaxLength="3" Width="100%" runat="server" OnTextChanged="txtCantidad_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtCantidad" ErrorMessage="" CssClass="required-field-validator" SetFocusOnError="True" runat="server">
                                                </asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Precio --%>
                                        <asp:TemplateField HeaderText="Precio" ItemStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPrecioUnitario" CssClass="form-control numeric-money select-onfocus" MaxLength="8" Width="100%" runat="server" OnTextChanged="txtPrecioUnitario_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Precio Rebajado --%>
                                        <asp:TemplateField HeaderText="Precio Rebajado" ItemStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPrecioRebajado" CssClass="form-control numeric-money select-onfocus" MaxLength="8" Width="100%" runat="server" OnTextChanged="txtPrecioRebajado_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- SubTotal --%>
                                        <asp:TemplateField HeaderText="SubTotal" ItemStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSubTotal" CssClass="form-control numeric-money" MaxLength="8" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Observacion --%>
                                        <asp:TemplateField HeaderText="Observación">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtObservacion" CssClass="form-control" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- Eliminar --%>
                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.haotu.net/up/4234/24/212-bin.png" />
                                    </Columns>
                                </asp:GridView>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <label>
                                        <asp:Label Text="Subtotal" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtImporteTotal" CssClass="form-control numeric-money-positive" runat="server" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <label>
                                        <asp:Label Text="IVA" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtIva" CssClass="form-control numeric-money-positive" runat="server" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <label>
                                        <asp:Label Text="Importe Total" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtTotalConImpuestos" CssClass="form-control numeric-money-positive" runat="server" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAddGridRow" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                    </div>

                </div>

                <%-- Separador vertical --%>
                <hr />

                <%-- Botón ListaPrecios, Guardar y Restablecer --%>
                <div class="btn-models">
                    <a href="ProductosPorPlanComercial.aspx" class="btn btn-info">
                        <asp:Label Text="Volver a la Lista" runat="server" />
                    </a>
                </div>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClientClick="ConfirmComprobante(); return false;" OnClick="btnSave_Click" />
                </div>

                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>