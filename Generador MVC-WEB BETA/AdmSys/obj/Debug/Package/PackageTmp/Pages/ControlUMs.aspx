﻿<%@ Page Title="Control UMs" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ControlUMs.aspx.cs" Inherits="Pages.ControlUMs" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #chart1div {
            height: 400px;
        }

        #chart2div {
            height: 250px;
            margin-top: 75px;
            margin-left: -15px;
        }

        .background, .content {
            background-color: #f2f2f2;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            margin-left: 0;
            margin-right: 0;
            margin-bottom: 15px;
        }

        .card {
            height: 400px;
            border: 1px solid #d6d6d6;
            background: white;
            border-radius: 5px;
            box-shadow: 0 1px 4px rgba(154, 154, 154, 0.3);
            margin: 10px;
        }

        .title-fix {
            font-family: Verdana;
            font-size: 13px;
        }

        .title-fix-container {
            padding-top: 15px;
            text-align: center;
        }

        #chart1div > div > div > a {
            display: none !important;
        }

        #chart2div > div > div > a {
            display: none !important;
        }

        @media only screen and (max-width: 768px) {
            .card {
                height: 200px;
            }

            #chart1div {
                height: 200px;
            }

            #chart2div {
                height: 120px;
                margin-top: 15px;
                margin-left: -25px;
            }
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>

    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        var AMCHART_SERIAL_CONFIG;
        var AMCHART_PIE_CONFIG;

        function drawEstado(chartDataEstado) {

            AMCHART_SERIAL_CONFIG = {
                "type": "serial",
                "titles": [{
                    "text": "Estado de las UMs",
                    "bold": false
                }],
                "theme": "none",
                "marginRight": 70,
                "dataProvider": chartDataEstado,
                "startDuration": 0,
                "graphs": [{
                    "balloonText": "<b>[[category]]: [[value]]</b>",
                    "fillColorsField": "color",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "cantidad"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "estado",
                "categoryAxis": {
                    "gridPosition": "start",
                    "labelRotation": 0
                },
                "export": {
                    "enabled": false
                }
            };
        }

        function drawConexion(chartDataConexion) {

            AMCHART_PIE_CONFIG = {
                "type": "pie",
                "theme": "none",
                "dataProvider": chartDataConexion,
                "startDuration": 0,
                "titleField": "conexion",
                "valueField": "cantidad",
                "labelRadius": 5,
                "colorField": "color",

                "radius": "42%",
                "innerRadius": "0%",
                "labelText": "[[title]]",
                "export": {
                    "enabled": false
                }
            };
        }

        AmCharts.ready(function () {
            var chart1 = AmCharts.makeChart("chart1div", $.extend(true, {}, AMCHART_SERIAL_CONFIG, { "theme": "none" }));
            var chart2 = AmCharts.makeChart("chart2div", $.extend(true, {}, AMCHART_PIE_CONFIG, { "theme": "none" }));
        });

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="Control UMs" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>

    <div class="row background">
        <div class="col-lg-8 card">
            <asp:UpdatePanel ID="upGraphEstado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="chart1div"></div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-lg-3 card">
            <asp:UpdatePanel ID="upGraphConexion" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-6 title-fix-container">
                            <asp:Label runat="server" CssClass="title-fix" Text="Estado de las Conexiones"></asp:Label>
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12" id="chart2div"></div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>