﻿<%@ Page Title="Cuenta Bancaria" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="CuentaBancaria.aspx.cs" Inherits="Pages.CuentaBancaria" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Cuenta Bancaria?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del CuentaBancariao --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Distribuidor" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Banco" runat="server" />
                        </label>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:DropDownList ID="ddlBanco" CssClass="form-control select-single" runat="server">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Descripción" runat="server" />
                        </label>
                        <asp:TextBox ID="txtDescripcion" CssClass="form-control" Text="" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtDescripcion" runat="server" />
                    </div>

                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón CuentaBancarias, Guardar y Restablecer --%>
            <div class="btn-models">
                <a href="CuentasBancarias.aspx" class="btn btn-info">
                    <asp:Label Text="Volver a la Lista" runat="server" />
                </a>
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
            </div>
        </div>
    </div>
</asp:Content>