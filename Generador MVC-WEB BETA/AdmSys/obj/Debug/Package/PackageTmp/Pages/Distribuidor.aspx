﻿<%@ Page Title="Distribuidor" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Distribuidor.aspx.cs" Inherits="Pages.Distribuidor" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el Distribuidor?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del Distribuidoro --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Cuit" runat="server" />
                        </label>
                        <asp:TextBox ID="txtCuit" CssClass="form-control text-lower email" Text="" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtCuit" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Razon Social" runat="server" />
                        </label>
                        <asp:TextBox ID="txtRazonSocial" CssClass="form-control" Text="" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtRazonSocial" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Email" runat="server" />
                        </label>
                        <asp:TextBox ID="txtEmail" CssClass="form-control text-lower email" Text="" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtEmail" runat="server" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Telefono" runat="server" />
                        </label>
                        <asp:TextBox ID="txtTelefono" CssClass="form-control" Text="" runat="server" />
                        <asp:RequiredFieldValidator ErrorMessage="" CssClass="required-field-validator" ControlToValidate="txtEmail" runat="server" />
                    </div>

                    <div class="form-group col-lg-6 col-md-8 col-sm-12 col-xs-12">
                        <label>
                            <asp:Label Text="Dirección" runat="server" />
                        </label>
                        <div class="input-group">
                            <asp:TextBox ID="txtDireccion" CssClass="form-control text-address" runat="server" />
                            <span class="input-group-btn">
                                <button id="btnAbrirDireccion" runat="server" class="btn btn-default" type="button" data-toggle="modal" data-target="#address-modal">
                                    <i class="fa fa-map-marker"></i>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <label>
                            <asp:Label Text="Imagen" runat="server" />
                        </label>
                        <asp:FileUpload ID="fileImagen" runat="server" CssClass="form-control file" data-show-preview="false" />
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Imputación Parcial" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:CheckBox ID="chkPermiteImputacionParcial" runat="server" />
                                <asp:Label Text="Permitir" runat="server" />
                            </label>
                        </div>

                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Pago Sobrante" runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:CheckBox ID="chkPermitePagoSobrante" runat="server" />
                                <asp:Label Text="Permitir" runat="server" />
                            </label>
                        </div>

                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Invertir Cta Cte." runat="server" />
                        </label>

                        <div class="checkbox">
                            <label>
                                <asp:Label Text="(-)" runat="server" />
                                <asp:CheckBox ID="chkInvertir" runat="server" />
                                <asp:Label Text="(+)" runat="server" />
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>
                                <asp:Label Text="Id Remitero" runat="server" />
                            </label>
                            <asp:TextBox ID="txtIdRemitero" CssClass="form-control numeric-integer-positive" Text="" runat="server" />
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label>
                                <asp:Label Text="Ultimo Remito" runat="server" />
                            </label>
                            <asp:TextBox ID="txtUltimoRemito" CssClass="form-control numeric-integer-positive" Text="" runat="server" />
                        </div>

                    </div>

                </div>

                <div class="row">
                    <h4>Usuario Web</h4>
                    <hr />
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Email" runat="server" />
                        </label>
                        <asp:TextBox ID="txtEmailUsuario" CssClass="form-control text-lower email" Text="" runat="server" OnTextChanged="txtEmail_TextChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtEmailUsuario" runat="server" />
                    </div>

                    <div class="row" id="divUsuarioInicial" runat="server">
                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Nombre" runat="server" />
                            </label>
                            <asp:TextBox ID="txtNombre" CssClass="form-control text-capital" Text="" runat="server" Enabled="false" />
                            <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNombre" runat="server" />
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Apellido" runat="server" />
                            </label>
                            <asp:TextBox ID="txtApellido" CssClass="form-control text-capital" Text="" runat="server" Enabled="false" />
                            <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtApellido" runat="server" />
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>
                                &nbsp;
                            </label>
                            <asp:Button ID="btnSendValidationEmail" Text="Reenviar email de validación" CssClass="form-control btn btn-primary" runat="server" Visible="False" OnClick="btnSendValidationEmail_Click" CausesValidation="False" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <h4>Asignaciones</h4>
                    <hr />
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label>
                            <asp:Label Text="Unidades de negocios" runat="server" />
                        </label>
                        <asp:GridView ID="gvUnidadesNegocio" CssClass="table table-striped table-bordered table-hover checkable"
                            Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvUnidadesNegocio_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                            <Columns>

                                <%-- Id encriptado --%>
                                <asp:BoundField HeaderText="IdUnidadNegocio" DataField="IdUnidadNegocio" />

                                <%-- Checkbox --%>
                                <asp:TemplateField HeaderText="Asignada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelected" runat="server" Checked='<%# ((int)Eval("Asignada") == 0 ? false : true) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- Descripción --%>
                                <asp:BoundField HeaderText="Unidad Negocio" DataField="Descripcion" ItemStyle-CssClass="" />
                            </Columns>
                        </asp:GridView>
                    </div>

                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">

                        <label>
                            <asp:Label Text="Regiones" runat="server" />
                        </label>
                        <asp:GridView ID="gvRegiones" CssClass="table table-striped table-bordered table-hover checkable"
                            Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvUnidadesNegocio_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                            <Columns>
                                <%-- Id encriptado --%>
                                <asp:BoundField HeaderText="IdRegion" DataField="IdRegion" />

                                <%-- Checkbox --%>
                                <asp:TemplateField HeaderText="Asignada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelected" runat="server" Checked='<%# ((int)Eval("Asignada") == 0 ? false : true) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- Descripción --%>
                                <asp:BoundField HeaderText="Region" DataField="Descripcion" ItemStyle-CssClass="" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón Distribuidors, Guardar y Restablecer --%>
            <div class="btn-models">
                <a href="Distribuidores.aspx" class="btn btn-info">
                    <asp:Label Text="Volver a la Lista" runat="server" />
                </a>
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
            </div>

            <br />
            <br />
        </div>
    </div>
</asp:Content>