﻿<%@ Page Title="Distribuidores" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Distribuidores.aspx.cs" Inherits="Pages.Distribuidores" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Tabla de resultados de búsqueda de Distribuidores --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvDistribuidores" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" data-model="Distribuidor.aspx" data-Order="3-asc"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvDistribuidores_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:BoundField HeaderText="#" DataField="IdDistribuidor" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField HeaderText="Cuit" DataField="Cuit" />
                            <asp:BoundField HeaderText="Razon Social" DataField="RazonSocial" />
                            <asp:BoundField HeaderText="Email" DataField="Email" />
                            <asp:BoundField HeaderText="Observaciones" DataField="Observacion" />
                            <asp:BoundField HeaderText="Fecha Alta" DataField="FHAlta" ItemStyle-CssClass="col-date-time show-total" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>