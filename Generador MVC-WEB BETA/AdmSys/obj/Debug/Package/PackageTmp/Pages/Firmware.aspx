﻿<%@ Page Title="Firmwares de UMs y Teclados" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Firmware.aspx.cs" Inherits="Pages.Firmware" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        .panel-body {
            padding-top: 20px;
        }

        .separador-vertical {
            height: 7px;
        }

        .btn-td {
            padding-top: 2px !important;
            padding-bottom: 0px !important;
        }

        .btn-edit {
            height: 28px;
        }

        .top-buttons {
            height: 15px !important;
        }

        /* Primera y ultima columna de la tabla de dispositivos en los que activar el software, ocultas */
        #cphBody_gvDispActualizar > tbody > tr:nth-child(1) > th:nth-child(1) {
            display: none;
        }

        #cphBody_gvDispActualizar > tbody > tr > td:nth-child(1) {
            display: none;
        }

        #cphBody_gvDispActualizar > tbody > tr:nth-child(1) > th:nth-child(6) {
            display: none;
        }

        #cphBody_gvDispActualizar > tbody > tr > td:nth-child(6) {
            display: none;
        }

        /* Primera y ultima columna de la tabla de dispositivos en los que activar el software, ocultas */
        #cphBody_gvActivarAhorSoftDisp > tbody > tr:nth-child(1) > th:nth-child(1) {
            display: none;
        }

        #cphBody_gvActivarAhorSoftDisp > tbody > tr > td:nth-child(1) {
            display: none;
        }

        #cphBody_gvActivarAhorSoftDisp > tbody > tr:nth-child(1) > th:nth-child(6) {
            display: none;
        }

        #cphBody_gvActivarAhorSoftDisp > tbody > tr > td:nth-child(6) {
            display: none;
        }

        .progress-bar-info {
            background-color: #ced2d0 !important;
        }

        .divPercent {
            right: 0;
            position: absolute;
            margin-top: 7px;
            margin-right: 14px;
            color: darkgray;
            z-index: 99999;
        }

        .button-historial {
            font-size: inherit;
            padding-left: 9px;
            padding-top: 6px;
            padding-right: 9px;
            padding-bottom: 6px;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Cuando se modifica el serial de la UM el botón buscar se vuelve azul ---------------
            BindEvtsTxtBuscar();

            // Evento actualización de UpdatePanel
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {

                BindEvtsTxtBuscar();

                // Evento click sobre una pestaña dispositivo
                $('#tabsDevs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                    // Pestaña dispositivo seleccionada
                    var clickedTab = $(e.target).attr("href")

                    // Guarda la pestaña dispositivo seleccionada en el control hdnTabDev
                    $("#cphBody_hdnTabDev").text(clickedTab);
                });
            });

            function BindEvtsTxtBuscar() {

                $("#cphBody_txtSerialUM").change(function () {
                    $("#cphBody_btnBuscar").attr('class', 'btn btn-primary');
                });

                $("#cphBody_txtSerialUM").keydown(function () {
                    $("#cphBody_btnBuscar").attr('class', 'btn btn-primary');
                });

                // La tecla Enter presionada dentro de txtSerialUM hace click en el boton buscar
                $("#cphBody_txtSerialUM").keypress(function (e) {
                    if (e.which == 13) {
                        setTimeout(function () {
                            $("#cphBody_btnBuscar").click();
                        }, 1);
                    }
                });
            }
            // ------------------------------------------------------------------------------------

            // Radio buttons con las opciones de activación del soft de la UM

            // Automática
            $('input[type=radio][id=cphBody_optUmAutomatica]').change(function () {
                $("#divUmFHProg").hide();
            });

            // Manual
            $('input[type=radio][id=cphBody_optUmManual]').change(function () {
                $("#divUmFHProg").hide();
            });

            // Programada
            $('input[type=radio][id=cphBody_optUmProgramada]').change(function () {
                $("#divUmFHProg").show();
            });

            // Radio buttons con las opciones de activación del soft del Disp

            // Automática
            $('input[type=radio][id=cphBody_optDispAutomatica]').change(function () {
                $("#divDispFHProg").hide();
                $("#cphBody_gvDispActualizar").show();
                $("#divReferencias").show();
            });

            // Manual
            $('input[type=radio][id=cphBody_optDispManual]').change(function () {
                $("#divDispFHProg").hide();
                $("#cphBody_gvDispActualizar").hide();
                $("#divReferencias").hide();
            });

            // Programada
            $('input[type=radio][id=cphBody_optDispProgramada]').change(function () {
                $("#divDispFHProg").show();
                $("#cphBody_gvDispActualizar").show();
                $("#divReferencias").show();
            });

            // Oculta el switchery de la fila que contiene el dispositivo en cuyo boton Enviar software se hizo click
            $('#modalActivacionSoftDisp').on('shown.bs.modal', function () {
                $('#cphBody_gvDispActualizar tr').each(function () {
                    if ($(this).find('td').length > 0) {
                        var x = $(this).closest('tr').find('td')[0].innerHTML;
                        var y = $('#cphBody_hdnIdDispositivo').val();
                        if (x == y) {
                            $(this).closest('tr').find('td').find('.switchery').hide();
                        }
                        else {
                            $(this).closest('tr').find('td').find('.switchery').show();
                        }

                        var idSoftInt = parseInt($(this).find('td')[5].innerHTML);
                        var idSoftAInst = parseInt($("#cphBody_hdnIdSoftDisp").val());
                        if (idSoftInt < idSoftAInst) {

                            // Upgrade
                            $(this).css('color', 'green')
                        } else {
                            if (idSoftInt == idSoftAInst) {

                                // Reinstall
                                $(this).css('color', 'orange')
                            } else {

                                // Downgrade
                                $(this).css('color', 'red')
                            }
                        }
                    }
                });
            })
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Guarda en el hdnTabDev el primer dispositivo
        function FirstDevTab() {
            $("#cphBody_hdnTabDev").text("#cphBody_rptContent_divContent_0");
        }

        // Enfoca la pestaña dispositivo seleccionada
        function TabFocused() {
            var storedTab = $("#cphBody_hdnTabDev").text();
            if (storedTab != "") {
                $('#tabsDevs a[href="' + storedTab + '"]').tab('show');
            }
        }

        // Muestra el contenido de la página
        function ShowContent() {
            $("#divContent").show();
        }

        // Oculta el contenido de la página
        function HideContent() {
            $("#divContent").hide();
        }

        // Muestra las pestañas de los Dispositivos
        function ShowDevices() {
            $("#cphBody_upDispositivos").show();
        }

        // Oculta las pestañas de los Dispositivos
        function HideDevices() {
            $("#cphBody_upDispositivos").hide();
        }

        // Muestra el modal parámetro de la UM
        function AbrirModalParamValue(sender) {

            var id = $(sender).closest("tr").find("td:nth-child(1)").text().trim();
            var name = $(sender).closest("tr").find("td:nth-child(2)").text().trim();
            var value = $(sender).closest("tr").find("td:nth-child(3)").text().trim();
            var type = $(sender).closest("tr").find("td:nth-child(6)").text().trim();
            var lengh = $(sender).closest("tr").find("td:nth-child(7)").text().trim();

            $("#cphBody_txtParamID").val(id);
            $("#cphBody_lblParamName").text(name);
            $("#cphBody_txtParamValue").val(value);

            $("#cphBody_txtParamValue").attr('maxlength', lengh);

            $("#modalParamValue").modal('show');
        }

        // Oculta el modal parámetro de la UM
        function CerrarModalParamValue() {
            $("#modalParamValue").modal('hide');
        }

        // Oculta el modal opciones de activación del soft de la UM
        function CerrarModalOpcActSoftUM() {
            $("#modalActivacionSoftUM").modal('hide');
        }

        // Obtiene el Id del soft seleccionado en el tab del dispositivo en el que se hizo click en el boton Enviar Software
        function GetIdSoftDisp(t) {
            var ddl = $(t).attr('id').replace('btnEnviarSoftDisp', 'ddlSoftDisp');
            var idSoftDisp = $("#" + ddl + " option:selected").val();
            $("#cphBody_hdnIdSoftDisp").val(idSoftDisp);
        }

        // Oculta el modal opciones de activación del soft del Disp
        function CerrarModalOpcActSoftDisp() {
            $("#modalActivacionSoftDisp").modal('hide');
        }

        function CerrarModalOpcActAhoraSoftDisp() {
            $("#modalActivarAhoraSoftDisp").modal('hide');
        }

        // Actualiza periodicamente el progreso del envío del soft a la UM --------------------------------------------------------------------------
        var timerUM;

        function timerUM_Start(EnvioRecepcionActivacion) {
            $(".panelVerSoftUM").hide();
            $(".panelVerSoftDisp").hide();

            clearInterval(timerUM);

            if (EnvioRecepcionActivacion == "Envio") {
                timerUM = setInterval(PercentEnvioSoftUM, 3000);
                PercentEnvioSoftUM();
            }

            if (EnvioRecepcionActivacion == "Recepcion") {
                timerUM = setInterval(ConfRecepcionSoftUM, 10000);
                ConfRecepcionSoftUM();
            }

            if (EnvioRecepcionActivacion == "Activacion") {
                timerUM = setInterval(ConfActivacionSoftUM, 10000);
                ConfActivacionSoftUM();
            }
        }

        function timerUM_Stop() {
            $(".panelVerSoftUM").show();
            $(".panelVerSoftDisp").show();

            clearInterval(timerUM);
        }

        function PercentEnvioSoftUM() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/PercentEnvioSoftUM.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Percentage !== "undefided") {
                    $('#pbSoftInstallUM').css('width', data.Percentage + '%').attr('aria-valuenow', data.Percentage);
                    $('#divPercentageSoftUM').html(data.Percentage + " %");
                    if (data.Percentage >= 100) {
                        timerUM_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }

        function ConfRecepcionSoftUM() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/ConfRecepcionSoftUM.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Result !== "undefided") {
                    if (data.Result == "Disponible") {
                        timerUM_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }

        function ConfActivacionSoftUM() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/ConfActivacionSoftUM.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Result !== "undefided") {
                    if (data.Result == "Ok") {
                        timerUM_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }

        // Actualiza periodicamente el progreso del envío del soft al/los Dispositivos -----------------------------------------------------------------
        var timerDisp;

        function timerDisp_Start(EnvioRecepcionActivacion) {
            $(".panelVerSoftUM").hide();
            $(".panelVerSoftDisp").hide();

            clearInterval(timerDisp);

            if (EnvioRecepcionActivacion == "Envio") {
                timerDisp = setInterval(PercentEnvioSoftDisp, 3000);
                PercentEnvioSoftDisp();
            }

            if (EnvioRecepcionActivacion == "Recepcion") {
                timerDisp = setInterval(ConfRecepcionSoftDisp, 10000);
                ConfRecepcionSoftDisp();
            }

            if (EnvioRecepcionActivacion == "Activacion") {
                timerDisp = setInterval(ConfActivacionSoftDisp, 10000);
                ConfActivacionSoftDisp();
            }
        }

        function timerDisp_Stop() {
            $(".panelVerSoftUM").show();
            $(".panelVerSoftDisp").show();

            clearInterval(timerDisp);
        }

        function PercentEnvioSoftDisp() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/PercentEnvioSoftDisp.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Percentage !== "undefided") {
                    $('#pbSoftInstallDisp').css('width', data.Percentage + '%').attr('aria-valuenow', data.Percentage);
                    $('#divPercentageSoftDisp').html(data.Percentage + " %");
                    if (data.Percentage >= 100) {
                        timerDisp_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }

        function ConfRecepcionSoftDisp() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/ConfRecepcionSoftDisp.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Result !== "undefided") {
                    if (data.Result == "Disponible") {
                        timerDisp_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }

        function ConfActivacionSoftDisp() {

            var serialUM = $("#cphBody_hdnSerialUM").val();

            $.ajax("../Handlers/ConfActivacionSoftDisp.ashx?serialUM=" + serialUM).done(function (data) {
                if (data.Result !== "undefided") {
                    if (data.Result == "Ok") {
                        timerDisp_Stop();
                        __doPostBack('<%= btnBuscar.UniqueID %>', "");
                    }
                }
            });

        }
        // ------------------------------------------------------------------------------------------------------------------------------------------

        function BarraProgresoDisp(porcentaje) {
            $('#pbSoftInstallDisp').css('width', porcentaje + '%').attr('aria-valuenow', porcentaje);
        }

        function ConfirmCancelarEnvioSoftUM() {

            swal({
                title: "Cancelar envío",
                text: "¿Confirma la cancelación del envío del soft a la UM?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: true
            },
            function () {
                __doPostBack('<%= btnCancelarEnvioSoftUM.UniqueID %>', "");
            });

        }

        function ConfirmCancelarEnvioSoftDisp() {

            swal({
                title: "Cancelar envío",
                text: "¿Confirma la cancelación del envío del soft al/los Dispositivos?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: true
            },
            function () {
                __doPostBack('<%= btnCancelarEnvioSoftDisp.UniqueID %>', "");
            });

        }

        function ConfirmCancelarActivacionSoftUM() {

            swal({
                title: "Cancelar activación",
                text: "¿Confirma la cancelación de la activación del soft en la UM?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: true
            },
            function () {
                __doPostBack('<%= btnCancelarActivarSoftUM.UniqueID %>', "");
            });

        }

        function ConfirmCancelarActivacionSoftDisp() {

            swal({
                title: "Cancelar activación",
                text: "¿Confirma la cancelación de la activación del soft en el/los Dispositivos?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: true
            },
            function () {
                __doPostBack('<%= btnCancelarActivarSoftDisp.UniqueID %>', "");
            });

        }

    </script>

</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Guarda la pestaña Dispositivo seleccionada --%>
    <asp:HiddenField ID="hdnTabDev" runat="server" Value="" />

    <%-- UpdatePanel dummy para que los botones que no actualizan la interface no generen un PostBack que recargue toda la página --%>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate></ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAceptarEnviarSoftUM" />
            <asp:AsyncPostBackTrigger ControlID="btnAceptarEnviarSoftDisp" />
            <asp:AsyncPostBackTrigger ControlID="btnActivarAhorSoftDisp" />
        </Triggers>
    </asp:UpdatePanel>

    <!-- Valor del parámetro de la UM -->
    <div id="modalParamValue" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Parámetro" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">
                    <label>
                        <asp:Label ID="lblParamName" Text="Parámetro" runat="server" />
                    </label>
                    <asp:TextBox ID="txtParamID" runat="server" Style="display: none;" />
                    <asp:TextBox ID="txtParamValue" runat="server" CssClass="form-control" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                    <asp:Button ID="btnAceptarParamValue" Text="Aceptar" runat="server" CssClass="btn btn-primary" OnClick="btnAceptarParamValue_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- Opciones de Activación del software de la UM -->
    <div id="modalActivacionSoftUM" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Activación del software de la UM" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <div style="text-align: center;">

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optUmAutomatica" runat="server" GroupName="UM" Checked="True" />
                                <asp:Label Text="Automática" runat="server" title="La activación se realizará cuando finalice el envío del software" />
                            </label>
                        </div>

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optUmManual" runat="server" GroupName="UM" />
                                <asp:Label Text="Manual" runat="server" title="La activación debe realizarse manualmente" />
                            </label>
                        </div>

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optUmProgramada" runat="server" GroupName="UM" />
                                <asp:Label Text="Programada" runat="server" title="La activación se realizará en la fecha y hora indicada" />
                            </label>
                        </div>

                    </div>

                    <div id="divUmFHProg" class="row" style="margin-left: 20px; margin-right: 20px; display: none;">

                        <%-- Fecha de activación programada --%>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Fecha" runat="server" />
                            </label>
                            <asp:TextBox ID="txtFechaActivacionSoftUM" CssClass="form-control date-picker" runat="server" />
                        </div>

                        <%-- Hora de activación programada --%>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Hora" runat="server" />
                            </label>
                            <asp:TextBox ID="txtHoraActivacionSoftUM" CssClass="form-control time-picker" runat="server" />
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                    <asp:Button ID="btnAceptarEnviarSoftUM" Text="Aceptar" runat="server" CssClass="btn btn-primary" OnClick="btnAceptarEnviarSoftUM_Click" />
                </div>
            </div>

        </div>
    </div>

    <!-- Opciones de Activación del firmware del Dispositivo -->
    <div id="modalActivacionSoftDisp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Activación del software del Dispositivo" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <div style="text-align: center;">

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optDispAutomatica" runat="server" GroupName="Disp" Checked="True" />
                                <asp:Label Text="Automática" runat="server" title="La activación se realizará cuando finalice el envío del software" />
                            </label>
                        </div>

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optDispManual" runat="server" GroupName="Disp" />
                                <asp:Label Text="Manual" runat="server" title="La activación debe realizarse manualmente" />
                            </label>
                        </div>

                        <div class="radio-inline">
                            <label>
                                <asp:RadioButton ID="optDispProgramada" runat="server" GroupName="Disp" />
                                <asp:Label Text="Programada" runat="server" title="La activación se realizará en la fecha y hora indicada" />
                            </label>
                        </div>

                    </div>

                    <div id="divDispFHProg" class="row" style="margin-left: 20px; margin-right: 20px; display: none;">

                        <%-- Fecha de activación programada --%>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Fecha" runat="server" />
                            </label>
                            <asp:TextBox ID="txtFechaActivacionSoftDisp" CssClass="form-control date-picker" runat="server" />
                        </div>

                        <%-- Hora de activación programada --%>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Hora" runat="server" />
                            </label>
                            <asp:TextBox ID="txtHoraActivacionSoftDisp" CssClass="form-control time-picker" runat="server" />
                        </div>

                    </div>

                    <div class="separador-vertical"></div>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvDispActualizar" CssClass="table table-striped table-bordered table-hover"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>

                                    <%-- Id --%>
                                    <asp:BoundField HeaderText="Id" DataField="Id" />

                                    <%-- Checkbox --%>
                                    <asp:TemplateField HeaderText="Seleccionada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDispositivo" runat="server" Checked="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Id Dispositivo --%>
                                    <asp:TemplateField HeaderText="Id Disp." Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdDisp" Text='<%# Eval("ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Serial --%>
                                    <asp:BoundField HeaderText="Serial" DataField="Serial" />

                                    <%-- Modelo --%>
                                    <asp:BoundField HeaderText="Modelo" DataField="Modelo" />

                                    <%-- Version --%>
                                    <asp:BoundField HeaderText="Versión" DataField="VerSoft" />

                                    <%-- Id Software instalado --%>
                                    <asp:BoundField HeaderText="Id Soft." DataField="IdDispSoft" />

                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">

                    <%-- Referencia de colores --%>
                    <div id="divReferencias" class="pull-left" style="display: inline; margin-top: 5px;">
                        <asp:Label Text="Downgrade" runat="server" Style="color: red;" />
                        &nbsp;
                            &nbsp;
                            <asp:Label Text="Reinstall" runat="server" Style="color: orange;" />
                        &nbsp;
                            &nbsp;
                            <asp:Label Text="Upgrade" runat="server" Style="color: green;" />
                    </div>

                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                    <asp:Button ID="btnAceptarEnviarSoftDisp" Text="Aceptar" runat="server" CssClass="btn btn-primary" OnClick="btnAceptarEnviarSoftDisp_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- Opciones de Activar ahora el firmware del Dispositivo -->
    <div id="modalActivarAhoraSoftDisp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Activar ahora el software del/los Dispositivo/s" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvActivarAhorSoftDisp" CssClass="table table-striped table-bordered table-hover"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>

                                    <%-- Id --%>
                                    <asp:BoundField HeaderText="Id" DataField="Id" />

                                    <%-- Checkbox --%>
                                    <asp:TemplateField HeaderText="Seleccionada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDispositivo" runat="server" Checked="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Id Dispositivo --%>
                                    <asp:TemplateField HeaderText="Id Disp." Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdDisp" Text='<%# Eval("ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Serial --%>
                                    <asp:TemplateField HeaderText="Serial del Dispositivo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerialDisp" Text='<%# Eval("Serial") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Modelo --%>
                                    <asp:BoundField HeaderText="Modelo" DataField="Modelo" />

                                    <%-- Version --%>
                                    <asp:BoundField HeaderText="Versión" DataField="VerSoft" />

                                    <%-- Id Software instalado --%>
                                    <asp:BoundField HeaderText="Id Soft." DataField="IdDispSoft" />

                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                    <asp:Button ID="btnActivarAhorSoftDisp" Text="Aceptar" runat="server" CssClass="btn btn-primary" OnClick="btnActivarAhorSoftDisp_Click" />
                </div>
            </div>
        </div>
    </div>

    <%-- Botones Historial _._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._. --%>

    <!-- Modal Historial de software de la UM -->
    <div class="modal fade" id="modalHistorialSoftUM" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document" style="width: 98%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <asp:Label Text="Historial de software de la UM" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:GridView ID="gvHistorialSoftUM" CssClass="table table-striped table-bordered grid-view"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>
                                    <%-- Responsive + - --%>
                                    <asp:BoundField HeaderText="" />

                                    <asp:BoundField HeaderText="Fecha hora detect." DataField="fechaHoraDetectado" />
                                    <asp:BoundField HeaderText="Fecha hora inst." DataField="fhinstalacion" />
                                    <asp:BoundField HeaderText="Software" DataField="software" />
                                    <asp:BoundField HeaderText="Descripcion" DataField="descripcion" />
                                    <asp:BoundField HeaderText="Instalacion" DataField="instalacion" />
                                    <asp:BoundField HeaderText="Resultado" DataField="resultado" />
                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnHistorialSoftUM" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Historial de asignación de dispositivos -->
    <div class="modal fade" id="modalHistorialDevs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <asp:Label Text="Historial de asignaciones de Dispositivos" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:GridView ID="gvHistorialDevs" CssClass="table table-striped table-bordered grid-view"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>
                                    <%-- Responsive + - --%>
                                    <asp:BoundField HeaderText="" />

                                    <asp:BoundField HeaderText="Fecha hora" DataField="FHAlta" />
                                    <asp:BoundField HeaderText="Serial" DataField="Serial" />
                                    <asp:BoundField HeaderText="Tipo" DataField="Tipo" />
                                    <asp:BoundField HeaderText="Modelo" DataField="Modelo" />
                                    <asp:BoundField HeaderText="Versión" DataField="VerSoft" />
                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnHistorialDevs" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Historial de software del Dispositivo -->
    <div class="modal fade" id="modalHistorialSoftDevs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document" style="width: 98%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <asp:Label Text="Historial de software del Dispositivo" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <%--
                        Los parámetros: UpdateMode="Conditional" ChildrenAsTriggers="false"
                        sirven para invocar el metodo Update() desde el codebihind.
                    --%>
                    <asp:UpdatePanel ID="upHistorialSoftDisp" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <asp:GridView ID="gvHistorialSoftDisp" CssClass="table table-striped table-bordered grid-view"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>
                                    <%-- Responsive + - --%>
                                    <asp:BoundField HeaderText="" />

                                    <asp:BoundField HeaderText="Fecha hora detect." DataField="fechaHoraDetectado" />
                                    <asp:BoundField HeaderText="Fecha hora inst." DataField="fhinstalacion" />
                                    <asp:BoundField HeaderText="Version" DataField="version" />
                                    <asp:BoundField HeaderText="Descripcion" DataField="descripcion" />
                                    <asp:BoundField HeaderText="Instalacion" DataField="instalacion" />
                                    <asp:BoundField HeaderText="Resultado" DataField="resultado" />
                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Historial de asignación a UMs -->
    <div class="modal fade" id="modalHistorialUMs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <asp:Label Text="Historial de asignaciones a UMs" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">

                    <%--
                        Los parámetros: UpdateMode="Conditional" ChildrenAsTriggers="false"
                        sirven para invocar el metodo Update() desde el codebihind.
                    --%>
                    <asp:UpdatePanel ID="upHistorialUMs" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <asp:GridView ID="gvHistorialUMs" CssClass="table table-striped table-bordered grid-view"
                                Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                <Columns>
                                    <%-- Responsive + - --%>
                                    <asp:BoundField HeaderText="" />

                                    <asp:BoundField HeaderText="Fecha hora" DataField="FHAlta" />
                                    <asp:BoundField HeaderText="Serial" DataField="Serial" />
                                    <asp:BoundField HeaderText="Tipo" DataField="Tipo" />
                                    <asp:BoundField HeaderText="Modelo" DataField="Modelo" />
                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%-- _._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._. --%>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

            <%-- Botones --%>
            <div class="top-buttons">

                <%-- Buscar UM --%>
                <div class="form-inline text-center" style="background: url('../Content/images/pcb.png'); padding: 5px; border-radius: 5px;">
                    <div class="form-group">

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <asp:TextBox ID="txtSerialUM" Text="" runat="server" CssClass="form-control text-upper" placeholder="Serial UM" MaxLength="8" Style="width: 130px; display: table-caption; font-size: small; text-align: center;" />
                                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" CssClass="btn btn-default" OnClientClick="$('#cphBody_btnBuscar').attr('class', 'btn btn-default');" OnClick="btnBuscar_Click" />

                                <asp:HiddenField ID="hdnSerialUM" runat="server" />
                                <asp:HiddenField ID="hdnIdDispositivo" runat="server" />
                                <asp:HiddenField ID="hdnIdSoftDisp" runat="server" />

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="separador-vertical"></div>
    <div class="separador-vertical"></div>

    <%-- Contenido --%>
    <div id="divContent" class="row" style="display: none;">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="pnlEnvioFallidoSoftUM" runat="server" CssClass="alert alert-danger alert-dismissible" role="alert" Visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>
                                    <asp:Label Text="Atención: " runat="server" />
                                </strong>
                                <asp:Label Text="Se produjo un error durante el envío del firmware para la UM. Revise el historial para más información." runat="server" />
                            </asp:Panel>

                            <asp:Panel ID="pnlEnvioFallidoSoftDisp" runat="server" CssClass="alert alert-danger alert-dismissible" role="alert" Visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>
                                    <asp:Label Text="Atención: " runat="server" />
                                </strong>
                                <asp:Label Text="Se produjo un error durante el envío del firmware para el/los Dispositivo/s. Revise el historial para más información." runat="server" />
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <!-- Nav tabs detalles de la UM -->
                    <ul id="tabsUM" class="nav nav-tabs" role="tablist">

                        <%-- Tab Detalles de la UM --%>
                        <li role="presentation" class="active">
                            <a href="#divDetalles" aria-controls="divDetalles" role="tab" data-toggle="tab">
                                <asp:Label Text="Detalles" runat="server" />
                            </a>
                        </li>

                        <%-- Tab Parámetros de la UM --%>
                        <li role="presentation">
                            <a href="#divParams" aria-controls="divParams" role="tab" data-toggle="tab">
                                <asp:Label Text="Parámetros" runat="server" />
                            </a>
                        </li>

                        <%-- Tab Interfaces Digitales --%>
                        <li role="presentation">
                            <a href="#divIDigitales" aria-controls="" role="tab" data-toggle="tab">
                                <asp:Label Text="Interfaces Digitales" runat="server" />
                            </a>
                        </li>

                        <%-- Tab Interfaces Analógicas --%>
                        <li role="presentation">
                            <a href="#divIAnalogicas" aria-controls="" role="tab" data-toggle="tab">
                                <asp:Label Text="Interfaces Analógicas" runat="server" />
                            </a>
                        </li>

                        <%-- Tab Variables --%>
                        <li role="presentation">
                            <a href="#divVariables" aria-controls="" role="tab" data-toggle="tab">
                                <asp:Label Text="Variables" runat="server" />
                            </a>
                        </li>

                        <%-- Tab Notas --%>
                        <li role="presentation">
                            <a href="#divNotas" aria-controls="" role="tab" data-toggle="tab">
                                <asp:Label Text="Notas" runat="server" />
                            </a>
                        </li>
                    </ul>

                    <%-- Detalle de la UM buscada --%>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <%-- Detalles de la UM --%>
                        <div id="divDetalles" class="tab-pane active" role="tabpanel">
                            <div class="separador-vertical"></div>

                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <%-- Estado: OnLine/OffLine --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Estado" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <strong>
                                                <asp:Label ID="lblEstado" Text="..." CssClass="form-control-static" runat="server" />
                                            </strong>
                                        </div>
                                    </div>

                                    <%-- Tipo de UM --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Tipo" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <strong>
                                                <asp:Label ID="lblTipo" Text="..." CssClass="form-control-static text-primary" runat="server" />
                                            </strong>
                                        </div>
                                    </div>

                                    <%-- Modelo de la UM --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Modelo" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblModelo" Text="..." CssClass="form-control-static" runat="server" />
                                        </div>
                                    </div>

                                    <%-- Empresa de telecomunicaciones del chip --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Telco" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblTelco" Text="..." CssClass="form-control-static" runat="server" />
                                        </div>
                                    </div>

                                    <%-- Número de línea telefónica --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Número de línea" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblNroLinea" Text="..." CssClass="form-control-static" runat="server" />
                                        </div>
                                    </div>

                                    <%-- Descripción el software instalado --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Software" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblDescSoft" Text="..." CssClass="form-control-static" runat="server" />
                                        </div>
                                    </div>

                                    <%-- Versión del software instalado --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Versión de software" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblVerSoft" Text="..." CssClass="form-control-static" runat="server" />
                                            <asp:HiddenField ID="hdnSoftId" runat="server" />
                                        </div>
                                    </div>

                                    <%-- Fecha Hora de la última conexión --%>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Fecha hora última conexión" runat="server" />
                                        </label>
                                        <div class="control-div-static">
                                            <asp:Label ID="lblFHUltCxn" Text="..." CssClass="form-control-static" runat="server" />
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>

                        <%-- Parámetros de la UM --%>
                        <div id="divParams" class="tab-pane" role="tabpanel">
                            <div class="separador-vertical"></div>

                            <div style="margin-bottom: 5px; color: darkgray;">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 5px;">
                                    <strong>
                                        <asp:Label Text="Referencias Estado" runat="server" />
                                    </strong>
                                </div>

                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

                                    <i class="fa fa-hourglass-o" aria-hidden="true" style="color: red;"></i>
                                    <asp:Label Text="Pendiente: " runat="server" Style="color: red;" />
                                    <asp:Label Text="El parámetro aún no fue encolado para el envío por el Srv. de Configuración." runat="server" />
                                </div>

                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <i class="fa fa-cog" aria-hidden="true" style="color: orange;"></i>
                                    <asp:Label Text="Procesado: " runat="server" Style="color: orange;" />
                                    <asp:Label Text="El parámetro fue encolado para el envío por el Srv. de Configuración." runat="server" />
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <i class="fa fa-check" aria-hidden="true" style="color: green;"></i>
                                    <asp:Label Text="Enviado: " runat="server" Style="color: green;" />
                                    <asp:Label Text="El parámetro se envió." runat="server" />
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">

                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <asp:GridView ID="gvParams" CssClass="table table-striped table-bordered grid-view" data-order="5-desc"
                                            Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Sin datos para mostrar.">
                                            <Columns>

                                                <%-- Responsive + - --%>
                                                <asp:BoundField HeaderText="" />

                                                <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" Text='<%# Eval("ID") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Parámetro" DataField="Parametro" />

                                                <asp:TemplateField HeaderText="Valor">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValue" Text='<%# Eval("Valor") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Codigo en UM" DataField="CodigoEnUM" />

                                                <asp:TemplateField HeaderText="Estado">
                                                    <ItemTemplate>
                                                        <i class="fa fa-<%# ((string)Eval("Estado") == "Pendiente") ? "hourglass-o" : ((string)Eval("Estado") == "Procesado") ? "cog" : "check" %>"
                                                            aria-hidden="true" style="color: <%# ((string)Eval("Estado") == "Pendiente") ? "red" : ((string)Eval("Estado") == "Procesado") ? "orange" : "green" %>;"></i>
                                                        <span style="color: <%# ((string)Eval("Estado") == "Pendiente") ? "red" : ((string)Eval("Estado") == "Procesado") ? "orange" : "green" %>;"><%# Eval("Estado") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Tipo" DataField="Tipo" />
                                                <asp:BoundField HeaderText="Longitud" DataField="Longitud" />

                                                <%-- Acción sobre el parámetro --%>
                                                <asp:TemplateField HeaderText="Editar" ItemStyle-CssClass="text-center btn-td">
                                                    <ItemTemplate>
                                                        <button type="button" class="btn btn-default btn-edit" onclick="AbrirModalParamValue(this); return false;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        </button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                                        <asp:AsyncPostBackTrigger ControlID="btnAceptarParamValue" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>

                        </div>

                        <%-- Interfaces Digitales --%>
                        <div id="divIDigitales" class="tab-pane" role="tabpanel">
                            <div class="separador-vertical"></div>
                            <div class="text-warning" style="text-align: center;">
                                <h3>Interfaces Digitales</h3>
                                <h4 style="color: orange;">En contrucción</h4>
                            </div>
                            <div class="separador-vertical"></div>
                        </div>

                        <%-- Interfaces Analógicas --%>
                        <div id="divIAnalogicas" class="tab-pane" role="tabpanel">
                            <div class="separador-vertical"></div>
                            <div class="text-warning" style="text-align: center;">
                                <h3>Interfaces Analógicas</h3>
                                <h4 style="color: orange;">En contrucción</h4>
                            </div>
                            <div class="separador-vertical"></div>
                        </div>

                        <%-- Variables --%>
                        <div id="divVariables" class="tab-pane" role="tabpanel">
                            <div class="separador-vertical"></div>
                            <div class="text-warning" style="text-align: center;">
                                <h3>Variables</h3>
                                <h4 style="color: orange;">En contrucción</h4>
                            </div>
                            <div class="separador-vertical"></div>
                        </div>

                        <%-- Notas --%>
                        <div id="divNotas" class="tab-pane" role="tabpanel">
                            <div class="separador-vertical"></div>
                            <div class="text-warning" style="text-align: center;">
                                <h3>Notas</h3>
                                <h4 style="color: orange;">En contrucción</h4>
                            </div>
                            <div class="separador-vertical"></div>
                        </div>

                    </div>

                    <div class="separador-vertical"></div>

                    <%-- Envio - Progreso del envío/Cancelación del envío - Activacion/Canclelacion de activacion del Software de la UM --%>
                    <asp:UpdatePanel ID="upSoftUM" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <%-- Version del soft a enviar a la UM --%>
                            <asp:Panel ID="pnlVerSoftUM" runat="server" CssClass="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 panelVerSoftUM">
                                <div class="row">

                                    <%-- Versiones de software disponibles para la UM --%>
                                    <div class="form-group col-lg-10 col-md-9 col-sm-8 col-xs-7">
                                        <asp:DropDownList ID="ddlSoftUm" runat="server" CssClass="select-single" OnSelectedIndexChanged="ddlSoftUm_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>

                                    <%-- Botón enviar software en la UM --%>
                                    <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-5">
                                        <asp:Button ID="btnEnviarSoftUM" CssClass="btn btn-default col-lg-12 col-md-12 col-sm-12 col-xs-12" Text="Enviar" runat="server" data-toggle="modal" data-target="#modalActivacionSoftUM" OnClientClick="$('#cphBody_optUmAutomatica').click(); return false;" />
                                    </div>

                                </div>
                            </asp:Panel>

                            <%-- Progreso del envío y cancelación de envío --%>
                            <asp:Panel ID="pnlEnvioSoftUM" runat="server" CssClass="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">

                                    <%-- Barra de progreso de envío del soft a la UM --%>
                                    <div class="form-group col-lg-10 col-md-9 col-sm-8 col-xs-7">
                                        <div class="progress" style="height: 29px; margin-bottom: 0px;">
                                            <div style="margin-top: 3px; position: absolute; margin-left: 5px; padding: 3px;">
                                                <strong>
                                                    <asp:Label ID="lblInfoSoftEnviandoUM" Text="..." runat="server" />
                                                </strong>
                                            </div>
                                            <div id="divPercentageSoftUM" class="divPercent">0 %</div>
                                            <div id="pbSoftInstallUM" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Botón cancelar envío de software en la UM --%>
                                    <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-5">
                                        <asp:Button ID="btnCancelarEnvioSoftUM" Text="Cancelar" runat="server" CssClass="btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12" OnClientClick="ConfirmCancelarEnvioSoftUM(); return false;" OnClick="btnCancelarEnvioSoftUM_Click" />
                                    </div>

                                </div>
                            </asp:Panel>

                            <%-- Activar/Cancelar el soft enviado UM --%>
                            <asp:Panel ID="pnlActivarSoftUM" runat="server" CssClass="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">

                                    <div class="form-group col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                        <strong>
                                            <asp:Label ID="lblSoftEnviadoUM" Text="Software enviado a la UM..." runat="server" CssClass="blink" Style="padding-left: 3px;" />
                                        </strong>
                                    </div>

                                    <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-6">

                                        <%-- Activar el soft en la UM --%>
                                        <asp:Button ID="btnActivarSoftUM" Text="Activar ahora" runat="server" CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" OnClick="btnActivarSoftUM_Click" />

                                    </div>

                                    <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-6">

                                        <%-- Cancelar la activación del soft en la UM --%>
                                        <asp:Button ID="btnCancelarActivarSoftUM" Text="Cancelar activación" runat="server" CssClass="btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12" OnClientClick="ConfirmCancelarActivacionSoftUM(); return false;" OnClick="btnCancelarActivarSoftUM_Click" />

                                    </div>

                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%-- Historial de Software y Dispositivos --%>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">

                        <asp:Button ID="btnHistorialSoftUM" Text="Historial de software de la UM" runat="server" CssClass="btn btn-default button-historial" data-toggle="modal" data-target="#modalHistorialSoftUM" OnClick="btnHistorialSoftUM_Click" />
                        <asp:Button ID="btnHistorialDevs" Text="Historial de asignación de Dispositivos" runat="server" CssClass="btn btn-default button-historial" data-toggle="modal" data-target="#modalHistorialDevs" OnClick="btnHistorialDevs_Click" />

                    </div>

                    <%-- Dispositivos --%>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlSinDispositivos" runat="server" Style="width: 100%; text-align: center; margin-top: 89px; padding: 5px; background-color: whitesmoke; border-radius: 3px;">
                                <h5>
                                    <asp:Label Text="Sin dispositivos conectados" runat="server" CssClass="blink" />
                                </h5>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel ID="upDispositivos" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <!-- Nav tabs dispositivos asignados -->
                            <ul id="tabsDevs" class="nav nav-tabs" role="tablist">

                                <asp:Repeater ID="rptTabs" runat="server" OnItemDataBound="rptTabs_ItemDataBound">
                                    <ItemTemplate>

                                        <li role="presentation">
                                            <a id="aTab" runat="server" href="#" role="tab" data-toggle="tab">
                                                <asp:Label ID="lblTabCaption" Text="..." runat="server" />
                                            </a>
                                        </li>

                                    </ItemTemplate>
                                </asp:Repeater>

                            </ul>

                            <!-- Tab panes detalle de dispositivos asignados -->
                            <div class="tab-content">

                                <asp:Repeater ID="rptContent" runat="server" OnItemDataBound="rptContent_ItemDataBound">
                                    <ItemTemplate>

                                        <div id="divContent" runat="server" role="tabpanel" class="tab-pane">

                                            <%-- Serial del Dispositivo por si hay muchas pestañas --%>
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                                <h5>
                                                    <strong>
                                                        <asp:Label ID="lblContentCaption" Text="..." runat="server" Style="color: #337abe;" />
                                                    </strong>
                                                </h5>
                                            </div>

                                            <%-- Tipo de dispositivo --%>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <label>
                                                    <asp:Label Text="Tipo" runat="server" />
                                                </label>
                                                <div class="control-div-static">
                                                    <asp:Label ID="lblTipoDisp" Text="..." CssClass="form-control-static" runat="server" />
                                                </div>
                                            </div>

                                            <%-- Modelo del dispositivo --%>
                                            <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                <label>
                                                    <asp:Label Text="Modelo" runat="server" />
                                                </label>
                                                <div class="control-div-static">
                                                    <asp:Label ID="lblModeloDisp" Text="..." CssClass="form-control-static" runat="server" />
                                                </div>
                                            </div>

                                            <%-- Descripción del software instalado --%>
                                            <div class="form-group col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                                <label>
                                                    <asp:Label Text="Software" runat="server" />
                                                </label>
                                                <div class="control-div-static">
                                                    <asp:Label ID="lblDescSoftDisp" Text="..." CssClass="form-control-static" runat="server" />
                                                </div>
                                            </div>

                                            <%-- Versón del software instalado --%>
                                            <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                <label>
                                                    <asp:Label Text="Versión del software" runat="server" />
                                                </label>
                                                <div class="control-div-static">
                                                    <asp:Label ID="lblVerSoftDisp" Text="..." CssClass="form-control-static" runat="server" />
                                                </div>
                                            </div>

                                            <%-- Envío de software al dispositivo --%>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                <asp:UpdatePanel ID="upSoftDisp" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                                    <ContentTemplate>

                                                        <%-- Version del soft a enviar al/los Dispositivos --%>
                                                        <asp:Panel ID="pnlVerSoftDisp" class="panelVerSoftDisp" runat="server" Visible="true">
                                                            <div class="row">

                                                                <%-- Versiones de software disponibles para el Dispositivo --%>
                                                                <div class="form-group col-lg-10 col-md-9 col-sm-8 col-xs-7">
                                                                    <asp:DropDownList ID="ddlSoftDisp" runat="server" CssClass="select-single" />
                                                                </div>

                                                                <%-- Botón enviar software al Dispositivo --%>
                                                                <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-5">
                                                                    <asp:Button ID="btnEnviarSoftDisp" CssClass="btn btn-info col-lg-12 col-md-12 col-sm-12 col-xs-12" Text="Enviar" runat="server" data-toggle="modal" data-target="#modalActivacionSoftDisp" OnClientClick="$('#cphBody_optDispAutomatica').click(); GetIdSoftDisp(this); $('#cphBody_hdnIdDispositivo').val($(this).attr('IdDispositivo')); return false;" />
                                                                </div>

                                                            </div>
                                                        </asp:Panel>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <%-- Historial de Software y UMs --%>
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">

                                                <div class="separador-vertical"></div>

                                                <asp:Button ID="btnHistorialSoftDisp" Text="Historial de software del Dispositivo" runat="server" CssClass="btn btn-default button-historial" data-toggle="modal" data-target="#modalHistorialSoftDevs" OnClick="btnHistorialSoftDisp_Click" />
                                                <asp:Button ID="btnHistorialUMs" Text="Historial de asignación a UMs" runat="server" CssClass="btn btn-default button-historial" data-toggle="modal" data-target="#modalHistorialUMs" OnClick="btnHistorialUMs_Click" />

                                                <div class="separador-vertical"></div>

                                            </div>

                                        </div>

                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <div class="separador-vertical"></div>

                    <asp:UpdatePanel ID="upEnvioActivacionSoftDisp" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <%-- Progreso del envío y cancelación de envío --%>
                            <asp:Panel ID="pnlEnvioSoftDisp" runat="server" CssClass="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" Visible="true">
                                <div class="row">

                                    <%-- Barra de progreso de envío del soft al Dispositivo --%>
                                    <div class="form-group col-lg-10 col-md-9 col-sm-8 col-xs-7">
                                        <div class="progress" style="height: 29px; margin-bottom: 0px;">
                                            <div style="margin-top: 3px; position: absolute; margin-left: 5px; padding: 3px;">
                                                <strong>
                                                    <asp:Label ID="lblInfoSoftEnviandoDisp" Text="..." runat="server" />
                                                </strong>
                                            </div>
                                            <div id="divPercentageSoftDisp" class="divPercent">0 %</div>
                                            <div id="pbSoftInstallDisp" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Botón cancelar envío de software al Dispositivo --%>
                                    <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-5">
                                        <asp:Button ID="btnCancelarEnvioSoftDisp" Text="Cancelar" runat="server" CssClass="btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12" OnClientClick="ConfirmCancelarEnvioSoftDisp(); return false;" OnClick="btnCancelarEnvioSoftDisp_Click" />
                                    </div>

                                </div>
                            </asp:Panel>

                            <%-- Activar/Cancelar el soft enviado Dispositivo --%>
                            <asp:Panel ID="pnlActivarSoftDisp" runat="server" CssClass="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" Visible="true">
                                <div class="row">

                                    <div class="form-group col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                        <strong>
                                            <asp:Label ID="lblSoftEnviadoDisp" Text="Software enviado al Dispositivo..." runat="server" CssClass="blink" Style="padding-left: 3px;" />
                                        </strong>
                                    </div>

                                    <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-6">

                                        <%-- Activar el soft en el Dispositivo--%>
                                        <asp:Button ID="btnActivarSoftDisp" Text="Activar ahora" runat="server" CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" data-toggle="modal" data-target="#modalActivarAhoraSoftDisp" />

                                    </div>

                                    <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-6">

                                        <%-- Cancelar la activación del soft en el Dispositivo --%>
                                        <asp:Button ID="btnCancelarActivarSoftDisp" Text="Cancelar activación" runat="server" CssClass="btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12" OnClientClick="ConfirmCancelarActivacionSoftDisp(); return false;" OnClick="btnCancelarActivarSoftDisp_Click" />

                                    </div>

                                </div>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

</asp:Content>