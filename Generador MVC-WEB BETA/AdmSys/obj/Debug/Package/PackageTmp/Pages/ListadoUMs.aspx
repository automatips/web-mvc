﻿<%@ Page Title="Listado UMs" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ListadoUMs.aspx.cs" Inherits="Pages.ListadoUMs" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div style="display: none;">

            <%-- Panel de filtros --%>
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <%-- Filtro UM --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Serial UM" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombre" CssClass="form-control" autocomplete="off" runat="server" />
                            </div>

                            <%-- Filtro País --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="País" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlPais" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlPais_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>

                            <%-- Filtro Reseller --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Reseller" runat="server" />
                                </label>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <asp:DropDownList ID="ddlReseller" CssClass="form-control select-single" runat="server">
                                        </asp:DropDownList>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlPais" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>

                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>

                </div>
            </div>

                </div>

            <%-- Tabla de resultados de búsqueda de UMs --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvUMs" CssClass="table table-striped table-bordered table-hover grid-view crypto-id"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvUMs_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column">
                                <ItemTemplate>
                                    <asp:Label ID="lblCryptoId" runat="server" Text='<%# Eval("Id_UM") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:BoundField DataField="Id_UM" HeaderText="UM" />
                            <asp:BoundField DataField="Distribuidor" HeaderText="Distribuidor" />
                            <asp:BoundField DataField="Reseller" HeaderText="Reseller" />
                            <asp:BoundField DataField="Contratista" HeaderText="Contratista" />
                            <asp:BoundField DataField="TipoModelo" HeaderText="Tipo Modelo" />
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" />
                            <asp:BoundField DataField="Soft" HeaderText="Soft" />
                            <asp:BoundField DataField="Empresa" HeaderText="Empresa" />
                            <asp:BoundField DataField="Línea" HeaderText="Línea" />
                            <asp:BoundField DataField="Modo Conexión" HeaderText="Conexión" />
                            <asp:BoundField DataField="Señal" HeaderText="Señal" />
                            <asp:BoundField DataField="FHUltimaConexionExitosa" HeaderText="Última Conex. Exitosa" />
                            <asp:BoundField DataField="FHUltimaConexion" HeaderText="Última Conexión" />
                            <asp:BoundField DataField="FHApagado" HeaderText="Fecha de Apagado" />

                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>