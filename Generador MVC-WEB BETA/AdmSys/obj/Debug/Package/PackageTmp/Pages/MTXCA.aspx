﻿<%@ Page Title="Factura Electrónica con Detalle" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="MTXCA.aspx.cs" Inherits="Pages.MTXCA" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #cphBody_txtFechaComprobante {
            background-color: white !important;
        }

        #cphBody_gvComprobantes > tbody > tr > th {
            padding: 5px !important;
        }

        #cphBody_gvItems > tbody > tr > th {
            padding: 5px !important;
        }

        #cphBody_gvComprobantes > tbody > tr > td {
            padding: 3px !important;
        }

        #cphBody_gvItems > tbody > tr > td {
            padding: 3px !important;
        }

        .item-fe {
            padding: 3px !important;
            height: 23px;
        }

        #tablaTotales {
            width: 100%;
            margin-top: -15px;
            border-right-style: solid;
            border-right-width: 1px;
            border-right-color: #dddddd;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-bottom-color: #dddddd;
            border-left-style: solid;
            border-left-width: 1px;
            border-left-color: #dddddd;
            margin-bottom: 10px;
        }

        textarea.form-control {
            height: 23px;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script src="../Scripts/vendor/inputmask.min.js"></script>
    <script src="../Scripts/vendor/autosize.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        function MostrarError() {
            $("#modalErrors").modal('show');
        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">

                <div id="modalAutorizando" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">
                                    <asp:Label Text="Autorizando comprobante" runat="server" />
                                </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <asp:Label Text="Por favor espere..." runat="server" />
                                </p>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <%-- Modal Errores --%>
                <div id="modalErrors" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #f0ad4e; border-top-left-radius: 6px; border-top-right-radius: 6px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h2 class="modal-title">
                                    <asp:Label Text="Atención" runat="server" />
                                    <small>
                                        <asp:Label Text="Corrija los siguientes items" runat="server" />
                                    </small>
                                </h2>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblError" runat="server" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    <asp:Label Text="Cerrar" runat="server" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <%-- Punto de Venta - Tipo de Comprobante --%>
                <div class="row" style="margin-left: -22px; margin-right: -22px;">

                    <%-- Punto de Venta --%>
                    <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <label>
                            <asp:Label Text="Punto de Ventas a utilizar" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlPuntoVenta" CssClass="form-control select-single" runat="server" />
                    </div>

                    <%-- Tipo de Comprobante --%>
                    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <label>
                            <asp:Label Text="Tipo de Comprobante" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlTipoComprobante" CssClass="form-control select-single" runat="server" />
                    </div>

                </div>

                <%-- Fecha - Conceptos --%>
                <div class="row" style="margin-left: -22px; margin-right: -22px;">

                    <%-- Fecha del Comprobante --%>
                    <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
                        <label>
                            <asp:Label Text="Fecha del Comprobante" runat="server" />
                        </label>
                        <asp:TextBox ID="txtFechaComprobante" CssClass="form-control date-picker" runat="server" />
                        <asp:RequiredFieldValidator ControlToValidate="txtFechaComprobante" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                    </div>

                    <%-- Conceptos a incluir --%>
                    <div class="form-group col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <label>
                            <asp:Label Text="Conceptos a incluir" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlConceptos" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlConceptos_SelectedIndexChanged" />
                        <asp:RequiredFieldValidator ControlToValidate="ddlConceptos" InitialValue="-1" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                    </div>

                    <%-- Fechas Servicios --%>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="pnlFechasServicios" runat="server" Visible="false">

                                <%-- Período Facturado - Desde --%>
                                <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <label>
                                        <asp:Label Text="Período Facturado - Desde" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtServicioDesde" CssClass="form-control date-picker" runat="server" />
                                </div>

                                <%-- Período Facturado - Hasta --%>
                                <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <label>
                                        <asp:Label Text="Período Facturado - Hasta" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtServicioHasta" CssClass="form-control date-picker" runat="server" />
                                </div>

                                <%-- Vto. para el Pago --%>
                                <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                    <label>
                                        <asp:Label Text="Vto. para el Pago" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtVencimientoPago" CssClass="form-control date-picker" runat="server" />
                                </div>

                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlConceptos" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>

                <%-- CUIT - Razon Social - Domicilio Comercial - Email - Observaciones --%>
                <div class="row" style="margin-left: -22px; margin-right: -22px;">

                    <%-- CUIT --%>
                    <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="CUIT" runat="server" />
                        </label>
                        <asp:TextBox ID="txtCUIT" Text="" CssClass="form-control cuit" runat="server" AutoPostBack="true" OnTextChanged="txtCUIT_TextChanged" />
                        <asp:RequiredFieldValidator ControlToValidate="txtCUIT" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                    </div>

                    <asp:UpdatePanel ID="upIVA" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <%-- Razón Social --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Razón Social" runat="server" />
                                </label>
                                <asp:TextBox ID="txtRazonSocial" CssClass="form-control text-upper" runat="server" Enabled="false" />
                            </div>

                            <%-- Domicilio Comercial --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Domicilio Comercial" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDomicilioComercial" CssClass="form-control" runat="server" Enabled="false" />
                            </div>

                            <%-- Condición IVA --%>
                            <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Condición IVA" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlCondicionIVA" CssClass="form-control select-single" runat="server" Enabled="false">
                                    <asp:ListItem Text="IVA Responsable Inscripto" Value="1" />
                                    <asp:ListItem Text="IVA Responsable no Inscripto" Value="2" />
                                    <asp:ListItem Text="IVA no Responsable" Value="3" />
                                    <asp:ListItem Text="IVA Sujeto Exento" Value="4" />
                                    <asp:ListItem Text="Consumidor Final" Value="5" />
                                    <asp:ListItem Text="Responsable Monotributo" Value="6" />
                                    <asp:ListItem Text="Sujeto no Categorizado" Value="7" />
                                    <asp:ListItem Text="Proveedor del Exterior" Value="8" />
                                    <asp:ListItem Text="Cliente del Exterior" Value="9" />
                                    <asp:ListItem Text="IVA Liberado – Ley Nº 19.640" Value="10" />
                                    <asp:ListItem Text="IVA Responsable Inscripto – Agente de Percepción" Value="11" />
                                    <asp:ListItem Text="Pequeño Contribuyente Eventual" Value="12" />
                                    <asp:ListItem Text="Monotributista Social" Value="13" />
                                    <asp:ListItem Text="Pequeño Contribuyente Eventual Social" Value="14" />
                                </asp:DropDownList>
                            </div>

                            <%-- Email --%>
                            <div class="form-group col-lg-4 col-md-3 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Email" runat="server" />
                                </label>
                                <asp:TextBox ID="txtEmail" CssClass="form-control text-lower" runat="server" Enabled="false" />
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%-- Observaciones --%>
                    <div class="form-group col-lg-8 col-md-7 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <asp:TextBox ID="txtObservaciones" CssClass="form-control" runat="server" MaxLength="2000" />
                    </div>

                </div>

                <%-- Comprobntes Asociados --%>
                <div class="row" style="text-align: -webkit-center; margin-top: 5px;">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <%-- Detalle del Comprobante --%>
                            <asp:GridView ID="gvComprobantes" CssClass="table table-striped table-bordered table-hover inline-edit"
                                Width="500px" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvComprobantes_RowDeleting"
                                EmptyDataText="Sin comprobantes asociados" Style="margin-bottom: 10px;">
                                <Columns>

                                    <%-- IdComprobante --%>
                                    <asp:TemplateField HeaderText="IdComprobante" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdComprobante" CssClass="item-fe form-control-static" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Tipo de Comprobante --%>
                                    <asp:TemplateField HeaderText="Tipo de Comprobante" ItemStyle-Width="35%">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlTipoComprobanteAsociado" CssClass="item-fe form-control" runat="server" AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Punto de Venta --%>
                                    <asp:TemplateField HeaderText="Pto. Vta." ItemStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPtoVta" CssClass="item-fe form-control numeric-only" runat="server" MaxLength="4"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtPtoVta" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Comprobante --%>
                                    <asp:TemplateField HeaderText="Comprobante" ItemStyle-Width="40%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtComprobante" CssClass="item-fe form-control numeric-only" runat="server" MaxLength="8"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtComprobante" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Eliminar --%>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="../content/images/delete-item.png" />
                                </Columns>
                            </asp:GridView>

                            <%-- Botón Agregar nuevo Comprobante Asociado --%>
                            <div style="margin-bottom: 20px; text-align: center;">
                                <asp:Button ID="btnAddComprobante" CssClass="btn btn-primary btn-sm" Text="Agregar Comprobante Asociado" OnClick="btnAddComprobante_Click" runat="server" />
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddComprobante" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>

                <%-- Detalle --%>
                <div class="row">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <%-- Detalle del Comprobante --%>
                            <asp:GridView ID="gvItems" CssClass="table table-striped table-bordered table-hover inline-edit"
                                Width="100%" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvItems_RowDeleting"
                                EmptyDataText="Sin detalle">
                                <Columns>

                                    <%-- IdItem --%>
                                    <asp:TemplateField HeaderText="IdItem" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdItem" CssClass="item-fe form-control-static" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Código --%>
                                    <asp:TemplateField HeaderText="Código" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCodigo" CssClass="item-fe form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtCodigo" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Producto / Servicio --%>
                                    <asp:TemplateField HeaderText="Producto/Servicio" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtProductoServicio" CssClass="item-fe form-control" runat="server" TextMode="MultiLine" onkeyup="autosize($('textarea'));"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtProductoServicio" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Cantidad --%>
                                    <asp:TemplateField HeaderText="Cant." ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCantidad" CssClass="item-fe form-control numeric-integer-positive" runat="server" AutoPostBack="true" OnTextChanged="txtCantidad_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtCantidad" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Unidad de Medida --%>
                                    <asp:TemplateField HeaderText="U. Medida" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlUnidadMedida" CssClass="item-fe form-control" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Precio Unitario --%>
                                    <asp:TemplateField HeaderText="P. Unit." ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPrecioUnitario" CssClass="item-fe form-control numeric-decimal" runat="server" AutoPostBack="true" OnTextChanged="txtPrecioUnitario_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtPrecioUnitario" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Bonificación % --%>
                                    <asp:TemplateField HeaderText="Bonif.%" ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPorcentajeBonificacion" CssClass="item-fe form-control numeric-decimal" runat="server" AutoPostBack="true" OnTextChanged="txtPorcentajeBonificacion_TextChanged"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Bonificación $ --%>
                                    <asp:TemplateField HeaderText=" Bonif.$" ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtImporteBonificacion" CssClass="item-fe form-control numeric-decimal" runat="server" AutoPostBack="true" OnTextChanged="txtImporteBonificacion_TextChanged"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Subtotal --%>
                                    <asp:TemplateField HeaderText="Subtotal" ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSubtotal" CssClass="item-fe form-control numeric-decimal" runat="server" ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Alícuota IVA --%>
                                    <asp:TemplateField HeaderText="Alíc. IVA " ItemStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlAlicuotaIVA" CssClass="item-fe form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAlicuotaIVA_SelectedIndexChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Importe IVA --%>
                                    <asp:TemplateField HeaderText="Imp. IVA" ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtImporteIVA" CssClass="item-fe form-control numeric-decimal" runat="server" ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Subtotal con IVA --%>
                                    <asp:TemplateField HeaderText="Subt.c/IVA" ItemStyle-Width="7%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSubtotalConIVA" CssClass="item-fe form-control numeric-decimal" runat="server" ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Eliminar --%>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="../content/images/delete-item.png" />
                                </Columns>
                            </asp:GridView>

                            <%-- Fila de Totales --%>
                            <asp:Panel ID="pnlTotales" runat="server">
                                <table id="tablaTotales">
                                    <tr>
                                        <td style="width: 5%;"></td>
                                        <td style="width: 30%;"></td>
                                        <td style="width: 5%;"></td>
                                        <td style="width: 10%;"></td>
                                        <td style="width: 7%;"></td>
                                        <td style="width: 7%;"></td>
                                        <td style="width: 7%; padding-left: 5px;">
                                            <strong>
                                                <asp:Label ID="lblTotalImporteBonificacion" Text="0.00" runat="server" CssClass="numeric-decimal" />
                                            </strong>
                                        </td>
                                        <td style="width: 7%; padding-left: 5px;">
                                            <strong>
                                                <asp:Label ID="lblTotalSubtotal" Text="0.00" runat="server" CssClass="numeric-decimal" />
                                            </strong>
                                        </td>
                                        <td style="width: 8%;"></td>
                                        <td style="width: 7%; padding-left: 5px;">
                                            <strong>
                                                <asp:Label ID="lblTotalImporteIVA" Text="0.00" runat="server" CssClass="numeric-decimal" />
                                            </strong>
                                        </td>
                                        <td style="width: 7%; padding-left: 5px;">
                                            <strong>
                                                <asp:Label ID="lblTotalSubtotalConIVA" Text="0.00" runat="server" CssClass="numeric-decimal" />
                                            </strong>
                                        </td>
                                        <td style="color: white;">XX
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                            <%-- Botón Agregar nuevo Item --%>
                            <div style="margin-bottom: 20px; text-align: center;">
                                <asp:Button ID="btnAddItem" CssClass="btn btn-primary btn-sm" Text="Agregar Item" OnClick="btnAddItem_Click" runat="server" />
                            </div>

                            <%-- IVA desglosado --%>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">

                                <%-- Importe Neto no Gravado --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="Importe Neto no Gravado" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtImporteNetoNoGravado" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- Importe Exento --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="Importe Exento" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtImporteExento" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- Importe Neto Gravado --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="Importe Neto Gravado" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtImporteNetoGravado" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">

                                <%-- IVA 27 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 27 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva27" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- IVA 21 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 21 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva21" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- IVA 10,5 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 10,5 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva105" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">

                                <%-- IVA 5 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 5 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva5" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- IVA 2,5 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 2,5 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva25" runat="server" Text="0" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                                <%-- IVA 0 % --%>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon" style="width: 50%; text-align: right;">
                                        <asp:Label Text="IVA 0 %" runat="server" />
                                    </span>
                                    <asp:TextBox ID="txtIva0" runat="server" Text="0.00" CssClass="form-control numeric-decimal" ReadOnly="true" Style="font-weight: bold; background-color: whitesmoke;" />
                                </div>

                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddItem" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>

            <div class="bottom-buttons " style="border-top: solid; border-top-width: 1px; border-top-color: lightgray; padding-top: 10px;">

                <%-- Botón Autorizar --%>
                <div class="row text-center">
                    <asp:Button ID="btnAutorizar" Text="Autorizar" CssClass="btn btn-success" runat="server" OnClientClick="$('#modalAutorizando').modal('show');" OnClick="btnAutorizar_Click" />
                </div>

            </div>
        </div>
    </div>
</asp:Content>
