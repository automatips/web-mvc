﻿<%@ Page Title="Oportunidad" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Oportunidad.aspx.cs" Inherits="Pages.Oportunidad" %>

<%@ Register Src="~/Controls/WebUserControl1.ascx" TagPrefix="uc" TagName="WebUserControl1" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>
    <link href="../Content/vendor/timeline.min.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            $("#modalBajaOportuniad").modal('show');
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <!-- Modal -->
            <div id="modalBajaOportuniad" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label Text="Dar de baja la Oportunidad" runat="server" />
                            </h4>
                        </div>
                        <div class="modal-body">

                            <%-- Descripcion --%>
                            <div class="form-group">
                                <label>
                                    <asp:Label Text="Descripción" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDetalleBaja" CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>

                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDarDeBaja" Text="Dar de baja" runat="server" CssClass="btn btn-danger" OnClick="btnDarDeBaja_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <asp:Label Text="Cerrar" runat="server" />
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server" OnClientClick="ConfirmDelete(); return false;" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" OnClick="btnEdit_Click" />
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <!-- Nav tabs -->
                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#oportunidad" aria-controls="oportunidad" role="tab" data-toggle="tab">
                                <asp:Label Text="Oportunidad" runat="server" />
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#contactos" aria-controls="contactos" role="tab" data-toggle="tab">
                                <asp:Label Text="Contactos" runat="server" />
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#lineaTiempo" aria-controls="lineaTiempo" role="tab" data-toggle="tab">
                                <asp:Label Text="Línea de Tiempo" runat="server" />
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <%-- Oportunidad - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                        <div role="tabpanel" class="tab-pane active" id="oportunidad">
                            <br />

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <asp:Label Text="Descripción" runat="server" />
                                </label>
                                <asp:TextBox ID="txtOportunidadDescripcion" CssClass="form-control" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtOportunidadDescripcion" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <asp:Label Text="Detalle" runat="server" />
                                </label>
                                <asp:TextBox ID="txtOportunidadDetalle" CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>
                        </div>

                        <%-- Contactos - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                        <div role="tabpanel" class="tab-pane" id="contactos">
                            <br />

                            <%-- Nombre --%>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label>
                                    <asp:Label Text="Nombre" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblNombre" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Apellido --%>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label>
                                    <asp:Label Text="Apellido" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblApellido" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Email --%>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Email" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblEmail" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Tel. Fijo --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>
                                    <asp:Label Text="Teléfono Fijo" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblFijo" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Tel. Movil --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>
                                    <asp:Label Text="Teléfono Móvil" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblMovil" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Empresa --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>
                                    <asp:Label Text="Empresa" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblEmpresa" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Pais --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Pais" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblPais" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Region --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Region" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblRegion" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- Ciudad --%>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Ciudad" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label ID="lblCiudad" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                        </div>

                        <%-- Línea de tiempo - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                        <div role="tabpanel" class="tab-pane" id="lineaTiempo">
                            <br />

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <label>
                                            <asp:Label Text="Nota" runat="server" />
                                        </label>
                                        <asp:TextBox ID="txtDetalle" CssClass="form-control" runat="server" TextMode="MultiLine" Style="height: 66px;" />
                                    </div>

                                    <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label>
                                            <asp:Label Text="Tipo de movimiento" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="ddlTipoMovimiento" CssClass="form-control select-single" runat="server" />
                                    </div>

                                    <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Button ID="btnAceptar" Text="Aceptar" runat="server" CssClass="form-control btn btn-primary" OnClick="btnAceptar_Click" />
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <hr />
                                        <div class="qa-message-list" id="wallmessages">

                                            <asp:Repeater ID="rptMovimientos" runat="server">
                                                <ItemTemplate>

                                                    <div class="message-item">
                                                        <div class="message-inner">
                                                            <div class="message-head clearfix">
                                                                <div class="user-detail">
                                                                    <h5 class="handle">
                                                                        <asp:Label ID="lblUsuario" runat="server" />
                                                                    </h5>
                                                                    <div class="post-meta">
                                                                        <div class="asker-meta">
                                                                            <span class="qa-message-when">
                                                                                <span class="qa-message-when-data">
                                                                                    <asp:Label Text='<%# Eval("Usuario") %>' CssClass="date-time" runat="server" Style="margin-right: 5px;" />
                                                                                </span>
                                                                                [
                                                                                <span class="qa-message-when-data">
                                                                                    <asp:Label Text='<%# Eval("FHAlta") %>' CssClass="date-time" runat="server" Style="font-weight: 100; margin-right: 5px;" />
                                                                                </span>
                                                                                <span class="qa-message-when-data pl-5">
                                                                                    <asp:Label Text='<%# Eval("TipoMovimiento") %>' runat="server" />
                                                                                </span>
                                                                                ]
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="qa-message-content">
                                                                <asp:Label Text='<%# Eval("Detalle") %>' CssClass="date-time" runat="server" Style="font-weight: 100;" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAceptar" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="bottom-buttons ">

        <%-- Botón Oportunidades --%>
        <div class="btn-models">
            <a href="Oportunidades.aspx" class="btn btn-info">
                <asp:Label Text="Volver al listado" runat="server" />
            </a>
        </div>

        <%-- Botones Guardar y Restablecer --%>
        <div class="btns-reset-save">
            <button type="reset" class="btn btn-default">
                <asp:Label Text="Restablecer" runat="server" />
            </button>
            <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
        </div>

    </div>

    </div>
    </div>

</asp:Content>
