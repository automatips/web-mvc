﻿<%@ Page Title="Carga de Producto/Servicio" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ProductoServicioCarga.aspx.cs" Inherits="Pages.ProductoServicioCarga" %>
<%@ Register Src="~/Controls/ucProductoServicioCarga.ascx" TagPrefix="uc" TagName="ucProductoServicioCarga" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" Visible="false" OnClick="btnDelete_Click" runat="server" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    
	                     <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <uc:ucProductoServicioCarga runat="server" ID="ucProductoServicioCarga1" />
                        </div>
                
                </div>
            </div>

            <div class="bottom-buttons ">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <a href="ProductoServicioListar.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado de Producto/Servicio" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <a href="ProductoServicioListar.aspx" class="btn btn-danger">
                        <asp:Label Text="Cancelar" runat="server" />
                    </a>
                    <asp:Button ID="btnSave" Text="Guardar Producto/Servicio" OnClick="btnSave_Click" CssClass="btn btn-success" runat="server" />
                </div>

            </div>

        </div>
    </div>

</asp:Content>