﻿<%@ Page Title="Lista de Producto/Servicio" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ProductoServicioListar.aspx.cs" Inherits="Pages.ProductoServicioListar" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">
    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%--<asp:Button ID="btnNuevo" Text="Nuevo Producto/Servicio" CssClass="btn btn-success pull-right" runat="server" />--%>
                <a href="ProductoServicioCarga.aspx" class="btn btn-success pull-right">
                     <asp:Label Text="Nuevo Producto/Servicio" runat="server" />
                </a>

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <asp:UpdatePanel ID="upGridViewProductoServicio" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		                <ContentTemplate>
                        <asp:GridView ID="gvProductoServicio" 
                            CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" 
                            data-model="ProductoServicioCarga.aspx"
                            Width="100%" 
                            AutoGenerateColumns="False" 
                            runat="server" 
                            OnRowDataBound="gvProductoServicio_RowDataBound" 
                            EmptyDataText="Sin datos para mostrar.">
                            <Columns>
                                <asp:BoundField HeaderText="" />
                                <%-- cryptoID --%>
                                <asp:BoundField HeaderText="#" DataField="IdProductoServicio" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                <asp:BoundField HeaderText="Fecha de alta" DataField="FHalta" ItemStyle-CssClass="date-time" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Unidad Negocio" DataField="IdUnidadNegocio" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Id Servicio Tipo" DataField="IdServicioTipo" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Código" DataField="CodigoProducto" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Descripción" DataField="Descripcion" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Observación" DataField="Observacion" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Imagen" DataField="IdImagen" ItemStyle-Width="100"/>
                                <asp:BoundField HeaderText="Fecha Discontinuado" DataField="FHDiscontinuado" ItemStyle-CssClass="date-time" ItemStyle-Width="100"/>
                            </Columns>
                        </asp:GridView>
		                </ContentTemplate>
	                </asp:UpdatePanel>

                </div>
            </div>

        </div>
    </div>

</asp:Content>