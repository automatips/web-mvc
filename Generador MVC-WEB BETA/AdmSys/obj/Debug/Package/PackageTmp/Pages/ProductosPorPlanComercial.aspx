﻿<%@ Page Title="Listas de precios" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ProductosPorPlanComercial.aspx.cs" Inherits="Pages.ProductosPorPlanComercial" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left">
	                    <asp:UpdatePanel ID="upPlanComercial" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                    <ContentTemplate>
                                <label>
                                    <asp:Label Text="Plan Comercial" runat="server" />
                                </label>
                                <asp:DropDownList 
                                    ID="ddlPlanComercial" 
                                    CssClass="form-control select-single"
                                    AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlPlanComercial_OnSelectedIndexChanged"
                                    runat="server">
                                </asp:DropDownList>
		                    </ContentTemplate>
	                    </asp:UpdatePanel>
                    </div>

                    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">
                        <div>
                            <label>
                                <asp:Label Text="Agregar producto" runat="server" />
                            </label>
                        </div>
                        <div>
                            <asp:LinkButton ID="btnAgregarProducto"
                                runat="server"
                                CssClass="btn btn-success"
                                Style="margin-left: 5px;"
                                title="Agregar producto a la lista de precios"
                                OnClick="btnAgregarProducto_Click"
                                >
                                <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                            </asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <asp:UpdatePanel ID="upGridViewListaPrecio" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		                <ContentTemplate>
                            <asp:GridView ID="gvListaPrecio" 
                                CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" 
                                data-model="ProductosPorPlanComercialConfiguracion.aspx"
                                Width="100%" 
                                AutoGenerateColumns="False" 
                                runat="server" 
                                OnRowDataBound="gvListaPrecio_RowDataBound" 
                                EmptyDataText="No existen productos asignados al plan comercial seleccionado."
                                OnSelectedIndexChanging="gvListaPrecio_SelectedIndexChanging"
                                OnRowDeleting="gvListaPrecio_RowDeleting"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="" />
                                    <%-- cryptoID --%>
                                    <asp:BoundField HeaderText="#" DataField="IdListaPrecio" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <asp:BoundField HeaderText="Fecha de alta" DataField="FHalta" ItemStyle-CssClass="date-time" ItemStyle-Width="100"/>
                                    <asp:BoundField HeaderText="Producto asignado" DataField="DescripcionProducto" ItemStyle-Width="250"/>
                                    <asp:BoundField HeaderText="Configuración de precio" DataField="PrecioConfiguracion" ItemStyle-Width="250"/>

                                </Columns>
                            </asp:GridView>
		                </ContentTemplate>
	                </asp:UpdatePanel>

                </div>

            </div>

        </div>
    </div>

</asp:Content>