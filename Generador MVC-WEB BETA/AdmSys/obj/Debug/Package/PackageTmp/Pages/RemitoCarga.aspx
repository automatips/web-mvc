﻿<%@ Page Title="Remito" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="RemitoCarga.aspx.cs" Inherits="Pages.RemitoCarga" %>
<%@ Register Src="~/Controls/ucGridViewSolicitudesDeServicioRemitar.ascx" TagPrefix="uc" TagName="ucGridViewSolicitudesDeServicioRemitar" %>
<%@ Register Src="~/Controls/ucGridViewSolicitudesDeTallerRemitar.ascx" TagPrefix="uc" TagName="ucGridViewSolicitudesDeTallerRemitar" %>
<%@ Register Src="~/Controls/ucProductosRemitar.ascx" TagPrefix="uc" TagName="ucProductosRemitar" %>
<%@ Register Src="~/Controls/ucIngresarRemito.ascx" TagPrefix="uc" TagName="ucIngresarRemito" %>
<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        </style>
    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>

    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
            {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnBaja_click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" OnClick="btnEdit_Click" />
            </h3>
        </div>
    </div>
    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <br />

                        <div class="panel-body">

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h5>
                                        <asp:Label ID="lblPanelAlta" Text="Aún no se ingresó el remito" runat="server" />
                                    </h5>                                        
                                </div>

                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <asp:TextBox ID="txtNumeroRemito"
                                    runat="server"
                                    CssClass="form-control pull-right"
                                    PlaceHolder=""
                                    Visible="true"
                                    Enabled="false"
                                    Width="200"
                                    />
                                </div>

                            </div>

                            <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />	                            

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <uc:ucGridViewSolicitudesDeServicioRemitar 
                                    runat="server" 
                                    id="ucGridViewSolicitudesDeServicioRemitar1" 
                                    />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <uc:ucGridViewSolicitudesDeTallerRemitar 
                                    runat="server" 
                                    id="ucGridViewSolicitudesDeTallerRemitar1" 
                                    />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Button ID="btnObtenerProductosRemitar"
                                    Text="Obtener productos"
                                    CssClass="btn btn-info col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-rigt"
                                    runat="server"
                                    OnClick="btnObtenerProductosRemitar_Click"
                                    AutoPostBack="true"
                                    Style="margin-bottom: 5px;" />
                            </div>

                            <uc:ucProductosRemitar runat="server" id="ucProductosRemitar1" />

                            <uc:ucIngresarRemito runat="server" id="ucIngresarRemito1" />
                                    
                            <%-- Boton ingresar remito --%>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:UpdatePanel ID="updPanelBtnRemitoAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:Button ID="btnEnviarRemitoAlta"
                                            Text="Ingresar remito"
                                            CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-rigt"
                                            runat="server"
                                            OnClick="btnEnviarRemitoAlta_Click"
                                            Style="margin-bottom: 5px;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <%-- Boton descargar pdf de solicitud --%>
                                
                        <asp:UpdatePanel ID="upBtnDescargaRemito" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Button ID="btnDescargarPDFRemito"
                                    Text="IMPRIMIR REMITO"
                                    CssClass="btn btn-info col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-rigt"
                                    runat="server"
                                    Visible="false"
                                    OnClick="btnDescargarPDFRemito_Click"
                                    AutoPostBack="true"
                                    Style="margin-bottom: 5px;" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>