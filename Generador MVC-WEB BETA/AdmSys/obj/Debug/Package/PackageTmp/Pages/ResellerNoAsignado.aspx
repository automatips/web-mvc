﻿<%@ Page Title="Asignar" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="ResellerNoAsignado.aspx.cs" Inherits="Pages.ResellerNoAsignado" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {

        }
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del ResellerNoAsignadoo --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel-body">
                <div class="row">

                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Razón  Social" runat="server" />
                        </label>
                        <asp:TextBox ID="txtRazonSocial" CssClass="form-control" Text="" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Unidad Negocio" runat="server" />
                        </label>
                        <asp:TextBox ID="txtUnidadNegocio" CssClass="form-control" Text="" runat="server" />
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label>
                            <asp:Label Text="Distribuidor a Asignar" runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>
                            <asp:Label Text="Observaciones" runat="server" />
                        </label>
                        <textarea class="form-control" rows="3" id="txtObservacion" runat="server"></textarea>
                    </div>
                    <div class="form-group col-xs-12">
                        <h4 class="page-header">
                            <asp:Label ID="lblUM" Text="UMs Asignadas al Reseller:" runat="server" />
                        </h4>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvUMs" CssClass="table table-striped table-bordered table-hover grid-view"
                                    Width="100%" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        <asp:BoundField HeaderText="#" DataField="IdVendingReseller" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <asp:BoundField HeaderText="UM" DataField="Id_UM" />
                                        <asp:BoundField HeaderText="Observacion" DataField="Observaciones" />
                                        <asp:BoundField HeaderText="Fecha Alta" DataField="FechaAltaAsignacion" ItemStyle-CssClass="date-time" />
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <%-- Botón ResellerNoAsignados, Guardar y Restablecer --%>
            <div class="btn-models">
                <a href="ResellersNoAsignados.aspx" class="btn btn-info">
                    <asp:Label Text="Volver a la Lista" runat="server" />
                </a>
            </div>
            <div class="btns-reset-save">
                <button type="reset" class="btn btn-default">
                    <asp:Label Text="Restablecer" runat="server" />
                </button>
                <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
            </div>

            <br />
            <br />
        </div>
    </div>
</asp:Content>