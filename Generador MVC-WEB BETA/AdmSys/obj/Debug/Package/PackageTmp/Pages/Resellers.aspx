﻿<%@ Page Title="Resellers" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Resellers.aspx.cs" Inherits="Pages.Resellers" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Nuevo --%>
                <asp:Button ID="btnNuevo" Text="Nuevo" CssClass="btn btn-primary pull-right" runat="server" OnClick="btnNuevo_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="row">

                    <%-- Filtros --%>
                    <div class="panel panel-default">

                        <div class="panel-body">

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Unidad Negocio" runat="server" />
                                </label>

                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="btnFiltrar_Click" runat="server">
                                        </asp:DropDownList>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>

                        </div>
                    </div>

                    <%-- Botones Filtrar y Restablecer --%>
                    <div class="col-lg-12 text-center">
                        <asp:Button ID="btnFiltrar" Text="Filtrar" CssClass="btn btn-info" runat="server" OnClick="btnFiltrar_Click" />
                        <button type="reset" class="btn btn-default">
                            <asp:Label Text="Restablecer" runat="server" />
                        </button>
                    </div>
                </div>
            </div>

            <%-- Tabla de resultados de búsqueda de DistribuidoresResellers --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvDistribuidoresResellers" CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" data-model="Reseller.aspx"
                        Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gvDistribuidoresResellers_RowDataBound" EmptyDataText="Sin datos para mostrar.">
                        <Columns>
                            <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <%-- Responsive + - --%>
                            <asp:BoundField HeaderText="" />
                            <%-- cryptoID --%>
                            <asp:BoundField HeaderText="#" DataField="IdReseller" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                            <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column">
                                <ItemTemplate>
                                    <asp:Label ID="lblComplexId" runat="server" Text='<%# Eval("IdReseller") + "*" + Eval("IdDistribuidorUnidadNegocio") + "*" + Eval("IdDistribuidorReseller") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Reseller" DataField="RazonSocial" />
                            <asp:BoundField HeaderText="Fecha de Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                    <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                </Triggers>
            </asp:UpdatePanel>

            <%-- Información adicional o ayuda --%>
            <div class="well">
                <h4>
                    <asp:Label Text="Información adicional o ayuda" runat="server" />
                </h4>
                <p>
                    <asp:Label Text="Tincidunt integer eu augue augue nunc elit dolor, luctus placerat scelerisque euismod, iaculis eu lacus nunc mi elit, vehicula ut laoreet ac, aliquam sit amet justo nunc tempor, metus vel." runat="server" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>