﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetearPassword.aspx.cs" Inherits="Pages.SetearPassword" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema Administrativo">
    <meta name="author" content="MF">

    <title>Access Control</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="~/Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Bootstrap --%>
    <link href="~/Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="~/Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="~/Content/site.css?v.1.0" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #imgLogo {
            padding: 20px;
        }

        #login-app-name {
            text-align: center;
            font-variant: small-caps;
            background-color: #00496d;
            padding: 10px;
            border-radius: 3px;
            color: white;
        }

        #bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            font-size: xx-small;
            width: 100%;
            color: #337ab7;
            text-align: center;
        }

        #foot {
            text-align: center;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img id="imgLogo" class="img-responsive center-block" src="../Content/images/logo-login.png" />

                    </div>
                    <div class="panel-body">

                        <form id="form1" runat="server">
                            <div>
                                <asp:Label Text="" runat="server" ID="lblResult" />
                                <br />
                                <br />
                                <asp:HyperLink NavigateUrl="~/" runat="server" ID="lnkLogin" Visible="false" Text="Iniciar Sesión" />
                            </div>
                            <fieldset id="fsUser" runat="server">

                                <div class="form-group">
                                    <label>
                                        <asp:Label Text="Nombre" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtNombre" CssClass="form-control text-capital" Text="" runat="server" ReadOnly="true" />
                                </div>

                                <div class="form-group">
                                    <label>
                                        <asp:Label Text="Apellido" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtApellido" CssClass="form-control text-capital" Text="" runat="server" ReadOnly="true" />
                                </div>

                                <div class="form-group">
                                    <label>
                                        <asp:Label Text="Login Web" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtLoginUsuario" CssClass="form-control" Text="" runat="server" ReadOnly="true" />
                                </div>

                                <div class="form-group ">
                                    <label>
                                        <asp:Label Text="Nueva Password" runat="server" />
                                    </label>
                                    <asp:TextBox CssClass="form-control text-lower" Text="" runat="server" ID="txtNuevaPassword" TextMode="Password" />
                                    <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtNuevaPassword" runat="server" />
                                </div>

                                <div class="form-group ">
                                    <label>
                                        <asp:Label Text="Confirmar Password" runat="server" />
                                    </label>
                                    <asp:TextBox CssClass="form-control text-lower" Text="" runat="server" ID="txtConfirmarPassword" TextMode="Password" />
                                    <asp:RequiredFieldValidator CssClass="required-field-validator" ControlToValidate="txtConfirmarPassword" runat="server" Display="Dynamic" />
                                    <asp:CompareValidator CssClass="required-field-validator" ControlToCompare="txtNuevaPassword" ControlToValidate="txtConfirmarPassword" ErrorMessage="Las Passwords no coinciden" runat="server" Display="Dynamic" />
                                </div>

                                <asp:Button ID="btnValidate" Text="Aceptar" CssClass="btn btn-lg btn-info btn-block" runat="server" OnClick="btnValidate_Click" />
                            </fieldset>
                        </form>

                        <%-- Este sitio utiliza cookies --%>
                        <h5 id="foot" class="info">
                            <small>
                                <asp:Label Text="Este sitio utiliza cookies para mantener su sesión iniciada." runat="server" />
                            </small>
                            <br />
                            <small>
                                <asp:Label Text="Recuerde cerrar la sesión al finalizar." runat="server" />
                            </small>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <%-- Pie de página --%>
        <div class="centered">
            <div id="bottom" class="text-center">
                <span><strong>Version 1.0</strong></span>
                <br />
                <span>©</span>
                <span>2017</span>
                <span>-</span>
                <span>Sistema Administrativo</span>
            </div>
        </div>
    </div>
</body>
</html>