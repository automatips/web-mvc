﻿<%@ Page Title="Solicitud de Servicio" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="SolicitudDeServicio.aspx.cs" Inherits="Pages.SolicitudDeServicio" %>
<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>
    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>

    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <!-- Detalle de los términos y condiciones para la solicitud -->
    <div id="modalTerminosYCondiciones" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Términos y condiciones para la solicitud de servicio" runat="server" />
                    </h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upTerminosCondiciones" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Label ID="lblTerminosYCondiciones" Text="Aún no ha generado una solicitud." runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnBaja_click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" OnClick="btnEdit_Click" />
            </h3>
        </div>
    </div>
    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <%-- Alta --%>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br />

                            <asp:Panel ID="pnlAlta" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>
                                    <asp:Label ID="lblPanelAlta" Text="Aún no se ingresó la solicitud" runat="server" />
                                </h5>
                                <div class="panel-body">

                                    <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <label>
                                                <asp:Label Text="Mail Informe" runat="server" />
                                            </label>
                                            <asp:TextBox ID="txtMailInforme" CssClass="form-control" runat="server" TextMode="SingleLine" />
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <label>
                                                <asp:Label Text="Observaciones" runat="server" />
                                            </label>
                                            <asp:TextBox ID="txtObsAlta" CssClass="form-control" runat="server" TextMode="MultiLine" />
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <label>
                                                <asp:Label Text="Forma de pago" runat="server" />
                                            </label>
                                            <asp:DropDownList 
                                                ID="ddlFormaPago" 
                                                CssClass="form-control select-single" 
                                                runat="server"/>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:UpdatePanel ID="upDetalleCarrito" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                        <ContentTemplate>
                                            <center>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h3>
                                                        <asp:Label Text="Productos agregados al carrito" runat="server" />
                                                    </h3>
                                                    <asp:GridView ID="gvListaProductosCargados" CssClass="table table-bordered table-hover grid-view table-condensed"
                                                        Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Aún no ha cargado productos en la presente solicitud de servicio."
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanging="gvListaProductosCargados_SelectedIndexChanging"
                                                        >
                                                        <Columns>
                                                            <asp:BoundField HeaderText="#" DataField="IdListaPrecio" Visible="true" ItemStyle-Width="1"/>
                                                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-Width="50">
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <asp:TextBox 
                                                                            ID="txtCantidadProductos"
                                                                            runat="server"
                                                                            CssClass="form-control numeric-integer-positive"
                                                                            With="50"
                                                                            AutoPostBack="true"
                                                                            OnTextChanged="txtCantidadProductos_TextChanged"
                                                                            >
                                                                        </asp:TextBox>
                                                                    </center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Código" DataField="CodigoProducto" HTMLEnCode="false" ItemStyle-Width="50"/>
                                                            <asp:BoundField HeaderText="Descripción" DataField="DescripcionProducto" HTMLEnCode="false" ItemStyle-Width="150"/>
                                                            <asp:BoundField HeaderText="Subtotal IVA Precio Lista" DataField="ImporteProductoLista" HTMLEnCode="false" ItemStyle-CssClass="numeric-money show-total" ItemStyle-Width="50"/>
                                                            <asp:BoundField HeaderText="Subtotal IVA Precio Contado" DataField="ImporteProductoContado" HTMLEnCode="false" ItemStyle-CssClass="numeric-money show-total" ItemStyle-Width="50"/>
                                                            <asp:TemplateField HeaderText="Quitar" ItemStyle-Width="1">
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <asp:LinkButton 
                                                                            ID="btnQuitarProducto"
                                                                            runat="server"
                                                                            CssClass="btn btn-danger"
                                                                            Style="margin-left: 5px;"
                                                                            title="Quitar del carrito"
                                                                            CommandName="Select"
                                                                            >
                                                                            <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                                                        </asp:LinkButton>
                                                                    </center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </center>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Button ID="btnVolverAlListado" Text="Volver al listado de solicitudes" CssClass="btn btn-info col-lg-12 col-md-12 col-sm-12 col-xs-12" runat="server" OnClick="btnVolverAlListado_Click"/>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:UpdatePanel ID="updPanelBtnSolicitudAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                            <ContentTemplate>
                                                <asp:Button ID="btnEnviarSolicitudAlta"
                                                    Text="Ingresar solicitud"
                                                    CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                    runat="server"
                                                    OnClick="btnEnviarSolicitudAlta_Click"
                                                    Style="margin-bottom: 5px;" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <%-- Tabla de resultados de búsqueda de ListasPrecios --%>
                                    <asp:UpdatePanel runat="server" ID="upListaPrecios" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <center>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h3>
                                                        <asp:Label Text="Productos disponibles por plan comercial" runat="server" />
                                                    </h3>

                                                    <asp:GridView ID="gvListasPrecios" 
                                                        CssClass="table table-striped table-bordered table-hover crypto-id grid-view" 
                                                        Width="100%" AutoGenerateColumns="False" runat="server" 
                                                        EmptyDataText="Sin artículos coincidentes con el plan comercial aplicado."
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanging="gvListasPrecios_SelectedIndexChanging"
                                                        >
                                                        <Columns>
                                                            <asp:BoundField HeaderText="#" DataField="IdListaPrecio" ItemStyle-Width="1"/>
                                                            <asp:TemplateField ItemStyle-Width="1" Visible="false">
                                                                <HeaderTemplate>Image</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <img src='data:image/jpg;base64,<%# 
                                                                                Eval("ArchivoContenido") != System.DBNull.Value 
                                                                                ? 
                                                                                Convert.ToBase64String((byte[])Eval("ArchivoContenido")) : 
                                                                                string.Empty %>
                                                                            ' alt="" height="50" width="50" onmouseover="Large(this)" />
                                                                    </center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Código" DataField="CodigoProducto" HTMLEnCode="false" ItemStyle-Width="1" />
                                                            <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" HTMLEnCode="false" ItemStyle-Width="100" />
                                                            <asp:BoundField HeaderText="Fecha Alta" DataField="FHAlta" HTMLEnCode="false" ItemStyle-CssClass="date-time"  ItemStyle-Width="100" />
                                                            <asp:TemplateField HeaderText="Tipo" ItemStyle-Width="100">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" Text='<%# ((bool)Eval("Servicio") == true ? "Servicio" : "Producto") %>' ItemStyle-Width="100" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Precio Lista" DataField="PrecioLista" ItemStyle-CssClass="numeric-money" ItemStyle-Width="100" />
                                                            <asp:BoundField HeaderText="Precio Contado" DataField="PrecioContado" ItemStyle-CssClass="numeric-money" ItemStyle-Width="100" />
                                                            <asp:BoundField HeaderText="Moneda" DataField="Moneda" ItemStyle-Width="50" />
                                                            <asp:TemplateField HeaderText="Agregar" ItemStyle-Width="1">
                                                                <ItemTemplate>
                                                                    <center>
                                                                        <asp:LinkButton 
                                                                            ID="btnAgregarProductoServicio"
                                                                            runat="server"
                                                                            CssClass="btn btn-success"
                                                                            Style="margin-left: 5px;"
                                                                            title="Agregar al carrito"
                                                                            CommandName="Select"
                                                                            >
                                                                            <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                                                        </asp:LinkButton>

                                                                        <asp:LinkButton 
                                                                            ID="btnAgregado"
                                                                            runat="server"
                                                                            CssClass="btn btn-info"
                                                                            Style="margin-left: 5px;"
                                                                            title="Item agregado al carrito"
                                                                            Visible="false"
                                                                            Enabled="false"
                                                                            >
                                                                            <span aria-hidden="true" class="glyphicon glyphicon-ok"></span>
                                                                        </asp:LinkButton>
                                                                    </center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </center>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:UpdatePanel ID="UpdatePaanelDescargarPdfSolicitud" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                        <ContentTemplate>
                                            <asp:Button ID="btnDescargarPDFSolicitud"
                                                Text="Imprimir SDS para firmar"
                                                CssClass="btn btn-info col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-rigt"
                                                runat="server"
                                                Visible="false"
                                                OnClick="btnDescargarPDFSolicitud_Click"
                                                Style="margin-bottom: 5px;" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </asp:Panel>
                        </div>

                        <%-- Aceptacion --%>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br />
                            <asp:Panel ID="pnlAceptacion" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>
                                    <asp:Label ID="lblPanelAceptacion" Text="Aún no se aceptó la solicitud" runat="server" />
                                </h5>

                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <asp:Label Text="Observaciones" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtObsAceptacion" Enabled="false" CssClass="form-control" runat="server" TextMode="MultiLine" />
                                </div>

                                <%-- Importacion del comprobante --%>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>
                                        <asp:Label ID="lblImportarComprobanteFirmado" Text="Importar comprobante firmado" runat="server" />
                                    </label>
                                    <asp:Label ID="lblEncrypId" runat="server" Visible="false"></asp:Label>
                                    <asp:FileUpload ID="fileImagen" runat="server" CssClass="form-control file" Enabled="false" data-show-preview="false" />
                                    <%--<asp:Button ID="btnSubirArchivo" Text="Subir" CssClass="btn btn-success" runat="server" OnClick="btnSubirArchivo_Click" />--%>
                                </div>

                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="hlDescargarPDF"
                                        Text="Descargar comprobante de solicitud firmado por el cliente"
                                        NavigateUrl="#"
                                        runat="server"
                                        Target="_blank"
                                        Visible="false"
                                        CssClass="btn btn-info" />
                                </div>

                                <div>
                                    <asp:Button ID="btnEnviarSolicitudAceptacion" Text="Aceptar solicitud"
                                        CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right"
                                        runat="server" Enabled="false" OnClick="btnEnviarSolicitudAceptacion_Click" Style="margin-bottom: 5px;" />
                                </div>

                            </asp:Panel>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>