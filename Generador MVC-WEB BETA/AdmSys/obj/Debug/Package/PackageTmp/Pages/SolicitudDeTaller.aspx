﻿<%@ Page Title="Solicitud de Taller" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="SolicitudDeTaller.aspx.cs" Inherits="Pages.SolicitudDeTaller" %>
<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>
<%@ Register Src="~/Controls/ucGestionDeEnvio.ascx" TagPrefix="uc" TagName="ucGestionDeEnvio" %>
<%@ Register Src="~/Controls/ucSDTDetalleFajaIngreso.ascx" TagPrefix="uc" TagName="ucSDTDetalleFajaIngreso" %>
<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>
    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>

    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
            {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        $(function ()
        {
            $(document).on
            ('click', '.borrar', function (event)
                {
                    event.preventDefault();
                    $(this).closest('tr').remove();
                }
            );
        });

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <div id="modalHistorialDeSDT" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="Historial de cambios en la SDT actual: " runat="server" />
                    </h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upSDTHistorico" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <div style="overflow-x: auto; overflow-y: hidden">
                                <asp:GridView ID="gvSDTHistorico" CssClass="table table-bordered table-hover col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                    Width="100%" AutoGenerateColumns="False" runat="server" EmptyDataText="Aún no existe historial asociado a la presente solicitud de taller."
                                    AutoPostBack="true">
                                    <Columns>
                                        <asp:BoundField HeaderText="FH de transacción" DataField="FHTransaccion" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="SDT Nro" DataField="SDTNro" />
                                        <asp:BoundField HeaderText="Mail informe" DataField="MailInforme" />

                                        <asp:BoundField HeaderText="FH Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Cuenta Envío" DataField="IdCuentaEnvio" />
                                        <asp:BoundField HeaderText="Obs Envío" DataField="ObservacionesEnvio" />

                                        <asp:BoundField HeaderText="Obs Finalización" DataField="ObservacionesFinalizacion" />
                                        <asp:BoundField HeaderText="Unidad de Negocio" Visible="false" DataField="IdunidadNegocio" />
                                        <asp:BoundField HeaderText="Distribuidor Unidad Negocio" Visible="false" DataField="IdDistribuidorUnidadNegocio" />
                                        <asp:BoundField HeaderText="Id Plan Comercial" Visible="false" DataField="IdPlanComercial" />
                                        <asp:BoundField HeaderText="Id Reseller" Visible="false" DataField="IdReseller" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <asp:Label Text="Cerrar" runat="server" />
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server" OnClick="btnBaja_click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" OnClick="btnEdit_Click" />
            </h3>
        </div>
    </div>
    <%-- Contenido --%>
    <div class="row">
        <br />

        <asp:Panel ID="pnlAlta" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h5>
                <asp:Label ID="lblPanelAlta" Text="Aún no se ingresó la solicitud. Seleccione las UM en donde ocurren las incidencias, para agregarlas a la solicitud o asignar algunos de los DISPOSITIVOS o ACCESORIOS." runat="server" />
            </h5>
                   
            <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />
            
	        <asp:UpdatePanel ID="upErrorHandler" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		        <ContentTemplate>
			        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;">
				        <label>
					        <asp:Label ID="lblErrorHandler" Text="" runat="server" />
				        </label>
			        </div>
		        </ContentTemplate>
	        </asp:UpdatePanel>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                <asp:UpdatePanel ID="upGvSDTDetalle" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:GridView ID="gvSDTDetalle" CssClass="table table-bordered table-hover"
                            Width="100%" AutoGenerateColumns="False" runat="server" 
                            EmptyDataText="Aún no existe hardware asociado a la presente solicitud de taller."
                            OnRowDeleting="gvSDTDetalle_RowDeleting"
                            AutoPostBack="true">
                            <Columns>
                                <asp:BoundField HeaderText="Descripción" DataField="descripcionHardware" HtmlEncode="false" ItemStyle-Width="200" />
                                <asp:BoundField HeaderText="Id Serial" DataField="idSerial" HtmlEncode="false" ItemStyle-Width="75" />
                                <asp:BoundField HeaderText="Tipo Hardware" DataField="tipoHardware" HtmlEncode="false" ItemStyle-Width="75" />

                                <asp:TemplateField HeaderText="Detalle de la incidencia">
                                    <ItemTemplate>
                                        <asp:TextBox
                                            ID="txtDescripcionIncidencia"
                                            runat="server"
                                            CssClass="form-control"
                                            placeholder="Ejemplos: Fallas en la comunicación; Se necesita cambio de backlight, etc" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Estado" DataField="sdtDetalleEstado" ItemStyle-Width="100" />

                                <asp:TemplateField HeaderText="Faja de ingreso" ItemStyle-Width="250">
                                    <ItemTemplate>
                                        <uc:ucSDTDetalleFajaIngreso runat="server" ID="ucSDTDetalleFajaIngresoRow" />
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Quitar" ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="btnQuitarArtefacto"
                                                runat="server"
                                                CssClass="btn btn-danger"
                                                Style="margin-bottom: 5px;"
                                                CommandName="Delete"
                                                ItemStyle-Width="50"
                                                title="Eliminar de la lista">
                                                <span aria-hidden="true" class="glyphicon glyphicon-trash"></span>
                                            </asp:LinkButton>
                                        </center>

                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:UpdatePanel ID="upCantidadBultos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <label>
                                            <asp:Label ID="lblCantidadBultos" Text="" Font-Size="Medium" runat="server" />
                                        </label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <asp:Button ID="btnImprimirSDT"
                                        Text="IMPRIMIR SDT"
                                        CssClass="btn btn-info col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-rigt"
                                        runat="server"
                                        Visible="false"
                                        OnClick="btnImprimirSDT_Click"
                                        Style="margin-bottom: 5px;" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" runat="server" id="mnuTabSeleccionUM" href="#menuUM">Selección de UM's</a></li>
                <li><a data-toggle="tab" runat="server" id="mnuTabSeleccionDispositivos" href="#menuDISPOSITIVOS">Selección de DISPOSITIVOS</a></li>
                <li><a data-toggle="tab" runat="server" id="mnuTabSeleccionAccesorio" href="#menuACCESORIOS">Carga de ACCESORIOS</a></li>
                <li><a data-toggle="tab" runat="server" id="mnuEnvio" href="#menuEnvio">Gestión de envío</a></li>
            </ul>

            <div class="tab-content">

                <div id="menuUM" class="tab-pane fade in active">
                    <p></p>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                        <label>
                            <asp:Label Text="Listado de UM asociadas a los filtros seleccionados" runat="server" />
                        </label>
                        <asp:UpdatePanel ID="upListaUMFiltradas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <asp:GridView ID="gvUM"
                                    CssClass="table table-bordered table-hover grid-view"
                                    Width="100%"
                                    AutoGenerateColumns="False"
                                    runat="server"
                                    EmptyDataText="No existen UM's bajo los filtros ingresados "
                                    OnSelectedIndexChanging="gvUM_SelectedIndexChanging"
                                    AutoPostBack="true">
                                    <Columns>
                                        <asp:BoundField HeaderText="" DataField="" />
                                        <asp:BoundField HeaderText="Id UM" DataField="Identificador" Visible="false" />
                                        <asp:BoundField HeaderText="ID SERIAL" DataField="Serial" />
                                        <asp:BoundField HeaderText="Ultima Conexion" DataField="UltimaConexion" Visible="false" />
                                        <asp:BoundField HeaderText="Ultima Conexion Exitosa" DataField="UltimaConexionExitosa" Visible="false" />
                                        <asp:BoundField HeaderText="Modelo" DataField="Modelo_Nombre" />
                                        <asp:BoundField HeaderText="Descripción" DataField="Modelo_Descripcion" />
                                        <asp:BoundField HeaderText="Versión del Software" DataField="Software_Version" />
                                        <asp:BoundField HeaderText="Nombre del Software" DataField="Software_Nombre" Visible="false" />
                                        <asp:BoundField HeaderText="Descripción del Software" DataField="Software_Descripcion" Visible="false" />

                                        <asp:BoundField HeaderText="C.Area chip 1" DataField="CodArea_Chip1" />
                                        <asp:BoundField HeaderText="Telefono chip 1" DataField="NroTelefono_Chip1" Visible="false" />
                                        <asp:BoundField HeaderText="C.Area chip 2" DataField="CodArea_Chip2" Visible="false" />
                                        <asp:BoundField HeaderText="Telefono chip 2" DataField="NroTelefono_Chip2" Visible="false"/>

                                        <asp:TemplateField HeaderText="Agregar" ItemStyle-Width="50">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:LinkButton ID="btnAgregarUM"
                                                        runat="server"
                                                        CssClass="btn btn-success"
                                                        CommandName="Select"
                                                        Style="margin-bottom: 5px;"
                                                        ItemStyle-Width="50"
                                                        title="Agregar UM">
                                                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                                    </asp:LinkButton>
                                                </center>

                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="En Garantía" ItemStyle-Width="50">
                                            <ItemTemplate>
                                                <asp:CheckBox
                                                    ID="cbxEnGarantia"
                                                    Enabled="false"
                                                    runat="server" Width="50"
                                                    DataField="EnGarantia" />
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div id="menuDISPOSITIVOS" class="tab-pane fade">
                <p></p>

	                <asp:UpdatePanel ID="upCargaDeDispositivo" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                <ContentTemplate>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <asp:Label Text="Seleccione el tipo de carga de dispositivo:" runat="server" />
                                </label>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DropDownList 
                                    ID="ddlTipoCargaDispositivo" 
                                    CssClass="form-control select-single" 
                                    runat="server"
                                    AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlTipoCargaDispositivo_OnSelectedIndexChanged"
                                    >
                                </asp:DropDownList>
                            </div>

	                        <asp:UpdatePanel ID="upDispositivoSinSerial" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" Visible="false">
		                        <ContentTemplate>
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <label>
                                                <asp:Label Text="Agregar dispositivo sin IDSERIAL predeterminado" runat="server" />
                                            </label>
                                        </div>

                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
	                                        <asp:UpdatePanel ID="upTipoDispositivo" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                                        <ContentTemplate>
                                                    <label>
                                                        <asp:Label Text="Tipo" runat="server" />
                                                    </label>

                                                    <asp:DropDownList 
                                                        ID="ddlTipoDispositivo" 
                                                        CssClass="form-control select-single"
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlTipoDispositivo_OnSelectedIndexChanged"
                                                        runat="server">
                                                    </asp:DropDownList>
		                                        </ContentTemplate>
	                                        </asp:UpdatePanel>
                                        </div>

                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
	                                        <asp:UpdatePanel ID="upModeloProducto" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		                                        <ContentTemplate>
                                                    <label>
                                                        <asp:Label Text="Modelo" runat="server" />
                                                    </label>

                                                    <asp:DropDownList 
                                                        ID="ddlModeloProducto" 
                                                        CssClass="form-control select-single"
                                                        runat="server">
                                                    </asp:DropDownList>
		                                        </ContentTemplate>
	                                        </asp:UpdatePanel>
                                        </div>

                                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">
                                            <div>
                                                <label>
                                                    <asp:Label Text="Agregar dispositivo" runat="server" />
                                                </label>
                                            </div>
                                            <div>
                                                <asp:LinkButton ID="btnAgregarDispositivoSinSerial"
                                                    runat="server"
                                                    CssClass="btn btn-success"
                                                    Style="margin-left: 5px;"
                                                    title="Agregar dispositivo a la solicitud de taller"
                                                    OnClick="btnAgregarDispositivoSinSerial_Click"
                                                    >
                                                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
		                        </ContentTemplate>
	                        </asp:UpdatePanel>

	                        <asp:UpdatePanel ID="upDispositivosConSerial" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" Visible="true">
		                        <ContentTemplate>
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                        <p></p>
                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <label>
                                                <asp:Label Text="Listado de Dispositivos asociados a las UM seleccionadas" runat="server" />
                                            </label>
                                        </div>

                                        <asp:UpdatePanel ID="upListaDispositivosFiltrados" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvDispositivos"
                                                    CssClass="table table-bordered table-hover grid-view"
                                                    Width="100%" 
                                                    AutoGenerateColumns="False" 
                                                    runat="server" 
                                                    EmptyDataText="No existen dispositivos bajo los filtros ingresados. Seleccione UM a las cuales pertenezcan los Dispositivos que desea revisar."
                                                    OnSelectedIndexChanging="gvDispositivos_SelectedIndexChanging"
                                                    AutoPostBack="true">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="" DataField="" />
                                                        <asp:BoundField HeaderText="ID SERIAL" DataField="Serial" />
                                                        <asp:BoundField HeaderText="Tipo" DataField="Tipo" />
                                                        <asp:BoundField HeaderText="Modelo" DataField="Modelo" />

                                                        <asp:TemplateField HeaderText="Agregar" ItemStyle-Width="50">
                                                            <ItemTemplate>
                                                                <center>
                                                                    <asp:LinkButton ID="btnAgregarDispositivo"
                                                                        runat="server"
                                                                        CssClass="btn btn-success"
                                                                        CommandName="Select"
                                                                        Style="margin-bottom: 5px;"
                                                                        ItemStyle-Width="50"
                                                                        title="Agregar Dispositivo">
                                                                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                                                    </asp:LinkButton>
                                                                </center>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="50px" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="En Garantía" ItemStyle-Width="50">
                                                            <ItemTemplate>
                                                                <asp:CheckBox
                                                                    ID="cbxEnGarantia"
                                                                    Enabled="false"
                                                                    runat="server" Width="50"
                                                                    AutoPostBack="true"
                                                                    DataField="EnGarantia" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="50px" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
		                        </ContentTemplate>
	                        </asp:UpdatePanel>

		                </ContentTemplate>
	                </asp:UpdatePanel>

                </div>

                <div id="menuACCESORIOS" class="tab-pane fade">
                <p></p>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                        <p></p>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">

                            <label>
                                <asp:Label Text="Descripción" runat="server" />
                            </label>

                            <asp:TextBox
                                ID="txtAccesorioDescripcion"
                                CssClass="form-control"
                                PlaceHolder="Ingrese descripción del accesorio"
                                runat="server"
                                TextMode="SingleLine" />
                        </div>

                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">

                            <label>
                                <asp:Label Text="Tipo de accesorio" runat="server" />
                            </label>

                            <asp:TextBox
                                ID="txtAccesorioTipoHardware"
                                CssClass="form-control"
                                PlaceHolder="Ingrese el tipo de accesorio"
                                runat="server"
                                TextMode="SingleLine" />
                        </div>

                        <p></p>

                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">

                            <div class="container col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                            </div>

                            <div class="container col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <asp:Button ID="btnAgregarAccesorio"
                                    Text="Agregar accesorio"
                                    CssClass="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-rigt"
                                    runat="server"
                                    OnClick="btnAgregarAccesorio_Click"
                                    Style="margin-bottom: 5px;" />
                            </div>


                        </div>

                    </div>
                </div>

                <div id="menuEnvio" class="tab-pane fade">
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                        <p></p>
                        <uc:ucGestionDeEnvio runat="server" id="ucGestionDeEnvio1"/>
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>
                    <asp:Label Text="Observaciones" runat="server" />
                </label>
                <asp:TextBox ID="txtObsAlta"
                    CssClass="form-control"
                    PlaceHolder="Ingrese las observaciones pertinentes..."
                    runat="server"
                    TextMode="MultiLine" />
            </div>

            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label>
                    <asp:Label Text="Mail Informe" runat="server" />
                </label>
                <asp:UpdatePanel ID="upTxtMailInforme" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                    <asp:TextBox ID="txtMailInforme"
                        CssClass="form-control"
                        PlaceHolder="ingresecorreoanotificar@ejemplocontrolglobal.com"
                        runat="server"
                        TextMode="SingleLine" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <%-- Boton ingresar solicitud --%>
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <asp:UpdatePanel ID="updPanelBtnSolicitudAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Button ID="btnEnviarSolicitudAlta"
                                Text="Ingresar SDT"
                                CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                runat="server"
                                OnClick="btnEnviarSolicitudAlta_Click"
                                Style="margin-bottom: 5px;" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <button type="button"
                        class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12"
                        id="btnVerHistorialSDT"
                        runat="server"
                        data-toggle="modal" data-target="#modalHistorialDeSDT">
                        Ver historial de SDT
                    </button>
                </div>
            </div>

            <%-- Boton descargar pdf de solicitud --%>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <asp:UpdatePanel ID="UpdatePaanelDescargarPdfSolicitud" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:Button ID="btnDescargarPDFSolicitud"
                            Text="Generar solicitud para firmar"
                            CssClass="btn btn-info col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-rigt"
                            runat="server"
                            Visible="false"
                            OnClick="btnDescargarPDFSolicitud_Click"
                            AutoPostBack="true"
                            Style="margin-bottom: 5px;" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </asp:Panel>
    </div>
</asp:Content>