﻿<%@ Page Title="Solicitudes de Servicios" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="SolicitudesDeServicios.aspx.cs" Inherits="Pages.SolicitudesDeServicios" %>
<%@ Register Src="~/Controls/ucFiltro_DistribuidorUnidadNegocioReseller.ascx" TagPrefix="uc" TagName="ucFiltro_DistribuidorUnidadNegocioReseller" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <asp:Button ID="btnNuevaSolicitud" Text="Nueva 'Solicitud de servicio'" CssClass="btn btn-success pull-right" runat="server" OnClick="btnNuevaSolicitud_Click"/>
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <div class="panel-body">
                        <uc:ucFiltro_DistribuidorUnidadNegocioReseller runat="server" ID="ucFiltro_DistribuidorUnidadNegocioReseller1" />

                        <asp:UpdatePanel ID="upGridViewSolicitudesDeServicios" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <asp:GridView ID="gvSolicitudesDeServicios" 
                                    CssClass="table table-striped table-bordered table-hover grid-view crypto-id clickable" 
                                    data-model="SolicitudDeServicio.aspx"
                                    Width="100%" AutoGenerateColumns="False" runat="server" 
                                    EmptyDataText="No hay solicitudes de servicios que apliquen a los filtros seleccionados"
                                    >
                                    <Columns>
                                        <%-- Requeridas para las GridViews clickeables - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        <%-- Responsive + - --%>
                                        
                                        <asp:BoundField HeaderText="" />
                                        <%-- cryptoID --%>
                                        
                                        <asp:BoundField HeaderText="#" DataField="IdSDS" HeaderStyle-CssClass="crypto-id-column" ItemStyle-CssClass="crypto-id-column" />
                                        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                        
                                        <asp:BoundField HeaderText="Id Distribuidor Unidad Negocio" DataField="IdDistribuidorUnidadNegocio" />
                                        <asp:BoundField HeaderText="SDS Nro" DataField="SDSNro" />
                                        <asp:BoundField HeaderText="Mail informe" DataField="MailInforme" />

                                        <asp:BoundField HeaderText="Id Plan Comercial" Visible="false" DataField="IdPlanComercial" />
                                        <asp:BoundField HeaderText="Id Reseller" Visible="false" DataField="IdReseller" />
                                        <asp:BoundField HeaderText="Id Imagen" Visible="false" DataField="IdImagenSDS" />

                                        <asp:BoundField HeaderText="Fecha de Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Obs Alta" DataField="ObservacionAlta" />

                                        <asp:BoundField HeaderText="Fecha de Aceptación" DataField="FHAceptacion" ItemStyle-CssClass="date-time" />
                                        <asp:BoundField HeaderText="Obs Aceptación" DataField="ObservacionAceptacion" />

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>

        </div>
    </div>

</asp:Content>