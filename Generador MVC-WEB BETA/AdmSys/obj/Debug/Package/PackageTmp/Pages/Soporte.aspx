﻿<%@ Page Title="Soporte" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="Soporte.aspx.cs" Inherits="Pages.Soporte" %>

<%@ Register Src="~/Controls/ucFiltrosJerarquia.ascx" TagPrefix="uc" TagName="ucFiltrosJerarquia" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>
    <%-- timeline --%>
    <link href="../Content/vendor/timeline.min.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server" OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">

                <div class="row">

                    <uc:ucFiltrosJerarquia runat="server" ID="ucFiltrosJerarquia" />

                </div>

                <br />

                <!-- Pestañas Detalle e Historial -->
                <ul id="tabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tabDetalle" role="tab" data-toggle="tab">
                            <asp:Label Text="Detalle" runat="server" />
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#tabHistorial" role="tab" data-toggle="tab">
                            <asp:Label Text="Historial" runat="server" />
                        </a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div id="tabDetalle" role="tabpanel" class="tab-pane active">
                        <br />

                        <%-- ID --%>
                        <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="ID" runat="server" />
                            </label>
                            <div class="control-div-static">
                                <asp:Label ID="lblID" Text="..." CssClass="form-control-static" runat="server" />
                            </div>
                        </div>

                        <%-- Soporte Tipo --%>
                        <div class="form-group col-lg-6 col-md-5 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Tipo de Soporte" runat="server" />
                            </label>
                            <asp:DropDownList ID="ddlSoporteTipo" CssClass="form-control select-single" runat="server" />
                            <asp:RequiredFieldValidator ControlToValidate="ddlSoporteTipo" InitialValue="k9lH3ouqFnI=" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                        </div>

                        <%-- Hardware --%>
                        <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Hardware" runat="server" />
                            </label>
                            <div class="checkbox">
                                <label>
                                    <asp:Label Text="No" runat="server" />
                                    <asp:CheckBox ID="chkHardware" runat="server" AutoPostBack="true" OnCheckedChanged="chkHardware_OnCheckedChanged" />
                                    <asp:Label Text="Si" runat="server" />
                                </label>
                            </div>
                        </div>

                        <%-- Prioridad --%>
                        <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Prioridad" runat="server" />
                            </label>
                            <asp:DropDownList ID="ddlPrioridad" CssClass="form-control select-single" runat="server" />
                            <asp:RequiredFieldValidator ControlToValidate="ddlPrioridad" InitialValue="k9lH3ouqFnI=" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                        </div>

                        <%-- Asunto --%>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <asp:Label Text="Asunto" runat="server" />
                            </label>
                            <asp:TextBox ID="txtAsunto" CssClass="form-control" runat="server" />
                            <asp:RequiredFieldValidator ControlToValidate="txtAsunto" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                        </div>

                        <%-- Detalle --%>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <asp:Label Text="Detalle" runat="server" />
                            </label>
                            <asp:TextBox ID="txtDetalle" CssClass="form-control" runat="server" TextMode="MultiLine" />
                            <asp:RequiredFieldValidator ControlToValidate="txtDetalle" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                        </div>

                        <%-- Detalle Hardware --%>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:Panel ID="pnlHardware" runat="server" Visible="false">

                                    <%-- UM --%>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="UM" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="DropDownList1" CssClass="form-control select-single" runat="server" />
                                    </div>

                                    <%-- Dispositivo --%>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>
                                            <asp:Label Text="Dispositivo" runat="server" />
                                        </label>
                                        <asp:DropDownList ID="DropDownList2" CssClass="form-control select-single" runat="server" />
                                    </div>

                                </asp:Panel>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="chkHardware" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <%-- Imágenes --%>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <asp:Label Text="Imágenes" runat="server" />
                            </label>
                            <asp:FileUpload ID="fuImagenes" runat="server" CssClass="form-control file" data-show-preview="true" multiple="multiple" accept="image/x-png,image/gif,image/jpeg" />
                        </div>

                        <%-- Mail notificacion --%>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>
                                <asp:Label Text="Email para notificación" runat="server" />
                            </label>
                            <asp:TextBox ID="txtMailNotificacion" CssClass="form-control" runat="server" />
                        </div>

                    </div>

                    <div id="tabHistorial" role="tabpanel" class="tab-pane">
                        <br />

                        <div class="qa-message-list" style="margin-left: 10px; margin-right: 10px;">

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <div class="message-item">
                                        <div id="xxx" class="message-inner">
                                            <div class="message-head clearfix">
                                                <div class="avatar pull-left">
                                                    <img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div>
                                                <div class="user-detail">
                                                    <h5 class="handle">
                                                        <asp:Label ID="Label1" runat="server" Text="Hola mundo" />
                                                    </h5>
                                                    <div class="post-meta">
                                                        <div class="asker-meta">
                                                            <span class="qa-message-what"></span>
                                                            <span class="qa-message-when">
                                                                <span class="qa-message-when-data">
                                                                    <asp:Label ID="Label2" CssClass="date-time" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="qa-message-content">
                                                <asp:Label ID="Label3" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-item">
                                        <div class="message-inner">
                                            <div class="message-head clearfix">
                                                <div class="avatar pull-left">
                                                    <img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div>
                                                <div class="user-detail">
                                                    <h5 class="handle">
                                                        <asp:Label ID="Label4" runat="server" />
                                                    </h5>
                                                    <div class="post-meta">
                                                        <div class="asker-meta">
                                                            <span class="qa-message-what"></span>
                                                            <span class="qa-message-when">
                                                                <span class="qa-message-when-data">
                                                                    <asp:Label ID="Label5" CssClass="date-time" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="qa-message-content">
                                                <asp:Label ID="Label6" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-item">
                                        <div class="message-inner">
                                            <div class="message-head clearfix">
                                                <div class="avatar pull-left">
                                                    <img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div>
                                                <div class="user-detail">
                                                    <h5 class="handle">
                                                        <asp:Label ID="Label7" runat="server" />
                                                    </h5>
                                                    <div class="post-meta">
                                                        <div class="asker-meta">
                                                            <span class="qa-message-what"></span>
                                                            <span class="qa-message-when">
                                                                <span class="qa-message-when-data">
                                                                    <asp:Label ID="Label8" CssClass="date-time" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="qa-message-content">
                                                <asp:Label ID="Label9" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-item">
                                        <div class="message-inner">
                                            <div class="message-head clearfix">
                                                <div class="avatar pull-left">
                                                    <img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div>
                                                <div class="user-detail">
                                                    <h5 class="handle">
                                                        <asp:Label ID="Label10" runat="server" />
                                                    </h5>
                                                    <div class="post-meta">
                                                        <div class="asker-meta">
                                                            <span class="qa-message-what"></span>
                                                            <span class="qa-message-when">
                                                                <span class="qa-message-when-data">
                                                                    <asp:Label ID="Label11" CssClass="date-time" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="qa-message-content">
                                                <asp:Label ID="Label12" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-item">
                                        <div class="message-inner">
                                            <div class="message-head clearfix">
                                                <div class="avatar pull-left">
                                                    <img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div>
                                                <div class="user-detail">
                                                    <h5 class="handle">
                                                        <asp:Label ID="Label13" runat="server" />
                                                    </h5>
                                                    <div class="post-meta">
                                                        <div class="asker-meta">
                                                            <span class="qa-message-what"></span>
                                                            <span class="qa-message-when">
                                                                <span class="qa-message-when-data">
                                                                    <asp:Label ID="Label14" CssClass="date-time" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="qa-message-content">
                                                <asp:Label ID="Label15" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>

                </div>

            </div>

            <div class="bottom-buttons ">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <a href="Soportes.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
                </div>

            </div>

        </div>
    </div>

</asp:Content>