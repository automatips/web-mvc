﻿<%@ Page Title="Desasignar Reseller" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="UMDesasignarReseller.aspx.cs" Inherits="Pages.UMDesasignarReseller" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del UMAsignarDistribuidor --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h2 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

            </h2>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Distribuidor" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlDistribuidor" CssClass="form-control select-single" AutoPostBack="true" OnSelectedIndexChanged="ddlDistribuidor_OnSelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Unidad Negocio" runat="server" />
                                </label>

                                <asp:DropDownList ID="ddlUnidadNegocio" CssClass="form-control select-single" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUnidadNegocio_OnSelectedIndexChanged">
                                </asp:DropDownList>

                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Reseller" runat="server" />
                                </label>
                                <asp:DropDownList ID="ddlReseller" CssClass="form-control select-single" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDistribuidor" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <hr />
                <h4>UM Disponibles</h4>
                <div class="row">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvUM" runat="server" CssClass="table table-striped table-bordered table-hover checkable " AutoGenerateColumns="false" DataKeyNames="UMId" ShowFooter="True" EmptyDataText="No hay más UMs">
                                <Columns>
                                    <%-- Id encriptado --%>
                                    <asp:BoundField HeaderText="Id" DataField="UMId" />

                                    <%-- Checkbox --%>
                                    <asp:TemplateField HeaderText="Seleccionada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRow" runat="server" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Disponibles: <%# gvUM.Rows.Count %>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Id_UM" HeaderText="Id" />
                                    <asp:BoundField DataField="FechaAlta" HeaderText="Fecha Alta" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Modelo" />
                                    <asp:BoundField DataField="Detalle" HeaderText="Detalle" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlUnidadNegocio" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <div class="bottom-buttons">

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Desasignar Seleccionadas" CssClass="btn btn-success" Enabled="True" runat="server" OnClick="btnSave_Click" />
                </div>

            </div>

        </div>
    </div>

</asp:Content>