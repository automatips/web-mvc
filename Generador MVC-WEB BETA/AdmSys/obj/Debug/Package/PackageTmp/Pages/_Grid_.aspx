﻿<%@ Page Title="_Grid_" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="_Grid_.aspx.cs" Inherits="Pages._Grid_" %>

<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {
        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Atras --%>
                <a class="btn btn-default pull-left" onclick="history.back();">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>

                <%-- Separador horizontal --%>
                <span class="pull-left">&nbsp;</span>

                <%-- Actualizar --%>
                <a class="btn btn-default pull-left" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />

            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <%-- Este control actua de gatillo del UpdatePanel. Hay que incluirlo dentro del UpdatePanel para que se ejecuten los RequestFieldValidators --%>
                            <div class="top-buttons">
                                <asp:Button ID="btnAddGridRow" CssClass="btn btn-info pull-right" Text="Agregar Escala" OnClick="btnAddGridRow_Click" runat="server" />
                            </div>

                            <asp:GridView ID="gvEscalas" CssClass="table table-striped table-bordered table-hover inline-edit"
                                Width="100%" AutoGenerateColumns="False" runat="server" OnRowDeleting="gvEscalas_RowDeleting"
                                EmptyDataText="Sin resultados">
                                <Columns>

                                    <%-- IdEscala --%>
                                    <asp:TemplateField HeaderText="IdEscala" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="lblIdEscala" CssClass="form-control-static" Width="100%" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Descripcion --%>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtDescripcion" CssClass="form-control text-capital" MaxLength="50" Width="100%" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDescripcion" ErrorMessage=""
                                                CssClass="required-field-validator"
                                                SetFocusOnError="True" runat="server">
                                            </asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Observacion --%>
                                    <asp:TemplateField HeaderText="Observación">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtObservacion" CssClass="form-control" MaxLength="500" Width="100%" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- CantInferior --%>
                                    <asp:TemplateField HeaderText="Cant. Inferior">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtCantInferior" CssClass="form-control numeric-integer" MaxLength="3" Width="100%" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtCantInferior" ErrorMessage=""
                                                CssClass="required-field-validator"
                                                SetFocusOnError="True" runat="server">
                                            </asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- CantSuperior --%>
                                    <asp:TemplateField HeaderText="Cant. Superior">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtCantSuperior" CssClass="form-control numeric-integer" MaxLength="3" Width="100%" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtCantSuperior" ErrorMessage=""
                                                CssClass="required-field-validator"
                                                SetFocusOnError="True" runat="server">
                                            </asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Moneda --%>
                                    <asp:TemplateField HeaderText="Moneda">
                                        <ItemTemplate>

                                            <asp:DropDownList ID="ddlMoneda" CssClass="form-control" Width="100%" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Precio --%>
                                    <asp:TemplateField HeaderText="Precio">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtPrecio" CssClass="form-control numeric-decimal" MaxLength="8" Width="100%" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtPrecio" ErrorMessage=""
                                                CssClass="required-field-validator"
                                                SetFocusOnError="True" runat="server">
                                            </asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Eliminar --%>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="http://www.haotu.net/up/4234/24/212-bin.png" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddGridRow" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <br />

                    <asp:Button ID="btnSaveGrid" CssClass="btn btn-default" runat="server" Text="Save Data" OnClick="btnSaveGrid_Click" />
                </div>
            </div>

            <%-- Separador vertical --%>
            <hr />

            <div class="bottom-buttons">

                <%-- Botón _Grid_ --%>
                <div class="btn-models">
                    <a href="_Grid_.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>