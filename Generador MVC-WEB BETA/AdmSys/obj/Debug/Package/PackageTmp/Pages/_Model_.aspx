﻿<%@ Page Title="_Model_" Language="C#" MasterPageFile="~/Pages/Page.Master" AutoEventWireup="true" CodeBehind="_Model_.aspx.cs" Inherits="Pages._Model_" %>

<%@ Register Src="~/Controls/WebUserControl1.ascx" TagPrefix="uc" TagName="WebUserControl1" %>


<%-- Head --%>
<asp:Content ID="cHead" ContentPlaceHolderID="cphHead" runat="server">

    <%-- CSSs ----------------------------------------------------------------------------------------%>

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Evento click sobre una pestaña
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // Pestaña seleccionada
                var clickedTab = $(e.target).attr("href")
                // Guarda la pestaña seleccionada
                document.getElementById('<%= hdnTab.ClientID %>').value = clickedTab;
            });

            // Enfoca la pestaña seleccionada
            var storedTab = document.getElementById('<%= hdnTab.ClientID %>').value;
            $('#tabs a[href="' + storedTab + '"]').tab('show');

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Solicita confirmación antes de eliminar
        function ConfirmDelete() {
            swal({
                title: "¿Eliminar el _Model_?",
                text: "Confirma la eliminación",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#ac2925",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },
            function () {
                __doPostBack('<%= btnDelete.UniqueID %>', "");
            });
        }

    </script>
</asp:Content>

<%-- Body --%>
<asp:Content ID="cBody" ContentPlaceHolderID="cphBody" runat="server">

    <%-- Hash del _Model_ --%>
    <asp:HiddenField ID="hdnHash" runat="server"></asp:HiddenField>

    <%-- Recuerda la pestaña seleccionada --%>
    <asp:HiddenField ID="hdnTab" runat="server" Value="" />

    <%-- Encabezado --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Título del formulario --%>
            <h3 class="page-header">

                <%-- Botón Actualizar --%>
                <a href="" class="btn btn-default pull-left">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </a>

                <%-- Titulo --%>
                <asp:Label ID="lblTitulo" Text="" runat="server" />

                <%-- Botón Eliminar --%>
                <asp:Button ID="btnDelete" Text="Dar de baja" CssClass="btn btn-danger pull-right" runat="server"
                    OnClientClick="ConfirmDelete(); return false;" OnClick="btnDelete_Click" />

                <%-- Separador horizontal --%>
                <span class="pull-right">&nbsp;</span>

                <%-- Botón Editar --%>
                <asp:Button ID="btnEdit" Text="Editar" CssClass="btn btn-warning pull-right" runat="server"
                    OnClick="btnEdit_Click" />
            </h3>

        </div>
    </div>

    <%-- Contenido --%>
    <div class="row">
        <div class="col-lg-12">

            <%-- Panel de controles del formulario --%>
            <div class="panel-body">
                <div class="row">

                    <%-- User Control --%>
                    <div class="row text-center">
                        <label>User Control</label>
                        <br />
                        <uc:WebUserControl1 runat="server" ID="webUserControl1" />
                    </div>

                    <br />

                    <%-- Mensajes a la User Interface desde el Code Bahind --%>
                    <div class="row text-center">
                        <div class="form-group">
                            <label>
                                <asp:Label Text="Mensajes de ejemplo" runat="server" />
                            </label>
                            <div>
                                <asp:Button ID="btnInfo" Text="Info" CssClass="btn btn-info" runat="server" OnClick="btnInfo_Click" />
                                <asp:Button ID="btnSuccess" Text="Success" CssClass="btn btn-success" runat="server" OnClick="btnSuccess_Click" />
                                <asp:Button ID="btnWarning" Text="Warning" CssClass="btn btn-warning" runat="server" OnClick="btnWarning_Click" />
                                <asp:Button ID="btnError" Text="Error" CssClass="btn btn-danger" runat="server" OnClick="btnError_Click" />
                            </div>
                        </div>
                    </div>

                    <!-- Nav tabs -->
                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#text" aria-controls="home" role="tab" data-toggle="tab">Text</a></li>
                        <li role="presentation"><a href="#numeric" aria-controls="numeric" role="tab" data-toggle="tab">Numeric</a></li>
                        <li role="presentation"><a href="#select" aria-controls="select" role="tab" data-toggle="tab">Select, Date & Time</a></li>
                        <li role="presentation"><a href="#check" aria-controls="check" role="tab" data-toggle="tab">Check</a></li>
                        <li role="presentation"><a href="#radio" aria-controls="radio" role="tab" data-toggle="tab">Radio</a></li>
                        <li role="presentation"><a href="#misc" aria-controls="misc" role="tab" data-toggle="tab">Misc</a></li>
                        <li role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <%-- Text --%>
                        <div role="tabpanel" class="tab-pane active" id="text">
                            <br />
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Nombre (con ejemplo de validación)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtNombre" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Texto Capital (.text-capital)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDescripcion" CssClass="form-control text-capital" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Texto MAYUSCULAS (.text-upper)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control text-upper" runat="server" />

                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Texto minusculas (.text-lower)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control text-lower" runat="server" />

                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Area" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>

                        </div>

                        <%-- Numeric --%>
                        <div role="tabpanel" class="tab-pane " id="numeric">
                            <br />

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero Only (.numeric-only)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-only" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero entero (.numeric-integer)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtEntero" CssClass="form-control numeric-integer" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtEntero" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero entero positivo (.numeric-integer-positive)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-integer-positive" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero entero negativo (.numeric-integer-negative)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-integer-negative" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero decimal (.numeric-decimal)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtDecimal" CssClass="form-control numeric-decimal" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtDecimal" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero decimal positivo (.numeric-decimal-positive)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-decimal-positive" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero decimal negativo (.numeric-decimal-negative)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-decimal-negative" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numerico moneda (.numeric-money)" runat="server" />
                                </label>
                                <asp:TextBox ID="txtMoneda" CssClass="form-control numeric-money" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtMoneda" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero moneda positivo (.numeric-money-positive)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-money-positive" runat="server" />
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Numero moneda negativa (.numeric-money-nagative)" runat="server" />
                                </label>
                                <asp:TextBox CssClass="form-control numeric-money-negative" runat="server" />
                            </div>
                        </div>

                        <%-- Select, Date & Time --%>
                        <div role="tabpanel" class="tab-pane " id="select">
                            <br />

                            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                <%-- Select --%>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <label>
                                        <asp:Label Text="Select (.select-single)" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlSelect" CssClass="form-control select-single" runat="server" />
                                    <asp:RequiredFieldValidator ControlToValidate="ddlSelect" InitialValue="-1" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                </div>

                                <%-- Multiple --%>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <label>
                                        <asp:Label Text="Multiple (.select-multiple)" runat="server" />
                                    </label>
                                    <asp:ListBox ID="lstMultiple" CssClass="form-control select-multiple" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>

                                <%-- Select AJAX --%>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <label>
                                        <asp:Label Text="Select AJAX (.select-ajax)" runat="server" />
                                    </label>
                                    <asp:DropDownList ID="ddlSelectAjax" CssClass="form-control select-ajax" runat="server" />
                                </div>


                            </div>

                            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                <%-- Date --%>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <label>
                                        <asp:Label Text="Fecha (.date-picker)" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtFecha" CssClass="form-control date-picker" runat="server" />
                                </div>

                                <%-- Time --%>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <label>
                                        <asp:Label Text="Hora (.time-picker)" runat="server" />
                                    </label>
                                    <asp:TextBox ID="txtHora" CssClass="form-control time-picker" runat="server" />
                                </div>

                            </div>

                        </div>

                        <%-- Checkboxes --%>
                        <div role="tabpanel" class="tab-pane " id="check">
                            <br />

                            <%-- Verticales --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Checkboxes verticales" runat="server" />
                                </label>

                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox runat="server" />
                                        <asp:Label Text="Chk 1" runat="server" />
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox runat="server" />
                                        <asp:Label Text="Chk 2" runat="server" />
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox runat="server" Enabled="False" />
                                        <asp:Label Text="Chk 3" runat="server" />
                                    </label>
                                </div>

                            </div>

                            <%-- Horizontales --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Checkboxes horizontales" runat="server" />
                                </label>
                                <br />

                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" />
                                    <asp:Label Text="Chk 1" runat="server" />
                                </label>
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" />
                                    <asp:Label Text="Chk 2" runat="server" />
                                </label>
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" Enabled="false" />
                                    <asp:Label Text="Chk 3" runat="server" />
                                </label>

                            </div>
                        </div>

                        <%-- Radios --%>
                        <div role="tabpanel" class="tab-pane " id="radio">
                            <br />

                            <%-- Verticales --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Radios verticales" runat="server" />
                                </label>

                                <div class="radio">
                                    <label>
                                        <asp:RadioButton runat="server" GroupName="Verticales" Checked="True" />
                                        <asp:Label Text="Opt 1" runat="server" />
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <asp:RadioButton runat="server" GroupName="Verticales" />
                                        <asp:Label Text="Opt 2" runat="server" />
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <asp:RadioButton runat="server" GroupName="Verticales" Enabled="False" />
                                        <asp:Label Text="Opt 3" runat="server" />
                                    </label>
                                </div>

                            </div>

                            <%-- Horizontales --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Radios horizontales" runat="server" />
                                </label>
                                <br />

                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" GroupName="Horizontales" Checked="True" />
                                    <asp:Label Text="Opt 1" runat="server" />
                                </label>
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" GroupName="Horizontales" />
                                    <asp:Label Text="Opt 2" runat="server" />
                                </label>
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" GroupName="Horizontales" Enabled="False" />
                                    <asp:Label Text="Opt 3" runat="server" />
                                </label>
                            </div>

                        </div>

                        <%-- Misc --%>
                        <div role="tabpanel" class="tab-pane " id="misc">
                            <br />

                            <%-- Static --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Estatico. Etiqueta larga para probar el recorte cuando excele la columna" runat="server" />
                                </label>
                                <div class="control-div-static">
                                    <asp:Label Text="email@example.com" CssClass="form-control-static" runat="server" />
                                </div>
                            </div>

                            <%-- File --%>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Archivo" runat="server" />
                                </label>
                                <input id="input-1a" type="file" class="form-control file" data-show-preview="false">
                            </div>

                            <%-- Address --%>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <label>
                                    <asp:Label Text="Dirección (.text-address)" runat="server" />
                                </label>
                                <div class="input-group">
                                    <asp:TextBox ID="txtDireccion" CssClass="form-control text-address" runat="server" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#address-modal">
                                            <i class="fa fa-map-marker"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>

                        </div>

                        <%-- Table checkeable --%>
                        <div role="tabpanel" class="tab-pane" id="table">
                            <br />

                            <asp:GridView ID="gv_Models_" CssClass="table table-striped table-bordered table-hover checkable"
                                Width="100%" AutoGenerateColumns="False" runat="server" OnRowDataBound="gv_Models__RowDataBound" EmptyDataText="Sin datos para mostrar.">
                                <Columns>

                                    <%-- Id encriptado --%>
                                    <asp:BoundField HeaderText="Id" DataField="IdModel" />

                                    <%-- Checkbox --%>
                                    <asp:TemplateField HeaderText="Seleccionada" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelected" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- Nombre --%>
                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" ItemStyle-CssClass="" />

                                    <%-- Descripción --%>
                                    <asp:BoundField HeaderText="Descripción" DataField="Descripcion" ItemStyle-CssClass="" />

                                </Columns>
                            </asp:GridView>

                        </div>

                    </div>

                </div>
            </div>

            <div class="bottom-buttons ">

                <%-- Botón _Models_ --%>
                <div class="btn-models">
                    <a href="_Models_.aspx" class="btn btn-info">
                        <asp:Label Text="Volver al listado" runat="server" />
                    </a>
                </div>

                <%-- Botones Guardar y Restablecer --%>
                <div class="btns-reset-save">
                    <button type="reset" class="btn btn-default">
                        <asp:Label Text="Restablecer" runat="server" />
                    </button>
                    <asp:Button ID="btnSave" Text="Guardar" CssClass="btn btn-success" Enabled="False" runat="server" OnClick="btnSave_Click" />
                </div>

            </div>

        </div>
    </div>

    <script>

        $('#cphBody_ddlSelectAjax').select2({
            minimumInputLength: 3,
            theme: "bootstrap",
            ajax: {
                url: '../Handlers/SelectAjax.ashx',
                dataType: 'json',
                delay: 250,
                placeholder: 'Buscando...',
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });

    </script>

</asp:Content>
