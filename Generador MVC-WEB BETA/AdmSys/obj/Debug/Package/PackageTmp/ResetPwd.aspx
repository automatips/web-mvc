﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPwd.aspx.cs" Inherits="ResetPwd" %>

<!DOCTYPE html>

<html lang="es">

<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema Administrativo">

    <title>Sistema Administrativo</title>

    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="Content/images/favicon.png" />

    <%-- CSSs ----------------------------------------------------------------------------------------%>
    <%-- SweetAlert --%>
    <link href="Content/vendor/sweetalert.min.css" rel="stylesheet" />

    <%-- Bootstrap --%>
    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- SweetAlert --%>
    <script src="Scripts/vendor/sweetalert.min.js"></script>
    <%-- Fonts --%>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <%-- Custom --%>
    <link href="Content/site.css" rel="stylesheet" />

    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        #divReCaptcha {
            text-align: -webkit-center;
            margin-bottom: 20px;
        }

        #divEmailSent {
            padding: 10px;
            text-align: -webkit-center;
            margin-top: 14px;
            background-color: whitesmoke;
            border-radius: 4px;
            color: #387338;
        }

        #bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            font-size: xx-small;
            width: 100%;
            color: #337ab7;
            z-index: -999;
        }
    </style>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>
    <%-- reCAPTCHA --%>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <%-- Script - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        function DisableControls() {
            $('#txtEmail').prop('disabled', true);
            $('#btnResetPwd').prop('disabled', true);
        }

    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h5>
                            <asp:Label Text="Obtener una nueva contraseña" runat="server" /></h5>
                    </div>
                    <div class="panel-body">
                        <form role="form" runat="server">
                            <asp:ScriptManager runat="server" />
                            <fieldset>
                                <div class="form-group">
                                    <asp:Label Text="Correo electrónico" runat="server" />
                                    <asp:TextBox ID="txtEmail" type="Email" CssClass="form-control" runat="server" />
                                    <asp:RequiredFieldValidator ControlToValidate="txtEmail" ErrorMessage="Ingrese la dirección de correo electrónico" CssClass="required-field-validator" SetFocusOnError="true" runat="server" />
                                </div>
                                <div id="divReCaptcha" class="form-group">
                                    <div class="g-recaptcha" data-sitekey="6Ldh-UkUAAAAAFq6B9uNW7_LEcV0wmD_3uDLoFC5"></div>
                                    <%--Clave del sitio--%>
                                </div>
                                <asp:Button ID="btnResetPwd" Text="Generar una nueva contraseña" CssClass="btn btn-md btn-success btn-block" runat="server" OnClick="btnResetPwd_OnClick" />
                            </fieldset>
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlEmailSent" runat="server" Visible="false">
                                        <div id="divEmailSent">
                                            <asp:Label Text="Se envió un correo electrónico con la nueva contraseña" ID="lblResult" runat="server" />
                                            <asp:HyperLink NavigateUrl="Apps.aspx" runat="server" Text="Iniciar sesión" />
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnResetPwd" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="centered">
            <div id="bottom" class="text-center">
                <span>©</span>
                <span>2018</span>
                <span>-</span>
                <span>Sistema Administrativo</span>
                <br />
                <small>
                    <asp:Label ID="lblVersion" Text="Versión: ..." runat="server" />
                </small>
            </div>
        </div>
    </div>
</body>
</html>