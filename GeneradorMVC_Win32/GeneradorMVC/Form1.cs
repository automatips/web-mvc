﻿using System;
using System.Windows.Forms;
//
using System.IO;

namespace GeneradorMVC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregarEntidad_Click(object sender, EventArgs e)
        {

        }

        protected void GeneracionCarpetas()
        {
            DirectoryInfo dirInf = new DirectoryInfo(txtCarpeta.Text + "\\Modelo\\");
            if (!dirInf.Exists)
            {
                Directory.CreateDirectory(txtCarpeta.Text + "\\Modelo\\");
            }
            dirInf = new DirectoryInfo(txtCarpeta.Text + "\\Negocio\\");
            if (!dirInf.Exists)
            {
                Directory.CreateDirectory(txtCarpeta.Text + "\\Negocio\\");
            }
            dirInf = new DirectoryInfo(txtCarpeta.Text + "\\Persistencia\\");
            if (!dirInf.Exists)
            {
                Directory.CreateDirectory(txtCarpeta.Text + "\\Persistencia\\");
            }

        }

        protected void GenerarCapas(string vNombre)
        {
            try
            {
                GeneracionCarpetas();
                GenerarBd(vNombre);
                GenerarModelo(vNombre);
                GenerarNegocio(vNombre);
                GenerarPersistencia(vNombre);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: "+ex.Message.ToString(),"Error");
            }
        }
        public void GenerarBd(string vNombre)
        {
            StreamWriter sw = new StreamWriter(txtCarpeta.Text + "\\ScriptBD_" + vNombre + ".sql");
            sw.WriteLine("-- Creacion de tabla");
            sw.WriteLine("CREATE TABLE [dbo].[" + vNombre + "]([Id" + vNombre + "] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_" + vNombre + "] PRIMARY KEY CLUSTERED ([Id" + vNombre + "] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];");
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.WriteLine("-- Sp Insercion");
            sw.WriteLine(
                @"CREATE PROCEDURE [dbo].[PRC_" + vNombre + "Insert]"+
                Environment.NewLine+
                "@Nombre varchar(50),@Descripcion varchar(max) "+
                Environment.NewLine +
                "AS DECLARE @SQLString varchar(MAX); set @SQLString =" +
                Environment.NewLine +
                " 'INSERT INTO '+DB_NAME()+'.dbo." + vNombre + "([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) " +
                Environment.NewLine +
                " VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'" +
                Environment.NewLine +
                " exec sp_sqlexec @SQLString ;"
                );
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.WriteLine("-- Sp Consultar por Id");
            sw.WriteLine("CREATE PROCEDURE [dbo].[PRC_" + vNombre + "Select] @Id" + vNombre + " int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [Id" + vNombre + "], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[" + vNombre + "] WHERE  ([Id" + vNombre + "] = @Id" + vNombre + " OR @Id" + vNombre + " IS NULL);");
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.WriteLine("-- Sp Listar por nombre");
            sw.WriteLine("CREATE PROCEDURE [dbo].[PRC_" + vNombre + "_ListarPorNombre] @nombre varchar(100) as begin select * from " + vNombre + " where Estado = 1 and Nombre like @nombre + '%' end");
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.WriteLine("-- Sp Actualizacion");
            sw.WriteLine(
                @"CREATE PROCEDURE [dbo].[PRC_" + vNombre + "Update] @Id" + vNombre + " int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS" +
                Environment.NewLine +
                "DECLARE @SQLString varchar(MAX);" +
                Environment.NewLine +
                "set @SQLString = 'UPDATE '+DB_NAME()+'.dbo." + vNombre + " SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [Id" + vNombre + "] = '+convert(varchar(50),@Id" + vNombre + ")+';' " +
                Environment.NewLine +
                "exec sp_sqlexec @SQLString"
                );
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.WriteLine("-- Sp Eliminacion");
            sw.WriteLine(
                @"CREATE PROCEDURE [dbo].[PRC_" + vNombre + "Delete] @Id" + vNombre + " int AS " +
                Environment.NewLine +
                "DECLARE @SQLString varchar(MAX);" +
                Environment.NewLine +
                "set @SQLString = 'DELETE FROM   [dbo].[" + vNombre + "] WHERE  [Id" + vNombre + "] = '+convert(varchar(50),@Id" + vNombre + ")+';'" +
                Environment.NewLine +
                "exec sp_sqlexec @SQLString"
                );
            sw.WriteLine(Environment.NewLine);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sw.Dispose();
            sw.Close();
        }
        public void GenerarModelo(string vNombre)
        {
            StreamWriter sw = new StreamWriter(txtCarpeta.Text + "\\Modelo\\" + vNombre + ".cs");
            sw.WriteLine("namespace Modelo");
            sw.WriteLine("{");
            sw.WriteLine("    public class " + vNombre + "");
            sw.WriteLine("    {");
            sw.WriteLine("        private int id;");
            sw.WriteLine("        private string nombre, descripcion;");
            sw.WriteLine("        private DateTime fhAlta, fhBaja;");
            sw.WriteLine("        private int estado;");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public int id" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return id; }");
            sw.WriteLine("            set { id = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public string nombre" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return nombre; }");
            sw.WriteLine("            set { nombre = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public string descripcion" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return descripcion; }");
            sw.WriteLine("            set { descripcion = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public DateTime fhAlta" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return fhAlta; }");
            sw.WriteLine("            set { fhAlta = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public DateTime fhBaja" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return fhBaja; }");
            sw.WriteLine("            set { fhBaja = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("        public int estado" + vNombre + "");
            sw.WriteLine("        {");
            sw.WriteLine("            get { return estado; }");
            sw.WriteLine("            set { estado = value; }");
            sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("    }");
            sw.WriteLine("}");
            sw.Dispose();
            sw.Close();
        }
        public void GenerarNegocio(string vNombre)
        {
            StreamWriter sw = new StreamWriter(txtCarpeta.Text + "\\Negocio\\" + vNombre + "Negocio.cs");
            sw.WriteLine("using System.Data;");
            sw.WriteLine("using Modelo;");
            sw.WriteLine("using Persistencia;");
            sw.WriteLine("namespace Negocio");
            sw.WriteLine("{");
            sw.WriteLine("    public class " + vNombre + "Negocio");
            sw.WriteLine("    {");
            sw.WriteLine("        " + vNombre + "Persistencia _" + vNombre + "Persistencia = new " + vNombre + "Persistencia();");
            sw.WriteLine("        public bool Insertar" + vNombre + "(" + vNombre + " " + vNombre + "Negocio)");
            sw.WriteLine("        {");
            sw.WriteLine("            return _" + vNombre + "Persistencia.Insertar" + vNombre + "(" + vNombre + "Negocio);");
            sw.WriteLine("        }");
            sw.WriteLine("        public bool Actualizar" + vNombre + "(" + vNombre + " " + vNombre + "Negocio)");
            sw.WriteLine("        {");
            sw.WriteLine("            return _" + vNombre + "Persistencia.Actualizar" + vNombre + "(" + vNombre + "Negocio);");
            sw.WriteLine("        }");
            sw.WriteLine("        public bool Eliminar" + vNombre + "(" + vNombre + " " + vNombre + "Negocio)");
            sw.WriteLine("        {");
            sw.WriteLine("            return _" + vNombre + "Persistencia.Eliminar" + vNombre + "(" + vNombre + "Negocio);");
            sw.WriteLine("        }");
            sw.WriteLine("        public DataTable Listar" + vNombre + "s(int Id" + vNombre + ")");
            sw.WriteLine("        {");
            sw.WriteLine("            return _" + vNombre + "Persistencia.Listar" + vNombre + "(Id" + vNombre + ");");
            sw.WriteLine("        }");
            sw.WriteLine("        public " + vNombre + " Consultar" + vNombre + "(int Id" + vNombre + ")");
            sw.WriteLine("        {");
            sw.WriteLine("            return _" + vNombre + "Persistencia.Consultar" + vNombre + "(Id" + vNombre + ");");
            sw.WriteLine("        }");
            sw.WriteLine("    }");
            sw.WriteLine("} ");
            sw.Dispose();
            sw.Close();
        }

        public void GenerarPersistencia(string vNombre)
        {
            StreamWriter sw = new StreamWriter(txtCarpeta.Text + "\\Persistencia\\" + vNombre + "Persistencia.cs");
            sw.WriteLine("using System;");
            sw.WriteLine("using System.Data;");
            sw.WriteLine("using Modelo;");
            sw.WriteLine("using System.Data.SqlClient;");
            sw.WriteLine("namespace Persistencia");
            sw.WriteLine("{");
            sw.WriteLine("    public class " + vNombre + "Persistencia");
            sw.WriteLine("    {");
            sw.WriteLine("        SqlConnection cnx;");
            sw.WriteLine("        " + vNombre + " entidad" + vNombre + " = new " + vNombre + "();");
            sw.WriteLine("        Conexion MiConexi = new Conexion();");
            sw.WriteLine("        SqlCommand cmd = new SqlCommand();");
            sw.WriteLine("        public " + vNombre + "Persistencia()");
            sw.WriteLine("        {");
            sw.WriteLine("            cnx = new SqlConnection(MiConexi.GetConex());");
            sw.WriteLine("        }");

            #region SP Insertar
            sw.WriteLine(Environment.NewLine);
                sw.WriteLine("        public bool Insertar" + vNombre + "(" + vNombre + " entidad" + vNombre + ")");
                sw.WriteLine("        {");
                sw.WriteLine("            cmd.Connection = cnx;");
                sw.WriteLine("            cmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("            cmd.CommandText = \"PRC_" + vNombre + "Insert\";");
                sw.WriteLine("            try");
                sw.WriteLine("            {");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@nombre\", SqlDbType.VarChar, 50));");
                sw.WriteLine("                cmd.Parameters[\"@nombre\"].Value = entidad" + vNombre + ".nombre" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@descripcion\", SqlDbType.Varchar,500));");
                sw.WriteLine("                cmd.Parameters[\"@descripcion\"].Value = entidad" + vNombre + ".descripcion" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cnx.Open();");
                sw.WriteLine("                cmd.ExecuteNonQuery();");
                sw.WriteLine("                return true;");
                sw.WriteLine("            }");
                sw.WriteLine("            catch (SqlException)");
                sw.WriteLine("            {");
                sw.WriteLine("                return false;");
                sw.WriteLine("            }");
                sw.WriteLine("        }");
            sw.WriteLine(Environment.NewLine);
            #endregion

            #region SP Actualizar
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("        public bool Actualizar" + vNombre + "(" + vNombre + " entidad" + vNombre + ")");
                sw.WriteLine("        {");
                sw.WriteLine("            cmd.Connection = cnx;");
                sw.WriteLine("            cmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("            cmd.CommandText = \"PRC_" + vNombre + "Update\";");
                sw.WriteLine("            try");
                sw.WriteLine("            {");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@Id" + vNombre + "\", SqlDbType.Int));");
                sw.WriteLine("                cmd.Parameters[\"@Id" + vNombre + "\"].Value = entidad" + vNombre + ".Id" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@nombre\", SqlDbType.VarChar, 50));");
                sw.WriteLine("                cmd.Parameters[\"@nombre\"].Value = entidad" + vNombre + ".nombre" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@descripcion\", SqlDbType.VarChar, 500));");
                sw.WriteLine("                cmd.Parameters[\"@descripcion\"].Value = entidad" + vNombre + ".descripcion" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@estado\", SqlDbType.Int));");
                sw.WriteLine("                cmd.Parameters[\"@estado\"].Value = entidad" + vNombre + ".estado" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cnx.Open();");
                sw.WriteLine("                cmd.ExecuteNonQuery();");
                sw.WriteLine("                return true;");
                sw.WriteLine("            }");
                sw.WriteLine("            catch (SqlException)");
                sw.WriteLine("            {");
                sw.WriteLine("                return false;");
                sw.WriteLine("            }");
                sw.WriteLine("        }");
                sw.WriteLine(Environment.NewLine);
            #endregion

            #region SP Eliminar
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("        public bool Eliminar" + vNombre + "(" + vNombre + " entidad" + vNombre + ")");
                sw.WriteLine("        {");
                sw.WriteLine("            cmd.Connection = cnx;");
                sw.WriteLine("            cmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("            cmd.CommandText = \"PRC_" + vNombre + "Delete\";");
                sw.WriteLine("            try");
                sw.WriteLine("            {");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@Id" + vNombre + "\", SqlDbType.Int));");
                sw.WriteLine("                cmd.Parameters[\"@Id" + vNombre + "\"].Value = entidad" + vNombre + ".Id" + vNombre + ";");
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine("                cnx.Open();");
                sw.WriteLine("                cmd.ExecuteNonQuery();");
                sw.WriteLine("                return true;");
                sw.WriteLine("            }");
                sw.WriteLine("            catch (SqlException)");
                sw.WriteLine("            {");
                sw.WriteLine("                return false;");
                sw.WriteLine("            }");
                sw.WriteLine("        }");
                sw.WriteLine(Environment.NewLine);
            #endregion

            sw.WriteLine("        public DataTable Listar" + vNombre + "(string parametro)");
            sw.WriteLine("        {");
            sw.WriteLine("            DataSet dts = new DataSet();");
            sw.WriteLine("            try");
            sw.WriteLine("            {");
            sw.WriteLine("                cmd.Connection = cnx;");
            sw.WriteLine("                cmd.CommandType = CommandType.StoredProcedure;");
            sw.WriteLine("                cmd.CommandText = \"PRC_" + vNombre + "_ListarPorNombre\";");
            sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@nombre\", parametro));");
            sw.WriteLine("                SqlDataAdapter miada;");
            sw.WriteLine("                miada = new SqlDataAdapter(cmd);");
            sw.WriteLine("                miada.Fill(dts, \"" + vNombre + "\");");
            sw.WriteLine("            }");
            sw.WriteLine("            catch (SqlException ex)");
            sw.WriteLine("            {");
            sw.WriteLine("                throw new Exception(ex.Message);");
            sw.WriteLine("            }");
            sw.WriteLine("            finally");
            sw.WriteLine("            {");
            sw.WriteLine("                cmd.Parameters.Clear();");
            sw.WriteLine("            }");
            sw.WriteLine("            return (dts.Tables[\"" + vNombre + "\"]);");
            sw.WriteLine("        }");
            sw.WriteLine("        public " + vNombre + " Consultar" + vNombre + "(int Id" + vNombre + ")");
            sw.WriteLine("        {");
            sw.WriteLine("            try");
            sw.WriteLine("            {");
            sw.WriteLine("                SqlDataReader dtr;");
            sw.WriteLine("                cmd.Connection = cnx;");
            sw.WriteLine("                cmd.CommandType = CommandType.StoredProcedure;");
            sw.WriteLine("                cmd.CommandText = \"PRC_" + vNombre + "Select\";");
            sw.WriteLine("                cmd.Parameters.Add(new SqlParameter(\"@codigo\", SqlDbType.VarChar, 10));");
            sw.WriteLine("                cmd.Parameters[\"@codigo\"].Value = codigo;");
            sw.WriteLine("                if (cnx.State == ConnectionState.Closed)");
            sw.WriteLine("                {");
            sw.WriteLine("                    cnx.Open();");
            sw.WriteLine("                }");
            sw.WriteLine("                dtr = cmd.ExecuteReader();");
            sw.WriteLine("                if (dtr.HasRows == true)");
            sw.WriteLine("                {");
            sw.WriteLine("                    dtr.Read();");
            sw.WriteLine("                    entidad" + vNombre + ".codigo" + vNombre + " = Convert.ToString(dtr[0]);");
            sw.WriteLine("                    entidad" + vNombre + ".nombre" + vNombre + " = Convert.ToString(dtr[1]);");
            sw.WriteLine("                }");
            sw.WriteLine("                cnx.Close();");
            sw.WriteLine("                cmd.Parameters.Clear();");
            sw.WriteLine("                return entidad" + vNombre + ";");
            sw.WriteLine("            }");
            sw.WriteLine("            catch (SqlException)");
            sw.WriteLine("            {");
            sw.WriteLine("                throw new Exception();");
            sw.WriteLine("            }");
            sw.WriteLine("            finally");
            sw.WriteLine("            {");
            sw.WriteLine("                if (cnx.State == ConnectionState.Open)");
            sw.WriteLine("                {");
            sw.WriteLine("                    cnx.Close();");
            sw.WriteLine("                }");
            sw.WriteLine("                cmd.Parameters.Clear();");
            sw.WriteLine("            }");
            sw.WriteLine("        }");
            sw.WriteLine("    }");
            sw.WriteLine("} ");
            sw.Close();
        }

        public void muestraError(string error)
        {
            MessageBox.Show(error);
        }

        public void muestraError(string error, Exception ex)
        {
            MessageBox.Show(error + " " + ex.Message);
        }

        private void btnGenerarCodigo_Click_1(object sender, System.EventArgs e)
        {
            foreach(DataGridViewRow registroEntidad in dgvEntidad.Rows)
            {
                string vNombre = registroEntidad.Cells[0].Value.ToString();
                GenerarCapas(vNombre);
            }
            MessageBox.Show("Archivos generados","Listo");
        }

        private void btnAgregarEntidad_Click_1(object sender, System.EventArgs e)
        {
            if (txtNombre.Text != "")
            {
                dgvEntidad.Rows.Add(txtNombre.Text);
                txtNombre.Text = "";
            }
        }

        private void btnExaminar_Click_1(object sender, System.EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            txtCarpeta.Text = folderBrowserDialog1.SelectedPath;
        }
    }
}
