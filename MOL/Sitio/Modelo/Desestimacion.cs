using System;


namespace Modelo
{
    public class Desestimacion
    {
        private int id;
        private string nombre, descripcion;
        private DateTime fhAlta, fhBaja;
        private int estado;


        public int idDesestimacion
        {
            get { return id; }
            set { id = value; }
        }


        public string nombreDesestimacion
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string descripcionDesestimacion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public DateTime fhAltaDesestimacion
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }


        public DateTime fhBajaDesestimacion
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }


        public int estadoDesestimacion
        {
            get { return estado; }
            set { estado = value; }
        }


    }
}
