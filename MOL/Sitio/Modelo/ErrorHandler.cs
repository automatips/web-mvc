﻿namespace Modelo
{
    public partial class ErrorHandler
    {
        public ErrorHandler()
        {
        }

        public string Mensaje { get; set; }

        public string Descripcion { get; set; }
    }
}
