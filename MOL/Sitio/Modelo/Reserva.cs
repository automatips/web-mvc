using System;


namespace Modelo
{
    public class Reserva
    {
        private int id;
        private string nombre, descripcion;
        private DateTime fhAlta, fhBaja;
        private int estado;


        public int idReserva
        {
            get { return id; }
            set { id = value; }
        }


        public string nombreReserva
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string descripcionReserva
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public DateTime fhAltaReserva
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }


        public DateTime fhBajaReserva
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }


        public int estadoReserva
        {
            get { return estado; }
            set { estado = value; }
        }


    }
}
