using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class AbogadoNegocio
    {
        AbogadoPersistencia _AbogadoPersistencia = new AbogadoPersistencia();
        public bool InsertarAbogado(Abogado AbogadoNegocio)
        {
            return _AbogadoPersistencia.InsertarAbogado(AbogadoNegocio);
        }
        public bool ActualizarAbogado(Abogado AbogadoNegocio)
        {
            return _AbogadoPersistencia.ActualizarAbogado(AbogadoNegocio);
        }
        public bool EliminarAbogado(Abogado AbogadoNegocio)
        {
            return _AbogadoPersistencia.EliminarAbogado(AbogadoNegocio);
        }
        public DataTable ListarAbogados(string pFiltro)
        {
            return _AbogadoPersistencia.ListarAbogado(pFiltro);
        }
        public Abogado ConsultarAbogado(int IdAbogado)
        {
            return _AbogadoPersistencia.ConsultarAbogado(IdAbogado);
        }

        public Abogado LoginAbogado(string pUsuario, string pPassword)
        {
            return _AbogadoPersistencia.LoginAbogado(pUsuario, pPassword);
        }
    }
} 
