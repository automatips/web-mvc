using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class DesestimacionNegocio
    {
        DesestimacionPersistencia _DesestimacionPersistencia = new DesestimacionPersistencia();
        public bool InsertarDesestimacion(Desestimacion DesestimacionNegocio)
        {
            return _DesestimacionPersistencia.InsertarDesestimacion(DesestimacionNegocio);
        }
        public bool ActualizarDesestimacion(Desestimacion DesestimacionNegocio)
        {
            return _DesestimacionPersistencia.ActualizarDesestimacion(DesestimacionNegocio);
        }
        public bool EliminarDesestimacion(Desestimacion DesestimacionNegocio)
        {
            return _DesestimacionPersistencia.EliminarDesestimacion(DesestimacionNegocio);
        }
        public DataTable ListarDesestimacions(string pFiltro)
        {
            return _DesestimacionPersistencia.ListarDesestimacion(pFiltro);
        }
        public Desestimacion ConsultarDesestimacion(int IdDesestimacion)
        {
            return _DesestimacionPersistencia.ConsultarDesestimacion(IdDesestimacion);
        }
    }
} 
