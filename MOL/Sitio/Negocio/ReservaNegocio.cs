using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class ReservaNegocio
    {
        ReservaPersistencia _ReservaPersistencia = new ReservaPersistencia();
        public bool InsertarReserva(Reserva ReservaNegocio)
        {
            return _ReservaPersistencia.InsertarReserva(ReservaNegocio);
        }
        public bool ActualizarReserva(Reserva ReservaNegocio)
        {
            return _ReservaPersistencia.ActualizarReserva(ReservaNegocio);
        }
        public bool EliminarReserva(Reserva ReservaNegocio)
        {
            return _ReservaPersistencia.EliminarReserva(ReservaNegocio);
        }
        public DataTable ListarReservas(string pFiltro)
        {
            return _ReservaPersistencia.ListarReserva(pFiltro);
        }
        public Reserva ConsultarReserva(int IdReserva)
        {
            return _ReservaPersistencia.ConsultarReserva(IdReserva);
        }
    }
} 
