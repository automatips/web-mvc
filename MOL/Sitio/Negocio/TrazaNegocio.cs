using System.Data;
using Modelo;
using Persistencia;
namespace Negocio
{
    public class TrazaNegocio
    {
        TrazaPersistencia _TrazaPersistencia = new TrazaPersistencia();
        public bool InsertarTraza(Traza TrazaNegocio)
        {
            return _TrazaPersistencia.InsertarTraza(TrazaNegocio);
        }
        public bool ActualizarTraza(Traza TrazaNegocio)
        {
            return _TrazaPersistencia.ActualizarTraza(TrazaNegocio);
        }
        public bool EliminarTraza(Traza TrazaNegocio)
        {
            return _TrazaPersistencia.EliminarTraza(TrazaNegocio);
        }
        public DataTable ListarTrazas(string pFiltro)
        {
            return _TrazaPersistencia.ListarTraza(pFiltro);
        }
        public Traza ConsultarTraza(int IdTraza)
        {
            return _TrazaPersistencia.ConsultarTraza(IdTraza);
        }

        public DataTable ObtenerValidacionesEmail(string pEmail, string pIp)
        {
            return _TrazaPersistencia.ObtenerValidacionesEmail(pEmail, pIp);
        }
    }
} 
