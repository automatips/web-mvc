using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class AbogadoPersistencia
    {
        SqlConnection cnx;
        Abogado entidadAbogado = new Abogado();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public AbogadoPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarAbogado(Abogado entidadAbogado)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_AbogadoInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@Nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@Nombre"].Value = entidadAbogado.nombreAbogado;

                cmd.Parameters.Add(new SqlParameter("@Telefono", SqlDbType.VarChar, 50));
                cmd.Parameters["@Telefono"].Value = entidadAbogado.telefonoAbogado;

                cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar, 50));
                cmd.Parameters["@Email"].Value = entidadAbogado.emailAbogado;

                cmd.Parameters.Add(new SqlParameter("@Direccion", SqlDbType.VarChar, 50));
                cmd.Parameters["@Direccion"].Value = entidadAbogado.direccionAbogado;

                cmd.Parameters.Add(new SqlParameter("@Localidad", SqlDbType.VarChar, 50));
                cmd.Parameters["@Localidad"].Value = entidadAbogado.localidadAbogado;

                cmd.Parameters.Add(new SqlParameter("@Provincia", SqlDbType.VarChar, 50));
                cmd.Parameters["@Provincia"].Value = entidadAbogado.provinciaAbogado;

                cmd.Parameters.Add(new SqlParameter("@Observaciones", SqlDbType.VarChar, 50));
                cmd.Parameters["@Observaciones"].Value = entidadAbogado.otrosDatosAbogado;

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarAbogado(Abogado entidadAbogado)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_AbogadoUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdAbogado", SqlDbType.Int));
                cmd.Parameters["@IdAbogado"].Value = entidadAbogado.idAbogado;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadAbogado.nombreAbogado;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadAbogado.descripcionAbogado;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadAbogado.estadoAbogado;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarAbogado(Abogado entidadAbogado)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_AbogadoDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdAbogado", SqlDbType.Int));
                cmd.Parameters["@IdAbogado"].Value = entidadAbogado.idAbogado;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarAbogado(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Abogado_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Abogado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Abogado"]);
        }
        public Abogado ConsultarAbogado(int IdAbogado)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_AbogadoSelect";
                cmd.Parameters.Add(new SqlParameter("@IdAbogado", SqlDbType.Int));
                cmd.Parameters["@IdAbogado"].Value = IdAbogado;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadAbogado.idAbogado = Convert.ToInt32(dtr[0]);
                    entidadAbogado.nombreAbogado = Convert.ToString(dtr[1]);
                    entidadAbogado.descripcionAbogado = Convert.ToString(dtr[2]);
                    entidadAbogado.fhAltaAbogado = Convert.ToDateTime(dtr[3]);
                    entidadAbogado.fhBajaAbogado = Convert.ToDateTime(dtr[4]);
                    entidadAbogado.estadoAbogado = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadAbogado;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }

        public Abogado LoginAbogado(string pUsuario, string pPassword)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Abogado_Login";

                cmd.Parameters.Add(new SqlParameter("@pUsuario", SqlDbType.VarChar, 50));
                cmd.Parameters["@pUsuario"].Value = pUsuario;

                cmd.Parameters.Add(new SqlParameter("@pPassword", SqlDbType.VarChar, 50));
                cmd.Parameters["@pPassword"].Value = pPassword;

                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadAbogado.idAbogado = Convert.ToInt32(dtr[0]);
                    entidadAbogado.nombreAbogado = Convert.ToString(dtr[1]);
                    entidadAbogado.descripcionAbogado = Convert.ToString(dtr[2]);
                    entidadAbogado.fhAltaAbogado = Convert.ToDateTime(dtr[3]);
                    entidadAbogado.estadoAbogado = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadAbogado;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
} 


