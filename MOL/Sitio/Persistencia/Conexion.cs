﻿using System.Configuration;

namespace Persistencia
{
    public class Conexion
    {
        public Conexion()
        {
        }
        public string GetConex()
        {
            string strConex = ConfigurationManager.ConnectionStrings["MOL"].ConnectionString;
            if (object.ReferenceEquals(strConex, string.Empty))
            {
                return string.Empty;
            }
            else
            {
                return strConex;
            }
        }
    }
}