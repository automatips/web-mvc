using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class ConsultaPersistencia
    {
        SqlConnection cnx;
        Consulta entidadConsulta = new Consulta();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public ConsultaPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarConsulta(Consulta entidadConsulta)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ConsultaInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadConsulta.nombreConsulta;

                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar,500));
                cmd.Parameters["@descripcion"].Value = entidadConsulta.descripcionConsulta;

                cmd.Parameters.Add(new SqlParameter("@telefono", SqlDbType.VarChar, 50));
                cmd.Parameters["@telefono"].Value = entidadConsulta.telefonoConsulta;

                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 100));
                cmd.Parameters["@email"].Value = entidadConsulta.emailConsulta;

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarConsulta(Consulta entidadConsulta)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ConsultaUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdConsulta", SqlDbType.Int));
                cmd.Parameters["@IdConsulta"].Value = entidadConsulta.idConsulta;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadConsulta.nombreConsulta;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadConsulta.descripcionConsulta;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadConsulta.estadoConsulta;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarConsulta(Consulta entidadConsulta)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ConsultaDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdConsulta", SqlDbType.Int));
                cmd.Parameters["@IdConsulta"].Value = entidadConsulta.idConsulta;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarConsulta(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Consulta_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Consulta");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Consulta"]);
        }
        public Consulta ConsultarConsulta(int IdConsulta)
        {
            try


            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ConsultaSelect";
                cmd.Parameters.Add(new SqlParameter("@IdConsulta", SqlDbType.Int));
                cmd.Parameters["@IdConsulta"].Value = IdConsulta;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadConsulta.idConsulta = Convert.ToInt32(dtr[0]);
                    entidadConsulta.nombreConsulta = Convert.ToString(dtr[1]);
                    entidadConsulta.descripcionConsulta = Convert.ToString(dtr[2]);
                    entidadConsulta.fhAltaConsulta = Convert.ToDateTime(dtr[3]);
                    entidadConsulta.fhBajaConsulta = Convert.ToDateTime(dtr[4]);
                    entidadConsulta.estadoConsulta = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadConsulta;
            }


            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
} 


