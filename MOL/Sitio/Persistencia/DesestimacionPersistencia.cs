using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class DesestimacionPersistencia
    {
        SqlConnection cnx;
        Desestimacion entidadDesestimacion = new Desestimacion();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public DesestimacionPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarDesestimacion(Desestimacion entidadDesestimacion)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_DesestimacionInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadDesestimacion.nombreDesestimacion;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar,500));
                cmd.Parameters["@descripcion"].Value = entidadDesestimacion.descripcionDesestimacion;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarDesestimacion(Desestimacion entidadDesestimacion)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_DesestimacionUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdDesestimacion", SqlDbType.Int));
                cmd.Parameters["@IdDesestimacion"].Value = entidadDesestimacion.idDesestimacion;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadDesestimacion.nombreDesestimacion;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadDesestimacion.descripcionDesestimacion;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadDesestimacion.estadoDesestimacion;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarDesestimacion(Desestimacion entidadDesestimacion)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_DesestimacionDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdDesestimacion", SqlDbType.Int));
                cmd.Parameters["@IdDesestimacion"].Value = entidadDesestimacion.idDesestimacion;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarDesestimacion(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Desestimacion_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Desestimacion");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Desestimacion"]);
        }
        public Desestimacion ConsultarDesestimacion(int IdDesestimacion)
        {
            try


            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_DesestimacionSelect";
                cmd.Parameters.Add(new SqlParameter("@IdDesestimacion", SqlDbType.Int));
                cmd.Parameters["@IdDesestimacion"].Value = IdDesestimacion;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadDesestimacion.idDesestimacion = Convert.ToInt32(dtr[0]);
                    entidadDesestimacion.nombreDesestimacion = Convert.ToString(dtr[1]);
                    entidadDesestimacion.descripcionDesestimacion = Convert.ToString(dtr[2]);
                    entidadDesestimacion.fhAltaDesestimacion = Convert.ToDateTime(dtr[3]);
                    entidadDesestimacion.fhBajaDesestimacion = Convert.ToDateTime(dtr[4]);
                    entidadDesestimacion.estadoDesestimacion = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadDesestimacion;
            }


            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
} 


