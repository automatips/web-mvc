using System;
using System.Data;
using Modelo;
using System.Data.SqlClient;
namespace Persistencia
{
    public class ReservaPersistencia
    {
        SqlConnection cnx;
        Reserva entidadReserva = new Reserva();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        public ReservaPersistencia()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }


        public bool InsertarReserva(Reserva entidadReserva)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ReservaInsert";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadReserva.nombreReserva;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar,500));
                cmd.Parameters["@descripcion"].Value = entidadReserva.descripcionReserva;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool ActualizarReserva(Reserva entidadReserva)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ReservaUpdate";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdReserva", SqlDbType.Int));
                cmd.Parameters["@IdReserva"].Value = entidadReserva.idReserva;
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Value = entidadReserva.nombreReserva;
                cmd.Parameters.Add(new SqlParameter("@descripcion", SqlDbType.VarChar, 500));
                cmd.Parameters["@descripcion"].Value = entidadReserva.descripcionReserva;
                cmd.Parameters.Add(new SqlParameter("@estado", SqlDbType.Int));
                cmd.Parameters["@estado"].Value = entidadReserva.estadoReserva;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public bool EliminarReserva(Reserva entidadReserva)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_ReservaDelete";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@IdReserva", SqlDbType.Int));
                cmd.Parameters["@IdReserva"].Value = entidadReserva.idReserva;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }


        public DataTable ListarReserva(string pFiltro)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_Reserva_ListByName";
                cmd.Parameters.Add(new SqlParameter("@nombre", pFiltro));
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Reserva");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Reserva"]);
        }
        public Reserva ConsultarReserva(int IdReserva)
        {
            try


            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ReservaSelect";
                cmd.Parameters.Add(new SqlParameter("@IdReserva", SqlDbType.Int));
                cmd.Parameters["@IdReserva"].Value = IdReserva;
                if (cmd.Connection.State == ConnectionState.Closed)
                {
                    cmd.Connection.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    entidadReserva.idReserva = Convert.ToInt32(dtr[0]);
                    entidadReserva.nombreReserva = Convert.ToString(dtr[1]);
                    entidadReserva.descripcionReserva = Convert.ToString(dtr[2]);
                    entidadReserva.fhAltaReserva = Convert.ToDateTime(dtr[3]);
                    entidadReserva.fhBajaReserva = Convert.ToDateTime(dtr[4]);
                    entidadReserva.estadoReserva = Convert.ToInt32(dtr[5]);
                }
                cmd.Connection.Close();
                cmd.Parameters.Clear();
                return entidadReserva;
            }


            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
} 


