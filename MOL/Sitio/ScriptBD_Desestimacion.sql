-- Creacion de tabla
CREATE TABLE [dbo].[Desestimacion]([IdDesestimacion] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_Desestimacion] PRIMARY KEY CLUSTERED ([IdDesestimacion] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


-- Sp Insercion
CREATE PROCEDURE [PRC_DesestimacionInsert]
@Nombre varchar(50),@Descripcion varchar(max) 
AS DECLARE @SQLString varchar(MAX); set @SQLString =
 'INSERT INTO '+DB_NAME()+'.dbo.Desestimacion([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) 
 VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'
 exec sp_sqlexec @SQLString ;


-- Sp Consultar por Id
CREATE PROCEDURE [PRC_DesestimacionSelect] @IdDesestimacion int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [IdDesestimacion], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[Desestimacion] WHERE  ([IdDesestimacion] = @IdDesestimacion OR @IdDesestimacion IS NULL AND FHBaja is NULL);


-- Sp Listar por nombre
CREATE PROCEDURE [PRC_Desestimacion_ListByName] @nombre varchar(50) as begin select * from Desestimacion where FHBaja IS NULL and Nombre like @nombre + '%' end


-- Sp Actualizacion
CREATE PROCEDURE [PRC_DesestimacionUpdate] @IdDesestimacion int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS
DECLARE @SQLString varchar(MAX);
set @SQLString = 'UPDATE '+DB_NAME()+'.dbo.Desestimacion SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [IdDesestimacion] = '+convert(varchar(50),@IdDesestimacion)+';' 
exec sp_sqlexec @SQLString


-- Sp Eliminacion
CREATE PROCEDURE [PRC_DesestimacionDelete] @IdDesestimacion int AS 
DECLARE @SQLString varchar(MAX);
set @SQLString = 'DELETE FROM   [dbo].[Desestimacion] WHERE  [IdDesestimacion] = '+convert(varchar(50),@IdDesestimacion)+';'
exec sp_sqlexec @SQLString


