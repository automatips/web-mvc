-- Creacion de tabla
CREATE TABLE [dbo].[Error]([IdError] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED ([IdError] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


-- Sp Insercion
CREATE PROCEDURE [PRC_ErrorInsert]
@Nombre varchar(50),@Descripcion varchar(max) 
AS DECLARE @SQLString varchar(MAX); set @SQLString =
 'INSERT INTO '+DB_NAME()+'.dbo.Error([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) 
 VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'
 exec sp_sqlexec @SQLString ;


-- Sp Consultar por Id
CREATE PROCEDURE [PRC_ErrorSelect] @IdError int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [IdError], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[Error] WHERE  ([IdError] = @IdError OR @IdError IS NULL AND FHBaja is NULL);


-- Sp Listar por nombre
CREATE PROCEDURE [PRC_Error_ListByName] @nombre varchar(50) as begin select * from Error where FHBaja IS NULL and Nombre like @nombre + '%' end


-- Sp Actualizacion
CREATE PROCEDURE [PRC_ErrorUpdate] @IdError int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS
DECLARE @SQLString varchar(MAX);
set @SQLString = 'UPDATE '+DB_NAME()+'.dbo.Error SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [IdError] = '+convert(varchar(50),@IdError)+';' 
exec sp_sqlexec @SQLString


-- Sp Eliminacion
CREATE PROCEDURE [PRC_ErrorDelete] @IdError int AS 
DECLARE @SQLString varchar(MAX);
set @SQLString = 'DELETE FROM   [dbo].[Error] WHERE  [IdError] = '+convert(varchar(50),@IdError)+';'
exec sp_sqlexec @SQLString


