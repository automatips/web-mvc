-- Creacion de tabla
CREATE TABLE [dbo].[Reserva]([IdReserva] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_Reserva] PRIMARY KEY CLUSTERED ([IdReserva] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


-- Sp Insercion
CREATE PROCEDURE [PRC_ReservaInsert]
@Nombre varchar(50),@Descripcion varchar(max) 
AS DECLARE @SQLString varchar(MAX); set @SQLString =
 'INSERT INTO '+DB_NAME()+'.dbo.Reserva([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) 
 VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'
 exec sp_sqlexec @SQLString ;


-- Sp Consultar por Id
CREATE PROCEDURE [PRC_ReservaSelect] @IdReserva int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [IdReserva], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[Reserva] WHERE  ([IdReserva] = @IdReserva OR @IdReserva IS NULL AND FHBaja is NULL);


-- Sp Listar por nombre
CREATE PROCEDURE [PRC_Reserva_ListByName] @nombre varchar(50) as begin select * from Reserva where FHBaja IS NULL and Nombre like @nombre + '%' end


-- Sp Actualizacion
CREATE PROCEDURE [PRC_ReservaUpdate] @IdReserva int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS
DECLARE @SQLString varchar(MAX);
set @SQLString = 'UPDATE '+DB_NAME()+'.dbo.Reserva SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [IdReserva] = '+convert(varchar(50),@IdReserva)+';' 
exec sp_sqlexec @SQLString


-- Sp Eliminacion
CREATE PROCEDURE [PRC_ReservaDelete] @IdReserva int AS 
DECLARE @SQLString varchar(MAX);
set @SQLString = 'DELETE FROM   [dbo].[Reserva] WHERE  [IdReserva] = '+convert(varchar(50),@IdReserva)+';'
exec sp_sqlexec @SQLString


