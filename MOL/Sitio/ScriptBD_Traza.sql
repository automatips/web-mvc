-- Creacion de tabla
CREATE TABLE [dbo].[Traza]([IdTraza] [int] IDENTITY(1,1) NOT NULL,[Nombre] [varchar](50) NOT NULL,[Descripcion] [varchar](max) NOT NULL,[FHAlta] [datetime] NOT NULL,[FHBaja] [datetime] NULL,[Estado] [int] NOT NULL, CONSTRAINT [PK_Traza] PRIMARY KEY CLUSTERED ([IdTraza] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


-- Sp Insercion
CREATE PROCEDURE [PRC_TrazaInsert]
@Nombre varchar(50),@Descripcion varchar(max) 
AS DECLARE @SQLString varchar(MAX); set @SQLString =
 'INSERT INTO '+DB_NAME()+'.dbo.Traza([Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado]) 
 VALUES ('''+@Nombre+''', '''+@Descripcion+''', GETDATE(), null, 1);'
 exec sp_sqlexec @SQLString ;


-- Sp Consultar por Id
CREATE PROCEDURE [PRC_TrazaSelect] @IdTraza int AS SET NOCOUNT ON SET XACT_ABORT ON BEGIN TRAN SELECT [IdTraza], [Nombre], [Descripcion], [FHAlta], [FHBaja], [Estado] FROM   [dbo].[Traza] WHERE  ([IdTraza] = @IdTraza OR @IdTraza IS NULL AND FHBaja is NULL);


-- Sp Listar por nombre
CREATE PROCEDURE [PRC_Traza_ListByName] @nombre varchar(50) as begin select * from Traza where FHBaja IS NULL and Nombre like @nombre + '%' end


-- Sp Actualizacion
CREATE PROCEDURE [PRC_TrazaUpdate] @IdTraza int, @Nombre varchar(50), @Descripcion varchar(max), @Estado int AS
DECLARE @SQLString varchar(MAX);
set @SQLString = 'UPDATE '+DB_NAME()+'.dbo.Traza SET Nombre ='''+@Nombre+''', Descripcion ='''+@Descripcion+''', Estado = 1 WHERE  [IdTraza] = '+convert(varchar(50),@IdTraza)+';' 
exec sp_sqlexec @SQLString


-- Sp Eliminacion
CREATE PROCEDURE [PRC_TrazaDelete] @IdTraza int AS 
DECLARE @SQLString varchar(MAX);
set @SQLString = 'DELETE FROM   [dbo].[Traza] WHERE  [IdTraza] = '+convert(varchar(50),@IdTraza)+';'
exec sp_sqlexec @SQLString


