﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Negocio;

namespace mejoropcionlegal
{
    public partial class Abogado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idAbogado"] == null || Session["nombreAbogado"] == null)
            {
                TrazarVisita();
            }
            else
            {
                Response.Redirect("Consultas.aspx");
            }
        }

        protected void TrazarVisita()
        {
            //INICIO Datos de Traza
            string pDescripcion = "Nueva Visita. Abogado.aspx;";
            UCTraza1.InsertarTraza(pDescripcion);
            //FIN Datos de Traza
        }

        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            string pDescripcionTraza = "";
            pDescripcionTraza += "LOGIN DE ABOGADO. ";

            if (txtUsuario.Text != string.Empty && txtPassword.Text != string.Empty)
            {
                Modelo.Abogado abogadoLogin = null;
                Negocio.AbogadoNegocio abogadoNegocio = new AbogadoNegocio();
                abogadoLogin = abogadoNegocio.LoginAbogado(txtUsuario.Text, txtPassword.Text);
                pDescripcionTraza += "Usuario: " + txtUsuario.Text + ";";
                pDescripcionTraza += "Password: " + txtPassword.Text + ";";
                if (abogadoLogin.idAbogado > 0)
                {
                    Session.Add("idAbogado", abogadoLogin.idAbogado.ToString());
                    Session.Add("nombreAbogado", abogadoLogin.nombreAbogado);
                    pDescripcionTraza += "idAbogado: " + abogadoLogin.idAbogado.ToString() + ";";
                    pDescripcionTraza += "nombreAbogado: " + abogadoLogin.nombreAbogado + ";";
                    UCTraza1.InsertarTraza(pDescripcionTraza);
                    Response.Redirect("Consultas.aspx");
                }
                else
                {
                    pDescripcionTraza += "Usuario: " + txtUsuario.Text + ";";
                    pDescripcionTraza += "Password: " + txtPassword.Text + ";";
                    pDescripcionTraza += "USUARIO INCORRECTO!;";
                    UCTraza1.InsertarTraza(pDescripcionTraza);
                    lblMensaje.Text = "USUARIO INCORRECTO!";
                }
            }
            else
            {
                pDescripcionTraza += "USUARIO VACÍO!;";
                UCTraza1.InsertarTraza(pDescripcionTraza);
                lblMensaje.Text = "USUARIO VACÍO!";
            }          
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            Response.Redirect("Registro.aspx");
        }
    }
}