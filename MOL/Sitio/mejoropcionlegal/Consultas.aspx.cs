﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Negocio;
using Modelo;

namespace mejoropcionlegal
{
    public partial class Consultas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idAbogado"] == null || Session["nombreAbogado"] == null)
            {
                Response.Redirect("Abogado.aspx");
            }
            else
            {
                string pDescripcionTraza = "Listado de consultas. ";
                pDescripcionTraza += "idAbogado: "+Session["idAbogado"].ToString()+";";
                pDescripcionTraza += "nombreAbogado: " + Session["nombreAbogado"].ToString() + ";";
                UCTraza1.InsertarTraza(pDescripcionTraza);
                lblNombreAbogado.Text = "Abogado conectado: "+ Session["nombreAbogado"].ToString();
                cargarConsultas();
            }            
        }

        protected void cargarConsultas()
        {
            ConsultaNegocio consultaNegocio = new ConsultaNegocio();
            gvConsultas.DataSource = consultaNegocio.ListarConsultas("");
            gvConsultas.DataBind();
        }

        protected void gvConsultas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvConsultas.PageIndex = e.NewPageIndex;
                gvConsultas.DataBind();
            }
            catch (Exception)
            {
            }
        }
    }
}