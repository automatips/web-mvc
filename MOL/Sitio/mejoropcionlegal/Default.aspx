﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mejoropcionlegal.Default" %>
<%@ Register Src="~/Controls/UCMenu.ascx" TagPrefix="uc" TagName="UCMenu" %>
<%@ Register Src="~/Controls/UCConsulta.ascx" TagPrefix="uc" TagName="UCConsulta" %>
<%@ Register Src="~/Controls/UCServicios.ascx" TagPrefix="uc" TagName="UCServicios" %>
<%@ Register Src="~/Controls/UCFooter.ascx" TagPrefix="uc" TagName="UCFooter" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Mejor Opcion Legal</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" />

</head>

<body>
  
  <uc:UCMenu runat="server" ID="UCMenu1" />

  <uc:UCConsulta runat="server" ID="UCConsulta1" />

  <uc:UCServicios runat="server" ID="UCServicios1" />

  <uc:UCFooter runat="server" ID="UCFooter1" />

  <uc:UCTraza runat="server" ID="UCTraza1" />

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>

</body>

</html>

