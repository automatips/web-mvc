﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mejoropcionlegal
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TrazarVisita();
            }            
        }

        protected void TrazarVisita()
        {
            //INICIO Datos de Traza
            string pDescripcion = "Nueva Visita. Default.aspx;";
            UCTraza1.InsertarTraza(pDescripcion);
            //FIN Datos de Traza
        }
    }
}