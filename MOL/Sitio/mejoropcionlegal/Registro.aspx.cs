﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Negocio;

namespace mejoropcionlegal
{
    public partial class Registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistroAbogado_Click(object sender, EventArgs e)
        {
            registrarAbogado();
        }

        protected void registrarAbogado()
        {
            string pDescripcionTraza = "";
            try
            {
                if (
                    txtNombreApellido.Text != string.Empty &&
                    txtTelefono.Text != string.Empty &&
                    txtEmail.Text != string.Empty &&
                    txtDireccion.Text != string.Empty &&
                    txtLocalidad.Text != string.Empty &&
                    txtProvincia.Text != string.Empty
                    )
                {
                    ErrorHandler eh = new ErrorHandler();
                    eh = UCEnvioMail1.validacionesSeguridad(txtEmail.Text, UCTraza1.ObtenerIp());

                    if (eh.Mensaje == "EXITO")
                    {
                        Modelo.Abogado abogadoRegistro = new Modelo.Abogado();
                        abogadoRegistro.nombreAbogado = txtNombreApellido.Text;
                        abogadoRegistro.telefonoAbogado = txtTelefono.Text;
                        abogadoRegistro.emailAbogado = txtEmail.Text;
                        abogadoRegistro.direccionAbogado = txtDireccion.Text;
                        abogadoRegistro.localidadAbogado = txtLocalidad.Text;
                        abogadoRegistro.provinciaAbogado = txtProvincia.Text;
                        abogadoRegistro.otrosDatosAbogado = txtOtrosDatos.Text;

                        Negocio.AbogadoNegocio abogadoNegocio = new AbogadoNegocio();
                        abogadoNegocio.InsertarAbogado(abogadoRegistro);
                        enviarMailRegistroAbogado(abogadoRegistro.emailAbogado);

                        txtNombreApellido.Visible = false;
                        txtTelefono.Visible = false;
                        txtEmail.Visible = false;
                        txtDireccion.Visible = false;
                        txtLocalidad.Visible = false;
                        txtProvincia.Visible = false;
                        txtOtrosDatos.Visible = false;
                        btnRegistroAbogado.Visible = false;

                        lblMensaje.ForeColor = System.Drawing.Color.Green;
                        lblMensaje.Text = "ABOGADO REGISTRADO EXITOSAMENTE!";
                        lblMensaje.BackColor = System.Drawing.Color.Aqua;

                        //INICIO Datos de Traza
                        pDescripcionTraza += "Registro de Abogado. ABOGADO REGISTRADO EXITOSAMENTE;";
                        pDescripcionTraza += "Nombre: " + txtNombreApellido.Text + ";";
                        pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                        pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                        pDescripcionTraza += "Direccion: " + txtDireccion.Text + ";";
                        pDescripcionTraza += "Localidad: " + txtLocalidad.Text + ";";
                        pDescripcionTraza += "Provincia: " + txtProvincia.Text + ";";
                        pDescripcionTraza += "Otros datos: " + txtOtrosDatos.Text + ";";
                        UCTraza1.InsertarTraza(pDescripcionTraza);


                        pDescripcionTraza += Environment.NewLine;
                        pDescripcionTraza += "Su registro será procesado a la brevedad.";

                        UCEnvioMail1.enviarMail
                        (
                            abogadoRegistro.emailAbogado, //Destinatario
                            "Comprobante de registro. Mejor Opcion Legal", //Asunto
                            pDescripcionTraza.Replace(";", Environment.NewLine) //Cuerpo
                        );
                        //FIN Datos de Traza
                    }
                    else
                    {
                        lblMensaje.Text = eh.Mensaje;
                        lblMensaje.BackColor = System.Drawing.Color.Aqua;
                    }

                }
                else
                {
                    lblMensaje.ForeColor = System.Drawing.Color.Red;
                    lblMensaje.Text = "NECESITAS COMPLETAR LOS CAMPOS PARA PROCESAR EL REGISTRO!";

                    //INICIO Datos de Traza
                    pDescripcionTraza += "Registro de Abogado. DATOS INCOMPLETOS;";
                    pDescripcionTraza += "Nombre: " + txtNombreApellido.Text + ";";
                    pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                    pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                    pDescripcionTraza += "Direccion: " + txtDireccion.Text + ";";
                    pDescripcionTraza += "Localidad: " + txtLocalidad.Text + ";";
                    pDescripcionTraza += "Provincia: " + txtProvincia.Text + ";";
                    pDescripcionTraza += "Otros datos: " + txtOtrosDatos.Text + ";";
                    UCTraza1.InsertarTraza(pDescripcionTraza);
                    //FIN Datos de Traza
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error desconocido al procesar la consulta. Reintente nuevamente. ";
                //INICIO Datos de Traza
                pDescripcionTraza += "Error desconocido al procesar la consulta";
                pDescripcionTraza += "Nombre: " + txtNombreApellido.Text + ";";
                pDescripcionTraza += "Telefono: " + txtTelefono.Text + ";";
                pDescripcionTraza += "Email: " + txtEmail.Text + ";";
                pDescripcionTraza += "Direccion: " + txtDireccion.Text + ";";
                pDescripcionTraza += "Localidad: " + txtLocalidad.Text + ";";
                pDescripcionTraza += "Provincia: " + txtProvincia.Text + ";";
                pDescripcionTraza += "Otros datos: " + txtOtrosDatos.Text + ";";
                pDescripcionTraza += "Ex Message: " + ex.Message+";";
                pDescripcionTraza += "Ex StackTrace: " + ex.StackTrace+";";
                UCTraza1.InsertarTraza(pDescripcionTraza);
                //FIN Datos de Traza
            }
        }

        protected void enviarMailRegistroAbogado(string pEmail)
        {

        }
    }
}