﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Abogado.aspx.cs" Inherits="mejoropcionlegal.Abogado" %>
<%@ Register Src="~/Controls/UCMenu.ascx" TagPrefix="uc" TagName="UCMenu" %>
<%@ Register Src="~/Controls/UCFooter.ascx" TagPrefix="uc" TagName="UCFooter" %>
<%@ Register Src="~/Controls/UCServicios.ascx" TagPrefix="uc" TagName="UCServicios" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Mejor Opcion Legal</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" />

</head>

<body>
  <uc:UCMenu runat="server" ID="UCMenu1" />

  <section id="main-slider" class="no-margin">
    <div class="carousel slide">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(images/slider/bg1.jpg)">
          <div class="container">
            <div class="row slide-margin">


              <div class="col-sm-6 animation animated-item-4">
                    <form id="Form1" runat="server">
                        <div class="panel-body">

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <asp:Label ID="lblMensaje" Text="" ForeColor="Red" runat="server" />
                                </label>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="txtUsuario" PlaceHolder="USUARIO" CssClass="form-control" runat="server" TextMode="SingleLine" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="txtPassword" PlaceHolder="PASSWORD" CssClass="form-control" runat="server" TextMode="Password" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Button ID="btnIniciarSesion" Text="INICIAR SESION" CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server" OnClick="btnIniciarSesion_Click"/>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Button ID="btnRegistro" Text="¿No sos miembro? REGISTRATE" CssClass="btn btn-primary col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server" OnClick="btnRegistro_Click"/>
                            </div>

                        </div>
                    </form>
              </div>

            </div>
          </div>
        </div>
        <!--/.item-->
      </div>
      <!--/.carousel-inner-->
    </div>
    <!--/.carousel-->
  </section>
  <!--/#main-slider-->

  <uc:UCServicios runat="server" ID="UCServicios1" />

  <uc:UCFooter runat="server" ID="UCFooter1" />
  <uc:UCTraza runat="server" ID="UCTraza1" />
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>
 
</body>

</html>