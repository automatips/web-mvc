﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCConsulta.ascx.cs" Inherits="mejoropcionlegal.Controls.UCConsulta" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>
<%@ Register Src="~/Controls/UCEnvioMail.ascx" TagPrefix="uc" TagName="UCEnvioMail" %>
<section id="main-slider" class="no-margin">
    <div class="carousel slide">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(images/slider/bg1.jpg)">
          <div class="container">
            <div class="row slide-margin">
              <div class="col-sm-6">
                <div class="carousel-content">
                    <h2 class="animation animated-item-1">Contanos <span>Tu caso</span></h2>
<%--                    <p class="animation animated-item-2">Necesitamos esta información para poder asesorarte de una mejor manera. Cuidaremos tu información</p>
                  --%>
                    <form id="Form1" runat="server">
                        <div class="panel-body">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <asp:Label ID="lblMensaje" Text="" ForeColor="Red" runat="server" />
                                </label>
                                <asp:TextBox ID="txtNombre" PlaceHolder="Ingresa tu nombre y apellido..." CssClass="form-control" runat="server" TextMode="SingleLine" />
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="txtTelefono" PlaceHolder="Ingresa tu teléfono..." CssClass="form-control" runat="server" TextMode="SingleLine" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="txtEmail" PlaceHolder="Ingresa tu correo electrónico..." CssClass="form-control" runat="server" TextMode="SingleLine" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="txtConsulta" PlaceHolder="Contanos tu consulta..." CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Button ID="btnEnviarConsulta" Text="Enviar consulta" CssClass="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server" OnClick="btnEnviarConsulta_Click"/>
                                <asp:Button ID="btnEnviarOtraConsulta" Text="Enviar otra consulta" CssClass="btn btn-primary col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server" Visible="false" OnClick="btnEnviarOtraConsulta_Click"/>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            </div>
                        </div>
                    </form>
                </div>
              </div>

              <div class="col-sm-6 hidden-xs animation animated-item-4">
                <div class="slider-img">
                  <img src="images/slider/img3.png" class="img-responsive">
                </div>
              </div>

            </div>
          </div>
        </div>
        <!--/.item-->
      </div>
      <!--/.carousel-inner-->
    </div>
    <!--/.carousel-->
    <uc:UCTraza runat="server" ID="UCTraza1" />
      <uc:UCEnvioMail runat="server" ID="UCEnvioMail1" />
  </section>
  <!--/#main-slider-->