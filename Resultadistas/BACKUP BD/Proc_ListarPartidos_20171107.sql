set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Proc_ListarPartidos]
	@p_idFecha int,
	@p_idUsuario int,
	@p_modo int
AS
BEGIN
	if @p_modo = 2
		begin
			select * from
			partidos
			where idFecha = @p_idFecha
			and id not in 
			(
				select idPartido
				from RespuestaPartido
				where idUsuario = @p_idUsuario
			)
		end
	else
		begin
			select * from
			partidos
			where idFecha = @p_idFecha
			and id in 
			(
				select idPartido
				from RespuestaPartido
				where idUsuario = @p_idUsuario
			)
		end
END




