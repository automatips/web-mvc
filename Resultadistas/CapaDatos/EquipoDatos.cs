﻿using System;
using System.Data;
using CapaEntidad;
using System.Data.SqlClient;
namespace CapaDatos
{
    public class EquipoDatos
    {
        SqlConnection cnx;
        Equipo equipoEntidad = new Equipo();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public EquipoDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public bool CargarEquipo(Equipo equipoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Proc_CargarEquipo";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@p_IdEquipo", SqlDbType.Int));
                cmd.Parameters["@p_IdEquipo"].Value = equipoEntidad.IdEquipo;

                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = equipoEntidad.NombreEquipo;

                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public bool EliminarEquipo(Equipo equipoEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_EquipoEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = equipoEntidad.IdEquipo;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ListarEquipos(string p_NombreEquipo)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ListarEquipos";
                cmd.Parameters.Add(new SqlParameter("@p_Nombre", SqlDbType.VarChar));
                cmd.Parameters["@p_Nombre"].Value = p_NombreEquipo;
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Equipos");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Equipos"]);
        }

        public Equipo ConsultarEquipo(string codigo)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ConsultarEquipoPorId";
                cmd.Parameters.Add(new SqlParameter("@p_idEquipo", SqlDbType.Int));
                cmd.Parameters["@p_idEquipo"].Value = Convert.ToInt32(codigo);
                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return equipoEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}