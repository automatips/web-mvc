﻿using System;
using System.Data;
using CapaEntidad;
using System.Data.SqlClient;
namespace CapaDatos
{
    public class FechaDatos
    {
        SqlConnection cnx;
        Fecha FechaEntidad = new Fecha();
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public FechaDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }

        public bool CargarFecha(Fecha FechaEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Proc_CargarFecha";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@p_idFecha", SqlDbType.Int));
                cmd.Parameters["@p_idFecha"].Value = FechaEntidad.IdFecha;

                cmd.Parameters.Add(new SqlParameter("@p_nombreFecha", SqlDbType.VarChar));
                cmd.Parameters["@p_nombreFecha"].Value = FechaEntidad.NombreFecha;

                cmd.Parameters.Add(new SqlParameter("@p_fechaInicio", SqlDbType.VarChar));
                cmd.Parameters["@p_fechaInicio"].Value = FechaEntidad.FechaInicioFecha;

                cmd.Parameters.Add(new SqlParameter("@p_fechafin", SqlDbType.VarChar));
                cmd.Parameters["@p_fechafin"].Value = FechaEntidad.FechaFinFecha;

                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public bool EliminarFecha(Fecha FechaEntidad)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "proc_FechaEliminar";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@codigo", SqlDbType.VarChar, 5));
                cmd.Parameters["@codigo"].Value = FechaEntidad.IdFecha;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ListarFechas()
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ListarFechas";
                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Fecha");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Fecha"]);
        }

        public Fecha ConsultarFecha(string codigo)
        {
            try
            {
                SqlDataReader dtr;
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Proc_ConsultarFechaPorId";

                cmd.Parameters.Add(new SqlParameter("@p_idFecha", SqlDbType.Int));
                cmd.Parameters["@p_idFecha"].Value = Convert.ToInt32(codigo);
                if (cnx.State == ConnectionState.Closed)
                {
                    cnx.Open();
                }
                dtr = cmd.ExecuteReader();
                if (dtr.HasRows == true)
                {
                    dtr.Read();
                    FechaEntidad.IdFecha = Convert.ToInt32(dtr[0]);
                    FechaEntidad.NombreFecha = Convert.ToString(dtr[1]);
                    FechaEntidad.FechaInicioFecha = Convert.ToString(dtr[2]);
                    FechaEntidad.FechaFinFecha = Convert.ToString(dtr[3]);
                }
                cnx.Close();
                cmd.Parameters.Clear();
                return FechaEntidad;
            }
            catch (SqlException)
            {
                throw new Exception();
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
        }
    }
}