﻿namespace CapaEntidad
{
    public class Partido
    {
        private Fecha fecha = new Fecha();
        private int id, idEquipo1, idEquipo2, golesEquipo1, golesEquipo2;
        private string fechaHora;

        public Fecha FechaPartido
        {
            get { return fecha; }
            set { fecha = value; }
        }

        public int GolesEquipo2Partido
        {
            get { return golesEquipo2; }
            set { golesEquipo2 = value; }
        }

        public int GolesEquipo1Partido
        {
            get { return golesEquipo1; }
            set { golesEquipo1 = value; }
        }

        public int IdEquipo2Partido
        {
            get { return idEquipo2; }
            set { idEquipo2 = value; }
        }

        public int IdEquipo1Partido
        {
            get { return idEquipo1; }
            set { idEquipo1 = value; }
        }

        public int IdPartido
        {
            get { return id; }
            set { id = value; }
        }


        public string FechaHoraPartido
        {
            get { return fechaHora; }
            set { fechaHora = value; }
        }
    }
}
