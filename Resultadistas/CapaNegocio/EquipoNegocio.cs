﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class EquipoNegocio
    {
        EquipoDatos _EquipoDatos = new EquipoDatos();

        public bool CargarEquipo(Equipo equipoEntidad)
        {
            return _EquipoDatos.CargarEquipo(equipoEntidad);
        }

        public bool EliminarEquipo(Equipo equipoEntidad)
        {
            return _EquipoDatos.EliminarEquipo(equipoEntidad);
        }

        public DataTable ListarEquipos(string p_nombre)
        {
            return _EquipoDatos.ListarEquipos(p_nombre);
        }

        public Equipo ConsultarEquipo(string codigo)
        {
            return _EquipoDatos.ConsultarEquipo(codigo);
        }
    }
}