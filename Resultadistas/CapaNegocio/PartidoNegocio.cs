﻿using System.Data;
using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class PartidoNegocio
    {
        PartidoDatos _PartidoDatos = new PartidoDatos();

        public bool CargarPartido(Partido PartidoEntidad)
        {
            return _PartidoDatos.CargarPartido(PartidoEntidad);
        }

        public bool EliminarPartido(Partido PartidoEntidad)
        {
            return _PartidoDatos.EliminarPartido(PartidoEntidad);
        }

        public DataTable ListarPartidos(int idFecha, int idUsuario, int modo)
        {
            return _PartidoDatos.ListarPartidos(idFecha, idUsuario, modo);
        }

        public Partido ConsultarPartido(string codigo)
        {
            return _PartidoDatos.ConsultarPartido(codigo);
        }
    }
}