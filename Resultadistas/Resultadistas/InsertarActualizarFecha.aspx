﻿<%@ Page Title="" Language="C#" MasterPageFile="~/principal.Master" AutoEventWireup="true" CodeBehind="InsertarActualizarFecha.aspx.cs" Inherits="Resultadistas.InsertarActualizarFecha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="panel panel-default">
    <div class="panel-heading">
    Insertar/Actualizar Fecha
        <asp:Label ID="lblIdUsuario" runat="server" Visible="False"></asp:Label>
&nbsp;</div>
    <div class="panel-body">

    <form id="form1" runat="server" style="">
        <div style="text-align: center;">
            <table border="0" style="margin: 0 auto;">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="Label1" runat="server" Text="Codigo   "></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtCodigo" runat="server" MaxLength="10" CssClass="form-control" Width="619px" placeholder="Codigo" style="margin-right: 0px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtNombre" runat="server" MaxLength="100" CssClass="form-control" Width="619px" placeholder="Nombre" style="margin-right: 0px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombre" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Nombre&#39; no puede ser vacío</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3" rowspan="2">
                        <asp:Label ID="Label18" runat="server" Text="Fecha de inicio"></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtFechaCreacion" runat="server" MaxLength="10" Width="168px" Enabled="False"></asp:TextBox>
                        <asp:Button ID="btnFechaCreacion" runat="server" Text="..." OnClick="btnFechaCreacion_Click" Width="42px" Height="22px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" class="auto-style1">
                        <asp:Calendar ID="calFechaCreacion" runat="server" OnSelectionChanged="calFechaCreacion_SelectionChanged" Visible="False"></asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFechaCreacion" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Fecha de inicio&#39; no puede ser vacío</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3" rowspan="2">
                        <asp:Label ID="Label19" runat="server" Text="Fecha de fin"></asp:Label>
                    </td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtFechaFin" runat="server" MaxLength="10" Width="168px" Enabled="False"></asp:TextBox>
                        <asp:Button ID="btnFechaFin" runat="server" Text="..." OnClick="btnFechaFin_Click" Width="42px" Height="22px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" class="auto-style1">
                        <asp:Calendar ID="calFechaFin" runat="server" OnSelectionChanged="calFechaFin_SelectionChanged" Visible="False"></asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFechaFin" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Fecha de fin&#39; no puede ser vacío</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnGrabar" runat="server" OnClick="btnGrabar_Click" Text="Grabar" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" />
                        <asp:Button ID="btnActualizar" runat="server" OnClick="btnActualizar_Click" Text="Actualizar" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" />
                        <asp:Button ID="btnCancelar" runat="server" OnClick="btnCancelar_Click" Text="Cancelar" CssClass="btn btn-info" />
                        <asp:Button ID="btnSalir" runat="server" OnClick="btnSalir_Click" Text="Salir" CssClass="btn btn-info" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style2">
                        &nbsp;</td>
                </tr>
                </table>
        </div>
 
    </form>

</asp:Content>
