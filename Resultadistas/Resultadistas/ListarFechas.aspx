﻿<%@ Page Title="" Language="C#" MasterPageFile="~/principal.Master" AutoEventWireup="true" CodeBehind="ListarFechas.aspx.cs" Inherits="Resultadistas.ListarFechas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style4 {
            text-align: right;
            width: 209px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="panel panel-default">
    <div class="panel-heading">
        Listar Fechas
        <asp:Label ID="lblIdUsuario" runat="server" Visible ="False"></asp:Label>
    </div>
    <div class="panel-body">

<form id="form1" runat="server" style="">
        <div style="text-align: center;">
            <table border="0" style="margin: 0 auto;">
                <tr>
                    <td style="text-align: left">
                        <asp:Button ID="btnNuevoFecha" runat="server" OnClick="btnNuevoFecha_Click" Text="Nueva Fecha" CssClass="btn btn-info" Visible="False" />
                                </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:GridView ID="GridViewDatos"
                            runat="server" 
                            AllowSorting="true" 
                            AutoGenerateColumns="False" 
                            CellPadding="4"
                            ForeColor="#333333" 
                            GridLines="None"
                            OnRowDeleting="GridViewDatos_RowDeleting"
                            OnRowCommand="GridViewDatos_RowCommand" 
                            AllowPaging="True"
                            OnPageIndexChanging="GridViewDatos_PageIndexChanging" 
                            PageSize="50" Height="69px" Width="1056px" 
                            CssClass="auto-style3" OnSorting="GridViewDatos_Sorting">
                            
                            <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle forecolor="Red" CssClass="table table-bordered" />
                                <emptydatatemplate>
                                    ¡No hay Fechas con los criterios seleccionados!  
                                </emptydatatemplate> 

                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <%--<asp:TemplateField HeaderText="Eliminar" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                                            CommandName="Delete" ImageUrl="~/Imagenes/delete.gif"
                                            OnClientClick="return confirm('Esta seguro que desea eliminar el registro?');"
                                            Text="Eliminar" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:ButtonField ButtonType="Image" CommandName="Actualizar"
                                    HeaderText="Editar" ImageUrl="~/Imagenes/lapiz.png" Text="Botón" />                                

                                <asp:BoundField DataField="id" HeaderText="Id" SortExpression="id"/>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre Fecha" SortExpression="Nombre" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                
                        </asp:GridView>
                    </td>
                </tr>              
                                 
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

</asp:Content>
