﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;
using System.Data;

namespace Resultadistas
{
    public partial class ListarFechas : System.Web.UI.Page
    {
        FechaNegocio fechaNegocio = new FechaNegocio();
        Fecha fechaEntidad = new Fecha();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    ValidarLogin();
                    ListarDatos();
                }
            }
            catch (Exception)
            {

            }
        }
        private void ValidarLogin()
        {
            string strUser = Session["CodigoUsuario"].ToString();
            if (Convert.ToInt32(strUser) == 0)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblIdUsuario.Text = strUser;
                if (Convert.ToInt32(strUser) == 3)
                {
                    btnNuevoFecha.Visible = true;
                }
            }
        }
        private void ListarDatos()
        {
            try
            {
                lblError.Text = "";

                GridViewDatos.DataSource = fechaNegocio.ListarFechas();
                GridViewDatos.DataBind();

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message + " " + ex.StackTrace;
            }
        }

        protected void btnNuevoFecha_Click(object sender, EventArgs e)
        {
            Session["CodigoFecha"] = "0";
            Response.Redirect("~/InsertarActualizarFecha.aspx");
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ListarDatos();
        }

        protected void GridViewDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = GridViewDatos.Rows[e.RowIndex];
                string strcod = Convert.ToString(row.Cells[2].Text);

                {
                    fechaEntidad.IdFecha = Convert.ToInt32(strcod);
                }
                if (fechaNegocio.EliminarFecha(fechaEntidad) == true)
                {
                    ListarDatos();
                }
                else
                {
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GridViewDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (lblIdUsuario.Text == "3")
                {
                    short indicefila;
                    indicefila = Convert.ToInt16(e.CommandArgument);
                    string strcod;
                    if (indicefila >= 0 & indicefila < GridViewDatos.Rows.Count)
                    {
                        //Warning: cambiar GridViewDatos.Rows[indicefila].Cells[3].Text por GridViewDatos.Rows[indicefila].Cells[2].Text al corregir look&feel
                        //WArning 2: 05/10/2017: Se pide cambio para que no muestre botón eliminar, bajar a 1 del array
                        strcod = GridViewDatos.Rows[indicefila].Cells[1].Text;
                        if (e.CommandName == "Actualizar")
                        {
                            Session["CodigoFecha"] = strcod;
                            Response.Redirect("~/InsertarActualizarFecha.aspx");
                        }
                    }                    
                }
                
            }
            catch (Exception)
            {
            }
        }

        protected void GridViewDatos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridViewDatos.PageIndex = e.NewPageIndex;
                GridViewDatos.DataBind();
                ListarDatos();
            }
            catch (Exception)
            {
            }
        }

        protected void GridViewDatos_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dtbl = new DataTable();
            dtbl = fechaNegocio.ListarFechas();//here get the datatable from db
            if (ViewState["Sort Order"] == null)
            {
                dtbl.DefaultView.Sort = e.SortExpression + " DESC";
                GridViewDatos.DataSource = dtbl;
                GridViewDatos.DataBind();
                ViewState["Sort Order"] = "DESC";
            }
            else
            {
                dtbl.DefaultView.Sort = e.SortExpression + "" + " ASC";
                GridViewDatos.DataSource = dtbl;
                GridViewDatos.DataBind();
                ViewState["Sort Order"] = null;
            }
        }

    }
}