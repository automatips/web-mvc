﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Resultadistas.Login" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Resultadistas - Login</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Iniciar sesión en Resultadistas</div>
      <div class="card-body">

        <form id="form1" runat="server" style="">

          <table style="width:99%;">
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td>
		
		            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50" placeholder="Email" style="margin-right: 0px" Width="300px" TextMode="Email"></asp:TextBox>
		
	            </td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td>
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Email&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
	            </td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td>
		            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" placeholder="Password" style="margin-right: 0px" Width="300px" TextMode="Password"></asp:TextBox>
	            </td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Password&#39; no puede ser vacío</asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td><asp:Button ID="btnLogin" runat="server" Text="Iniciar Sesión" CssClass="btn btn-info" OnClick="btnLogin_Click" ValidationGroup="ValidacionesForm" /></td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td>
                    <asp:Label ID="lblError" runat="server" ForeColor="#FF3300"></asp:Label>
                </td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td><a class="d-block small mt-3" href="Registro.aspx">¿Aún no tiene usuario? Registre su cuenta</a></td>
            </tr>
            <tr>
	            <td class="text-right">
		            &nbsp;</td>
	            <td><a class="d-block small" href="forgot-password.html">¿Olvidó su contraseña?</a></td>
            </tr>
            </table>

        </form>

      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
