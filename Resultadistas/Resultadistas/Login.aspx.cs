﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaNegocio;
using CapaEntidad;
using System.Data;


namespace Resultadistas
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        public string obtenerIP()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Usuario usuarioRegistro = new Usuario();
            usuarioRegistro.EmailUsuario = txtEmail.Text;
            usuarioRegistro.PasswordUsuario = txtPassword.Text;
            UsuarioNegocio usuarioNegocio = new UsuarioNegocio();
            int vIdResultado = usuarioNegocio.LoginUsuario(usuarioRegistro);
            if (vIdResultado > 0)
            {
                Session.Clear();
                Session["CodigoUsuario"] = vIdResultado.ToString();
                Response.Redirect("~/ListarPartidos.aspx");
            }
            else
            {
                lblError.Text = "Email y/o Password no válido";
            }
        }

        protected void txtEmail_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}