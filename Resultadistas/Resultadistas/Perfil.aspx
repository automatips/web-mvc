﻿<%@ Page Title="" Language="C#" MasterPageFile="~/principal.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="Resultadistas.Perfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
    <table style="width: 116%; height: 257px;">
            <tr>
                <td class="auto-style1" colspan="2">
                    Actualizar perfil<asp:Label ID="lblIdUsuario" runat="server" Text="0" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="text-right">
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="50" placeholder="Nombre completo" style="margin-right: 0px" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombre" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Nombre&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <asp:Label ID="Label2" runat="server" Text="Id Facebook"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtIdFacebook" runat="server" CssClass="form-control" MaxLength="50" placeholder="Id de Facebook" style="margin-right: 0px" Width="300px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtIdFacebook" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Id Facebook&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <asp:Label ID="Label3" runat="server" Text="Email"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50" placeholder="Email" style="margin-right: 0px" Width="300px" TextMode="Email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Email&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" placeholder="Password" style="margin-right: 0px" Width="300px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Password&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <asp:Label ID="Label5" runat="server" Text="Confirme password"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtConfirmePassword" runat="server" CssClass="form-control" MaxLength="50" placeholder="Confirme password" style="margin-right: 0px" Width="300px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td class="text-right">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtConfirmePassword" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Confirme password&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>

            <tr>
            <td class="text-right">
                <asp:Label ID="Label6" runat="server" Text="Hincha de"></asp:Label>
            </td>
            <td class="auto-style2">
                    
                <asp:DropDownList ID="ddlEquipo" runat="server" Height="46px" Width="171px">
                </asp:DropDownList>
                    
            </td>
            </tr>

                <tr>
                <td class="auto-style1">
                    
                </td>
                <td class="auto-style2">
                        &nbsp;</td>
            </tr>
            <td class="auto-style1">
                
            </td>
            <td class="auto-style2">
                     <asp:Button ID="btnRegistro" runat="server" Text="Actualizar usuario" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" OnClick="btnRegistro_Click" />
            </td>
        </tr>
                <tr>
            <td class="auto-style1">
                
                &nbsp;</td>
            <td class="auto-style2">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        </table>
</form>
</asp:Content>
