﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Resultadistas.Registro" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Resultadistas Web</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
    <style type="text/css">
        .auto-style1 {
            text-align: right;
            width: 190px;
        }
    </style>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Registrese en RESULTADISTAS</div>
      <div class="card-body">

        <form id="form1" runat="server" style="">
        
            <table style="width: 116%; height: 257px;">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="50" placeholder="Nombre completo" style="margin-right: 0px" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombre" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Nombre&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="Id Facebook"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtIdFacebook" runat="server" CssClass="form-control" MaxLength="50" placeholder="Id de Facebook" style="margin-right: 0px" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtIdFacebook" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Id Facebook&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label3" runat="server" Text="Email"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50" placeholder="Email" style="margin-right: 0px" Width="300px" TextMode="Email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Email&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" placeholder="Password" style="margin-right: 0px" Width="300px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Password&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label5" runat="server" Text="Confirme password"></asp:Label>
                </td>
                <td class="auto-style2">
                        <asp:TextBox ID="txtConfirmePassword" runat="server" CssClass="form-control" MaxLength="50" placeholder="Confirme password" style="margin-right: 0px" Width="300px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
		
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtConfirmePassword" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="ValidacionesForm">El campo &#39;Confirme password&#39; no puede ser vacío</asp:RequiredFieldValidator>
		
                </td>
            </tr>

            <tr>
            <td class="auto-style1">
                <asp:Label ID="Label6" runat="server" Text="Hincha de"></asp:Label>
            </td>
            <td class="auto-style2">
                    
                <asp:DropDownList ID="ddlEquipo" runat="server" Height="46px" Width="171px">
                </asp:DropDownList>
                    
            </td>
            </tr>

                <tr>
                <td class="auto-style1">
                    
                </td>
                <td class="auto-style2">
                        <a class="d-block small mt-3" href="Login.aspx">¿Ya tiene usuario? Ingrese con su cuenta</a>
                </td>
            </tr>
            <td class="auto-style1">
                
            </td>
            <td class="auto-style2">
                     <asp:Button ID="btnRegistro" runat="server" Text="Crear usuario" CssClass="btn btn-info" ValidationGroup="ValidacionesForm" OnClick="btnRegistro_Click" />
            </td>
        </tr>
                <tr>
            <td class="auto-style1">
                
                &nbsp;</td>
            <td class="auto-style2">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        </table>
          
        </form>


      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
