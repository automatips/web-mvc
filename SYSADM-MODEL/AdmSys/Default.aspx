﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>
<html lang="es">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="Sistema de gestión de eventos">
    <meta name="author" content="MF">

    <title>Sistema Administrativo</title>
    <%-- Favicon --%>
    <link rel="shortcut icon" type="image/png" href="Content/images/favicon.png" />
    <%-- Bootstrap --%>
    <link href="Content/vendor/bootstrap.min.css" rel="stylesheet">
    <%-- Fonts --%>
    <link href="Content/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <%-- Custom --%>
    <link href="Content/site.css" rel="stylesheet" />
    <%-- Style - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ---%>
    <style>
        body {
            overflow-y: hidden;
            background-color: white;
        }

        #site-name {
            width: 100%;
            position: absolute;
            z-index: -99999;
            text-align: -webkit-center;
            height: inherit;
            margin-top: 10px;
            color: #00518d;
            font-size: 30px;
            font-family: 'Roboto Condensed', sans-serif;
        }

        #navbar {
            margin-bottom: 0;
        }

        nav > div.navbar-header {
            width: 100%;
            background: url(content/images/bg.png);
        }

            nav > div.navbar-header > ul {
                float: right;
            }

        .navbar-brand {
            padding: 5px !important;
        }

        #side-menu {
            border-right-width: 1px;
            border-right-style: solid;
            border-right-color: #e7e7e7;
            height: calc(100vh - 51px) !important;
            overflow-y: auto;
            background: url(content/images/bg.png);
        }

        #page-wrapper {
            padding: 0px;
            border: none;
            background: url("Content/images/logo-watermark.png") center center no-repeat;
        }

        #content-page {
            padding: 0px;
        }

        #iframe-page {
            height: calc(100vh - 56px);
        }

        .services-up {
            font-size: large;
            color: green;
        }

        .services-up-down {
            font-size: large;
            color: orange;
        }

        .services-down {
            font-size: large;
            color: red;
        }

        .service-up {
            color: green !important;
            margin-right: 5px;
        }

        .service-down {
            color: red !important;
            margin-right: 5px;
        }

        .service-unknown {
            color: gray !important;
            margin-right: 5px;
        }

        #lnkSeccionControlGlobal {
            border-left-style: solid;
            border-left-width: 10px;
            border-left-color: #2196f3;
        }

        #lnkSeccionDistribuidor {
            border-left-style: solid;
            border-left-width: 10px;
            border-left-color: #9C27B0;
        }
    </style>

    <%-- JSs -----------------------------------------------------------------------------------------%>

    <%-- jQuery --%>
    <script src="Scripts/vendor/jquery.min.js"></script>
    <%-- Bootstrap --%>
    <script src="Scripts/vendor/bootstrap.min.js"></script>
    <%-- CollapsibleMenu --%>
    <script src="Scripts/vendor/collapsiblemenu.min.js"></script>
    <%-- Notify --%>
    <script src="Scripts/vendor/notify.min.js"></script>

    <script>

        // Events  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        $(document).ready(function () {

            // Aplica el plugin menu colapsible
            $("#lateral-menu").collapsibleMenu();

            // Colapsa todos los sub-niveles del menú lateral
            $("#side-menu ul").css("display", "none");

            // Carga la correcta barra lateral y colapsa la barra lateral cuando se redimensiona la ventana.
            // Establece el valor min-height del elemento #page-wrapper
            $(function () {
                $(window).bind("load resize", function () {

                    var topOffset = 50;
                    var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;

                    if (width < 768) { // Menu mobile

                        $('div.navbar-collapse').addClass('collapse');

                        // Los .item-menu colapsan el menu
                        $('.item-menu').attr('data-toggle', 'collapse');
                        $('.item-menu').attr('data-target', '.navbar-collapse');

                        topOffset = 100;

                    } else { // Menu desktop

                        $('div.navbar-collapse').removeClass('collapse');

                        // Los .item-menu no colapsan el menu
                        $('.item-menu').attr('data-toggle', '');
                        //$('.item-menu').attr('data-target', '');

                    }

                    var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
                    height = height - topOffset;
                    if (height < 1) height = 1;
                    if (height > topOffset) {
                        $("#page-wrapper").css("min-height", (height) + "px");
                    }

                });

                var url = window.location;
                var element = $('ul.nav a').filter(function () {
                    return this.href == url;
                }).addClass('active').parent();

                while (true) {
                    if (element.is('li')) {
                        element = element.parent().addClass('in').parent();
                    } else {
                        break;
                    }
                }

            });

            // Evento al presionar una tecla en el buscador
            $("#search").keyup(function () {
                MenuSearch();
            });

            // Borrar la expresion de búsqueda
            $("#clear").click(function () {
                $("#search").val("");
                MenuSearch();
            });

            // Mantiene resaltado el item de menú activo
            $("#lateral-menu a").click(function () {

                var colorCG = $(this).parents("#lnkSeccionControlGlobal").css("border-left-color")
                var colorDistribuidor = $(this).parents("#lnkSeccionDistribuidor").css("border-left-color")

                // Item de menú inactivo
                $("#lateral-menu a").css("color", "rgb(42, 119, 141)");
                $("#lateral-menu a").css("background-color", "transparent");

                // Item de menú activo
                if (typeof colorDistribuidor === "undefined") {
                    $(this).css("background-color", colorCG);
                } else {
                    $(this).css("background-color", colorDistribuidor);
                }
                $(this).css("color", "#ffffff");

            });

        });

        // Functions  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        // Función de búsqueda de items del menú
        function MenuSearch() {
            var filter = $("#search").val(), count = 0;
            $("#side-menu li:not(.sidebar-search)").each(function () {
                if (filter == "") {
                    $('#clear').prop('disabled', true);
                    $(this).show();
                    $("#side-menu ul").css("display", "none");
                } else if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $('#clear').prop('disabled', false);
                    $(this).hide();
                } else {
                    $('#clear').prop('disabled', false);
                    $("#side-menu ul").css("display", "block");
                    $(this).show();
                }
            });
        }

        function NuevoPreContacto(idPreContacto) {

            $.notify(
                {
                    icon: 'glyphicon glyphicon-envelope',
                    message: 'Se registró un nuevo Pre-Contacto',
                    url: 'Pages/PreContacto.aspx?cryptoID=' + idPreContacto,
                },
                {
                    element: 'body',
                    type: "info",
                    allow_dismiss: true,
                    newest_on_top: true,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: 60000,
                    timer: 1000,
                    url_target: '_blank',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );

        }

    </script>
</head>
<body>
    <div id="wrapper">
        <nav id="navbar" class="navbar navbar-default navbar-static-top" role="navigation">

            <%-- Titulo del sitio --%>
            <div id="site-name" class="navbar-brand visible-lg visible-md visible-sm">
                <asp:Label Text="Sistema Administrativo" runat="server" CssClass="hidden-sm hidden-xs" />
                <small>
                    <asp:Label ID="lblApp" Text="..." runat="server" />
                </small>
            </div>

            <div class="navbar-header">

                <%-- Botón menú colapsado --%>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">
                        <asp:Label Text="Menú" runat="server" />
                    </span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <%-- Isologo header --%>
                <img id="imgLogo" src="Content/images/logo-xs.png" class="navbar-brand" />

                <%-- Menu del usuario --%>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span>
                                <asp:Label ID="lblNombre" Text="Usuario loggeado" runat="server" />
                            </span>
                            <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="Pages/Usuario.aspx" class="item-menu" target="iframePage" id="lnkPerfilUsuario" runat="server">
                                    <i class="fa fa-user fa-fw"></i>
                                    <asp:Label Text="Perfil de usuario" runat="server" />
                                </a>
                            </li>
                            <li>
                                <a href="Pages/CambiarPassword.aspx" class="item-menu" target="iframePage" runat="server">
                                    <i class="fa fa-key fa-fw"></i>
                                    <asp:Label Text="Cambiar Contraseña" runat="server" />
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="Login.aspx">
                                    <i class="fa fa-sign-out fa-fw"></i>
                                    <asp:Label Text="Cerrar sesión" runat="server" />
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>

            <%-- Menu lateral --%>
            <div id="lateral-menu" class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul id="side-menu" class="nav">

                        <%-- Buscador de elementos del menú --%>
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input id="search" type="text" class="form-control" placeholder="Buscar..." autocomplete="off">
                                <span class="input-group-btn">
                                    <button id="clear" class="btn btn-default" type="button" disabled="disabled">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </span>
                            </div>
                        </li>

                        <%-- : : : : : : : : : : : : : : : : : : :   Items del menu   : : : : : : : : : : : : : : : : : : : : --%>

                        <%--
                        -----------------------------------------------------------
                            Ejemplos
                        -----------------------------------------------------------
                        <li>
                            <a href="#">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                <asp:Label Text="Ejemplos" runat="server" />
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Pages/_Models_.aspx" target="iframePage">
                                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        <asp:Label Text="Model" runat="server" />
                                    </a>
                                </li>
                                <li>
                                    <a href="Pages/_Grid_.aspx" target="iframePage">
                                        <i class="fa fa-table" aria-hidden="true"></i>
                                        <asp:Label Text="In Line Edit" runat="server" />
                                    </a>
                                </li>
                            </ul>
                        </li>
                        --%>

                        <%--
                        -----------------------------------------------------------
                            Control Global
                        -----------------------------------------------------------
                        --%>
                        <li id="lnkSeccionControlGlobal" runat="server" data-menu-securable="mnuSectionControlGlobal">

                            <%-- Control Global --%>
                            <a href="#">
                                <i class="fa fa-fw fa-thumbs-up" aria-hidden="true"></i>
                                <asp:Label Text="Control Global" runat="server" />
                            </a>
                            <ul class="nav nav-second-level">

                                <%-- CRM --%>
                                <li id="Li51" runat="server">

                                    <%-- CRM --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="CRM" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Oportunidades --%>
                                            <a href="Pages/Oportunidades.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Oportunidades" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Pre-Contactos --%>
                                            <a href="Pages/PreContactos.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Pre-Contactos" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Contactos --%>
                                            <a href="#">
                                                <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                                <asp:Label Text="Contactos" runat="server" />
                                            </a>
                                            <ul class="nav nav-fourth-level">
                                                <li>

                                                    <%-- Personas --%>
                                                    <a href="Pages/Contactos.aspx" class="item-menu" target="iframePage" runat="server">
                                                        <asp:Label Text="Personas" runat="server" />
                                                    </a>
                                                </li>
                                                <li>

                                                    <%-- Empresas --%>
                                                    <a href="Pages/Empresas.aspx" class="item-menu" target="iframePage" runat="server">
                                                        <asp:Label Text="Empresas" runat="server" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>

                                            <%-- Email --%>
                                            <a href="Pages/Emails.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Emails" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>

                                    <%-- Distribuidores --%>
                                    <a href="Pages/Distribuidores.aspx" class="item-menu" target="iframePage" id="lnkDistribuidores" runat="server" data-menu-securable="true">
                                        <asp:Label Text="Distribuidores" runat="server" />
                                    </a>
                                </li>

                                <%-- Resellers --%>
                                <li id="lnkSectionControlGlobalResellers" runat="server" data-menu-securable="mnuSectionControlGlobalResellers">

                                    <%-- Resellers --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Resellers" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Asignar Distribuidor --%>
                                            <a href="Pages/DistribuidoresResellers.aspx" class="item-menu" target="iframePage" id="lnkDistribuidoresResellers" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Asignar Distribuidor" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Resellers Desasignados --%>
                                            <a href="Pages/ResellersNoAsignados.aspx" class="item-menu" target="iframePage" id="lnkResellersNoAsignados" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Resellers Desasignados" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="Li7" runat="server">

                                    <%-- CashVend --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-money" aria-hidden="true"></i>
                                        <asp:Label Text="CashVend" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <a href="Pages/CashVendResellersConfig.aspx" class="item-menu" target="iframePage" id="A21" runat="server">
                                                <asp:Label Text="Configuración de Resellers" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <a href="Pages/CashVendMovimientos.aspx" class="item-menu" target="iframePage" id="A22" runat="server">
                                                <asp:Label Text="Movimientos" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="lnkSectionControlGlobalSoportes" runat="server" >

                                    <%-- Soportes --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Soportes" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Listado --%>
                                            <a href="Pages/Soportes.aspx" class="item-menu" target="iframePage" id="lnkSoportes" runat="server">
                                                <asp:Label Text="Listado de Soportes" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- UM --%>
                                <li id="lnkSectionControlGlobalUM" runat="server" data-menu-securable="mnuSectionControlGlobalUM">

                                    <%-- UM --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="UMs & Teclados" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="Pages/UMAsignarDistribuidor.aspx" class="item-menu" target="iframePage" id="A13" runat="server">
                                                <asp:Label Text="Asignar Distribuidor" runat="server" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="Pages/UMDesasignarDistribuidor.aspx" class="item-menu" target="iframePage" id="A15" runat="server">
                                                <asp:Label Text="Desasignar Distribuidor" runat="server" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="Pages/UMAsignarReseller.aspx" class="item-menu" target="iframePage" id="A14" runat="server">
                                                <asp:Label Text="Asignar Reseller" runat="server" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="Pages/UMDesasignarReseller.aspx" class="item-menu" target="iframePage" id="A16" runat="server">
                                                <asp:Label Text="Desasignar Reseller" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- UMs Desasignadas --%>
                                            <a href="Pages/UMsNoAsignadas.aspx" class="item-menu" target="iframePage" id="lnkUMsNoAsignadas" runat="server" data-menu-securable="true">
                                                <asp:Label Text="UMs Desasignadas" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Listado UMs --%>
                                            <a href="Pages/ListadoUMs.aspx" class="item-menu" target="iframePage" id="A4" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Listado UMs" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Conexiones por Servidor --%>
                                            <a href="Pages/ConexionPorServidor.aspx" class="item-menu" target="iframePage" id="A23" runat="server">
                                                <asp:Label Text="Conexiones por Servidor" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Control UMs --%>
                                            <a href="Pages/UltCnnUMs.aspx" class="item-menu" target="iframePage" id="A5" runat="server">
                                                <asp:Label Text="Control UMs" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Teclado --%>
                                            <a href="Pages/Firmware.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Firmwares" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Configuraciones --%>
                                <li id="lnkSectionControlGlobalConfiguraciones" runat="server" data-menu-securable="mnuSectionControlGlobalConfiguraciones">

                                    <%-- Configuraciones --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Configuraciones" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Config. Soporte --%>
                                            <a href="#">
                                                <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                                <asp:Label Text="Soporte" runat="server" />
                                            </a>
                                            <ul class="nav nav-fourth-level">
                                                <li>

                                                    <%-- Tipos de Soporte --%>
                                                    <a href="#" class="item-menu" target="iframePage" id="A17" runat="server">
                                                        <asp:Label Text="Tipos de Soportes" runat="server" />
                                                    </a>
                                                </li>
                                                <li>

                                                    <%-- Tipos de Soporte Estado --%>
                                                    <a href="#" class="item-menu" target="iframePage" id="A18" runat="server">
                                                        <asp:Label Text="Tipos de Soportes Estados" runat="server" />
                                                    </a>
                                                </li>
                                                <li>

                                                    <%-- Tipos de Soporte Prioridades --%>
                                                    <a href="#" class="item-menu" target="iframePage" id="A19" runat="server">
                                                        <asp:Label Text="Tipos de Soportes Prioridades" runat="server" />
                                                    </a>
                                                </li>

                                                <li>

                                                    <%-- Tipos de Movimientos de Soporte --%>
                                                    <a href="#" class="item-menu" target="iframePage" id="A20" runat="server">
                                                        <asp:Label Text="Tipos de Soportes Movimientos" runat="server" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>

                                            <%-- Tipos de Servicios --%>
                                            <a href="Pages/ServiciosTipos.aspx" class="item-menu" target="iframePage" id="lnkServiciosTipos" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Tipos de Servicios" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Tipos de Cobro --%>
                                            <a href="Pages/TiposCobros.aspx" class="item-menu" target="iframePage" id="lnkTiposCobros" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Tipos de Cobros" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Tipos de Cobro --%>
                                            <a href="TestFactory/index.html" class="item-menu" target="iframePage" id="A10" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Test Factory" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Acceso a la Información --%>
                                <li id="lnkSectionControlGlobalAccesoInformacion" runat="server" >

                                    <%-- Acceso a la Información --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Acceso a la Información" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Dashboard --%>
                                            <a href="Content/html/Welcome.html?version=1.2.60.1" class="item-menu" target="iframePage">
                                                <i class="fa fa-fw fa-gears" aria-hidden="true"></i>
                                                <asp:Label Text="Dashboard" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Informes --%>
                                            <a href="Content/html/Welcome.html?version=1.2.60.1" class="item-menu" target="iframePage">
                                                <i class="fa fa-fw fa-play" aria-hidden="true"></i>
                                                <asp:Label Text="Informes" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Sistema de Usuarios --%>
                                <li id="lnkSectionControlGlobalSistemaUsuarios" runat="server" data-menu-securable="mnuSectionControlGlobalSistemaUsuarios">

                                    <%-- Sistema de Usuarios --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Sistema de Usuarios" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Sistemas --%>
                                            <a href="Pages/Sistemas.aspx" class="item-menu" target="iframePage" id="lnkSistemas" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-gears" aria-hidden="true"></i>
                                                <asp:Label Text="Sistemas" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Aplicaciones --%>
                                            <a href="Pages/Aplicaciones.aspx" class="item-menu" target="iframePage" id="lnkAplicaciones" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-play" aria-hidden="true"></i>
                                                <asp:Label Text="Aplicaciones" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Componentes de aplicacion --%>
                                            <a href="Pages/Componentes.aspx" class="item-menu" target="iframePage" id="lnkComponentes" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-puzzle-piece" aria-hidden="true"></i>
                                                <asp:Label Text="Componentes" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Perfiles --%>
                                            <a href="Pages/PerfilesSU.aspx" class="item-menu" target="iframePage" id="lnkPerfilesSU" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-user" aria-hidden="true"></i>
                                                <asp:Label Text="Perfiles" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Usuarios --%>
                                            <a href="Pages/UsuariosSU.aspx" class="item-menu" target="iframePage" id="lnkUsuariosSU" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-users" aria-hidden="true"></i>
                                                <asp:Label Text="Usuarios" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <%--
                        -----------------------------------------------------------
                            Distribuidor
                        -----------------------------------------------------------
                        --%>
                        <li id="lnkSeccionDistribuidor" runat="server" data-menu-securable="mnuSectionDistribuidor">

                            <%-- Distribuidor --%>
                            <a href="#">
                                <i class="fa fa-sitemap fa-fw"></i>
                                <asp:Label Text="Distribuidor" runat="server" />
                            </a>
                            <ul class="nav nav-second-level">

                                <%-- CRM --%>
                                <li id="Li6" runat="server">

                                    <%-- CRM --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="CRM" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Oportunidades --%>
                                            <a href="Pages/Oportunidades.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Oportunidades" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Resellers --%>
                                <li>
                                    <a href="Pages/Resellers.aspx" class="item-menu" target="iframePage" id="A6" runat="server" data-menu-securable="true">
                                        <asp:Label Text="Resellers" runat="server" />
                                    </a>
                                </li>

                                <%-- Comercial --%>
                                <li id="Li1" runat="server">

                                    <%-- Comercial --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Comercial" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Planes Comerciales --%>
                                            <a href="Pages/PlanesComerciales.aspx" class="item-menu" target="iframePage" id="lnkPlanesComerciales" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Planes Comerciales" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Productos --%>
                                            <a href="Pages/ProductoServicioListar.aspx" class="item-menu" target="iframePage" id="lknProductosServicios" runat="server">
                                                <asp:Label Text="Productos / Servicios" runat="server" />
                                            </a>
                                        </li>

                                        <li>

                                            <%-- Productos --%>
                                            <a href="Pages/ProductoKitListar.aspx" class="item-menu" target="iframePage" id="A24" runat="server">
                                                <asp:Label Text="Kits de Productos" runat="server" />
                                            </a>
                                        </li>

                                        <li>

                                            <%-- Listas de Precios --%>
                                            <a href="Pages/ProductosPorPlanComercial.aspx" class="item-menu" target="iframePage" id="lknListasPrecios" runat="server">
                                                <asp:Label Text="Listas de precios" runat="server" />
                                            </a>
                                        </li>

                                        <li>

                                            <%-- Asignar Plan Comercial --%>
                                            <a href="Pages/AsignacionesPlanes.aspx" class="item-menu" target="iframePage" id="lnkAsignacionesPlanes" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Asignar Plan Comercial" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Administracion --%>
                                <li id="Li2" runat="server" >

                                    <%-- Administracion --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Administración" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>

                                            <%-- Solicitudes de Servicios --%>
                                            <a href="Pages/SolicitudesDeServicios.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Solicitudes de Servicios" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Solicitudes de Taller --%>
                                            <a href="Pages/SolicitudesDeTaller.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Solicitudes de Taller" runat="server" />
                                            </a>
                                        </li>
                                        <li>
                                            <%-- Remitos --%>
                                            <a href="Pages/Remitar.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Remitos" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Cuentas Corrientes --%>
                                            <a href="Pages/ListadoCuentasCorrientes.aspx" class="item-menu" target="iframePage" id="A3" runat="server" data-menu-securable="true">
                                                <asp:Label Text="Cuentas Corrientes" runat="server" />
                                            </a>
                                        </li>

                                        <%-- Facturacion --%>
                                        <li id="Li3" runat="server" >

                                            <%-- Facturacion --%>
                                            <a href="#">
                                                <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                                <asp:Label Text="Facturacion" runat="server" />
                                            </a>
                                            <ul class="nav nav-fourth-level">
                                                <li>

                                                    <%-- Facturar --%>
                                                    <a href="Pages/Comprobantes.aspx" class="item-menu" target="iframePage" id="A9" runat="server" data-menu-securable="true">
                                                        <asp:Label Text="Comprobantes" runat="server" />
                                                    </a>
                                                </li>
                                                <li>

                                                    <%-- Pendientes de Facturar --%>
                                                    <a href="Pages/PendientesFacturar.aspx" class="item-menu" target="iframePage" id="A12" runat="server" data-menu-securable="true">
                                                        <asp:Label Text="Pendientes de Facturar" runat="server" />
                                                    </a>
                                                </li>

                                                <%-- Factura Electrónica --%>
                                                <li runat="server">

                                                    <%-- Factura Electrónica --%>
                                                    <a href="#">
                                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                                        <asp:Label Text="Factura Electrónica" runat="server" />
                                                    </a>
                                                    <ul class="nav nav-third-level">
                                                        <li>

                                                            <%-- F.E. con Detalle --%>
                                                            <a href="Pages/MTXCAs.aspx" class="item-menu" target="iframePage" runat="server">
                                                                <asp:Label Text="F.E. con Detalle" runat="server" />
                                                            </a>
                                                        </li>
                                                        <li>

                                                            <%-- Desglose IVA --%>
                                                            <a href="Pages/DesgloseIVA.aspx" class="item-menu" target="iframePage" runat="server">
                                                                <asp:Label Text="Desglose IVA" runat="server" />
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>

                                        <%-- Pagos & Bancos --%>
                                        <li id="Li4" runat="server" >

                                            <%-- Pagos & Bancos --%>
                                            <a href="#">
                                                <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                                <asp:Label Text="Pagos & Bancos" runat="server" />
                                            </a>
                                            <ul class="nav nav-fourth-level">
                                                <li>
                                                    <%-- Bancos --%>
                                                    <a href="Pages/Bancos.aspx" class="item-menu" target="iframePage" id="A7" runat="server" data-menu-securable="true">
                                                        <asp:Label Text="Bancos" runat="server" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <%-- Cuentas Bancarias --%>
                                                    <a href="Pages/CuentasBancarias.aspx" class="item-menu" target="iframePage" id="A8" runat="server" data-menu-securable="true">
                                                        <asp:Label Text="Cuentas Bancarias" runat="server" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <%-- Cheques --%>
                                                    <a href="Pages/Cheques.aspx" class="item-menu" target="iframePage" id="A11" runat="server" data-menu-securable="true">
                                                        <asp:Label Text="Cheques" runat="server" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>

                                <%--
                                    -> Manufactura
                                --%>
                                <li id="Li5" runat="server" >
                                    <%-- Manufactura --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Manufactura" runat="server" />
                                    </a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <%-- Pendientes desde taller --%>
                                            <a href="Pages/PendientesManufactura.aspx" class="item-menu" target="iframePage" runat="server">
                                                <asp:Label Text="Pendientes" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <%-- Permisos --%>
                                <li id="lnkSectionDistribuidorPermisos" runat="server" data-menu-securable="mnuSectionDistribuidorPermisos">

                                    <%-- Permisos --%>
                                    <a href="#">
                                        <i class="fa fa-fw fa-arrow-right" aria-hidden="true"></i>
                                        <asp:Label Text="Permisos" runat="server" />
                                    </a>
                                    <ul class="nav nav-fourth-level">
                                        <li>

                                            <%-- Perfiles --%>
                                            <a href="Pages/Perfiles.aspx" class="item-menu" target="iframePage" id="A1" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-user" aria-hidden="true"></i>
                                                <asp:Label Text="Perfiles" runat="server" />
                                            </a>
                                        </li>
                                        <li>

                                            <%-- Usuarios --%>
                                            <a href="Pages/Usuarios.aspx" class="item-menu" target="iframePage" id="A2" runat="server" data-menu-securable="true">
                                                <i class="fa fa-fw fa-users" aria-hidden="true"></i>
                                                <asp:Label Text="Usuarios" runat="server" />
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div id="content-page" class="container-fluid">

                <%-- Paginas --%>
                <iframe id="iframe-page" name="iframePage" src="Content/html/Welcome.html?version=<%=Version%>" frameborder="0" width="100%"></iframe>

            </div>
        </div>
    </div>
</body>
</html>
