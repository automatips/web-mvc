// Objeto configuración regional para formatear los datos según las preferencias del usuario.
var regionalSettings;

// Columna de la GridView por la que ordenar.
function Col(grid) {
    if (grid.getAttribute('data-Order')) {
        var data = grid.getAttribute('data-Order');
        data = data.substring(0, data.indexOf("-"));
        return parseInt(data);
    } else {
        return 0;
    }
}

// Criterio de orden (asc/desc).
function Asc(grid) {
    if (grid.getAttribute('data-Order')) {
        var data = grid.getAttribute('data-Order');
        return data.substring(data.indexOf("-") + 1);
    } else {
        return "asc";
    }
}

// Función de inicialización de los plugins.
function Init() {

    // Para el plugin Autonumeric. Si el símbolo de la moneda se muestra como prefijo, se agrega un espacio para separarlo del número.
    var moneySymbolSeparator = "";
    if (regionalSettings.MoneySymbolPosition == "p") {
        moneySymbolSeparator = " ";
    }

    // Para el plugin Autonumeric. Números. Mantener los ceros a la izquierda una vez que el control pierde el foco.
    var numericLeadZeros = "allow";
    if (regionalSettings.NumericLeadZeros) {
        numericLeadZeros = "keep";
    }

    // Para el plugin Autonumeric. Moneda. Mantener los ceros a la izquierda una vez que el control pierde el foco.
    var moneyLeadZeros = "allow";
    if (regionalSettings.MoneyLeadZeros) {
        moneyLeadZeros = "keep";
    }

    // Horas en formato 12 o 24.
    var timeFormat = false;
    if (regionalSettings.TimeFormat == 12) {
        timeFormat = true;
    }

    // .grid-view, show-total, show-buttons ---------------------------------------------------------------------------------------------------------
    if ($(".grid-view").length > 0) {

        // Recorre las tablas con la clase grid-view.
        $('.grid-view').each(function () {

            $(this).hide();

            // La tabla tiene filas?
            if ($(this).find("tbody tr").length > 1) {

                // Si la tabla tiene el plugin aplicado no se vuelve a aplicar.
                if (!$.fn.dataTable.isDataTable(this)) {

                    // Si la tabla no tiene thead se utiliza la primera fila del tbody como thead.
                    if ($(this).find("thead").length == 0) {

                        // Agrega el thead a la tabla.
                        $(this).prepend('<thead></thead>');

                        // Primera fila del tbody como thead (tbody.tr.td -> thead.tr.th)
                        $(this).find('thead').append($(this).find("tr:eq(0)"));
                    }

                    // Se agregan los data-order a las fechas y fechas horas para poder ordenarlas haciendo click en los titulos de las columnas.
                    $("#" + this.id + " td").each(function () {

                        // col-date-time
                        if (this.className == "col-date-time") {
                            var separador;
                            if ($(this).html().indexOf("/") !== -1) {
                                separador = "/";
                            }
                            if ($(this).html().indexOf("-") !== -1) {
                                separador = "-";
                            }
                            if ($(this).html().indexOf(".") !== -1) {
                                separador = ".";
                            }

                            if (typeof separador !== 'undefined') {
                                var fh = $(this).html().split(" ");

                                var sf = fh[0];
                                var d = sf.split(separador)[0];
                                var M = sf.split(separador)[1];
                                var y = sf.split(separador)[2];

                                var sh = fh[1];
                                var h = sh.split(":")[0];
                                var m = sh.split(":")[1];
                                var s = sh.split(":")[2];

                                if ($(this).html().indexOf("AM") > 0) {
                                    if (h == "12") h = "00";
                                }

                                if ($(this).html().indexOf("PM") > 0) {
                                    if (h != 12) {
                                        h = parseInt(h) + 12;
                                    }
                                }

                                var data = y + "-" + M + "-" + d + " " + h + ":" + m + ":" + s;
                                $(this).attr('data-Order', data);
                            }
                        }
                    });

                    // Si la tabla tiene al menos una columna que totalice se agrega el tfoot.
                    if ($(this).find('tr td.show-total').length > 0) {

                        // Si la tabla no tiene tfoot se clona desde el thead.
                        if ($(this).find("tfoot").length == 0) {

                            // Agrega el tfoot a la tabla.
                            $(this).append('<tfoot></tfoot>');

                            // Clona el thead en el tfoot.
                            $(this).find("thead tr").clone().appendTo($(this).find("tfoot"));

                            // Limpia el tfoot.
                            $(this).find("tfoot tr th").html('');
                        }
                    }

                    // Si luego de utilizar la primer fila de tbody com othead, la tabla no tiene filas en el tbody no se aplicac el plugin.
                    if ($(this).find("tbody tr").length > 0) {

                        // Parametros de inicializacion del plugin.
                        GridViewOptions = {
                            language: { // Idioma.
                                url: "../Content/vendor/datatables.languages/Spanish.json"
                            },
                            responsive: { // Responsive.
                                details: {
                                    type: 'column'
                                }
                            },
                            columnDefs: [
                            { // Primera columna con el +/- según las dimensiones de la pantalla.
                                targets: 0,
                                className: 'control'
                            },
                            { // Ultima columna siempre visible.
                                targets: -1,
                                className: 'all'
                            }],
                            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]], // Opciones de paginación.

                            // Evento para totalizar columnas.
                            "footerCallback": function (row, data, start, end, display) {

                                // Objeto tabla.
                                var api = this.api(), data;

                                var procCols = "";

                                $(this).find('tr td.show-total').each(function () {

                                    // Indice de la columna.
                                    var colIndex = this.cellIndex;
                                    if ($(this).parent().first().html().trim().startsWith("<td class=\"crypto-id-column\">")) {
                                        colIndex++;
                                    }

                                    if (procCols.indexOf("|" + colIndex + "|") == -1) {
                                        procCols = procCols + "|" + colIndex + "|";

                                        var dataIndex = colIndex;

                                        // Totaliza la tabla.
                                        var table = api.column(dataIndex).data();
                                        var totalTable = 0;
                                        var tempTable = "";
                                        for (var i = 0; i < table.length; i++) {
                                            tempTable = table[i];

                                            // Se limpia el dato quitándole la configuración regional.
                                            tempTable = tempTable.replace("&nbsp;", 0);
                                            tempTable = tempTable.replace(regionalSettings.MoneySymbol, "");
                                            tempTable = tempTable.replace(regionalSettings.MoneyGroupSeparator, "");
                                            tempTable = tempTable.replace(regionalSettings.MoneyDecimalSymbol, ".");
                                            tempTable = tempTable.replace(" ", "");

                                            // Sumar o contar.
                                            if (isNaN(Number(tempTable))) {
                                                totalTable++;
                                            } else {
                                                totalTable = totalTable + Number(tempTable);
                                            }
                                        }
                                        totalTable = Math.round(totalTable * 100) / 100; // Redondea a 2 decimales.

                                        // Totaliza la página.
                                        var page = api.column(dataIndex, { page: 'current' }).data();
                                        var totalPage = 0;
                                        var tempPage = "";
                                        for (var i = 0; i < page.length; i++) {
                                            tempPage = page[i];

                                            // Se limpia el dato quitándole la configuración regional.
                                            tempPage = tempPage.replace("&nbsp;", 0);
                                            tempPage = tempPage.replace(regionalSettings.MoneySymbol, "");
                                            tempPage = tempPage.replace(regionalSettings.MoneyGroupSeparator, "");
                                            tempPage = tempPage.replace(regionalSettings.MoneyDecimalSymbol, ".");
                                            tempPage = tempPage.replace(" ", "");

                                            // Sumar o contar.
                                            if (isNaN(Number(tempPage))) {
                                                totalPage++;
                                            } else {
                                                totalPage = totalPage + Number(tempPage);
                                            }
                                        }
                                        totalPage = Math.round(totalPage * 100) / 100; // Redondea a 2 decimales.

                                        // Selector del tfoot.th
                                        if ($(this).parent().first().html().trim().startsWith("<td class=\"crypto-id-column\">")) {
                                            colIndex--;
                                        }
                                        var selector = 'tfoot th:eq(' + (colIndex).toString() + ')';

                                        // Muestra los totales. Se aplica la misma clase que la columna para luego aplicar el plugin Autonumeric.
                                        var inputPage = '<span class="t_' + this.classList.value + '">' + totalPage + '</span>';
                                        var inputTable = '<span class="t_' + this.classList.value + '">' + totalTable + '</span>';

                                        // Se muestra el resultado.
                                        $(this).parent().parent().parent().find(selector).html(inputPage + "<br><b>" + inputTable + "</b>");

                                        // Formatea los totales de pagina y tabla.
                                        setTimeout(function () {
                                            var options = {
                                                aDec: ".",
                                                aForm: true,
                                                aPad: false,
                                                aSep: "",
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: "keep",
                                                mDec: "0",
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "999999999",
                                                vMin: "0"
                                            };
                                            $(".t_numeric-only").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: false,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: "0",
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "999999999",
                                                vMin: "-999999999"
                                            };
                                            $(".t_numeric-integer").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: false,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: "0",
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "999999999",
                                                vMin: "0"
                                            };
                                            $(".t_numeric-integer-positive").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: false,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: "0",
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "0",
                                                vMin: "-999999999"
                                            };
                                            $(".t_numeric-integer-negative").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.NumericFillDecimalsWithZeros,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: regionalSettings.NumericDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "999999999.99",
                                                vMin: "-999999999.99"
                                            };
                                            $(".t_numeric-decimal").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.NumericFillDecimalsWithZeros,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: regionalSettings.NumericDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "999999999.99",
                                                vMin: "0"
                                            };
                                            $(".t_numeric-decimal-positive").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.NumericDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.NumericFillDecimalsWithZeros,
                                                aSep: regionalSettings.NumericGroupSeparator,
                                                aSign: "",
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: numericLeadZeros,
                                                mDec: regionalSettings.NumericDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: "p",
                                                vMax: "0",
                                                vMin: "-999999999.99"
                                            };
                                            $(".t_numeric-decimal-negative").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.MoneyDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.MoneyFillDecimalsWithZeros,
                                                aSep: regionalSettings.MoneyGroupSeparator,
                                                aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
                                                dGroup: "3",
                                                lZero: moneyLeadZeros,
                                                mDec: regionalSettings.MoneyDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: regionalSettings.MoneySymbolPosition,
                                                vMax: "999999999.99",
                                                vMin: "-999999999.99"
                                            };
                                            $(".t_numeric-money").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.MoneyDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.MoneyFillDecimalsWithZeros,
                                                aSep: regionalSettings.MoneyGroupSeparator,
                                                aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: moneyLeadZeros,
                                                mDec: regionalSettings.MoneyDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: regionalSettings.MoneySymbolPosition,
                                                vMax: "999999999.99",
                                                vMin: "0"
                                            };
                                            $(".t_numeric-money-positive").autoNumeric('init', options);
                                            var options = {
                                                aDec: regionalSettings.MoneyDecimalSymbol,
                                                aForm: true,
                                                aPad: regionalSettings.MoneyFillDecimalsWithZeros,
                                                aSep: regionalSettings.MoneyGroupSeparator,
                                                aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
                                                anDefault: null,
                                                dGroup: "3",
                                                lZero: moneyLeadZeros,
                                                mDec: regionalSettings.MoneyDecimalDigits,
                                                mRound: "S",
                                                nBracket: null,
                                                pSign: regionalSettings.MoneySymbolPosition,
                                                vMax: "0",
                                                vMin: "-999999999.99"
                                            };
                                            $(".t_numeric-money-negative").autoNumeric('init', options);

                                            // Imortes negativos en los totales en rojo.
                                            $("span.t_numeric-money, span.t_numeric-money-positive, span.t_numeric-negative").each(function () {
                                                if (this.innerText.startsWith("-")) {
                                                    $(this).css("color", "#d43b30");
                                                    $(this).css("text-shadow", "0px 0px 1px rgba(235, 235, 235, 1)");
                                                }
                                            });
                                        }, 1);
                                    }
                                });
                            }
                        };

                        // Aplica el plugin DataTable.
                        $(this).dataTable(GridViewOptions);

                        // Si tiene columnas busca el criterio de orden.
                        if (this.rows[0].cells.length > 1) {

                            // Tiene creiterio de orden?
                            if (this.getAttribute('data-Order')) {

                                // Ordena por el criterio.
                                $(this).DataTable().order([Col(this), Asc(this)]);
                            }
                        }

                        // Si la grilla tiene la clase .show-buttons se agregan los botones de exportación.
                        if ($(this).hasClass("show-buttons")) {

                            // Agrega los botones de exportacón inmediatamente despues del wrapper del datatable.
                            var buttons = new $.fn.dataTable.Buttons(this, {
                                buttons: [
                                    {
                                        extend: 'copy',
                                        exportOptions: {
                                            columns: ':visible'
                                        }
                                    },
                                    {
                                        extend: 'excel',
                                        exportOptions: {
                                            columns: ':visible'
                                        }
                                    },
                                    {
                                        extend: 'csv',
                                        exportOptions: {
                                            columns: ':visible'
                                        }
                                    },
                                    {
                                        extend: 'print',
                                        exportOptions: {
                                            columns: ':visible'
                                        }
                                    }
                                ]
                            }).container().appendTo($(this).parent());

                            // copiar
                            $(".buttons-copy").html('<i class="fa fa-files-o" aria-hidden="true"></i>');
                            $('.buttons-copy').prop('title', 'Copiar');

                            // exportar a excel
                            $(".buttons-excel").html('<i class="fa fa-file-excel-o" aria-hidden="true"></i>');
                            $('.buttons-excel').prop('title', 'Exportar a Excel');

                            // exportar a csv
                            $(".buttons-csv").html('<i class="fa fa-file-text-o" aria-hidden="true"></i>');
                            $('.buttons-csv').prop('title', 'Exportar a CSV');

                            // imprimir
                            $(".buttons-print").html('<i class="fa fa-print" aria-hidden="true"></i>');
                            $('.buttons-print').prop('title', 'Imprimir');

                            $("a.dt-button").addClass("btn-sm btn-primary");
                        } // fin validacion grilla con botones
                    } // fin validacion hay filas en el tbody despues de usar el primer tbody como thead
                } // fin de la validacion de tabla con el plugin ya aplicado
            } // fin de la validacion de filas en la grilla

            $(this).fadeIn(350);
        }); // fin del loop que recorre las grid-view
    };

    // .numeric-only --------------------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-only").length > 0) {
        var NumericIntegerOptions = {
            aDec: ".",
            aForm: false,
            aPad: false,
            aSep: "",
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: "keep",
            mDec: "0",
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "999999999",
            vMin: "0"
        };
        $('.numeric-only').autoNumeric('init', NumericIntegerOptions);
    };

    // .numeric-integer -----------------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-integer").length > 0) {
        var NumericIntegerOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: false,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: "0",
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "999999999",
            vMin: "-999999999"
        };
        $('.numeric-integer').autoNumeric('init', NumericIntegerOptions);
    };

    // .numeric-integer-positive --------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-integer-positive").length > 0) {
        var NumericIntegerPositiveOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: false,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: "0",
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "999999999",
            vMin: "0"
        };
        $('.numeric-integer-positive').autoNumeric('init', NumericIntegerPositiveOptions);
    };

    // .numeric-integer-negative --------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-integer-negative").length > 0) {
        var NumericIntegerNegativeOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: false,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: "0",
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "0",
            vMin: "-999999999"
        };
        $('.numeric-integer-negative').autoNumeric('init', NumericIntegerNegativeOptions);
    };

    // .numeric-decimal -----------------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-decimal").length > 0) {
        var NumericDecimalOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.NumericFillDecimalsWithZeros,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: regionalSettings.NumericDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "999999999.99",
            vMin: "-999999999.99"
        };
        $('.numeric-decimal').autoNumeric('init', NumericDecimalOptions);
    };

    // .numeric-decimal-positive --------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-decimal-positive").length > 0) {
        var NumericDecimalPositiveOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.NumericFillDecimalsWithZeros,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: regionalSettings.NumericDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "999999999.99",
            vMin: "0"
        };
        $('.numeric-decimal-positive').autoNumeric('init', NumericDecimalPositiveOptions);
    };

    // .numeric-decimal-negative --------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-decimal-negative").length > 0) {
        var NumericDecimalNegativeOptions = {
            aDec: regionalSettings.NumericDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.NumericFillDecimalsWithZeros,
            aSep: regionalSettings.NumericGroupSeparator,
            aSign: "",
            anDefault: null,
            dGroup: "3",
            lZero: numericLeadZeros,
            mDec: regionalSettings.NumericDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: "p",
            vMax: "0",
            vMin: "-999999999.99"
        };
        $('.numeric-decimal-negative').autoNumeric('init', NumericDecimalNegativeOptions);
    };

    // .numeric-money -------------------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-money").length > 0) {
        var NumericMoneyOptions = {
            aDec: regionalSettings.MoneyDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.MoneyFillDecimalsWithZeros,
            aSep: regionalSettings.MoneyGroupSeparator,
            aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
            dGroup: "3",
            lZero: moneyLeadZeros,
            mDec: regionalSettings.MoneyDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: regionalSettings.MoneySymbolPosition,
            vMax: "999999999.99",
            vMin: "-999999999.99"
        };
        $('.numeric-money').autoNumeric('init', NumericMoneyOptions);

        // Importes negativos en rojo.
        $('.numeric-money').change(function () {
            if (this.value.startsWith("-")) {
                $(this).css("color", "#d43b30");
                $(this).css("text-shadow", "0px 0px 1px rgba(235, 235, 235, 1)");
            }
        })
    };

    // .numeric-money-positive ----------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-money-positive").length > 0) {
        var NumericMoneyPositiveOptions = {
            aDec: regionalSettings.MoneyDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.MoneyFillDecimalsWithZeros,
            aSep: regionalSettings.MoneyGroupSeparator,
            aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
            anDefault: null,
            dGroup: "3",
            lZero: moneyLeadZeros,
            mDec: regionalSettings.MoneyDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: regionalSettings.MoneySymbolPosition,
            vMax: "999999999.99",
            vMin: "0"
        };
        $('.numeric-money-positive').autoNumeric('init', NumericMoneyPositiveOptions);
    };

    // .numeric-money-negaive -----------------------------------------------------------------------------------------------------------------------
    if ($(".numeric-money-negative").length > 0) {
        var NumericMoneyNegativeOptions = {
            aDec: regionalSettings.MoneyDecimalSymbol,
            aForm: false,
            aPad: regionalSettings.MoneyFillDecimalsWithZeros,
            aSep: regionalSettings.MoneyGroupSeparator,
            aSign: regionalSettings.MoneySymbol + moneySymbolSeparator,
            anDefault: null,
            dGroup: "3",
            lZero: moneyLeadZeros,
            mDec: regionalSettings.MoneyDecimalDigits,
            mRound: "S",
            nBracket: null,
            pSign: regionalSettings.MoneySymbolPosition,
            vMax: "0",
            vMin: "-999999999.99"
        };
        $('.numeric-money-negative').autoNumeric('init', NumericMoneyNegativeOptions);

        // Importes negativos en rojo.
        $(".numeric-money-negative").css("color", "#d43b30");
        $(".numeric-money-negative").css("text-shadow", "0px 0px 1px rgba(235, 235, 235, 1)");
    };

    // .select-single -------------------------------------------------------------------------------------------------------------------------------
    if ($(".select-single").length > 0) {
        $(".select-single").select2({
            theme: "bootstrap"
        });
    }

    // .select-multiple -----------------------------------------------------------------------------------------------------------------------------
    if ($(".select-multiple").length > 0) {
        $(".select-multiple").attr("multiple", "multiple");
        $(".select-multiple").select2({
            theme: "bootstrap"
        });
    }

    // .date-picker ---------------------------------------------------------------------------------------------------------------------------------
    if ($(".date-picker").length > 0) {

        // Idiomas.
        // Inglés - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        $.fn.datepicker.dates['en'] = {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        // Español - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            format: "dd/mm/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        // parámetros de inicializacion del plugin DatePicker.
        var DatePickerOptions = {
            format: regionalSettings.DateFormat.replace("MM", "mm"),    // Formato de fecha.
            weekStart: regionalSettings.FirstDayWeek,                   // Primer día de la semana.

            // parametros fijos -------------------------------------------------------------------------
            todayBtn: true,                                             // Boton Hoy.
            autoclose: true,                                            // Cerrar al seleccionar.
            todayHighlight: true,                                       // Resaltar la fecha actual.
            language: 'es'                                              // Idioma.
        }
        $(".date-picker").datepicker(DatePickerOptions);
        $(".date-picker").prop('readonly', true);
    };

    // .time-picker ---------------------------------------------------------------------------------------------------------------------------------
    if ($(".time-picker").length > 0) {
        var TimePickerOptions = {
            twelvehour: timeFormat,                                     // Formato de hora (12/24).

            // parametros fijos -------------------------------------------------------------------------
            autoclose: true                                             // El control se cierra al seleccionar los minutos.
        }
        $(".time-picker").timepicker(TimePickerOptions);
        $(".time-picker").prop('readonly', true);
    };

    // checkbox -------------------------------------------------------------------------------------------------------------------------------------
    if ($("input[type=checkbox]").length > 0) {
        $("input[type=checkbox]").each(function () {

            // Evita que el plugin se vuelva a aplicar sobre un checkbox que ya lo tiene aplicado.
            if (!$(this).data("switchery")) {
                var init = new Switchery(this, { size: 'small', color: '#9e9e9e' });
            }
        });
    };

    // .clickable -----------------------------------------------------------------------------------------------------------------------------------
    $('.clickable').on('click', 'td', function () {
        if (this.innerHTML != "") {
            if (typeof $(this).parent().find('td.crypto-id-column')[0] !== "undefined") {

                // Muestra el div procesando
                $("#busy").show();

                // CryptoID del Modelo
                var cryptoID = $(this).parent().find('td.crypto-id-column')[0].innerText;

                // Pagina destino
                var destPage = $(this).parent().parent().parent().attr("data-model");

                if (destPage.indexOf('?') >= 0) {
                    window.location.href = destPage + '&cryptoID=' + cryptoID;
                }
                else {
                    window.location.href = destPage + '?cryptoID=' + cryptoID;
                }
            }
        }
    });

    // .checkable -----------------------------------------------------------------------------------------------------------------------------------
    $('.checkable').on('click', 'td', function () {
        if (this.innerHTML.indexOf('type="checkbox"') == -1 && this.innerHTML != "") {

            // Checkbox.
            var checkbox = $(this).parent().find('td input:checkbox')[0];
            if (!checkbox.disabled) {
                if (checkbox.checked) {
                    checkbox.checked = false;
                } else {
                    checkbox.checked = true;
                }

                // Cambia el estado de switchery segun el estado del checkbox.
                if (typeof Event === 'function' || !document.fireEvent) {
                    var event = document.createEvent('HTMLEvents');
                    event.initEvent('change', true, true);
                    checkbox.dispatchEvent(event);
                } else {
                    checkbox.fireEvent('onchange');
                }
            }
        }
    });

    // .cuit ----------------------------------------------------------------------------------------------------------------------------------------
    if ($(".cuit").length > 0) {
        $(".cuit").mask("00-00000000-0");
    }

    // Importes negativos en las tablas en rojo.
    $('td.numeric-money, td.numeric-money-positive, td.numeric-money-negative').each(function () {
        if (this.innerText.startsWith("-")) {
            $(this).css("color", "#d43b30");
            $(this).css("text-shadow", "0px 0px 1px rgba(235, 235, 235, 1)");
        }
    });

    // Selecciona el contenido del control cuando se enfoca.
    $('.select-onfocus').focus(function () {
        $(this).select();
    })

    $('input:password').password({
        shortPass: 'La contraseña es demasiado corta',
        badPass: 'Débil. Combine mayúsculas, minúsculas, numeros',
        goodPass: 'Media. Incluya caracteres especiales',
        strongPass: 'Contraseña fuerte',
        containsUsername: 'La contraseña contiene el nombre de usuario',
        enterPass: 'Ingrese la contraseña',
        showPercent: false,
        showText: true, // shows the text tips
        animate: true, // whether or not to animate the progress bar on input blur/focus
        animateSpeed: 'fast', // the above animation speed
        username: false, // select the username field (selector or jQuery instance) for better password checks
        usernamePartialMatch: true, // whether to check for username partials
        minimumLength: 6 // minimum password length (below this threshold, the score is 0)
    });
};