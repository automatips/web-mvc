﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace GestionContable
{
    public partial class Concepto : ISUD<Concepto>
    {
        public Concepto() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdConcepto { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Tipo { get; set; }
    }
}