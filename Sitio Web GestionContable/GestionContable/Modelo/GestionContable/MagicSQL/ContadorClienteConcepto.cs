﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace GestionContable
{
    public partial class ContadorClienteConcepto : ISUD<ContadorClienteConcepto>
    {
        public ContadorClienteConcepto() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdContadorClienteConcepto { get; set; }

        public DateTime? FHAlta { get; set; }

        public int? IdContadorCliente { get; set; }

        public int? IdConcepto { get; set; }

        public string Valor { get; set; }

        public int? Orden { get; set; }

        public int? IdPeriodo { get; set; }
    }
}