﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace GestionContable
{
    public partial class Periodo : ISUD<Periodo>
    {
        public Periodo() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdPeriodo { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Nombre { get; set; }

        public DateTime? FHInicio { get; set; }

        public DateTime? FHFin { get; set; }
    }
}