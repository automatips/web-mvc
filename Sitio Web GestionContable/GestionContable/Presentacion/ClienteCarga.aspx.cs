﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MagicSQL;
using GestionContable;
using Code;

namespace Presentacion
{
    public partial class ClienteCarga : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                int idContadorCliente = 0;
                if (Request["idContadorCliente"] != null)
                {
                    try
                    {
                        idContadorCliente = Convert.ToInt32(Request["idContadorCliente"]);
                        Session.Add("magenta", idContadorCliente.ToString());

                        ContadorCliente contadorCliente 
                            = new ContadorCliente().
                                Select().
                                    Where(it => 
                                        it.IdContadorCliente == idContadorCliente
                                        ).FirstOrDefault();

                        txtNombre.Text = contadorCliente.NombreApellido;
                        txtNumeroTelefono.Text = "";
                        txtCuit.Text = contadorCliente.CUIT;
                        cargarConceptos(idContadorCliente);
                        btnAltaCliente.Visible = false;
                        btnModificarCliente.Visible = true;
                    }
                    catch
                    {
                        Response.Redirect("login.aspx");
                    }
                }
                else
                {
                    cargarConceptos(0);
                    btnAltaCliente.Visible = true;
                    btnModificarCliente.Visible = false;
                    gvConceptoSinAsignar.Enabled = false;
                }
                
                upConceptoAsignadas.Update();
                upConceptoSinAsignar.Update();
            }     
        }

        protected void cargarConceptos(int idContadorCliente)
        {
            List<Concepto> listaConceptos 
                = new Concepto().
                    Select();

            List<ContadorClienteConcepto> listaCCC
                = new ContadorClienteConcepto().
                    Select().Where(it =>
                        it.IdContadorCliente == idContadorCliente
                        ).ToList();

            List<Concepto> listaConceptosFiltrado =
                new List<Concepto>();

            foreach(Concepto con in listaConceptos)
            {
                bool asignado = false;

                foreach(ContadorClienteConcepto conA in listaCCC)
                {
                    if (con.IdConcepto == conA.IdConcepto)
                    {
                        asignado = true;
                    }
                }
                if (!asignado)
                {
                    listaConceptosFiltrado.Add(con);
                }
            }

            gvConceptoSinAsignar.DataSource = listaConceptosFiltrado.ToDataTable();
            gvConceptoSinAsignar.DataBind();

            upConceptoSinAsignar.Update();



            DataTable dtCCC = new DataTable();
            dtCCC.Columns.Add("IdContadorClienteConcepto", typeof(int));
            dtCCC.Columns.Add("ConceptoNombre", typeof(string));
            dtCCC.Columns.Add("Orden", typeof(int));
            foreach (ContadorClienteConcepto item in listaCCC)
            {
                dtCCC.Rows.Add(
                    item.IdContadorClienteConcepto,
                    listaConceptos.Where(it => it.IdConcepto == item.IdConcepto).FirstOrDefault().Nombre,
                    item.Orden
                    );
            }

            gvConceptoAsignadas.DataSource = dtCCC;
            gvConceptoAsignadas.DataBind();

            foreach(GridViewRow gvr in gvConceptoAsignadas.Rows)
            {
                int idConcepto = Convert.ToInt32(gvConceptoAsignadas.Rows[gvr.RowIndex].Cells[0].Text);
                foreach(ContadorClienteConcepto concepto in listaCCC)
                {
                    if (idConcepto == concepto.IdContadorClienteConcepto)
                    {
                        ((TextBox)gvConceptoAsignadas.Rows[gvr.RowIndex].Cells[2].FindControl("txtValor")).Text = concepto.Valor;
                    }
                }
            }

            upConceptoAsignadas.Update();
        }

        protected void gvConceptoSinAsignar_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvConceptoSinAsignar_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idConceptoAsignar = Convert.ToInt32(gvConceptoSinAsignar.Rows[e.NewSelectedIndex].Cells[0].Text);

            int idContador = 0;

            int idContadorCliente = 0;

            if (Session["valkirya"] != null)
            {
                idContador = Convert.ToInt32(Session["valkirya"].ToString());
            }

            if (Session["magenta"]!= null)
            {
                idContadorCliente = Convert.ToInt32(Session["magenta"].ToString());
            }

            if (idContador>0 && idContadorCliente>0)
            {
                int calcularOrden = new ContadorClienteConcepto().Select().Where(it => it.IdContadorCliente == idContadorCliente).ToList().Count;

                ContadorClienteConcepto conceptoAsignar = new ContadorClienteConcepto();
                conceptoAsignar.FHAlta = DateTime.Now;
                conceptoAsignar.IdConcepto = idConceptoAsignar;
                conceptoAsignar.IdContadorCliente = idContadorCliente;
                conceptoAsignar.Orden = calcularOrden+1;

                conceptoAsignar.Insert();
                
                Response.Redirect("ClienteCarga.aspx?idContadorCliente="+idContadorCliente.ToString());
            }
        }

        protected void gvConceptoAsignadas_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvConceptoAsignadas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idContadorClienteConcepto = Convert.ToInt32(gvConceptoAsignadas.Rows[e.NewSelectedIndex].Cells[0].Text);
            int ordenActual = Convert.ToInt32(gvConceptoAsignadas.Rows[e.NewSelectedIndex].Cells[3].Text);

            int idContador = 0;

            int idContadorCliente = 0;

            if (Session["valkirya"] != null)
            {
                idContador = Convert.ToInt32(Session["valkirya"].ToString());
            }

            if (Session["magenta"] != null)
            {
                idContadorCliente = Convert.ToInt32(Session["magenta"].ToString());
            }


            if (idContador > 0 && idContadorCliente > 0)
            {
                int idCantidad = new ContadorClienteConcepto().Select().
                    Where(it =>
                        it.IdContadorCliente == idContadorCliente
                    ).ToList().Count;

                if (idCantidad > ordenActual)
                {
                    lblMensajeError.Text = "No puede eliminar un concepto asignado en un orden inferior a la cantidad de conceptos asociados";
                    upMensajeError.Update();
                }
                else
                {
                    ContadorClienteConcepto conceptoDesasignar =
                        new ContadorClienteConcepto().
                            Select().
                                Where(it =>
                                it.IdContadorClienteConcepto == idContadorClienteConcepto
                                ).FirstOrDefault();

                    if (conceptoDesasignar != null)
                    {
                        conceptoDesasignar.Delete();
                        Response.Redirect("ClienteCarga.aspx?idContadorCliente=" + idContadorCliente.ToString());
                    }
                }              
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void btnAltaCliente_Click(object sender, EventArgs e)
        {
            if (
                txtNombre.Text != string.Empty
                &&
                txtCuit.Text != string.Empty
                )
            {
                int idContador = 0;

                if (Session["valkirya"] != null)
                {
                    idContador = Convert.ToInt32(Session["valkirya"].ToString());
                }

                if (idContador > 0)
                {
                    ContadorCliente clienteCarga = new ContadorCliente();
                    clienteCarga.NombreApellido = txtNombre.Text;
                    clienteCarga.FHAlta = DateTime.Now;
                    clienteCarga.CUIT = txtCuit.Text;
                    clienteCarga.IdContador = idContador;
                    clienteCarga.Insert();

                    Response.Redirect("Clientes.aspx");
                }
            }
        }

        protected void btnModificarCliente_Click(object sender, EventArgs e)
        {
            int idContador = 0;

            int idContadorCliente = 0;

            if (Session["valkirya"] != null)
            {
                idContador = Convert.ToInt32(Session["valkirya"].ToString());
            }

            if (Session["magenta"] != null)
            {
                idContadorCliente = Convert.ToInt32(Session["magenta"].ToString());
            }

            if (idContadorCliente > 0 && idContador > 0)
            {
                ContadorCliente clienteActualizar =
                    new ContadorCliente().
                        Select().
                            Where(it =>
                                it.IdContadorCliente == idContadorCliente
                            ).FirstOrDefault();
                
                if (clienteActualizar != null)
                {
                    clienteActualizar.NombreApellido = txtNombre.Text;
                    clienteActualizar.CUIT = txtCuit.Text;

                    clienteActualizar.Update();
                }

                if (gvConceptoAsignadas.Rows.Count > 0)
                {
                    foreach (GridViewRow gvr in gvConceptoAsignadas.Rows)
                    {
                        int idConcepto = 
                        Convert.ToInt32(gvConceptoAsignadas.Rows[gvr.RowIndex].Cells[0].Text);

                        ContadorClienteConcepto concepto =
                            new ContadorClienteConcepto().
                                Select().Where(
                                it=> 
                                it.IdContadorClienteConcepto == idConcepto
                                ).FirstOrDefault();
                        if (concepto!= null)
                        {
                            string valorDetectado = ((TextBox)gvConceptoAsignadas.Rows[gvr.RowIndex].Cells[2].FindControl("txtValor")).Text;
                            if (valorDetectado != string.Empty)
                            {
                                concepto.Valor = valorDetectado;
                                concepto.Update();
                            }                         
                        }
                    }
                }
            }
            Response.Redirect("Clientes.aspx");
        }       
    }
}