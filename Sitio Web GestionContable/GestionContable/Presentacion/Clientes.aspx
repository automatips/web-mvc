﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="Presentacion.Clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script runat=server>
        string obtenerClientesDeContador()
        {
            GestionContable.
                Contador contador = new GestionContable.Contador().
                                    Select().
                                        Where(
                                            it =>
                                            it.IdContador == Convert.ToInt32(Session["valkirya"])
                                                    ).FirstOrDefault();
            List<
            GestionContable.
                ContadorCliente> clientesContador = new GestionContable.ContadorCliente().
                                    Select().
                                        Where(
                                            it =>
                                                it.IdContador == contador.IdContador
                                                    ).ToList();

            List<GestionContable.Concepto> listaConceptos = new GestionContable.Concepto().Select();


            List<GestionContable.ContadorClienteConcepto> listaClientesConceptoTotal = new List<GestionContable.ContadorClienteConcepto>();

            foreach(GestionContable.ContadorCliente cliente in clientesContador)
            {
                List<GestionContable.ContadorClienteConcepto> listaClientesConcepto =
                    new GestionContable.ContadorClienteConcepto().
                        Select().
                            Where(
                            it =>
                                it.IdContadorCliente == cliente.IdContadorCliente).ToList();
                foreach(GestionContable.ContadorClienteConcepto clicon in listaClientesConcepto)
                {
                    listaClientesConceptoTotal.Add(clicon);
                }
            }

            System.Data.DataTable tabla = new System.Data.DataTable();
            tabla.Columns.Add("NombreCliente", typeof(string));
            tabla.Columns.Add("TermCuit", typeof(string));
            tabla.Columns.Add("num1", typeof(string));
            tabla.Columns.Add("num2", typeof(string));
            tabla.Columns.Add("num3", typeof(string));
            tabla.Columns.Add("num4", typeof(string));
            tabla.Columns.Add("num5", typeof(string));
            tabla.Columns.Add("num6", typeof(string));
            tabla.Columns.Add("num7", typeof(string));

            string[] table = new string[clientesContador.Count];
            int counter = 0;


            foreach (GestionContable.ContadorCliente item in clientesContador)
            {
                List<GestionContable.ContadorClienteConcepto> listaContadorClienteConcepto =
                    listaClientesConceptoTotal.
                        Where(
                            it =>
                                it.IdContadorCliente == item.IdContadorCliente
                                ).ToList();


                table[counter] =
                    @"<tr>"
                        + @"<td><a href=""ClienteCarga.aspx?idContadorCliente="+item.IdContadorCliente.ToString()+@""">"+item.NombreApellido+"</a></td>"
                        + "<td>"+item.CUIT.Substring((item.CUIT.Length-1))+"</td>"
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,1)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,2)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,3)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,4)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,5)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,6)
                        +retornarCelda(listaConceptos,listaContadorClienteConcepto,7)

                    +"</tr>"
                    ;
                counter += 1;
            }
            return String.Join("", table);
        }

        public string retornarCelda(
                List<GestionContable.Concepto> listaConceptos,
                List<GestionContable.ContadorClienteConcepto> listaContadorClienteConcepto,
                int vOrdenCelda
            )
        {

            GestionContable.ContadorClienteConcepto contadorClienteConceptoCelda = listaContadorClienteConcepto.Where(it => it.Orden == vOrdenCelda).FirstOrDefault();
            string result = string.Empty;
            if (contadorClienteConceptoCelda != null)
            {
                string vNombreConcepto = listaConceptos.Where(it=>it.IdConcepto==listaContadorClienteConcepto.Where(it2=>it2.Orden==vOrdenCelda).FirstOrDefault().IdConcepto).FirstOrDefault().Nombre;
                string vValorConcepto = contadorClienteConceptoCelda.Valor;
                string vEtiquetaOK = "check";
                string vEtiquetaNULL = "times";
                string vEtiquetaMostrar = vEtiquetaNULL;

                string vColorCeldaRojo = "bgcolor="+@"""#FF0000""";
                string vColorCeldaVerde = "bgcolor="+@"""#00FF00""";
                string vColorCeldaSegunEstado = vColorCeldaRojo ;

                if (contadorClienteConceptoCelda.Valor != null && contadorClienteConceptoCelda.Valor != string.Empty)
                {
                    vEtiquetaMostrar = vEtiquetaOK;
                    vColorCeldaSegunEstado = vColorCeldaVerde;
                }

                try
                {
                    result =
                    @"<td "+vColorCeldaSegunEstado+@">
                        <div class=""form-group"">
                            <label class=""col-form-label"" for=""inputSuccess"">"+vNombreConcepto+@"</label>
                            <label class=""col-form-label"" for=""inputSuccess"">"+vValorConcepto+@"</label>
                            <i class=""fas fa-"+vEtiquetaMostrar+@"""></i>
                        </div>
                    </td>";
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

    </script>

    <section class="content">
      <div class="container-fluid">
        <div class="col-12">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">LISTA DE CLIENTES</h3>
            </div>                

            <!-- /.card-header -->
            <div class="card-body">

                <div class="row">
                    <asp:Button ID="btnAltaCliente"
                        Text="Ingresar nuevo Cliente"
                        CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right"
                        runat="server"
                        Style="margin-bottom: 5px;" 
                        OnClick="btnAltaCliente_Click"
                        />
                </div>

                <table id="tableTemplate" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>NOMBRE DEL CLIENTE</th>
                            <th>Term CUIT</th>
                            <th></th><!-- COLUMNA 1 -->
                            <th></th><!-- COLUMNA 2 -->
                            <th></th><!-- COLUMNA 3 -->
                            <th></th><!-- COLUMNA 4 -->
                            <th></th><!-- COLUMNA 5 -->
                            <th></th><!-- COLUMNA 6 -->
                            <th></th><!-- COLUMNA 7 -->
                        </tr>
                    </thead>

                    <tbody>
                        <% = obtenerClientesDeContador()%>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
      </div>
    </section>
</asp:Content>
