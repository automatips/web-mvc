﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MagicSQL;
using GestionContable;
using System.Data;
using Code;

namespace Presentacion
{
    public partial class Clientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["valkirya"]==null)
                {
                    Response.Redirect("login.aspx");
                }
            }
        }

        protected void gvSolicitudesDeTaller_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvSolicitudesDeTaller_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnAltaCliente_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteCarga.aspx");
        }
    }
}