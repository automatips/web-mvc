﻿
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Code
{
    internal class Utils
    {
        // Inicia el generador Random publico
        private static Random random = new Random();

        private static string BlankSpace(int spaces = 1)
        {
            string result = "";
            for (int s = 1; s < spaces + 1; s++)
            {
                result = result + "&nbsp;";
            }
            return result;
        }

        /// <summary>
        /// Tipos de mensajes: info, success, warning, error.
        /// </summary>
        public enum MessagesTypes
        {
            info,
            success,
            warning,
            error
        }

        /// <summary>
        /// Recupera el valor de una key del archivo de configuracion.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Genera el HASH del Modelo.
        /// </summary>
        /// <param name="jsonModel"></param>
        /// <returns></returns>
        public static string ModelHash(string jsonModel)
        {
            return Encrypt(jsonModel.Replace(" ", ""));
        }

        #region Cookies

        /// <summary>
        /// Crea una Cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        /// <param name="value"></param>
        public static void CreateCookie(string cookieName, string value)
        {
            HttpCookie httpCookie = new HttpCookie(cookieName);

            // encripta el valor de la cookie
            value = Encrypt(value);

            // establece el valor de la cookie
            httpCookie.Value = value;

            // establece la fecha de expiracion de la cookie
            httpCookie.Expires = DateTime.Now.AddYears(50);

            // agrega la cookie
            HttpContext.Current.Response.Cookies.Add(httpCookie);
        }

        /// <summary>
        /// Lee el valor de una Cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string ReadCookie(string cookieName)
        {
            // recupera la cookie del request
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[cookieName];

            string value = null;
            if (httpCookie != null)
            {
                // lee el valor de la cookie
                value = httpCookie.Value;

                // desencript el valor de la cookie. si se produce un error de elimina la cookie y se redirige a la pagina Apps.aspx.
                try
                {
                    value = Decrypt(value);
                }
                catch
                {
                    DeleteCookie(cookieName);
                    value = null;
                }
            }

            return value;
        }

        /// <summary>
        /// Elimina una cookie.
        /// </summary>
        /// <param name="cookieName"></param>
        public static void DeleteCookie(string cookieName)
        {
            // recupera la cookie del request
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (httpCookie != null)
            {
                // elimina la cookie
                HttpContext.Current.Response.Cookies.Remove(cookieName);

                // establece el valor de la cookie en null
                httpCookie.Value = null;

                // establece la fecha de expiracion de la cookie en una fecha pasada
                httpCookie.Expires = DateTime.Now.AddDays(-10);

                // establece las propiedades de la cookie
                HttpContext.Current.Response.SetCookie(httpCookie);
            }
        }

        #endregion Cookies

        /// <summary>
        /// Genera un número random para realizar las llamadas a funciones JjavaScript en la UI.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static string GetRandom(int minValue = 0, int maxValue = int.MaxValue)
        {
            return random.Next(minValue, maxValue).ToString();
        }

        /// <summary>
        /// Ejecuta una función JavaScript en el cliente.
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="parameters"></param>
        public static void ExecJsFn(string functionName, string parameters = "")
        {
            // JsFnId permite identificar de forma unívoca cada ejecución de JS.
            // Si se repite este valor para dos o mas ejecuciones, se ejecutará solo la primera.
            // Las llamadas a las funciones se encuentran agregadas cronológicamente al principio
            // de la ultima sección script del html.
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string JsFnId = GetRandom();
                ScriptManager.RegisterStartupScript(page, page.GetType(), "Utils.ExecJSFn." + JsFnId, functionName + "(" + parameters + ");\n", true);
            }
        }

        /// <summary>
        /// Ejecuta una función JavaScript en el cliente.
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="parameters"></param>
        public static void ExecJsFn(string functionName, object obj)
        {
            // JsFnId permite identificar de forma unívoca cada ejecución de JS.
            // Si se repite este valor para dos o mas ejecuciones, se ejecutará solo la primera.
            // Las llamadas a las funciones se encuentran agregadas cronológicamente al principio
            // de la ultima sección script del html.
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string JsFnId = GetRandom();
                ScriptManager.RegisterStartupScript(page, page.GetType(), "Utils.ExecJSFn." + JsFnId, functionName + "(" + obj.ToJson() + ");\n", true);
            }
        }

        public static void FocusAndSelect(string controlId)
        {
            string jsCode = @"
            $(%[id*='{0}']%).focus();
            $(%[id*='{0}']%).select();
            ".Replace("%", "\"").Replace("{0}", controlId);
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string JsFnId = GetRandom();
                ScriptManager.RegisterStartupScript(page, page.GetType(), JsFnId, jsCode, true);
            }
        }

        /// <summary>
        /// Muestra un mensaje en el UI.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="redirectTo"></param>
        public static void Message(string title, string message, MessagesTypes messageType, string redirectTo = "")
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string js = @"
    swal(
    {
        title: '{0}',
        text: '{1}',
        type: '{2}',
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '{3}',
        html: true
    },
    function()
    {
        var redirectTo = '{4}';
        if (redirectTo != '') {
            window.location.href = redirectTo;
        }
    });
                            ";
                string buttonColor = "";
                switch (messageType)
                {
                    case MessagesTypes.info:
                        buttonColor = "#5bc0de";
                        break;

                    case MessagesTypes.success:
                        buttonColor = "#5cb85c";
                        break;

                    case MessagesTypes.warning:
                        buttonColor = "#f0ad4e";
                        break;

                    case MessagesTypes.error:
                        buttonColor = "#d9534f";
                        break;
                }
                message = message.Replace("\n", "<br>");
                js = js.Replace("{0}", title).Replace("{1}", message).Replace("{2}", messageType.ToString()).Replace("{3}", buttonColor).Replace("{4}", redirectTo);
                ScriptManager.RegisterStartupScript(page, page.GetType(), "Utils.Message", js, true);
            }
        }

        /// <summary>
        /// Obtiene la URL solicitada.
        /// </summary>
        /// <returns></returns>
        public static string GetRequestedURL()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
        }

        /// <summary>
        /// Obtiene la IP que está realizando el Request.
        /// </summary>
        /// <returns></returns>
        public static string GetRequestIPAddress()
        {
            HttpContext context = HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        /// <summary>
        /// Obtiene el HTML de la página indicada en la URL.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetHTMLFromURL(string url)
        {
            string data = "";

            //System.Collections.Generic.List<string> uris = new CssLoader().GetUris(source).ToList();

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                Stream stream = httpWebResponse.GetResponseStream();
                StreamReader streamReader = null;

                if (httpWebResponse.CharacterSet == null)
                {
                    streamReader = new StreamReader(stream);
                }
                else
                {
                    streamReader = new StreamReader(stream, Encoding.GetEncoding(httpWebResponse.CharacterSet));
                }

                data = streamReader.ReadToEnd();

                httpWebResponse.Close();
                streamReader.Close();
            }
            return data;
        }

        /// <summary>
        /// Navega a la URL indicada y evita el error de thread.
        /// </summary>
        /// <param name="url"></param>
        public static void NavigateTo(string url)
        {
            HttpContext.Current.Response.Redirect(url, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Imprime la página indicada en el parámetro url.
        /// </summary>
        /// <param name="url"></param>
        public static void Print(string url)
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null)
            {
                string js = @"
function Print() {
    var printWindow = window.open('{0}');
    printWindow.addEventListener('load', function () {
        printWindow.print();
        printWindow.close();
    }, true);
}
setTimeout(Function('Print();'), 100);
";
                js = js.Replace("{0}", url);
                page.ClientScript.RegisterStartupScript(page.GetType(), "Print", js, true);
            }
        }

        #region Criptografía

        /// <summary>
        /// Encripta un string utilizando AES. Devuelve el resultado en Base64.
        /// </summary>
        /// <param name="decryptedText"></param>
        /// <returns></returns>
        public static string Encrypt(string decryptedText)
        {
            var decryptedBytes = Encoding.UTF8.GetBytes(decryptedText);
            return Convert.ToBase64String(Encrypt(decryptedBytes, GetRijndaelManaged(GetAppSetting("CryptoKey")))).Replace("+", "-").Replace("/", "_");
        }

        /// <summary>
        /// Desencripta un string en Base64 utilizando AES. Devuelve el resultado en texto plano.
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <returns></returns>
        public static string Decrypt(string encryptedText)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText.Replace("-", "+").Replace("_", "/"));
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(GetAppSetting("CryptoKey")))).Replace('\0', ' ').TrimEnd();
        }

        /// <summary>
        /// Implementa Encrypt de la clase RijndaelManaged.
        /// </summary>
        /// <param name="decryptedBytes"></param>
        /// <param name="rijndaelManaged"></param>
        /// <returns></returns>
        private static byte[] Encrypt(byte[] decryptedBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor().TransformFinalBlock(decryptedBytes, 0, decryptedBytes.Length);
        }

        /// <summary>
        /// Implementa Decrypt de la clase RijndaelManaged.
        /// </summary>
        /// <param name="encryptedData"></param>
        /// <param name="rijndaelManaged"></param>
        /// <returns></returns>
        private static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        private static RijndaelManaged GetRijndaelManaged(string secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        #endregion Criptografía

        #region Direccion
        
        private static byte[] GetStaticMap(string urlImage)
        {
            try
            {
                string html = string.Empty;
                Bitmap buddyIcon;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlImage);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        buddyIcon = new Bitmap(stream);
                    }
                }
                ImageConverter converter = new ImageConverter();
                return (byte[])converter.ConvertTo(buddyIcon, typeof(byte[]));
            }
            catch
            {
                return null;
            }
        }

        //public static Nullable<long> ArchivoAdd(long? archivoId, FileUpload archivo)
        //{
        //    //if (!archivo.HasFile) return archivoId;

        //    //var archivoNuevo = new Archivo();
        //    //if (archivoId > 0)
        //    //{
        //    //    archivoNuevo = new Archivo();
        //    //    archivoNuevo.Select(archivoId.Value);
        //    //}

        //    //MemoryStream target = new MemoryStream();
        //    //archivo.FileContent.CopyTo(target);
        //    //archivoNuevo.ArchivoContenido = target.ToArray();
        //    //archivoNuevo.FHAlta = DateTime.Now;
        //    //archivoNuevo.TipoArchivo = System.IO.Path.GetExtension(archivo.FileName);

        //    //if (archivoId > 0)
        //    //{
        //    //    archivoNuevo.Update();
        //    //}
        //    //else
        //    //{
        //    //    archivoNuevo.Insert();
        //    //}
        //    //return archivoNuevo.IdArchivo;
        //}

        #endregion Direccion
        
        #region Descargar Archivos

        public static void DescargarArchivo(System.Web.UI.Page page, string contentType, byte[] archivoContenido, string fileName)
        {
            page.Response.Clear();
            page.Response.Buffer = true;
            page.Response.Charset = "";
            page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            page.Response.ContentType = contentType;
            page.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename= {0}.{1}", fileName, contentType));
            page.Response.BinaryWrite(archivoContenido);
            page.Response.Flush();
            page.Response.End();
        }

        #endregion Descargar Archivos

        /// <summary>
        /// Devuelve el IdCuenta del sistema de usuarios leyendola de la cookie de session
        /// </summary>
        /// <returns></returns>
        public static int GetIdCuentaFromCookie()
        {
            try
            {
                dynamic login = Utils.ReadCookie("SessionSysAdm").ToDynamic();
                return login.IdCuenta;
            }
            catch
            {
                return -1;
            }
        }

        public static void GeneratePdf(string url, string pdf)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "PDFs\\";

            if (!pdf.EndsWith(".pdf"))
            {
                pdf = pdf + ".pdf";
            }

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo();

            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            processStartInfo.FileName = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
            processStartInfo.Arguments = url + " " + "\"" + path + pdf + "\"";
            process.StartInfo = processStartInfo;
            process.Start();
            process.WaitForExit();
        }

        #region Emails

        public static string GetPlainTextBodyFromMailMessage(MailMessage msg)
        {
            string html = "";
            if (msg.AlternateViews.Count > 0)
            {
                // Body HTML
                StreamReader streamReader = new StreamReader(msg.AlternateViews[0].ContentStream);
                html = streamReader.ReadToEnd();
            }
            else
            {
                // Body Plain
                html = msg.Body;
            }
            return Recodificar(html);
        }

        public static string Recodificar(string codificado)
        {
            codificado = codificado.Replace("=\r\n", "").Replace(" =20\r\n", "");
            codificado = codificado.Replace("=C2=A1Error!", "");
            codificado = codificado.Replace("3D\"", "\"");

            codificado = codificado.Replace("=E7", "ç");
            codificado = codificado.Replace("=F5", "õ");

            codificado = codificado.Replace("=C2=A0", " ").Replace("=09", " ");
            codificado = codificado.Replace("=C2=BF", "¿").Replace("=3F", "?");
            codificado = codificado.Replace("=C2=B0", "°");
            codificado = codificado.Replace("=C2=A9", "©");
            codificado = codificado.Replace("=C2=A1", "¡");

            codificado = codificado.Replace("=C3=C1", "Á").Replace("=C1", "Á");
            codificado = codificado.Replace("=C3=C9", "É");
            codificado = codificado.Replace("=C3=CD", "Í").Replace("=C3=8D", "Í");
            codificado = codificado.Replace("=C3=D3", "Ó").Replace("=C3=93", "Ó");
            codificado = codificado.Replace("=C3=DA", "Ú");

            codificado = codificado.Replace("=C3=E1", "á").Replace("=C3=A1", "á").Replace("=E0", "á").Replace("=E1", "á").Replace("=C3=A0", "á");
            codificado = codificado.Replace("=C3=E9", "é").Replace("=C3=A9", "é").Replace("=E9", "é");
            codificado = codificado.Replace("=C3=ED", "í").Replace("=C3=AD", "í").Replace("=ED", "í").Replace("=C3=AC", "í");
            codificado = codificado.Replace("=C3=F3", "ó").Replace("=C3=B3", "ó").Replace("=F3", "ó");
            codificado = codificado.Replace("=C3=FA", "ú").Replace("=C3=BA", "ú");

            codificado = codificado.Replace("=C3=D1", "Ñ");
            codificado = codificado.Replace("=C3=F1", "ñ").Replace("=C3=B1", "ñ").Replace("=F1", "ñ");

            // Convertir links en src
            string src = "";
            string alt = "";
            for (int i = 0; i < codificado.Length - 3; i++)
            {
                if (codificado.Substring(i, 3).ToLower().Equals("src"))
                {
                    alt = "";
                    if (codificado.IndexOf("\"", i) > 0)
                    {
                        src = codificado.Substring(codificado.IndexOf("\"", i));
                        int p = src.IndexOf("\"", 1) - 1;
                        src = src.Substring(1, p);
                    }
                }

                if (codificado.Substring(i, 3).ToLower().Equals("alt"))
                {
                    if (codificado.IndexOf("\"", i) > 0)
                    {
                        alt = codificado.Substring(codificado.IndexOf("\"", i));
                        int p = alt.IndexOf("\"", 1) - 1;
                        alt = alt.Substring(1, p);
                    }
                }

                if (src != "" && alt != "" && src.StartsWith("cid:"))
                {
                    codificado = codificado.Replace(src, alt);
                    src = "";
                    alt = "";
                }
            }

            return codificado;
        }

        #endregion Emails
        
    }
}