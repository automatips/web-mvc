﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MagicSQL;
using GestionContable;

namespace Presentacion
{
    public partial class ConceptoCarga : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarConceptos();
        }

        protected void cargarConceptos()
        {
            List<Concepto> listaConceptos = new Concepto().Select();

            List<ContadorClienteConcepto> listaCCC = new ContadorClienteConcepto().Select().Where(it => it.IdContadorCliente == 1).ToList();
            DataTable dtCCC = new DataTable();
            dtCCC.Columns.Add("IdContadorClienteConcepto", typeof(int));
            dtCCC.Columns.Add("ConceptoNombre", typeof(string));
            dtCCC.Columns.Add("Orden", typeof(int));
            foreach (ContadorClienteConcepto item in listaCCC)
            {
                dtCCC.Rows.Add(
                    item.IdContadorClienteConcepto,
                    listaConceptos.Where(it => it.IdConcepto == item.IdConcepto).FirstOrDefault().Nombre,
                    item.Orden
                    );
            }
        }

        protected void gvConceptoSinAsignar_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvConceptoSinAsignar_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void gvConceptoAsignadas_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvConceptoAsignadas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void btnAltaConcepto_Click(object sender, EventArgs e)
        {
            Concepto conceptoAlta = new Concepto();
            conceptoAlta.FHAlta = DateTime.Now;
            conceptoAlta.Nombre = txtNombre.Text;
            conceptoAlta.Tipo = txtTipo.Text;
            conceptoAlta.Descripcion = txtDescripcion.Text;
            conceptoAlta.Insert();
            Response.Redirect("ConceptoCarga.aspx");
        }
    }
}