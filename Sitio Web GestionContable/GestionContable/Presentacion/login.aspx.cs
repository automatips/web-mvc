﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestionContable;
using Code;
using MagicSQL;


namespace Presentacion.pages.examples
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            lblMensajeError.Text = "";
            Contador contadorLogin
                = new Contador().
                    Select().
                        Where(it =>
                            it.Email == txtEmail.Text
                            &&
                            it.Password == txtPassword.Text
                            ).FirstOrDefault();

            if (contadorLogin != null)
            {
                Session.Add("valkirya",contadorLogin.IdContador.ToString());
                Response.Redirect("Clientes.aspx");
            }
            else
            {
                lblMensajeError.Text = "Login incorrecto";
                upMensajeError.Update();
            }
        }
    }
    //valkiria=idContador
    //magenta=idClienteContador
}