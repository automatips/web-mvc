﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Presentacion.register" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestión Contable | Registrarme</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="index.aspx"><b>Gestión Contable</b></a>
  </div>

  <div class="card">
    <div class="card-body register-card-body" style="width:500px;">
      <p class="login-box-msg">Registrar nuevo Contador</p>

      <form runat="server">
          <asp:ScriptManager runat="server" />
        
        <div class="row">
            <asp:UpdatePanel ID="upMensajeError" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <asp:Label ID="lblMensajeError" Text="" ForeColor="Red" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

          <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <div class="input-group mb-3">
                <asp:TextBox 
                ID="txtNombreApellido" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="NOMBRE Y APELLIDO DEL CONTADOR"
                data-rule="minlen:4" 
                data-msg="Es requerido el nombre del contador"
                type="text"
                />
              <div class="validation"></div>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="far fa-address-card"></span>
                </div>
              </div>
            </div>
        </div>

        <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtMatriculaCuit" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="MATRICULA O CUIT"
                data-rule="minlen:4" 
                type="text"
                />
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-barcode"></span>
                </div>
              </div>
            </div>
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtProvincia" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="PROVINCIA"
                data-rule="minlen:4" 
                type="text"
                />
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-city"></span>
                </div>
                </div>
            </div>
        </div>

        <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <asp:TextBox 
            ID="txtDomicilioEstudio" 
            CssClass="form-control" 
            runat="server"
            Enabled="true"
            placeholder="DOMICILIO DEL ESTUDIO"
            data-rule="minlen:4" 
            type="text"
            />
          <div class="validation"></div>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-search-location"></span>
            </div>
          </div>
        </div>

        <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtTelefono" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="TELEFONO"
                data-rule="minlen:4" 
                type="text"
                />
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-phone-square"></span>
                </div>
              </div>
            </div>
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtEspecialidad" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="ESPECIALIDAD"
                data-rule="minlen:4" 
                type="text"
                />
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-network-wired"></span>
                </div>
                </div>
            </div>
        </div>

        <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <div class="input-group mb-3">
                <asp:TextBox 
                ID="txtEmail" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="EMAIL"
                data-rule="minlen:4" 
                data-msg="Es requerido el email del contador"
                type="text"
                />
              <div class="validation"></div>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-at"></span>
                </div>
              </div>
            </div>
        </div>

        <div class="input-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:5px;">
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtPassword" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="PASSWORD"
                data-rule="minlen:4" 
                type="password"
                />
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <asp:TextBox 
                ID="txtPasswordConfirmacion" 
                CssClass="form-control" 
                runat="server"
                Enabled="true"
                placeholder="CONFIRMACION"
                data-rule="minlen:4" 
                type="password"
                />
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               Acepto los <a href="#">términos y condiciones</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">

                <asp:UpdatePanel ID="upBotonAlta" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:Button ID="btnAltaContador"
                            Text="Registrarme"
                            CssClass="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right"
                            runat="server"
                            Style="margin-bottom: 5px;" 
                            OnClick="btnAltaContador_Click"
                            />
                    </ContentTemplate>
                </asp:UpdatePanel>

          </div>
          <!-- /.col -->
        </div>

      </form>

      <a href="login.aspx" class="text-center">Iniciar sesión con mi cuenta</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
</body>
</html>